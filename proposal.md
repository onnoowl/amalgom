## TEAM / PROJECT NAME:
The Ferris Fellas

### SUMMARY: “What are we doing?” (1 paragraph)

We are interested in developing a system for determining the best foot
placement of a robot walking across a voxelized terrain. To simulate
this, we will be creating an animated robot in a custom game engine
which walks across blocky terrain. Our work could later be applied to
a physical robot which identifies its surroundings as a voxel map. We
will start by creating a simple animated robot, and progressively work
towards more accurate and physically realistic foot placement interactions.

### MOTIVATION: “Who cares?” and “How could this be valuable or useful?” (1-2 paragraphs)

Voxel octrees are an established option for representing terrain for a robot to navigate. They offer great memory efficiencies because eight neighboring voxels (that align on the voxel grid) that all hold the same value can be represented as a single voxel. In robotics, rapid training and prototyping depends on being able to simulate behavior virtually. Code to allow simulated robotics pathing in different configurable terrains provides a useful testing framework.

### CHALLENGE: “Why is this hard?” (1 sentence to 1 paragraph)

There are many things that must be considered when making an accurate simulation of movement in the real world. Accurate physics for applying forces to propel a robot along a path is a massive undertaking that has taken Boston Dynamics years.

### EXPECTED APPROACH: “How will we do it?” (1-2 paragraph)

Our planned project has a massive scope. In the most complex interpretation, we’ll be trying to emulate what Boston Dynamics has been working on for years. Realistically, we plan on making simplifying assumptions and breaking down this project into a series of smaller goals that can allow us to get as close to the ideal end goal as possible, while allowing us to still finish with a working project if we do fall short. For example, we will likely assume that all limbs are infinitely strong and “sticky” in order to alleviate the need for complex physics calculations. That assumption means that the algorithm only needs to worry about range of motion calculations and basic kinematics. There’s existing work here in rust (see [k](https://docs.rs/k/0.18.0/k/)), but they don’t account for a moving source during plan execution. The limbs will need to move while the body is moving. This will either need to be forked, or we will need to create some simplified version.

While we may end up building a more sophisticated motion planning algorithm, we can build an MVP one using simple randomization. If we focus efforts on doing performant collision checks, then the planning algorithm can simply randomize the parameters for a motion and check if the path involves collisions. It can start with more optimal paths, and slowly decrease the optimality as it performs more retries, in order to ensure that more optimal paths are preferred.

### RELATED WORK: “What else has been done?” (1-2 paragraph)

The [Rust gear library]([gear](https://docs.rs/gear/0.6.0/gear/index.html) performs path planning using [jacobian solvers and inverse kinematics](https://www.academia.edu/download/34612234/iksurvey.pdf), and will serve as an excellent resource & reference implementation for investigating the possibility of a more sophisticated planning algorithm. We will need to conduct more detailed research to better understand if regular inverse kinematics handles the situation of a moving source-body. The source and target will need to move in tandem for the robot to be able to plan its motions effectively. If this is possible, then maybe we can fork the [Rust gear library]([gear](https://docs.rs/gear/0.6.0/gear/index.html) to use these concepts.

Voxel octree representations aren’t new to the world of robotics. Normally, voxel octrees are used to efficiently model 3D volumes. For robotics, octrees are adapted to allow for more probabilistic models that represent how confident a robot is that a volume is an obstacle. Systems like Octomap incorporate this data structure into ROS. Other systems go a step further and use voxels for SLAM systems (Voxel Map for Visual SLAM).
Realistic foot placement in robotics pathing has become one of the most widely known efforts in the field thanks to Boston Dynamic’s robots. Its direct correlation to human-like motion makes the problem very interesting, sparking many projects. One such project, Stanford Pupper, created an open source implementation on a small scale. While this implementation can only handle flat surfaces, it still offers a public solution for constantly running and even hopping.

### MILESTONES / TIMELINE /: (3-10 items)

| NAME or # | START DATE | END DATE | DESCRIPTION |
| :-------- | :--------- | :------- | :-------------------------------- |
| 0         |   09/19    |   09/23  | Create live reload (hot code swapping) setup for easy development |
| 1         |   09/19    |   09/23  | Create skeletal system for robot, perhaps with [this](https://docs.rs/k/0.18.0/k/) |
| 2         |   09/19    |   09/23  | Create method of visualization for robot   |
| 3         |   09/23    |   09/30  | Perform simple animations to ensure prior work is integrated properly |
| 4         |   09/23    |   09/30  | Implement efficient collision detection for voxel tree (for rays, AABBs, and more) |
| 5         |   09/23    |   10/07  | Identify approximate reachable region of limbs |
| 6         |   09/23    |   10/07  | Select foot placement candidates from regions |
| 7         |   09/23    |   10/07  | Modify [existing jacobian solver](https://docs.rs/gear/0.6.0/gear/index.html), or maybe just use simpler heuristic for [1-shoulder, 1-elbow] type limbs |
| 8         |   10/07    |   10/21  | Implement a planning-stage collision detection algorithm for moving limbs on a moving body |
| 9         |   10/07    |   11/04  | Create planning algorithm (simplest version of this might be to randomize plans that perform collision checks, and then retry on failure)|
| 10        |   10/07    |   11/04  | Create playback/interpreter for “plans” (maybe with async plan-ahead) |

### MATERIALS NEEDED: list of software/computation/hardware
No needed materials

### REFERENCES: citations in IEEE/ACM format
```
Armin Hornung. 2013. 3D Mapping with OctoMap. ROSCon Stutgart 2013 (2013). DOI:http://dx.doi.org/10.36288/roscon2013-900161
Kau, Nathan, and Aaron Schultz. “Stanfordroboticsclub/StanfordQuadruped.” GitHub, 20 June 2020, github.com/stanfordroboticsclub/StanfordQuadruped.
Muglikar, Manasi, et al. “Voxel Map for Visual SLAM.” IEEE International Conference on Robotics and Automation (ICRA), 4 Mar. 2020.
Ogura, Takashi, et al. “Gear 0.6.0.” Gear 0.6.0 - Docs.rs, 2020, docs.rs/crate/gear/0.6.0.
Ogura, Takashi, et al. “K 0.6.0.” K 0.18.0 - Docs.rs, 2020, docs.rs/k/0.18.0/k/.
Ryde, Julian, and Huosheng Hu. “3D Mapping with Multi-Resolution Occupied Voxel Lists.” Autonomous Robots, vol. 28, no. 2, 2009, pp. 169–185., doi:10.1007/s10514-009-9158-3.
```

Buss, S. R. 2004. Introduction to inverse kinematics with jacobian transpose, pseudoinverse and damped least squares methods. IEEE Journal of Robotics and Automation, 17(1-19), 16.
## Evaluation

This Project Proposal is worth 3 points.

You'll be graded on the following rubric:

| ITEM                                                                                  | POINTS |
| ------------------------------------------------------------------------------------- | ------ |
| Summary. Clear and brief.                                                             | 0.5    |
| Motivation. Connect project to something people care about (or should care about).    | 0.5    |
| Challenge. Explain why this is hard.                                                  | 0.25   |
| Expected Approach. Describe how you _think_ you'll do it.                             | 0.5    |
| Milestones. Clear dates with sufficient detail to know that you've reached that goal. | 0.5    |
| Related Work / References (at least 2)                                                | 0.25   |
| Submitting on time                                                                    | 0.5    |
