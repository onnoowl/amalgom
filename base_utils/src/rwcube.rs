use std::{
    cell::UnsafeCell,
    marker::PhantomData,
    ops::Range,
    sync::{
        atomic::{AtomicU64, Ordering},
        Arc, RwLock,
    },
};

use cgmath::Vector3;

use crate::ICoord;

/// A multi reader single writer circular 3D buffer
/// This uses minimal use of mutex locks to control the readable bounds of the underlying memory.
/// It is designed to minimize lock contention on those locks.
struct RwCube<T: Sized + Default, const N: usize> {
    /// interior 3D data. To be indexed like data[x][y][z]
    data: UnsafeCell<[[[T; N]; N]; N]>,

    /// What the reader would like the read_bounds to be expanded to
    // desired_bounds: AtomicCell<Option<Range<ICoord>>>,

    // /// 3D range for what is read-safe in `data`
    // /// These ranges are in the global coordinate system. Modding them by N gives you the offset/index into the cube
    // /// read_end - read_beg must be < N in all dimensions
    // /// These ranges need to be only shrunken by the reader thread, and grown by the writer thread
    // /// All uses of this lock will be very quick, minimizing lock contention
    // read_bounds: AtomicCell<Range<ICoord>>,
    // /// What the reader would like the read_bounds to be expanded to
    change_counter: AtomicU64, // incremented with every change. If different from last seen, then there's been a change
    bounds: RwLock<ControlBounds>,
    // reader_controlled_will_read: Mutex<Range<ICoord>>, // this must be a subset of the `can_read` property. Can only be expanded by the writer, shrunk by the reader
    // writer_controlled_can_read: Mutex<Range<ICoord>>, // this must be a superset of the `will_read` property. Can only be changed by the writer, read by the reader
}

unsafe impl<T: Sized + Default + Copy, const N: usize> Sync for RwCube<T, N> {}

#[derive(Debug, Clone)]
struct ControlBounds {
    readers_desired_bounds: Range<ICoord>, // Can only be changed by the reader, read by the writer. Declares what the reader wants to read
    writers_readable_bounds: Range<ICoord>, // Can only be changed by the writer, read by the reader. Declares what is read safe
}

#[derive(Clone)]
pub struct RwCubeReader<T: Sized + Default + Copy, const N: usize> {
    cube: Arc<RwCube<T, N>>,
    last_desired_bounds: Range<ICoord>,
}

pub struct RwCubeReadHandle<'a, T: Sized + Default + Copy, const N: usize> {
    cube: Arc<RwCube<T, N>>,
    valid_range: Range<ICoord>,
    _pht: PhantomData<&'a ()>,
}

fn coord_to_index(coord: &ICoord, size: usize) -> Vector3<usize> {
    let mut index = coord % (size as i64);
    if index.x < 0 {
        index.x += size as i64;
    }
    if index.y < 0 {
        index.y += size as i64;
    }
    if index.z < 0 {
        index.z += size as i64;
    }
    index.cast::<usize>().unwrap()
}

impl<'a, T: Sized + Default + Copy, const N: usize> RwCubeReadHandle<'a, T, N> {
    pub fn read(&self, coord: &ICoord) -> Option<T> {
        if within(coord, &self.valid_range) {
            let index = coord_to_index(&coord, N);
            let cube_data = unsafe { &*self.cube.data.get() };
            let data = cube_data[index[0]][index[1]][index[2]];
            Some(data)
        } else {
            None
        }
    }
}

pub struct RwCubeWriter<T: Sized + Default + Copy, const N: usize> {
    cube: Arc<RwCube<T, N>>,
    last_change_counter: u64,
    pending_write: Option<ControlBoundsDifIter>,
}

pub fn new_rwcube<T: Sized + Default + Copy, const N: usize>(
) -> (RwCubeWriter<T, N>, RwCubeReader<T, N>)
where
    [[[T; N]; N]; N]: Default,
{
    let zero = ICoord::new(0, 0, 0);
    let cube = Arc::new(RwCube {
        data: Default::default(),
        change_counter: AtomicU64::new(0),
        bounds: RwLock::new(ControlBounds {
            readers_desired_bounds: zero..zero,
            writers_readable_bounds: zero..zero,
        }),
    });

    (
        RwCubeWriter {
            cube: cube.clone(),
            last_change_counter: 0,
            pending_write: None,
        },
        RwCubeReader {
            cube,
            last_desired_bounds: ICoord::new(0, 0, 0)..ICoord::new(0, 0, 0),
        },
    )
}

impl<T: Sized + Default + Copy, const N: usize> RwCube<T, N> {
    fn copy_bounds(&self) -> ControlBounds {
        self.bounds.read().unwrap().clone()
    }
}

impl ControlBounds {
    fn intersection(&self) -> Range<ICoord> {
        let start = {
            let r = self.readers_desired_bounds.start;
            let w = self.writers_readable_bounds.start;
            ICoord::new(r.x.max(w.x), r.y.max(w.y), r.z.max(w.z))
        };
        let end = {
            let r = self.readers_desired_bounds.end;
            let w = self.writers_readable_bounds.end;
            ICoord::new(r.x.min(w.x), r.y.min(w.y), r.z.min(w.z))
        };
        start..end
    }

    /// Iterates the reader's bounds NOT contained by the writer's bounds
    fn dif(&self) -> ControlBoundsDifIter {
        let mut cur = self.readers_desired_bounds.start;
        cur.x -= 1; // this way it increments into the valid state
        ControlBoundsDifIter {
            bounds: self.clone(),
            cur,
        }
    }
}

struct ControlBoundsDifIter {
    pub bounds: ControlBounds,
    pub cur: ICoord,
}

impl Iterator for ControlBoundsDifIter {
    type Item = ICoord;

    fn next(&mut self) -> Option<Self::Item> {
        let e = self.bounds.readers_desired_bounds.clone();

        self.cur.x += 1;
        loop {
            self.skip_writers_area(0); // We can only skip over all the X's each time.

            // TODO: There must be a way to iterate over the readers bounds NOT covered by the writers bounds in a better way...
            // This is just the best we have for right now, it can skip over the X dimension, but not the others.

            if self.cur.x >= e.end.x {
                self.cur.x = e.start.x;
                self.cur.y += 1;

                if self.cur.y >= e.end.y {
                    self.cur.y = e.start.y;
                    self.cur.z += 1;

                    if self.cur.z >= e.end.z {
                        return None;
                    }
                }
            } else {
                break;
            }
        }
        // self.skip_writers_area(0);
        Some(self.cur)
    }
}

impl ControlBoundsDifIter {
    // Returns true if it had to skip
    fn skip_writers_area(&mut self, dim: usize) -> bool {
        if within(&self.cur, &self.bounds.writers_readable_bounds) {
            self.cur[dim] = self.bounds.writers_readable_bounds.end[dim];
            true
        } else {
            false
        }
    }
}

impl<T: Sized + Default + Copy, const N: usize> RwCubeReader<T, N> {
    pub fn read_handle(&self) -> RwCubeReadHandle<'_, T, N> {
        RwCubeReadHandle {
            cube: self.cube.clone(),
            valid_range: self.cube.copy_bounds().intersection(),
            _pht: PhantomData,
        }
    }

    /// Requests for the cube to make the given `range` readable
    pub fn request_cube_shift_to(&mut self, range: Range<ICoord>) {
        debug_assert!({
            let dif = range.end - range.start;
            let n = N as i64;
            dif.x >= 0 && dif.x <= n && dif.y >= 0 && dif.y <= n && dif.z >= 0 && dif.z <= n
        });
        {
            self.cube.bounds.write().unwrap().readers_desired_bounds = range.clone();
            self.cube.change_counter.fetch_add(1, Ordering::SeqCst);
        }
        self.last_desired_bounds = range;
    }

    /// Requests for the cube to be moved so that it is roughly centered around `coord`
    /// coord should be the coordinate of the feet of the player (ideally)
    pub fn request_cube_adjust_around(&mut self, coord: &ICoord, vel: Option<&Vector3<f32>>) {
        let vel_thresh: f32 = { N } as f32 / 2.0;

        if let Some(vel) = vel {
            if vel.x.abs() >= vel_thresh || vel.y.abs() >= vel_thresh || vel.z.abs() >= vel_thresh {
                let mut start = coord - ICoord::new(2, 2, 2);
                let mut end = coord + ICoord::new(3, 4, 3);

                let size = end - start;
                let remaining = Vector3::new(N, N, N).cast::<i64>().unwrap() - size;

                for dim in 0..3 {
                    if vel[dim] > vel_thresh {
                        end[dim] += remaining[dim];
                    } else if vel[dim] < -vel_thresh {
                        start[dim] -= remaining[dim];
                    } else {
                        let offset = remaining[dim] / 2;

                        start[dim] -= offset;
                        end[dim] += offset
                    }
                }
                debug_assert!(end.x - start.x <= N as i64);
                debug_assert!(end.y - start.y <= N as i64);
                debug_assert!(end.z - start.z <= N as i64);

                log::debug!(
                    "High Velocity: {:?}, moving cube to {:?}..{:?} around {:?}",
                    vel,
                    start,
                    end,
                    coord,
                );
                self.request_cube_shift_to(start..end);
                return;
            }
        }

        let third_val = N as i64 / 3;
        let third = ICoord::new(third_val, third_val, third_val);
        let middle =
            (self.last_desired_bounds.start + third)..(self.last_desired_bounds.end - third);
        if !within(coord, &middle) {
            let half_val = N as i64 / 2;
            let half = ICoord::new(half_val, half_val, half_val);
            let start = coord - half;
            let end = start + ICoord::new(N as i64, N as i64, N as i64);
            log::debug!("Moving cube to {:?}..{:?} around {:?}", start, end, coord);
            self.request_cube_shift_to(start..end)
        }
    }
}

impl<T: Sized + Default + Copy, const N: usize> RwCubeWriter<T, N> {
    /// Gives the writer a chance to fill in the cube as requested by the reader.
    /// Will call the `get_value` closure with a coordinate on an as-needed basis to get what the value to fill in is.
    pub fn fill_in<F: Fn(&ICoord) -> Option<T>>(&mut self, get_value: F) {
        // Get the current `pending_bound_change`, or get the next `desired_bounds` change

        if self.last_change_counter != self.cube.change_counter.load(Ordering::Acquire) {
            let bounds = self.cube.copy_bounds();
            self.pending_write = Some(bounds.dif());
        }

        if let Some(mut dif_iter) = self.pending_write.take() {
            // We will make this range readable
            let readable = dif_iter.bounds.readers_desired_bounds.clone();
            let mut incomplete = false;
            let mut last_coord = dif_iter.cur; // used to rewind if a read fails
            for coord in &mut dif_iter {
                // println!("Writing to {:?}", coord);
                let index = coord_to_index(&coord, N);
                let cube_data = unsafe { &mut *self.cube.data.get() };
                if let Some(val) = get_value(&coord) {
                    cube_data[index[0]][index[1]][index[2]] = val;
                    last_coord = coord;
                } else {
                    incomplete = true;
                    break;
                }
            }
            if incomplete {
                dif_iter.cur = last_coord;
                self.pending_write = Some(dif_iter);
                return;
            }

            {
                let mut bounds = self.cube.bounds.write().unwrap();
                bounds.writers_readable_bounds = readable;
            }

            self.last_change_counter = self.last_change_counter.wrapping_add(1);
        }
    }
}

/// Returns true if `c` is within `beg` and `end` (inclusive)
fn within(c: &ICoord, range: &Range<ICoord>) -> bool {
    for i in 0..3 {
        if !(range.start[i]..range.end[i]).contains(&c[i]) {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use rand::Rng;
    use std::{
        ops::RangeInclusive,
        sync::atomic::AtomicBool,
        time::{Duration, Instant},
    };

    use super::*;

    #[derive(Clone, Copy)]
    struct Coord(ICoord);
    impl Default for Coord {
        fn default() -> Self {
            Self(ICoord::new(0, 0, 0))
        }
    }

    #[test]
    fn rw_cube_coord_test() {
        const SIZE: usize = 9;
        const READ_OFFSET: RangeInclusive<i64> = -3..=3;

        let (mut writer, mut reader) = new_rwcube::<Coord, SIZE>();

        let writer_stop = Arc::new(AtomicBool::new(false));
        let writer_stop_clone = Arc::clone(&writer_stop);
        let writer_thread = std::thread::spawn(move || {
            while !writer_stop_clone.load(Ordering::Relaxed) {
                writer.fill_in(|coord| Some(Coord(*coord)));
            }
        });

        let mut center_coord = ICoord::new(0, 0, 0);

        'move_the_center_loop: for _ in 0..1000 {
            let started = Instant::now();

            if rand::random::<f32>() < 0.9 {
                center_coord.x += rand::thread_rng().gen_range(-(SIZE as i64), (SIZE + 1) as i64);
                center_coord.y += rand::thread_rng().gen_range(-(SIZE as i64), (SIZE + 1) as i64);
                center_coord.z += rand::thread_rng().gen_range(-(SIZE as i64), (SIZE + 1) as i64);
            } else {
                center_coord = ICoord::new(
                    rand::thread_rng().gen_range(-50, 50),
                    rand::thread_rng().gen_range(-50, 50),
                    rand::thread_rng().gen_range(-50, 50),
                )
            }

            reader.request_cube_adjust_around(&center_coord, None);

            'read_assert_loop: while started.elapsed() < Duration::from_secs(1) {
                let handle = reader.read_handle();

                for x in READ_OFFSET {
                    for y in READ_OFFSET {
                        for z in READ_OFFSET {
                            let read_coord = center_coord + ICoord::new(x, y, z);
                            if let Some(result) = handle.read(&read_coord) {
                                assert_eq!(result.0, read_coord);
                            } else {
                                // It's not there. Wait on the writer to do its job, or timeout and error
                                continue 'read_assert_loop;
                            }
                        }
                    }
                }

                // We did it!! All values read and checked. Let's move the center and test again.
                continue 'move_the_center_loop;
            }

            panic!("Timeout");
        }

        // stop
        writer_stop.store(true, Ordering::SeqCst);
        writer_thread.join().unwrap();
    }
}
