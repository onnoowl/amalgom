pub mod config;
pub mod dimension;
pub mod face;
pub mod live_reload;
pub mod rwcube;

pub use ron;

pub use config::CONFIG;
pub use dimension::Dimension;
pub use face::{Face, FACES};

use arrayvec::ArrayVec;
use cgmath::{InnerSpace, Matrix4, Point3, Vector2, Vector3};
use collision::Aabb3;

pub const ROOT_LEVEL: u8 = 11;
pub const TREE_MOVE_THRESHOLD: f64 = 1. / 6.;
pub const LOD_MULT: f32 = 6.0; //How close you have to be (as a multiplier of node size) before the LOD recurses

pub const MAX_LEVELS: usize = 32;

pub const COLLISION_CAGE_SIZE: usize = 32;

pub type Usize3 = Vector3<usize>;
pub type ICoord = Vector3<i64>;
pub type UCoord = Vector3<u64>;
pub type F32Coord = Vector3<f32>;
pub type F64Coord = Vector3<f64>;
pub type BB64 = Aabb3<f64>;
pub type BB32 = Aabb3<f32>;

/// A `Loc` (location) is a (coordinate, level) pair.
/// It indicates a unique node location in the tree.
/// It can be converted to an `Address` type by the tree.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Loc(
    /// The coordinate for the location
    pub ICoord,
    /// The level for the location
    pub u8,
);
/// A `Loc` (location) is a (coordinate, level) pair.
/// It indicates a unique node location in the tree.
/// It can be converted to an `Address` type by the tree.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Loc2D(
    /// The coordinate for the location
    pub Vector2<i64>,
    /// The level for the location
    pub u8,
);

// Converts a vector to a point
pub fn v_to_p<S: cgmath::BaseNum>(v: &Vector3<S>) -> Point3<S> {
    Point3::new(v.x, v.y, v.z)
}

pub fn p_to_v<S: cgmath::BaseNum>(p: &Point3<S>) -> Vector3<S> {
    Vector3::new(p.x, p.y, p.z)
}

pub const QUAD_OFFSETS: [(u8, u8); 4] = [(0, 0), (0, 1), (1, 0), (1, 1)];

pub const OCTAL_OFFSETS: [ICoord; 8] = [
    ICoord { x: 0, y: 0, z: 0 },
    ICoord { x: 1, y: 0, z: 0 },
    ICoord { x: 0, y: 1, z: 0 },
    ICoord { x: 1, y: 1, z: 0 },
    ICoord { x: 0, y: 0, z: 1 },
    ICoord { x: 1, y: 0, z: 1 },
    ICoord { x: 0, y: 1, z: 1 },
    ICoord { x: 1, y: 1, z: 1 },
];

pub fn calculate_visible_faces(
    pos: &ICoord,
    size: i64,
    view_coord: &Point3<f32>,
) -> ArrayVec<[Face; 6]> {
    let mut faces = ArrayVec::<[Face; 6]>::new(); //blank array, will overwrite
    Dimension::variants().for_each(|dim| {
        let d = dim as usize;
        let face_axis = dim.to_faces();
        let dif = view_coord[d] - pos[d] as f32;
        if dif < size as f32 {
            faces.push(face_axis[0]);
        }
        if dif > 0. {
            faces.push(face_axis[1]);
        }
    });
    faces
}

pub fn offset_to_index(offset: ICoord) -> usize {
    return (offset.x + offset.y * 2 + offset.z * 4) as usize;
}

pub const UV_FACE_VALS: [[Loc; 4]; 6] = [
    // front
    [
        Loc(ICoord { x: 0, y: 1, z: 0 }, 00),
        Loc(ICoord { x: 1, y: 1, z: 0 }, 01),
        Loc(ICoord { x: 1, y: 0, z: 0 }, 11),
        Loc(ICoord { x: 0, y: 0, z: 0 }, 10),
    ],
    // back
    [
        Loc(ICoord { x: 1, y: 1, z: 1 }, 00),
        Loc(ICoord { x: 0, y: 1, z: 1 }, 01),
        Loc(ICoord { x: 0, y: 0, z: 1 }, 11),
        Loc(ICoord { x: 1, y: 0, z: 1 }, 10),
    ],
    // left
    [
        Loc(ICoord { x: 1, y: 1, z: 0 }, 00),
        Loc(ICoord { x: 1, y: 1, z: 1 }, 01),
        Loc(ICoord { x: 1, y: 0, z: 1 }, 11),
        Loc(ICoord { x: 1, y: 0, z: 0 }, 10),
    ],
    // right
    [
        Loc(ICoord { x: 0, y: 1, z: 1 }, 00),
        Loc(ICoord { x: 0, y: 1, z: 0 }, 01),
        Loc(ICoord { x: 0, y: 0, z: 0 }, 11),
        Loc(ICoord { x: 0, y: 0, z: 1 }, 10),
    ],
    // top
    [
        Loc(ICoord { x: 1, y: 1, z: 0 }, 00),
        Loc(ICoord { x: 0, y: 1, z: 0 }, 01),
        Loc(ICoord { x: 0, y: 1, z: 1 }, 11),
        Loc(ICoord { x: 1, y: 1, z: 1 }, 10),
    ],
    // bottom
    [
        Loc(ICoord { x: 1, y: 0, z: 1 }, 00),
        Loc(ICoord { x: 0, y: 0, z: 1 }, 01),
        Loc(ICoord { x: 0, y: 0, z: 0 }, 11),
        Loc(ICoord { x: 1, y: 0, z: 0 }, 10),
    ],
];

impl Into<Matrix4<f32>> for Loc {
    fn into(self) -> Matrix4<f32> {
        let size = 2f32.powi(self.1 as i32);

        Matrix4::<f32>::from_translation(self.0.cast::<f32>().unwrap())
            * Matrix4::<f32>::from_scale(size)
    }
}

impl Into<Loc2D> for Loc {
    fn into(self) -> Loc2D {
        Loc2D(Vector2::new(self.0.x, self.0.z), self.1)
    }
}

pub fn safe_normalize<V: InnerSpace<Scalar = f32> + Copy + std::ops::Div<f32, Output = V>>(
    v: &V,
) -> V {
    let mag = v.magnitude();

    if mag > 0.0 {
        *v / mag
    } else {
        *v // all zero
    }
}

#[macro_export]
macro_rules! live_reload_load {
    ($path:expr) => {{
        let mut path = std::path::PathBuf::from(file!());
        path.pop();
        path.push($path);

        $crate::live_reload::LiveReload::new(
            $crate::ron::de::from_str(include_str!($path))
                .unwrap_or_else(|e| panic!("Failed to parse ron file at {:?}", path)),
            path,
        )
    }};
}
