//! Enum for Dimensions X, Y, and Z with a few convenience functions

use cgmath::{Matrix4, Vector3};

use crate::{face::Face, ICoord};

#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(usize)]
pub enum Dimension {
    X = 0,
    Y,
    Z,
}

impl Dimension {
    pub fn variants() -> impl Iterator<Item = Dimension> {
        static VARIANTS: &'static [Dimension] = &[Dimension::X, Dimension::Y, Dimension::Z];
        VARIANTS.iter().map(|x| *x)
    }

    pub fn to_faces(&self) -> [Face; 2] {
        match self {
            Dimension::X => [Face::Right, Face::Left],
            Dimension::Y => [Face::Bottom, Face::Top],
            Dimension::Z => [Face::Front, Face::Back],
        }
    }

    pub fn to_face_with_offset(&self, offset: bool) -> Face {
        self.to_faces()[offset as usize]
    }
}

impl From<Face> for Dimension {
    fn from(face: Face) -> Self {
        match face {
            Face::Front | Face::Back => Dimension::Z,
            Face::Left | Face::Right => Dimension::X,
            Face::Top | Face::Bottom => Dimension::Y,
        }
    }
}

#[derive(Debug, Clone)]
pub struct OcclusionPlane {
    pub orientation: Dimension,
    pub verts: [Vector3<f32>; 4],
}

const ZERO_VERT: Vector3<f32> = Vector3::<f32> {
    x: 0.,
    y: 0.,
    z: 0.,
};
const DEFAULT_VERTS: [Vector3<f32>; 4] = [ZERO_VERT, ZERO_VERT, ZERO_VERT, ZERO_VERT];

impl OcclusionPlane {
    pub fn new(iverts: [ICoord; 4], orientation: Dimension) -> Self {
        let mut fverts = DEFAULT_VERTS;
        for (fv, iv) in fverts.iter_mut().zip(iverts.iter()) {
            *fv = iv.cast::<f32>().unwrap();
        }
        OcclusionPlane {
            verts: fverts,
            orientation,
        }
    }

    pub fn project_to_clip_space(&self, view_projection_mat: &Matrix4<f32>) -> Self {
        let mut verts = DEFAULT_VERTS;
        for (out_v, in_v) in verts.iter_mut().zip(self.verts.iter()) {
            let projected = view_projection_mat * in_v.extend(1.0);
            *out_v = projected.truncate() / projected.w;
        }
        OcclusionPlane {
            orientation: self.orientation,
            verts,
        }
    }

    pub fn project_to_screen_space(&self, view_projection_mat: &Matrix4<f32>) -> Self {
        let mut result = self.project_to_clip_space(view_projection_mat);

        for v in &mut result.verts {
            *v = v.map(|c| ((c + 1.) / 2.));
        }

        result
    }
}
