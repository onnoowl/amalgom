//! Enum for a cube face and various face indexing functions

use crate::{ICoord, QUAD_OFFSETS};

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Face {
    Front = 0,
    Back,
    Left,
    Right,
    Top,
    Bottom,
}

pub const FACES: [Face; 6] = [
    Face::Front,
    Face::Back,
    Face::Left,
    Face::Right,
    Face::Top,
    Face::Bottom,
];

impl Face {
    pub fn variants() -> impl Iterator<Item = Face> {
        static VARIANTS: &'static [Face] = &[
            Face::Front,
            Face::Back,
            Face::Left,
            Face::Right,
            Face::Top,
            Face::Bottom,
        ];
        VARIANTS.iter().map(|x| *x)
    }

    pub fn from_two_indices(my_index: u8, neighb_index: u8) -> Option<Self> {
        let my_x = my_index & 1;
        let my_y = (my_index >> 1) & 1;
        let my_z = (my_index >> 2) & 1;
        let n_x = neighb_index & 1;
        let n_y = (neighb_index >> 1) & 1;
        let n_z = (neighb_index >> 2) & 1;

        let xne = my_x != n_x;
        let yne = my_y != n_y;
        let zne = my_z != n_z;

        // If any two bits are not equal, this is a diagonal, not an adjacent face. Return None
        if (xne && yne) || (yne && zne) || (xne && zne) {
            return None;
        }

        if xne {
            if n_x > my_x {
                Some(Face::Left)
            } else {
                Some(Face::Right)
            }
        } else if yne {
            if n_y > my_y {
                Some(Face::Top)
            } else {
                Some(Face::Bottom)
            }
        } else if zne {
            if n_z > my_z {
                Some(Face::Back)
            } else {
                Some(Face::Front)
            }
        } else {
            None
        }
    }

    pub fn flip(&self) -> Self {
        match self {
            Face::Front => Face::Back,
            Face::Back => Face::Front,
            Face::Left => Face::Right,
            Face::Right => Face::Left,
            Face::Top => Face::Bottom,
            Face::Bottom => Face::Top,
        }
    }

    /// self should be the direction of the leaf from the branch's perspective
    pub fn get_zipped_octal_and_quad(&self) -> impl Iterator<Item = (usize, usize)> + Clone {
        let (pairing_dim, pairing_val): (u8, u8) = match self {
            Face::Right => (0, 0),
            Face::Left => (0, 1),
            Face::Bottom => (1, 0),
            Face::Top => (1, 1),
            Face::Front => (2, 0),
            Face::Back => (2, 1),
        };

        #[allow(clippy::inconsistent_digit_grouping)]
        QUAD_OFFSETS.iter().map(move |(np_dim_a, np_dim_b)| {
            let index1 = match pairing_dim {
                0 => (np_dim_b << 2___) | (np_dim_a << 1___) | pairing_val,
                1 => (np_dim_b << 2___) | (pairing_val << 1) | np_dim_a,
                2 => (pairing_val << 2) | (np_dim_b << 1___) | np_dim_a,
                _ => unreachable!(),
            };
            let index2 = (np_dim_a << 1) | np_dim_b;
            (index1 as usize, index2 as usize)
        })
    }

    /// self should be dir_of_2
    pub fn get_zip_indices(&self) -> impl Iterator<Item = (usize, usize)> {
        let (pairing_dim, pairing_val): (u8, u8) = match self {
            Face::Right => (0, 0),
            Face::Left => (0, 1),
            Face::Bottom => (1, 0),
            Face::Top => (1, 1),
            Face::Front => (2, 0),
            Face::Back => (2, 1),
        };

        [(0_u8, 0_u8), (0, 1), (1, 0), (1, 1)]
            .iter()
            .map(move |(np_dim_a, np_dim_b)| {
                let index1 = match pairing_dim {
                    0 => (np_dim_b << 2___) | (np_dim_a << 1___) | pairing_val,
                    1 => (np_dim_b << 2___) | (pairing_val << 1) | np_dim_a,
                    2 => (pairing_val << 2) | (np_dim_b << 1___) | np_dim_a,
                    _ => unreachable!(),
                };
                let index2 = index1 ^ (1 << pairing_dim);
                (index1 as usize, index2 as usize)
            })
    }
}

impl From<Face> for ICoord {
    fn from(f: Face) -> Self {
        match f {
            Face::Front => (0, 0, -1).into(),
            Face::Back => (0, 0, 1).into(),
            Face::Left => (1, 0, 0).into(),
            Face::Right => (-1, 0, 0).into(),
            Face::Top => (0, 1, 0).into(),
            Face::Bottom => (0, -1, 0).into(),
        }
    }
}

impl From<Face> for u8 {
    fn from(f: Face) -> Self {
        f as u8
    }
}
impl From<Face> for u32 {
    fn from(f: Face) -> Self {
        f as u32
    }
}

impl From<Face> for usize {
    fn from(f: Face) -> Self {
        f as usize
    }
}

impl From<u8> for Face {
    fn from(f: u8) -> Self {
        match f {
            0 => Face::Front,
            1 => Face::Back,
            2 => Face::Left,
            3 => Face::Right,
            4 => Face::Top,
            5 => Face::Bottom,
            _ => panic!("Face::from::<u8>(...) must be given a u8 in interval [0, 5]"),
        }
    }
}

// from_two_indices tests
#[cfg(test)]
mod tests {

    use super::*;

    // This is the runner used by all test cases
    // It will convert boolean values depicting the values of the clauses
    // into the desired inputs to achieve those values for the clauses
    // xne - whether the x dimension of the indeces are not equal
    // yne - whether the y dimension of the indeces are not equal
    // zne - whether the z dimension of the indeces are not equal
    // expected - the expected predicate given these inputs
    fn face_indices_runner(xne: bool, yne: bool, zne: bool, expected: bool) {
        // different dimensions are just different bit shifts from each other in order zyx
        let my_index = ((zne as u8) << 2) | ((yne as u8) << 1) | xne as u8;

        assert_eq!(expected, Face::from_two_indices(my_index, 0).is_none());
    }

    // xne as major clause
    //  minor clause yne = true
    //  minor clause zne = false
    // Major clause = true so neither x nor y will be equal between indices.
    // Predicate will return true
    #[test]
    fn face_indices_racc_logic_test_xne_t() {
        face_indices_runner(true, true, false, true);
    }

    // xne as major clause
    //  minor clause yne = true
    //  minor clause zne = false
    // Major clause = false so only y will not be equal between indices.
    // Predicate will return false
    #[test]
    fn face_indices_racc_logic_test_xne_f() {
        face_indices_runner(false, true, false, false);
    }

    // yne as major clause
    //  minor clause zne = true
    //  minor clause xne = false
    // Major clause = true so neither y nor z will be equal between indices.
    // Predicate will return true
    #[test]
    fn face_indices_racc_logic_test_yne_t() {
        face_indices_runner(false, true, true, true);
    }

    // yne as major clause
    //  minor clause zne = true
    //  minor clause xne = false
    // Major clause = false so only z will not be equal between indices.
    // Predicate will return false
    #[test]
    fn face_indices_racc_logic_test_yne_f() {
        face_indices_runner(false, false, true, false);
    }

    // zne as major clause
    //  minor clause xne = true
    //  minor clause yne = false
    // Major clause = true so neither z nor x will be equal between indices.
    // Predicate will return true
    #[test]
    fn face_indices_racc_logic_test_zne_t() {
        face_indices_runner(true, false, true, true);
    }

    // zne as major clause
    //  minor clause xne = true
    //  minor clause yne = false
    // Major clause = false so only z will not be equal between indices.
    // Predicate will return false
    #[test]
    fn face_indices_racc_logic_test_zne_f() {
        face_indices_runner(true, false, false, false);
    }
}
