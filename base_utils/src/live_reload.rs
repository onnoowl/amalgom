#![allow(unused_imports)]
#![allow(dead_code)]

use std::{
    ops::{Deref, DerefMut},
    path::PathBuf,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
    time::Duration,
};

use hotwatch::{Event, Hotwatch};
use serde::Deserialize;

pub struct LiveReload<T: for<'de> Deserialize<'de>> {
    item: T,
    path: PathBuf,
    need_reloading: Arc<AtomicUsize>,
    last_counter: usize,

    #[cfg(debug_assertions)]
    _hotwatch: Hotwatch,
}

impl<T: for<'de> Deserialize<'de>> LiveReload<T> {
    pub fn new(initial: T, path: PathBuf) -> Self {
        let need_reloading = Arc::new(AtomicUsize::new(0));

        #[cfg(debug_assertions)]
        {
            let need_reloading_clone = need_reloading.clone();
            let path_clone = path.clone();

            let mut hotwatch = Hotwatch::new_with_custom_delay(Duration::from_millis(200))
                .expect("hotwatch failed to initialize");
            hotwatch
                .watch(&path_clone, move |event: Event| {
                    if let Event::Write(_path) = event {
                        need_reloading_clone.fetch_add(1, Ordering::Relaxed);
                    }
                })
                .unwrap_or_else(|e| log::warn!("Failed to watch file at {:?} {}", &path_clone, e));

            LiveReload {
                item: initial,
                path,
                need_reloading,
                last_counter: 0,
                _hotwatch: hotwatch,
            }
        }
        #[cfg(not(debug_assertions))]
        {
            LiveReload {
                item: initial,
                path,
                need_reloading,
                last_counter: 0,
            }
        }
    }

    /// Returns true if it hot-reloaded
    pub fn check_reload(&mut self) -> bool {
        #[cfg(debug_assertions)]
        {
            let need_reloading = self.need_reloading.load(Ordering::Relaxed);
            if need_reloading != self.last_counter {
                log::info!("Reloading {:?}", self.path);
                self.last_counter = need_reloading;
                if let Ok(parsed) = ron::de::from_str(
                    std::fs::read_to_string(&self.path)
                        .expect("Failed to read file for hot reload")
                        .as_ref(),
                ) {
                    self.item = parsed;
                } else {
                    log::error!("Could not parse config at path {:?}", self.path);
                }
                true
            } else {
                false
            }
        }
        #[cfg(not(debug_assertions))]
        {
            false
        }
    }
}

impl<T: for<'de> Deserialize<'de>> Deref for LiveReload<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.item
    }
}
impl<T: for<'de> Deserialize<'de>> DerefMut for LiveReload<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.item
    }
}
