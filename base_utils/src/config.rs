//! Global configuration object that takes command options

use lazy_static::lazy_static;
use structopt::StructOpt;

#[derive(StructOpt, Debug, Default)]
#[structopt(name = "basic")]
pub struct Config {
    #[structopt(short = "e", long)]
    pub no_extend: bool,

    #[structopt(short = "g", long)]
    pub no_gen: bool,

    #[structopt(short = "r", long)]
    pub no_lod_recurse: bool,

    #[structopt(short = "t", long)]
    pub time_travel: Option<usize>,

    #[structopt(long)]
    pub robot: bool,

    // For testing
    #[structopt(long)]
    pub nocapture: bool,

    // For testing
    #[structopt(long)]
    pub ignored: bool,
}

lazy_static! {
    pub static ref CONFIG: Config = Default::default();
    // Config::from_args();
}
