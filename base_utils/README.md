# Base_utils

This crate houses utility functions that don't require other crates from this project as dependencies.

## Modules

[config](src/config.rs) - Global configuration object that takes command options

[dimension](src/dimension.rs) - Enum for Dimensions X, Y, and Z with a few convenience functions

[face](src/face.rs) - Enum for a cube face and various face indexing functions
