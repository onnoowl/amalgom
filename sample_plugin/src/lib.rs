use game_utils::prelude::*;

#[derive(Debug, Default)]
struct SamplePlugin;

declare_plugin!(SamplePlugin);
impl Plugin for SamplePlugin {
    /// Get a name describing the `Plugin`.
    fn name(&self) -> &'static str {
        "sample_plugin"
    }
    /// A callback fired immediately after the plugin is loaded. Usually used
    /// for initialization.
    fn on_plugin_load(&self) {
        env_logger::from_env(env_logger::Env::default().default_filter_or("debug")).init();

        info!("SamplePlugin is loaded.");
    }
    /// A callback fired immediately before the plugin is unloaded. Use this if
    /// you need to do any cleanup.
    fn on_plugin_unload(&self) {}
}

impl ClientPlugin for SamplePlugin {
    fn register_listeners(&self, m: &mut ClientListenerManager) {
        m.on_keypress.add_listener(|builder, event| {
            builder
                .read_resource::<TestResource>()
                .with_query(
                    <(Read<Position>, Tagged<Model>)>::query()
                        .filter(!tag::<Static>() | changed::<Position>()),
                )
                .build(move |_commands, _world, _resource, _queries| {
                    info!("{:?}", event.get());
                    // let mut count = 0;
                    // {
                    //     for (entity, pos) in queries.iter_entities(&mut *world) {}
                    // }
                })
        })
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
struct Static;

#[derive(Debug)]
struct TestResource {}

#[derive(Copy, Clone, Debug, PartialEq)]
struct Position {}

#[derive(Copy, Clone, Debug, PartialEq)]
struct Model {}

// impl SamplePlugin {
//     fn register_listeners(&self) {

//     }
// }
