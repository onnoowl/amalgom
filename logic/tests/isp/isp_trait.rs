use std::io::Write;
use std::{
    collections::{HashMap, HashSet},
    fs::File,
    panic::UnwindSafe,
    sync::Mutex,
};

use base_utils::{ICoord, ROOT_LEVEL};
use logic::{
    generation::NoiseManager,
    terrain_manager::{GenerationDispatcher, TerrainManager},
    tree::*,
};

pub trait ISPTrait: Sized + Eq + std::hash::Hash + Clone + std::fmt::Debug
where
    for<'a> &'a Self: UnwindSafe,
{
    const N: usize;

    const TESTING_BUFFER_DEPTH_AND_MIN_LEVEL: u32 = 3;

    // To generate all pairs, count to 3^n. Treat the counter as base 3, and get 10 digits from it. Use these digits to generate the 10 values in ISP, since each field has 3 variants.
    fn all() -> Vec<Self> {
        // this allocation overestimates
        let mut all = Vec::with_capacity(3usize.pow(Self::N as u32));

        for mut counter in 0..(3i32.pow(Self::N as u32)) {
            // Through this loop, nums will get all combinations of values 0..3
            let mut nums = Vec::new();
            for _ in 0..Self::N {
                nums.push((counter % 3) as u8);
                counter /= 3;
            }
            if let Some(isp) = Self::from_nums(&nums) {
                if isp.is_valid() {
                    all.push(isp);
                }
            }
        }
        all
    }

    /// Returns a tuple of (ISPs_that_form_pairwise_coverage, infeasible_pairs, duplicate_pairs)
    fn pairwise_coverage() -> (Vec<Self>, Vec<String>, Vec<(String, String)>) {
        // this allocation overestimates
        let mut all = HashSet::<Self>::new();
        let mut infeasible = Vec::new();
        let mut duplicate = Vec::new();

        for first_i_in_pair in 0..(Self::N - 1) {
            for second_i_in_pair in (first_i_in_pair + 1)..Self::N {
                for pair_counter in 0..9 {
                    let first_val = pair_counter % 3;
                    let second_val = pair_counter / 3;

                    let mut infeasible_isp: Option<Self> = None;
                    'counter: for mut counter in 0..(3i32.pow(Self::N as u32)) {
                        // Through this loop, nums will get all combinations of values 0..3
                        let mut nums = Vec::new();
                        for i in 0..Self::N {
                            nums.push(if i == first_i_in_pair {
                                first_val
                            } else if i == second_i_in_pair {
                                second_val
                            } else {
                                let val = (counter % 3) as u8;
                                counter /= 3;
                                val
                            });
                        }
                        if let Some(mut isp) = Self::from_nums(&nums) {
                            if isp.is_valid() {
                                isp.make_pairing_desc(first_i_in_pair, second_i_in_pair);
                                infeasible_isp = None;
                                if let Some(already_there) = all.get(&isp) {
                                    // already present, duplicate
                                    duplicate.push((
                                        isp.get_pairing_desc().clone(),
                                        already_there.get_pairing_desc().clone(),
                                    ))
                                } else {
                                    all.insert(isp);
                                }
                                break 'counter;
                            } else {
                                infeasible_isp = Some(isp)
                            }
                        }
                    }
                    if let Some(mut inf) = infeasible_isp {
                        inf.make_pairing_desc(first_i_in_pair, second_i_in_pair);
                        infeasible.push(inf.get_pairing_desc().clone());
                    }
                }
            }
        }
        let mut pairs: Vec<Self> = all.drain().collect();
        pairs.sort_by_key(|p| p.get_pairing_desc().clone());
        (pairs, infeasible, duplicate)
    }

    fn make_pairing_desc(&mut self, pair_elem_1: usize, pair_elem_2: usize) {
        self.set_pairing_desc(format!(
            "({}, {})",
            self.char_to_str(pair_elem_1),
            self.char_to_str(pair_elem_2)
        ));
    }

    fn set_pairing_desc(&mut self, pairing_desc: String);
    fn get_pairing_desc(&self) -> &String;

    fn is_valid(&self) -> bool;

    fn from_nums(n: &[u8]) -> Option<Self>;

    fn char_to_str(&self, index: usize) -> String;

    // Do the setup for running the test
    fn run_test(
        &self,
        generation_dispatcher: &mut Mutex<Option<GenerationDispatcher>>,
    ) -> std::thread::Result<()> {
        // If poisoned, reset the mutex. We'll make a fresh Generation Dispatcher
        if generation_dispatcher.is_poisoned() {
            *generation_dispatcher = Mutex::new(None);
        }

        // Change to immutable ref to make it safe for the catch_unwind boundary
        let generation_dispatcher = &*generation_dispatcher;

        std::panic::catch_unwind(move || {
            let mut guard = generation_dispatcher
                .lock()
                .expect("Should have been reset if poisoned");

            let (sender, receiver) = std::sync::mpsc::channel();

            let mut mng = {
                let tree = {
                    let hsize = 2i64.pow(ROOT_LEVEL as u32 - 1);
                    LogicTree::new(
                        LogicNode::Unloaded(Default::default()),
                        ROOT_LEVEL as u32,
                        ICoord::new(-hsize, -hsize, -hsize),
                    )
                };

                let tg = guard
                    .take()
                    .unwrap_or_else(|| GenerationDispatcher::new(NoiseManager::default()));

                let mut mng = TerrainManager::with_fields(
                    tree,
                    tg,
                    sender,
                    Self::TESTING_BUFFER_DEPTH_AND_MIN_LEVEL,
                );
                mng.generation_dispatcher.max_outstanding_work = u32::MAX;
                mng
            };

            self.run_test_impl(&mut mng);

            drop(receiver);

            guard.replace(mng.generation_dispatcher);
        })
    }

    fn run_test_impl(&self, mng: &mut TerrainManager);

    fn test_pairwise_and_report(show_errors: bool, report_name: &str) {
        println!("Generating combinations...");
        let pairs = Self::all();
        let infeasible = Vec::<String>::new();
        let duplicates = Vec::<(String, String)>::new();
        println!("Testing {} combinations", pairs.len());

        // We keep this around in order to reuse the generation dispatcher
        let mut tg = Mutex::new(None);

        let mut fails = HashMap::new();

        for (i, isp) in pairs.iter().cloned().enumerate() {
            println!("Running {}/{} | isp {:?}", i, pairs.len(), isp);

            if let Err(err) = isp.run_test(&mut tg) {
                fails.insert(isp, err);
            }
        }

        println!(
            "\n ==== There were {}/{} failures ====\n",
            fails.len(),
            pairs.len()
        );

        if !fails.is_empty() {
            let mut f = File::create(report_name).unwrap();
            writeln!(
                f,
                "## Summary\nThere were {}/{} failures\n\n",
                fails.len(),
                pairs.len()
            )
            .unwrap();

            writeln!(f, "## Results:").unwrap();
            writeln!(f, "|Result|Target Pair|Characteristics Used For Test|").unwrap();
            writeln!(f, "|------|-----------|-----------------------------|").unwrap();
            for isp in pairs.iter() {
                if let Some(result) = fails.get(isp) {
                    if show_errors {
                        let result = {
                            if let Some(s) = result.downcast_ref::<&'static str>() {
                                s
                            } else if let Some(s) = result.downcast_ref::<String>() {
                                s
                            } else {
                                "unknown"
                            }
                        };
                        if result.contains("\n") {
                            writeln!(
                                f,
                                "|Failed|{}|{:?}| With Error:\n{{ ====\n{}\n==== }}",
                                isp.get_pairing_desc(),
                                isp,
                                result
                            )
                            .unwrap();
                        } else {
                            writeln!(
                                f,
                                "|Failed|{}|{:?}| With Error: \"{}\"",
                                isp.get_pairing_desc(),
                                isp,
                                result
                            )
                            .unwrap();
                        }
                    } else {
                        writeln!(f, "|Failed|{}|{:?}|", isp.get_pairing_desc(), isp).unwrap();
                    }
                } else {
                    writeln!(f, "|Passed|{}|{:?}|", isp.get_pairing_desc(), isp).unwrap();
                }
            }

            writeln!(f, "\n\n## Duplicate Pairs, covered by above tests:").unwrap();
            writeln!(
                f,
                "|Duplicate Target Pair|Target Pair That It Was Already Covered By|"
            )
            .unwrap();
            writeln!(
                f,
                "|---------------------|-----------------------------------------|"
            )
            .unwrap();
            for (target, covered_by) in duplicates.iter() {
                writeln!(f, "|{}|{}|", target, covered_by).unwrap();
            }

            writeln!(f, "\n\n## Infeasible Pairs:").unwrap();
            writeln!(f, "|Infeasible Pair|").unwrap();
            writeln!(f, "|---------------|").unwrap();
            for inf in infeasible.iter() {
                writeln!(f, "|{}|", inf).unwrap();
            }
        }

        assert!(fails.len() == 0);
    }

    fn simple_run_test(&self) {
        let (sender, receiver) = std::sync::mpsc::channel();

        let mut mng = {
            let tree = {
                let hsize = 2i64.pow(ROOT_LEVEL as u32 - 1);
                LogicTree::new(
                    LogicNode::Unloaded(Default::default()),
                    ROOT_LEVEL as u32,
                    ICoord::new(-hsize, -hsize, -hsize),
                )
            };
            let gen_disp = GenerationDispatcher::new(NoiseManager::default());

            let mut mng = TerrainManager::with_fields(
                tree,
                gen_disp,
                sender,
                Self::TESTING_BUFFER_DEPTH_AND_MIN_LEVEL,
            );
            mng.generation_dispatcher.max_outstanding_work = u32::MAX;
            mng
        };

        self.run_test_impl(&mut mng);

        drop(receiver);
    }
}
