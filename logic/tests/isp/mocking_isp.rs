use super::*;

use derivative::*;
use logic::terrain_manager::TerrainManager;

/// Characteristics and possible values for our input space partitioning
/// These describe a "chunk" X, and its many neighbors Yi (where i is an index).
#[derive(Clone, Derivative)]
#[derivative(PartialEq, Eq, Hash, Debug)]
pub struct MockingISP {
    // Description of what pair was being targeted
    #[derivative(PartialEq = "ignore", Hash = "ignore", Debug = "ignore")]
    pub pairing_desc: String,

    /// X's placement ordering
    pub x_placement_order: PlacementOrder,

    /// Max LOD (of X, and Yi)
    /// Max LOD should not count the u8::MAX LOD of unloadeds
    pub max_lod: ZOG,

    /// (Max LOD of Yi) - (LOD of X)
    /// Max LOD of Yi should not count the u8::MAX LOD of unloadeds
    pub max_yi_minus_x: Sign,

    /// Number of Yi that are Leafs
    pub num_yi_leafs: NSA,

    /// Number of Yi that are LODLeafs
    pub num_yi_lodleafs: NSA,

    /// Number of Yi that are Unloadeds
    pub num_yi_unloadeds: NSA,

    /// Number of Yi that are Branches
    pub num_yi_branches: NSA,

    /// X's Node Type (i.e. leaf/lodleaf/unloaded/branch)
    pub x_type: Type,

    /// X's relationship with a bigger chunk Y0
    pub x_rel_with_big_y: Relationship,

    /// Number of chunks that are "generation" chunks
    pub yi_gen_or_ext: GenMixedExt,

    /// Number of chunks that are "extension" chunks
    pub x_gen_or_ext: GenExt,

    /// Seed used for randomness
    #[derivative(PartialEq = "ignore", Hash = "ignore")]
    pub seed: u64,
}

impl ISPTrait for MockingISP {
    const N: usize = 11;

    fn is_valid(&self) -> bool {
        let types = [
            self.num_yi_leafs,
            self.num_yi_lodleafs,
            self.num_yi_unloadeds,
            self.num_yi_branches,
        ];
        let all_count = types.iter().filter(|zoa| **zoa == NSA::All).count();
        let none_count = types.iter().filter(|zoa| **zoa == NSA::None).count();

        // If one type is all, the rest must be zero
        if all_count != 0 && none_count != 3 {
            return false;
        }

        // If there are 3 nones, there must be one all
        if none_count == 3 && all_count != 1 {
            return false;
        }

        match self {
            // Cannot have all types as zero
            Self {
                num_yi_leafs: NSA::None,
                num_yi_lodleafs: NSA::None,
                num_yi_unloadeds: NSA::None,
                num_yi_branches: NSA::None,
                ..
            } => false,

            // If max_lod is zero, there cannot be a max_yi lod and x lod difference
            Self {
                max_lod: ZOG::Zero,
                max_yi_minus_x: Sign::Positive | Sign::Negative,
                ..
            } => false,

            // max_yi_minus_x of zero or negative implies there is not a bigger y, so x_rel_with_big_y should be NA
            Self {
                max_yi_minus_x: Sign::Zero | Sign::Negative,
                x_rel_with_big_y: Relationship::IsCloserChild | Relationship::IsFurtherChild,
                ..
            } => false,

            // max_yi_minus_x of positive implies there is a bigger y, so x_rel_with_big_y should not be NA
            Self {
                max_yi_minus_x: Sign::Positive,
                x_rel_with_big_y: Relationship::NA,
                ..
            } => false,

            // Else, true
            _ => true,
        }
    }

    fn from_nums(n: &[u8]) -> Option<Self> {
        use num::FromPrimitive;
        Some(Self {
            pairing_desc: String::new(),
            x_placement_order: FromPrimitive::from_u8(n[0])?,
            max_lod: FromPrimitive::from_u8(n[1])?,
            max_yi_minus_x: FromPrimitive::from_u8(n[2])?,
            num_yi_leafs: FromPrimitive::from_u8(n[3])?,
            num_yi_lodleafs: FromPrimitive::from_u8(n[4])?,
            num_yi_unloadeds: FromPrimitive::from_u8(n[5])?,
            num_yi_branches: FromPrimitive::from_u8(n[6])?,
            x_type: FromPrimitive::from_u8(n[7])?,
            x_rel_with_big_y: FromPrimitive::from_u8(n[8])?,
            yi_gen_or_ext: FromPrimitive::from_u8(n[9])?,
            x_gen_or_ext: FromPrimitive::from_u8(n[10])?,
            seed: rand::random(),
        })
    }

    fn char_to_str(&self, index: usize) -> String {
        match index {
            0 => format!("q1_x_placement_order: {:?}", self.x_placement_order),
            1 => format!("q2_max_lod: {:?}", self.max_lod),
            2 => format!("q3_max_yi_minus_x: {:?}", self.max_yi_minus_x),
            3 => format!("q4_num_yi_leafs: {:?}", self.num_yi_leafs),
            4 => format!("q5_num_yi_lodleafs: {:?}", self.num_yi_lodleafs),
            5 => format!("q6_num_yi_unloadeds: {:?}", self.num_yi_unloadeds),
            6 => format!("q7_num_yi_branches: {:?}", self.num_yi_branches),
            7 => format!("q8_x_type: {:?}", self.x_type),
            8 => format!("q9_x_rel_with_big_y: {:?}", self.x_rel_with_big_y),
            9 => format!("q10_yi_gen_or_ext: {:?}", self.yi_gen_or_ext),
            10 => format!("q11_x_gen_or_ext: {:?}", self.x_gen_or_ext),
            _ => unreachable!(),
        }
    }

    fn set_pairing_desc(&mut self, pairing_desc: String) {
        self.pairing_desc = pairing_desc
    }

    fn get_pairing_desc(&self) -> &String {
        &self.pairing_desc
    }

    fn run_test_impl(&self, mng: &mut TerrainManager) {
        mocking_runner::run(self, mng)
    }
}
