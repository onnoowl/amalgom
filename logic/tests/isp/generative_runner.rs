use super::*;
use base_utils::{ICoord, OCTAL_OFFSETS};
use logic::{
    terrain_manager::{TerrainManager, TerrainWorkDescriptor, TerrainWorkKind},
    tree::*,
};

use cgmath::vec3;
use rand::{prelude::*, rngs::StdRng};

pub fn run(isp: &GenerativeISP, mng: &mut TerrainManager) {
    let mut rng = StdRng::seed_from_u64(isp.seed);

    let buffer_depth_and_min_level = mng.get_buffer_depth_and_min_level() as u8;

    let (x_lod, max_y_lod) = {
        let max_lod: u8 = match isp.max_lod {
            ZOG::Zero => 0,
            ZOG::One => 1,
            ZOG::GreaterThanOne => rng.gen_range(2, 6),
        };

        match isp.max_yi_minus_x {
            Sign::Negative => (max_lod, max_lod - rng.gen_range(1, max_lod + 1)),
            Sign::Zero => (max_lod, max_lod),
            Sign::Positive => (max_lod - rng.gen_range(1, max_lod + 1), max_lod),
        }
    };

    let mut x = Vec::<TerrainWorkDescriptor>::new();
    let mut y = Vec::<TerrainWorkDescriptor>::new();

    let main_level = x_lod.max(max_y_lod) + buffer_depth_and_min_level;
    let main_size = 2i64.pow(main_level as u32);

    {
        let x_kind = match isp.x_gen_or_ext {
            GenExt::Generation => TerrainWorkKind::Generation,
            GenExt::Extension => TerrainWorkKind::Extension,
        };

        // x.push(fake_work(main_level, x_lod, vec3(0, 0, 0), x_kind));
        recurse_fake(
            &mut x,
            main_level,
            x_lod,
            vec3(0, 0, 0),
            &[x_kind],
            buffer_depth_and_min_level,
            &mut rng,
        );
        if isp.x_placement_order == PlacementOrder::Last {
            x.reverse();
        }
    }

    {
        let y_kinds: &[TerrainWorkKind] = match isp.yi_gen_or_ext {
            GenMixedExt::GenerationOnly => &[TerrainWorkKind::Generation],
            GenMixedExt::Mixed => &[TerrainWorkKind::Generation, TerrainWorkKind::Extension],
            GenMixedExt::ExtensionOnly => &[TerrainWorkKind::Extension],
        };

        let lods = (1..=6).map(|_| max_y_lod).collect::<Vec<_>>();

        // Prepare the rest of Ys
        for (coord, y_lod) in [
            vec3(main_size, 0, 0),
            vec3(-main_size, 0, 0),
            vec3(0, main_size, 0),
            vec3(0, -main_size, 0),
            vec3(0, 0, main_size),
            vec3(0, 0, -main_size),
        ]
        .iter()
        .zip(lods)
        {
            recurse_fake(
                &mut y,
                main_level,
                y_lod,
                *coord,
                y_kinds,
                buffer_depth_and_min_level,
                &mut rng,
            );
        }
    }

    // =========== Tidy up and submit the fake work ===========
    y.shuffle(&mut rng);

    // let mut pre_gen = Vec::new();
    // for d in x.iter().chain(y.iter()) {
    //     if d.kind == TerrainWorkKind::Extension {
    //         let gen = d.clone();

    //         pre_gen.push(fake_work(
    //             gen.level,
    //             rng.gen_range(gen.target_lod + 1, gen.level + 1),
    //             gen.coord,
    //             TerrainWorkKind::Generation,
    //         ))
    //     }
    // }

    let mut y1 = y;
    let y2 = y1.split_off(y1.len() / 2);

    let x_coord = x[0].coord;
    let x_lvl = x[0].level;

    // submit_work(pre_gen, mng);
    match isp.x_placement_order {
        PlacementOrder::First => {
            submit_work(x, mng);
            submit_work(y1, mng);
            submit_work(y2, mng)
        }
        PlacementOrder::Last => {
            submit_work(y1, mng);
            submit_work(y2, mng);
            submit_work(x, mng);
        }
        PlacementOrder::Middle => {
            submit_work(y1, mng);
            submit_work(x, mng);
            submit_work(y2, mng)
        }
    }

    // =========== Run assertions ===========

    let tree = &mng.tree;

    assert!(
        tree.get_node(&x_coord, x_lvl as u32)
            .unwrap()
            .get_max_dec_lod()
            >= max_y_lod,
        "X's max_dec_lod: {:?} max_y_lod: {:?}",
        tree.get_node(&x_coord, x_lvl as u32)
            .unwrap()
            .get_max_dec_lod(),
        max_y_lod
    );
}

fn fake_work(lvl: u8, lod: u8, coord: ICoord, kind: TerrainWorkKind) -> TerrainWorkDescriptor {
    debug_assert!(lvl - lod >= 3);
    TerrainWorkDescriptor {
        coord,
        level: lvl,
        target_lod: lod,
        kind,
    }
}

fn recurse_fake(
    work_list: &mut Vec<TerrainWorkDescriptor>,
    lvl: u8,
    lod: u8,
    coord: ICoord,
    kinds: &[TerrainWorkKind],
    buffer_depth_and_min_level: u8,
    rng: &mut StdRng,
) {
    if lvl - lod > buffer_depth_and_min_level {
        let hsize = 2i64.pow(lvl as u32) / 2;
        for off in OCTAL_OFFSETS.iter() {
            recurse_fake(
                work_list,
                lvl - 1,
                lod,
                off * hsize + coord,
                kinds,
                buffer_depth_and_min_level,
                rng,
            )
        }
    } else {
        let kind = kinds[rng.gen_range(0, kinds.len())];
        work_list.push(fake_work(lvl, lod, coord, kind))
    }
}

fn submit_work(work: Vec<TerrainWorkDescriptor>, mng: &mut TerrainManager) {
    for d in work {
        mng.generation_dispatcher
            .push_tentative_work(d, &cgmath::Point3::new(0., 0., 0.), 0);

        mng.generation_dispatcher
            .submit_tentative_work(&mut mng.tree);

        mng.check_for_and_process_finished_terrain(true).unwrap();
    }
}
