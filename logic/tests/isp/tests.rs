//! This file satisfies the Input Space Partitioning testing requirements.
//! To run these (and other) tests, thereby generating the reports, please run `cargo test`.

use super::*;

const ISP_SHOW_ERR: bool = true;

/// This test achieves pairwise coverage for the MockingISP test.
/// At runtime, it generates many different versions of the `MockingISP` struct. This struct holds values for all of characteristics.
/// By running the simulation with many different versions of the `MockingISP` struct, we achieve full pairwise coverage.
/// See the `mocking_isp.rs` file for the definition of the characteristics
/// See the `mocking_runner.rs` file for the implementation that runs the simulation/test given a `MockingISP` input
/// See the outputted file `mocking_isp_pairwise_report.md` file to see what characteristics were run, what pairs they covered, and what the results were.
#[test]
fn pairwise_mocking_isps_test() {
    MockingISP::test_pairwise_and_report(ISP_SHOW_ERR, "mocking_isp_pairwise_report.md")
}

/// This test achieves pairwise coverage for the GenerativeISP test.
/// At runtime, it generates many different versions of the `GenerativeISP` struct. This struct holds values for all of characteristics used by the GenerativeISP test.
/// By running the simulation with many different versions of the `GenerativeISP` struct, we achieve full pairwise coverage.
/// See the `mocking_isp.rs` file for the definition of the characteristics
/// See the `mocking_runner.rs` file for the implementation that runs the simulation/test given a `GenerativeISP` input
/// See the outputted file `generative_isp_pairwise_report.md` file to see what characteristics were run, what pairs they covered, and what the results were.
#[test]
fn pairwise_generative_isps_test() {
    GenerativeISP::test_pairwise_and_report(ISP_SHOW_ERR, "generative_isp_pairwise_report.md")
}
