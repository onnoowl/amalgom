use super::*;

use derivative::*;
use logic::terrain_manager::TerrainManager;

/// Characteristics and possible values for a smaller input space partitioning
/// These describe a "chunk" X, and its many neighbors Yi (where i is an index).
#[derive(Clone, Derivative)]
#[derivative(PartialEq, Eq, Hash, Debug)]
pub struct GenerativeISP {
    // Description of what pair was being targeted
    #[derivative(PartialEq = "ignore", Hash = "ignore", Debug = "ignore")]
    pub pairing_desc: String,

    /// X's placement ordering
    pub x_placement_order: PlacementOrder,

    /// Max LOD (of X, and Yi)
    /// Max LOD should not count the u8::MAX LOD of unloadeds
    pub max_lod: ZOG,

    /// (Max LOD of Yi) - (LOD of X)
    /// Max LOD of Yi should not count the u8::MAX LOD of unloadeds
    pub max_yi_minus_x: Sign,

    /// Number of chunks that are "generation" chunks
    pub yi_gen_or_ext: GenMixedExt,

    /// Number of chunks that are "extension" chunks
    pub x_gen_or_ext: GenExt,

    /// Seed used for randomness
    #[derivative(PartialEq = "ignore", Hash = "ignore")]
    pub seed: u64,
}

impl ISPTrait for GenerativeISP {
    const N: usize = 5;

    fn is_valid(&self) -> bool {
        match self {
            // If max_lod is zero, there cannot be a max_yi lod and x lod difference
            Self {
                max_lod: ZOG::Zero,
                max_yi_minus_x: Sign::Positive | Sign::Negative,
                ..
            } => false,

            // Else, true
            _ => true,
        }
    }

    fn from_nums(n: &[u8]) -> Option<Self> {
        use num::FromPrimitive;
        Some(Self {
            pairing_desc: String::new(),
            x_placement_order: FromPrimitive::from_u8(n[0])?,
            max_lod: FromPrimitive::from_u8(n[1])?,
            max_yi_minus_x: FromPrimitive::from_u8(n[2])?,
            yi_gen_or_ext: FromPrimitive::from_u8(n[3])?,
            x_gen_or_ext: FromPrimitive::from_u8(n[4])?,
            seed: rand::random(),
        })
    }

    fn char_to_str(&self, index: usize) -> String {
        match index {
            0 => format!("q1_x_placement_order: {:?}", self.x_placement_order),
            1 => format!("q2_max_lod: {:?}", self.max_lod),
            2 => format!("q3_max_yi_minus_x: {:?}", self.max_yi_minus_x),
            3 => format!("q10_yi_gen_or_ext: {:?}", self.yi_gen_or_ext),
            4 => format!("q11_x_gen_or_ext: {:?}", self.x_gen_or_ext),
            _ => unreachable!(),
        }
    }

    fn set_pairing_desc(&mut self, pairing_desc: String) {
        self.pairing_desc = pairing_desc
    }

    fn get_pairing_desc(&self) -> &String {
        &self.pairing_desc
    }

    fn run_test_impl(&self, mng: &mut TerrainManager) {
        generative_runner::run(self, mng);
    }
}
