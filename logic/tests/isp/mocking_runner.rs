use super::*;
use base_utils::{ICoord, OCTAL_OFFSETS};
use logic::{
    terrain_manager::{
        terrain_work::TerrainWork, TerrainManager, TerrainWorkDescriptor, TerrainWorkKind,
    },
    tree::*,
};

use cgmath::vec3;
use octree::traits::BranchNew;
use rand::{prelude::*, rngs::StdRng};

pub fn run(isp: &MockingISP, mng: &mut TerrainManager) {
    let mut rng = StdRng::seed_from_u64(isp.seed);

    let buffer_depth_and_min_level = mng.get_buffer_depth_and_min_level() as u8;

    let (x_lod, max_y_lod) = {
        let max_lod: u8 = match isp.max_lod {
            ZOG::Zero => 0,
            ZOG::One => 1,
            ZOG::GreaterThanOne => rng.gen_range(2, 6),
        };

        match isp.max_yi_minus_x {
            Sign::Negative => (max_lod, max_lod - rng.gen_range(1, max_lod + 1)),
            Sign::Zero => (max_lod, max_lod),
            Sign::Positive => (max_lod - rng.gen_range(1, max_lod + 1), max_lod),
        }
    };

    let mut x = Vec::<TerrainWork>::new();
    let mut y = Vec::<TerrainWork>::new();

    let main_level = x_lod.max(max_y_lod) + buffer_depth_and_min_level;
    let main_size = 2i64.pow(main_level as u32);

    let y_types = &{
        let mut v = Vec::new();
        if isp.num_yi_leafs != NSA::None {
            v.push(Type::Leaf);
        }
        if isp.num_yi_lodleafs != NSA::None {
            v.push(Type::LODLeaf);
        }
        if isp.num_yi_branches != NSA::None {
            v.push(Type::Branch);
        }
        v
    }[..];

    let y_kinds: &[TerrainWorkKind] = match isp.yi_gen_or_ext {
        GenMixedExt::GenerationOnly => &[TerrainWorkKind::Generation],
        GenMixedExt::Mixed => &[TerrainWorkKind::Generation, TerrainWorkKind::Extension],
        GenMixedExt::ExtensionOnly => &[TerrainWorkKind::Extension],
    };

    {
        let x_kind = match isp.x_gen_or_ext {
            GenExt::Generation => TerrainWorkKind::Generation,
            GenExt::Extension => TerrainWorkKind::Extension,
        };

        let mut x_fake_work = fake_work(isp.x_type, main_level, x_lod, vec3(0, 0, 0), x_kind);
        match isp.x_rel_with_big_y {
            Relationship::NA => {
                assert_eq!(main_level - x_lod, buffer_depth_and_min_level);
            }
            r @ (Relationship::IsCloserChild | Relationship::IsFurtherChild) => {
                let mut tmp = Vec::new();

                // IF there are no y_types (all unloaded), just use the x_types,
                // then we need to remember to not push any generated y (since they are unloaded)
                let x_types = &[isp.x_type];
                let types = if y_types.is_empty() {
                    x_types
                } else {
                    y_types.clone()
                };

                recurse_fake(
                    &mut tmp,
                    main_level,
                    x_lod,
                    vec3(0, 0, 0),
                    y_kinds,
                    types,
                    buffer_depth_and_min_level,
                    &mut rng,
                );
                // This place holder will have the right coordinates for where we want X.
                let index = match r {
                    Relationship::IsCloserChild => 0,
                    Relationship::IsFurtherChild => tmp.len() - 1,
                    _ => unreachable!(),
                };
                let x_place_holder = tmp.remove(index);
                x_fake_work.descriptor = x_place_holder.descriptor;
                x_fake_work.descriptor.kind = x_kind;

                // DON'T push Ys if all Ys are supposed to be unloadeds
                if !y_types.is_empty() {
                    // The rest will become Ys
                    y.append(&mut tmp)
                }
            }
        }
        x.push(x_fake_work)
    }

    // Prepare the the biggest Y (just behind the center X)
    recurse_fake(
        &mut y,
        main_level,
        max_y_lod,
        vec3(0, 0, -main_size),
        y_kinds,
        y_types,
        buffer_depth_and_min_level,
        &mut rng,
    );

    // Prepare the rest of Ys
    for coord in [
        vec3(main_size, 0, 0),
        vec3(-main_size, 0, 0),
        vec3(0, main_size, 0),
        vec3(0, -main_size, 0),
        vec3(0, 0, main_size),
    ]
    .iter()
    {
        recurse_fake(
            &mut y,
            main_level,
            rng.gen_range(0, max_y_lod + 1),
            *coord,
            y_kinds,
            y_types,
            buffer_depth_and_min_level,
            &mut rng,
        );
    }

    // =========== Tidy up and submit the fake work ===========

    y.shuffle(&mut rng);

    // Remove y's to simulate unloadeds
    match isp.num_yi_unloadeds {
        NSA::None => {}
        NSA::Some => {
            for _ in 0..rng.gen_range(1, y.len() - 1) {
                y.pop();
            }
        }
        NSA::All => y.clear(),
    }

    let x_coord = x[0].descriptor.coord;
    let x_lvl = x[0].descriptor.level;

    // let filter = |w: &mut TerrainWork| w.descriptor.kind == TerrainWorkKind::Extension;
    // let pre_gen = x
    //     .drain_filter(filter)
    //     .chain(y.drain_filter(filter))
    //     .map(|w| {
    //         fake_work(
    //             Type::LODLeaf,
    //             w.descriptor.level,
    //             rng.gen_range(w.descriptor.target_lod + 1, w.descriptor.level + 1),
    //             w.descriptor.coord,
    //             TerrainWorkKind::Generation,
    //         )
    //     })
    //     .collect();

    let mut y1 = y;
    let y2 = y1.split_off(y1.len() / 2);

    // submit_work(pre_gen, mng);
    match isp.x_placement_order {
        PlacementOrder::First => {
            submit_work(x, mng);
            submit_work(y1, mng);
            submit_work(y2, mng)
        }
        PlacementOrder::Last => {
            submit_work(y1, mng);
            submit_work(y2, mng);
            submit_work(x, mng);
        }
        PlacementOrder::Middle => {
            submit_work(y1, mng);
            submit_work(x, mng);
            submit_work(y2, mng)
        }
    }

    // =========== Run assertions ===========

    let tree = &mng.tree;

    match isp.num_yi_unloadeds {
        NSA::None => assert!(
            tree.get_node(&x_coord, x_lvl as u32)
                .unwrap()
                .get_max_dec_lod()
                >= max_y_lod
        ),
        NSA::Some => {
            // I don't think we can make any hard assertions here
            // Have to rely on the assert invariants built into the codebase
        }
        NSA::All => assert!(
            tree.get_node(&x_coord, x_lvl as u32)
                .unwrap()
                .get_max_dec_lod()
                == u8::MAX
        ),
    }
}

fn fake_work(ty: Type, lvl: u8, lod: u8, coord: ICoord, kind: TerrainWorkKind) -> TerrainWork {
    TerrainWork {
        descriptor: TerrainWorkDescriptor {
            coord,
            level: lvl,
            target_lod: lod,
            kind,
        },
        node: match ty {
            Type::Leaf => LogicNode::Leaf(LogicLeaf::default()),
            Type::LODLeaf => LogicNode::LODLeaf(LogicLeaf::default(), lod),
            Type::Branch => {
                let mut children: [LogicNode; 8] = Default::default();
                for c in children.iter_mut() {
                    if lod == 0 {
                        *c = LogicNode::Leaf(LogicLeaf::default())
                    } else {
                        *c = LogicNode::LODLeaf(LogicLeaf::default(), lod)
                    }
                }
                LogicNode::Branch(
                    LogicBranchData::new_branch(lvl, &children),
                    Box::new(children),
                )
            }
        },
    }
}

fn recurse_fake(
    work_list: &mut Vec<TerrainWork>,
    lvl: u8,
    lod: u8,
    coord: ICoord,
    kinds: &[TerrainWorkKind],
    types: &[Type],
    buffer_depth_and_min_level: u8,
    rng: &mut StdRng,
) {
    if !types.is_empty() && !kinds.is_empty() {
        if lvl - lod > buffer_depth_and_min_level {
            let hsize = 2i64.pow(lvl as u32) / 2;
            for off in OCTAL_OFFSETS.iter() {
                recurse_fake(
                    work_list,
                    lvl - 1,
                    lod,
                    off * hsize + coord,
                    kinds,
                    types,
                    buffer_depth_and_min_level,
                    rng,
                )
            }
        } else {
            let kind = kinds[rng.gen_range(0, kinds.len())];
            let ty = types[rng.gen_range(0, types.len())];
            work_list.push(fake_work(ty, lvl, lod, coord, kind))
        }
    }
}

fn submit_work(work: Vec<TerrainWork>, mng: &mut TerrainManager) {
    let buffer_depth_and_min_level = mng.get_buffer_depth_and_min_level();
    for w in work {
        TerrainManager::process_finished_terrain(
            w,
            &mut mng.tree,
            &mng.send_buffer,
            buffer_depth_and_min_level,
        )
        .unwrap()
    }
}
