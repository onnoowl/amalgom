//! Tools for gathering the vertices and indices required to draw something in the game

use crate::{
    generation::*,
    tree::{Block, LogicNode},
};
use base_utils::{Face, ICoord, Loc, FACES, OCTAL_OFFSETS, QUAD_OFFSETS, UV_FACE_VALS};
use game_utils::terrain_mesh::Vertex;
use octree::{Occlusion, QuadTree};

fn get_texture(bid: u8, f: Face) -> (u8, u8) {
    let mut rc = 13 + 11 * 16; //a random unused texture. Should show as black or purple.
    match bid {
        1 => rc = 1,
        2 => {
            rc = match f {
                Face::Bottom => 2,
                _ => 12 + 12 * 16,
                // 3 for old sides of grass
            }
        }
        3 => rc = 2,
        4 => rc = 16,
        5 | 53 => rc = 4,
        17 => rc = 4 + 16,
        31 => rc = 7 + 2 * 16,
        102 => rc = 1 + 3 * 16,
        175 => rc = 13,
        _ => log::warn!("Unknown texture id {}", bid),
    }
    (0, rc)
}

pub trait MakeMesh {
    #[allow(clippy::too_many_arguments)]
    fn add_data_from_face(
        level: u8,
        lod: u8,
        fd: &QuadTree,
        face: &Face,
        id: u8,
        pos: &ICoord,
        vertices: &mut Vec<Vertex>,
        indices: &mut Vec<u32>,
    ) {
        let scale = 2_i64.pow(level as u32);

        let occlusion = match fd {
            QuadTree::Leaf(occlusion) => occlusion,
            QuadTree::Branch(occlusion, children) => {
                if level == 0 {
                    log::error!("Got to level 0 and the quad tree still had a branch");
                    return;
                }

                if level <= lod {
                    if let Some(o) = occlusion {
                        o
                    } else {
                        log::error!("Tried to access quad tree mode data, but none existed");
                        return;
                    }
                } else {
                    // Easy except I need to change pos to the four locations based on face
                    let gap = scale / 2;

                    let quad_to_oct = match face {
                        Face::Front => {
                            |offset: &(i64, i64)| -> ICoord { ICoord::new(offset.0, offset.1, 0) }
                        }
                        Face::Back => {
                            |offset: &(i64, i64)| -> ICoord { ICoord::new(offset.0, offset.1, 1) }
                        }
                        Face::Left => {
                            |offset: &(i64, i64)| -> ICoord { ICoord::new(1, offset.0, offset.1) }
                        }
                        Face::Right => {
                            |offset: &(i64, i64)| -> ICoord { ICoord::new(0, offset.0, offset.1) }
                        }
                        Face::Top => {
                            |offset: &(i64, i64)| -> ICoord { ICoord::new(offset.0, 1, offset.1) }
                        }
                        Face::Bottom => {
                            |offset: &(i64, i64)| -> ICoord { ICoord::new(offset.0, 0, offset.1) }
                        }
                    };

                    for (child, offset) in children.iter().zip(QUAD_OFFSETS.iter()) {
                        Self::add_data_from_face(
                            level - 1,
                            lod,
                            child,
                            face,
                            id,
                            &(pos + quad_to_oct(&(offset.0 as i64, offset.1 as i64)) * gap),
                            vertices,
                            indices,
                        );
                    }

                    return;
                }
            }
        };

        match occlusion {
            Occlusion::Visible(_) => {
                let f = usize::from(*face);
                let (tex_palette, tex_id) = get_texture(id, *face);
                let face_vals = UV_FACE_VALS[f];
                let start_index: u32 = vertices.len() as u32;
                face_vals.iter().for_each(|Loc(p, c)| {
                    vertices.push(Vertex::new(
                        pos + (p * scale),
                        *face,
                        tex_palette,
                        tex_id,
                        *c,
                        level,
                    ));
                });
                indices.push(start_index);
                indices.push(start_index + 1);
                indices.push(start_index + 2);
                indices.push(start_index + 2);
                indices.push(start_index + 3);
                indices.push(start_index);
            }
            Occlusion::Occluded => {} // Default occluded
        }
    }

    /// get_texture must return a tuple with (texture_palette, texture_id). texture_palette must only use the least-significant 6 out of 8 bits.
    fn add_data_from_block(
        level: u8,
        lod: u8,
        b: &Block,
        pos: &ICoord,
        vertices: &mut [Vec<Vertex>; 6],
        indices: &mut [Vec<u32>; 6],
    ) {
        if b.id != 0 {
            for face in FACES.iter() {
                Self::add_data_from_face(
                    level,
                    lod,
                    b.face_data.get_side(face).as_ref(),
                    face,
                    b.id,
                    pos,
                    &mut vertices[*face as usize],
                    &mut indices[*face as usize],
                );
            }
        }
    }

    fn make_mesh(
        &mut self,
        level: u8,
        lod: u8,
        pos: &ICoord,
        vertices: &mut [Vec<Vertex>; 6],
        indices: &mut [Vec<u32>; 6],
    );
}

impl MakeMesh for Block {
    fn make_mesh(
        &mut self,
        level: u8,
        lod: u8,
        pos: &ICoord,
        vertices: &mut [Vec<Vertex>; 6],
        indices: &mut [Vec<u32>; 6],
    ) {
        Self::add_data_from_block(level, lod, self, pos, vertices, indices);
    }
}

impl MakeMesh for LogicNode {
    fn make_mesh(
        &mut self,
        level: u8,
        lod: u8,
        pos: &ICoord,
        vertices: &mut [Vec<Vertex>; 6],
        indices: &mut [Vec<u32>; 6],
    ) {
        let size = 2_i64.pow((level) as u32);
        match self {
            LogicNode::Branch(b, children) => {
                if level > lod {
                    let offsets = OCTAL_OFFSETS.iter().map(|offset| offset * size / 2);
                    children
                        .iter_mut()
                        .zip(offsets)
                        .for_each(|(child, offset)| {
                            child.make_mesh(level - 1, lod, &(pos + offset), vertices, indices);
                        });
                } else {
                    match &mut b.block {
                        Some(block) => {
                            Self::add_data_from_block(level, lod, block, pos, vertices, indices);
                        }
                        None => {
                            log::warn!(
                                "Mode averages being generated at pos: {:?}, level: {}, lod: {}",
                                pos,
                                level,
                                lod
                            );
                            b.block = Some(Block {
                                id: LogicNode::find_id_mode(children, level),
                                face_data: Default::default(),
                            });
                            Self::add_data_from_block(
                                level as u8,
                                lod,
                                b.block.as_ref().unwrap(),
                                pos,
                                vertices,
                                indices,
                            );
                        }
                    }
                }
            }
            LogicNode::Leaf(b) | LogicNode::LODLeaf(b, _) => {
                Self::add_data_from_block(level as u8, lod, &b.block, pos, vertices, indices);
            }
            LogicNode::Unloaded(_) => {
                log::warn!("Meshing unloadeds");
            } // Nobody cares about unloadeds. Let its family tree die off
        }
    }
}
