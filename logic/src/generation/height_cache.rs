use std::sync::Arc;

use lru::LruCache;
use noise::NoiseFn;

pub type HeightNoise = Arc<(dyn NoiseFn<[f64; 2]> + Send + Sync)>;

pub struct HeightCache {
    lru: LruCache<Region, Arc<[f64]>>,
    height_noise: HeightNoise,
}

#[derive(Hash, Eq, PartialEq, Clone, Debug)]
pub struct Region {
    pub x: i64,
    pub z: i64,
    pub level: u8,
    pub target_lod: u8,
}

#[derive(Clone)]
pub struct HeightHandle {
    region: Region,
    // TODO: turn into height pryamid?
    heights: Arc<[f64]>,
    height_noise: HeightNoise,

    buf_size: usize,
    chunk_size: i64,
    unit_size: i64,
    half_unit_size: f64,
}

impl HeightCache {
    pub fn new_with_cap(height_noise: HeightNoise, cap: usize) -> Self {
        Self {
            lru: LruCache::new(cap),
            height_noise,
        }
    }

    pub fn new(height_noise: HeightNoise) -> Self {
        // Only need like 700-800 ish? Overestimating here because the memory isn't super expensive.
        // This is about 16MB
        Self::new_with_cap(height_noise, 2000)
    }

    pub fn get(&mut self, x: i64, z: i64, level: u8, target_lod: u8) -> HeightHandle {
        let reg = Region {
            x,
            z,
            level,
            target_lod,
        };

        let buf_size = 2usize.pow(level as u32 - target_lod as u32);
        let chunk_size = 2i64.pow(level as u32);
        let unit_size = 2i64.pow(target_lod as u32);
        let half_unit_size = unit_size as f64 / 2.;

        let heights = if let Some(heights) = self.lru.get(&reg) {
            heights.clone()
        } else {
            let mut heights = Vec::with_capacity(buf_size * buf_size);
            let buf_size = buf_size as i64;
            for x_off in 0i64..buf_size {
                // Adding half here isn't exactly right, but it's close enough
                let x = (x_off * unit_size + x) as f64 + half_unit_size;
                for z_off in 0i64..buf_size {
                    let z = (z_off * unit_size + z) as f64 + half_unit_size;
                    heights.push(self.height_noise.get([x, z]))
                }
            }
            debug_assert_eq!(heights.len(), heights.capacity());
            let heights: Arc<[f64]> = heights.into(); //convert vec to arc slice
            self.lru.put(reg.clone(), heights.clone());
            // log::debug!("LRU size is now {:?}", self.lru.len());
            heights
        };

        HeightHandle {
            region: reg,
            heights: heights.clone(),
            height_noise: self.height_noise.clone(),

            buf_size,
            chunk_size,
            unit_size,
            half_unit_size,
        }
    }
}

impl HeightHandle {
    /// Attempts to fetch a height from the cached height map in this handle.
    /// If out of bounds, will instead sample from the noise function
    pub fn get(&self, x: f64, z: f64) -> f64 {
        let x = x.floor() as i64; // equivalent to (x - 0.5).round()
        let z = z.floor() as i64;

        let x_dif = x - self.region.x;
        let z_dif = z - self.region.z;
        if x_dif >= 0 && x_dif < self.chunk_size && z_dif >= 0 && z_dif < self.chunk_size {
            let x_off = (x_dif / self.unit_size) as usize;
            let z_off = (z_dif / self.unit_size) as usize;
            self.heights[x_off * (self.buf_size) + z_off]
        } else {
            // log::warn!("Cache miss!");

            // The + half_unit_size here is sorta inaccurate, but adding it here makes it consistently inaccurate
            self.height_noise.get([
                x as f64 + self.half_unit_size,
                z as f64 + self.half_unit_size,
            ])
        }
    }
}
