use super::{
    distorter::Distorter,
    height_cache::{HeightCache, HeightHandle},
    offset_perlin::OffsetPerlin,
};

use noise::{MultiFractal, NoiseFn, SuperSimplex};
use std::sync::Arc;

pub struct NoiseManager {
    pub density_noise: Arc<(dyn NoiseFn<[f64; 3]> + Send + Sync)>,
    pub height: HeightCache,
    pub distorter: Option<Arc<Distorter>>,
}
pub struct NoiseManagerHandle {
    pub density_noise: Arc<(dyn NoiseFn<[f64; 3]> + Send + Sync)>,
    pub height: HeightHandle,
    pub distorter: Option<Arc<Distorter>>,
}

impl NoiseManager {
    pub fn new_const(density: f64, height: f64, distorter: bool) -> Self {
        let distorter: Option<Arc<Distorter>> = if distorter {
            Some(Arc::new(Distorter::default()))
        } else {
            None
        };

        NoiseManager {
            density_noise: Arc::new(noise::Constant::new(density)),
            height: HeightCache::new(Arc::new(noise::Constant::new(height))),
            distorter,
        }
    }

    pub fn get(&mut self, x: i64, z: i64, level: u8, target_lod: u8) -> NoiseManagerHandle {
        NoiseManagerHandle {
            density_noise: self.density_noise.clone(),
            height: self.height.get(x, z, level, target_lod),
            distorter: self.distorter.clone(),
        }
    }
}

impl Default for NoiseManager {
    fn default() -> Self {
        NoiseManager {
            density_noise: Arc::new(SuperSimplex::new()),

            height: HeightCache::new(Arc::new(
                super::ridged_multi::RidgedMulti::<OffsetPerlin>::new()
                    .set_frequency(1.0 / 2100.0)
                    .set_octaves(10)
                    .set_amplitude(300.0)
                    .set_persistence(1.0)
                    .set_attenuation(2.0),
            )),
            distorter: None,
            // Normal ish
            // distorter: Some(Arc::new(
            //     Distorter::default()
            //         .set_frequency([0.005, 0.003, 0.005])
            //         .set_power([50.0, 1.0, 50.0])
            //         .set_roughness(1),
            // )),
            //lots of height, works well with mountains
            // distorter: Some(Arc::new(
            //     Distorter::default()
            //         .set_frequency([0.005, 0.005, 0.005])
            //         .set_power([50.0, 200.0, 50.0])
            //         .set_roughness(1),
            // )),
            // slightly exotic
            // distorter: Some(Arc::new(
            //     Distorter::default()
            //         .set_frequency([0.0008, 0.0005, 0.0008])
            //         .set_power([200.0, 200.0, 200.0])
            //         .set_roughness(1),
            // )),
            // More extreme
            // distorter: Some(Arc::new(
            //     Distorter::default()
            //         .set_frequency([0.008, 0.002, 0.008])
            //         .set_power([70.0, 20.0, 70.0])
            //         .set_roughness(1),
            // )),
            // Extreme
            // distorter: Some(Arc::new(
            //     Distorter::default()
            //         .set_frequency([0.005, 0.01, 0.005])
            //         .set_power([100.0, 100.0, 100.0])
            //         .set_roughness(1),
            // )))

            // Absurd
            // distorter: Some(Arc::new(
            //     Distorter::default()
            //         .set_frequency([0.005, 0.003, 0.005])
            //         .set_power([100.0, 500.0, 100.0])
            //         .set_roughness(1),
            // )),
        }
    }
}
