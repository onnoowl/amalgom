use noise::{Fbm, MultiFractal, NoiseFn, Seedable};

pub struct Distorter {
    /// Frequency value for the Turbulence function.
    pub frequency: [f64; 3],

    /// Controls the strength of the turbulence by affecting how much each
    /// point is moved.
    pub power: [f64; 3],

    /// Affects the roughness of the turbulence. Higher values are rougher.
    pub roughness: usize,

    seed: u32,
    x_distort_function: Fbm,
    y_distort_function: Fbm,
    z_distort_function: Fbm,
}

/// This impl is almost entirely stolen from noise::Turbulence

impl Default for Distorter {
    fn default() -> Self {
        Self {
            seed: Self::DEFAULT_SEED,
            frequency: Self::DEFAULT_FREQUENCY,
            power: Self::DEFAULT_POWER,
            roughness: Self::DEFAULT_ROUGHNESS,
            x_distort_function: Fbm::new()
                .set_seed(Self::DEFAULT_SEED)
                .set_octaves(Self::DEFAULT_ROUGHNESS)
                .set_frequency(Self::DEFAULT_FREQUENCY[0]),
            y_distort_function: Fbm::new()
                .set_seed(Self::DEFAULT_SEED + 1)
                .set_octaves(Self::DEFAULT_ROUGHNESS)
                .set_frequency(Self::DEFAULT_FREQUENCY[1]),
            z_distort_function: Fbm::new()
                .set_seed(Self::DEFAULT_SEED + 2)
                .set_octaves(Self::DEFAULT_ROUGHNESS)
                .set_frequency(Self::DEFAULT_FREQUENCY[2]),
        }
    }
}

impl Distorter {
    pub const DEFAULT_SEED: u32 = 0;
    pub const DEFAULT_FREQUENCY: [f64; 3] = [1.0, 1.0, 1.0];
    pub const DEFAULT_POWER: [f64; 3] = [1.0, 1.0, 1.0];
    pub const DEFAULT_ROUGHNESS: usize = 3;

    pub fn set_frequency(self, frequency: [f64; 3]) -> Self {
        Self {
            frequency,
            x_distort_function: self.x_distort_function.set_frequency(frequency[0]),
            y_distort_function: self.y_distort_function.set_frequency(frequency[1]),
            z_distort_function: self.z_distort_function.set_frequency(frequency[2]),
            ..self
        }
    }

    pub fn set_power(self, power: [f64; 3]) -> Self {
        Self { power, ..self }
    }

    pub fn set_roughness(self, roughness: usize) -> Self {
        Self {
            roughness,
            x_distort_function: self.x_distort_function.set_octaves(roughness),
            y_distort_function: self.y_distort_function.set_octaves(roughness),
            z_distort_function: self.z_distort_function.set_octaves(roughness),
            ..self
        }
    }

    pub fn distort(&self, x: f64, y: f64, z: f64) -> [f64; 3] {
        // Adopted from Turbulence::get

        // First, create offsets based on the input values to keep the sampled
        // points from being near a integer boundary. This is a result of
        // using perlin noise, which returns zero at integer boundaries.
        let x0 = x + 12414.0 / 65536.0;
        let y0 = y + 65124.0 / 65536.0;
        let z0 = z + 31337.0 / 65536.0;

        let x1 = x + 26519.0 / 65536.0;
        let y1 = y + 18128.0 / 65536.0;
        let z1 = z + 60943.0 / 65536.0;

        let x2 = x + 53820.0 / 65536.0;
        let y2 = y + 11213.0 / 65536.0;
        let z2 = z + 44845.0 / 65536.0;

        let new_x = x + (self.x_distort_function.get([x0, y0, z0]) * self.power[0]);
        let new_y = y + (self.y_distort_function.get([x1, y1, z1]) * self.power[1]);
        let new_z = z + (self.z_distort_function.get([x2, y2, z2]) * self.power[2]);

        [new_x, new_y, new_z]
    }
}

impl Seedable for Distorter {
    fn set_seed(self, seed: u32) -> Self {
        Self {
            seed,
            x_distort_function: self.x_distort_function.set_seed(seed),
            y_distort_function: self.y_distort_function.set_seed(seed + 1),
            z_distort_function: self.z_distort_function.set_seed(seed + 2),
            ..self
        }
    }

    fn seed(&self) -> u32 {
        self.seed
    }
}
