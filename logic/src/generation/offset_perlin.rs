use noise::{NoiseFn, Point2, Point3, Seedable};

use rand::{prelude::*, rngs::StdRng};

#[derive(Default)]
pub struct OffsetPerlin {
    x_off: f64,
    y_off: f64,
    z_off: f64,
    seed: u32,
    source: noise::Perlin,
}

impl NoiseFn<Point2<f64>> for OffsetPerlin {
    fn get(&self, point: Point2<f64>) -> f64 {
        self.source.get(add2(point, self.x_off, self.y_off))
    }
}

impl NoiseFn<Point3<f64>> for OffsetPerlin {
    fn get(&self, point: Point3<f64>) -> f64 {
        self.source
            .get(add3(point, self.x_off, self.y_off, self.z_off))
    }
}

impl Seedable for OffsetPerlin {
    fn set_seed(mut self, seed: u32) -> Self {
        self.seed = seed;
        let mut rng = StdRng::seed_from_u64(seed as u64);
        self.x_off = rng.gen_range(-10000., 10000.);
        self.y_off = rng.gen_range(-10000., 10000.);
        self.z_off = rng.gen_range(-10000., 10000.);
        self.source.set_seed(seed);
        self
    }

    fn seed(&self) -> u32 {
        self.seed
    }
}

#[inline]
pub fn add2<T: Copy + std::ops::Add<T, Output = T>>(p: [T; 2], x: T, y: T) -> [T; 2] {
    [p[0] + x, p[1] + y]
}

#[inline]
pub fn add3<T: Copy + std::ops::Add<T, Output = T>>(p: [T; 3], x: T, y: T, z: T) -> [T; 3] {
    [p[0] + x, p[1] + y, p[2] + z]
}
