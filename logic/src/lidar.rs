//! Lidar helper functions
//!
//! Austin Shafer - 2020

use crate::tree::LogicTree;
use base_utils::{Face, Loc};
use game_utils::intersection;

use cgmath::{EuclideanSpace, InnerSpace, Point3, Quaternion, Rotation, Vector3};
use collision::{Continuous, Frustum};
use std::collections::HashMap;

pub struct FrustIterator {
    /// The top left corner of the near plane in the frustum
    fi_top_left: Point3<f32>,
    /// vectors for translating across the face of the frustum
    fi_right: Vector3<f32>,
    fi_down: Vector3<f32>,
    /// The current indices into the resolution of our lidar
    fi_i: u32, // width
    fi_j: u32, // height
    fi_width: u32,
    fi_height: u32,
}

/// Non-consuming iterator over a frustum.
///
/// `focus` specifies a location in the frustum to focus on instead
/// of doing the full thing.
pub fn get_frustum_iter(frust: &Frustum<f32>, focus: Option<(f32, f32)>) -> FrustIterator {
    let top_left = frust.near.intersection(&(frust.left, frust.top)).unwrap();
    let bottom_left = frust
        .near
        .intersection(&(frust.left, frust.bottom))
        .unwrap();
    let top_right = frust.near.intersection(&(frust.right, frust.top)).unwrap();

    let mut ret = FrustIterator {
        fi_right: (top_right - top_left),
        fi_down: (bottom_left - top_left),
        fi_top_left: top_left,
        fi_width: 32,
        fi_height: 24,
        fi_i: 0,
        fi_j: 0,
    };

    // if the user gave us a point to focus on we want to reduce the
    // frustum iteration size to a "mini rectangle" around the focus
    if let Some((x, y)) = focus {
        // "size of the mini rectangle"
        // the percentage of the right and down vectors to use to create
        // a mini rectangle
        let perc = 0.2;
        // adjust the top left to the point we want to focus on
        ret.fi_top_left += ret.fi_right * x + ret.fi_down * y;
        // adjust the right/down size to be our mini rect size
        ret.fi_right *= perc;
        ret.fi_down *= perc;
        ret.fi_width = (ret.fi_width as f32 * perc) as u32;
        ret.fi_height = (ret.fi_height as f32 * perc) as u32;
        // now bring the top left up to the top left of the mini rect
        // focus should now be in the center of the mini rect
        ret.fi_top_left -= ret.fi_right / 2.0 + ret.fi_down / 2.0;
    }

    return ret;
}

impl Iterator for FrustIterator {
    // Our item type is a WindowId
    type Item = Point3<f32>;

    fn next(&mut self) -> Option<Point3<f32>> {
        // check if we have gone through the entire frustum
        if self.fi_i + self.fi_j * self.fi_width >= self.fi_width * self.fi_height {
            return None;
        }

        // otherwise find the translated position into the near plane
        let i = self.fi_i as f32;
        let j = self.fi_j as f32;
        let ret = self.fi_top_left
            + (self.fi_right * (i / (self.fi_width as f32)))
            + (self.fi_down * (j / (self.fi_height as f32)));

        if self.fi_i < self.fi_width {
            self.fi_i += 1;
        } else {
            self.fi_j += 1;
            self.fi_i = 0;
        }
        return Some(ret);
    }
}

/// Fills the block_highlight.locs hashmap with locations of the voxels selected.
/// Returns if locs was updated.
pub fn lidar(
    locs: &mut HashMap<Loc, bool>,
    max_dist: f32,
    target_level: u32,
    tree: &LogicTree,
    origin: &Point3<f32>,
    frustum: &Frustum<f32>,
) {
    locs.clear();

    // for a bunch of rays in the camera frustum
    for p in get_frustum_iter(frustum, None) {
        // log::debug!(
        //     "point {:?} vs target {:?} ", p, camera.target
        // );
        if let Some(loc) = try_with_vector(&(p - origin), origin, max_dist, target_level, tree) {
            locs.insert(loc, true);
        }
    }
}

fn try_with_vector(
    vector: &Vector3<f32>,
    origin: &Point3<f32>,
    max_dist: f32,
    target_level: u32,
    tree: &LogicTree,
) -> Option<Loc> {
    let intersection =
        intersection::ray_intersection(origin, vector, tree, target_level, Some(max_dist));

    match &intersection {
        Some(intersection) => {
            // check if the top face of the voxel is available
            // i.e. there are no voxels above it
            if let Some(leaf) = &intersection.node.get_leaf() {
                if !leaf.block.face_data.is_side_visible(&Face::Top) {
                    return None;
                }
            }

            Some(Loc(intersection.pos, intersection.level))
        }
        None => None,
    }
}

fn test_loc_is_best_match(
    loc: &Loc,
    ideal_point: &Point3<f32>,
    best_match: &mut Option<(f32, Loc)>,
) {
    let pos = loc.0;
    let level = loc.1;
    let pos_f32 = pos.cast::<f32>().unwrap();
    // let voxel = Point3::from_vec(pos_f32);
    let bb = intersection::get_bb(&pos_f32, level as u32);
    let voxel_top = intersection::get_bb_mid(&bb);
    // TODO: get location on visible part of the top??
    let dist = (ideal_point - voxel_top).magnitude();

    match best_match {
        Some(bm) => {
            if dist < bm.0 {
                *best_match = Some((
                    dist,
                    Loc(Point3::to_vec(voxel_top.cast::<i64>().unwrap()), level),
                ));
            }
        }
        None => {
            *best_match = Some((
                dist,
                Loc(Point3::to_vec(voxel_top.cast::<i64>().unwrap()), level),
            ))
        }
    };
}

/// Use lidar to find n optimal points for foot placement.
/// For each entry in `dirs` this will find the best voxel available from lidar. `dirs`
/// is a list of rotations which are deviations from straight forward and "comfortable" distances
/// which are the ideal placement distance.
/// This calls `lidar`.
pub fn lidar_n_points(
    max_dist: f32,
    dirs: &[(Quaternion<f32>, f32)],
    target_level: u32,
    tree: &LogicTree,
    origin: &Point3<f32>,
    target: &Point3<f32>,
    frustum: &Frustum<f32>,
) -> Vec<Loc> {
    debug_assert!(dirs.is_empty() || dirs[0].0.s.is_finite());

    let mut locs: Option<HashMap<Loc, bool>> = None;

    let mut final_candidates = HashMap::new();
    for dir in dirs {
        // Find a point the comfortable distance along this direction, and
        // then find the voxel loc closest to it
        // TODO: adjust the direction to match the way the camera is facing
        let ideal_point = origin + (dir.0.rotate_vector(target - origin).normalize() * dir.1);
        let mut best_match: Option<(f32, Loc)> = None;

        // First, try a simple ray cast
        {
            if let Some(loc) = try_with_vector(
                &(ideal_point - origin),
                origin,
                max_dist,
                target_level,
                tree,
            ) {
                test_loc_is_best_match(&loc, &ideal_point, &mut best_match);
            }
        }

        // If that doesn't work, use lidar
        if best_match.is_none() {
            // Get or MAKE the lidar map
            let locs = locs.get_or_insert_with(|| {
                let mut l = HashMap::new();
                lidar(&mut l, max_dist, target_level, tree, origin, frustum);
                l
            });

            for loc in locs.keys() {
                test_loc_is_best_match(loc, &ideal_point, &mut best_match)
            }
        }

        // TODO: prevent excess copies?
        if let Some(final_match) = best_match {
            log::debug!("Found final candidate voxel: {:#?}\n", final_match.1,);
            final_candidates.insert(final_match.1, true);
        }
    }

    let mut ret = Vec::new();
    ret.extend(final_candidates.keys());
    return ret;
}
