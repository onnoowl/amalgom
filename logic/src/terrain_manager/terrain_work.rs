use crate::{generation::NoiseManagerHandle, tree::LogicNode};
use base_utils::ICoord;
use derivative::*;

#[derive(Debug)]
pub struct TerrainWork {
    pub descriptor: TerrainWorkDescriptor,
    pub node: LogicNode,
}

#[derive(Derivative)]
#[derivative(Debug)]
pub struct TerrainWorkRequest {
    pub descriptor: TerrainWorkDescriptor,
    pub node: LogicNode,

    #[derivative(Debug = "ignore")]
    pub noise: NoiseManagerHandle,
}

#[derive(Clone, Debug)]
pub struct TerrainWorkDescriptor {
    pub coord: ICoord,
    pub level: u8,
    pub target_lod: u8,
    pub kind: TerrainWorkKind,
}

#[derive(Clone, Debug, PartialEq, Eq, Copy)]
pub enum TerrainWorkKind {
    Generation,
    Extension,
}

#[allow(dead_code)]
impl TerrainWorkKind {
    pub fn is_extension(&self) -> bool {
        match self {
            TerrainWorkKind::Generation => false,
            TerrainWorkKind::Extension => true,
        }
    }

    pub fn is_generation(&self) -> bool {
        match self {
            TerrainWorkKind::Generation => true,
            TerrainWorkKind::Extension => false,
        }
    }
}
