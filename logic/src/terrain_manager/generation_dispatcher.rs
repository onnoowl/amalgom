use super::terrain_work::{TerrainWork, TerrainWorkDescriptor, TerrainWorkRequest};
use crate::{generation::NoiseManager, generation::*, tree::*};
use base_utils::{ICoord, Loc, ROOT_LEVEL};
use game_utils::thread_pool::PriorityQueues;

use cgmath::{MetricSpace, Point3};
use std::sync::{
    mpsc::{self, Receiver},
    Arc,
};

const MAX_WORK_BUCKETS: usize = 4 * ROOT_LEVEL as usize;

pub struct GenerationDispatcher {
    priority_queues: Arc<PriorityQueues<TerrainWorkRequest>>,
    tentative_work_buckets: Vec<Vec<TerrainWorkDescriptor>>,
    pub current_outstanding_work: u32,
    pub max_outstanding_work: u32,

    noise: NoiseManager,

    terrain_gen_return_recv: Receiver<TerrainWork>,
}

impl GenerationDispatcher {
    pub fn new(noise: NoiseManager) -> Self {
        let (terrain_gen_return_send, terrain_gen_return_recv) = mpsc::channel();

        // Right now priority is done before work is submitted
        let priority_queues = PriorityQueues::with_size(1); //MAX_PRIORITY + 1);

        priority_queues.spawn_workers(
            move |terrain_work_req: TerrainWorkRequest| {
                let TerrainWorkRequest {
                    mut node,
                    noise,
                    descriptor,
                } = terrain_work_req;
                node.extend_with_noise(
                    descriptor.target_lod,
                    descriptor.level as u8,
                    &descriptor.coord,
                    &noise,
                );
                node.internal_culling();
                terrain_gen_return_send
                    .send(TerrainWork { node, descriptor })
                    .unwrap();
            },
            None,
        );

        let mut work_buckets = Vec::with_capacity(MAX_WORK_BUCKETS);
        for _ in 0..MAX_WORK_BUCKETS {
            work_buckets.push(Vec::new())
        }

        let max_outstanding_work = if base_utils::CONFIG.time_travel.is_none() {
            num_cpus::get() as u32 * 2
        } else {
            1
        };

        GenerationDispatcher {
            terrain_gen_return_recv,
            priority_queues,
            tentative_work_buckets: work_buckets,
            max_outstanding_work,
            current_outstanding_work: 0,
            noise,
        }
    }

    pub fn push_tentative_work(
        &mut self,
        terrain_work_descriptor: TerrainWorkDescriptor,
        player_pos: &Point3<f32>,
        buffer_depth_min_level: u32,
    ) {
        debug_assert!(
            terrain_work_descriptor.level - terrain_work_descriptor.target_lod
                >= buffer_depth_min_level as u8
        );
        // tg.priority_queues.queues[(target_lod as usize).min(MAX_PRIORITY)].push(
        self.tentative_work_buckets[Self::get_work_bucket_index(
            player_pos,
            &terrain_work_descriptor.coord,
            terrain_work_descriptor.level,
        )]
        .push(terrain_work_descriptor);
    }

    fn get_work_bucket_index(player_pos: &Point3<f32>, pos: &ICoord, level: u8) -> usize {
        let half_size = 2i64.pow((level - 1) as u32);
        let dist = player_pos.distance(Point3::new(
            (half_size + pos.x) as f32,
            (half_size + pos.y) as f32,
            (half_size + pos.z) as f32,
        ));

        let log_dist = dist.log2();

        let proportion = log_dist / (ROOT_LEVEL as f32);

        ((proportion * (MAX_WORK_BUCKETS as f32 - 1.0)) as usize).min(MAX_WORK_BUCKETS - 1)
    }

    pub fn submit_tentative_work(&mut self, tree: &mut LogicTree) {
        // log::debug!(
        //     "current_outstanding_work: {}, tentative_work_buckets lens: {:?}",
        //     self.current_outstanding_work,
        //     self.tentative_work_buckets
        //         .iter()
        //         .map(|wl| wl.len())
        //         .collect::<Vec<_>>()
        // );

        // Submit generated work
        if self.current_outstanding_work < self.max_outstanding_work {
            'work_list: for work_list in self.tentative_work_buckets.iter_mut() {
                while let Some(work_item) = work_list.pop() {
                    let node = if let Ok(node) =
                        tree.get_node_mut(&work_item.coord, work_item.level as u32)
                    {
                        let n = node.clone();
                        node.set_under_construction(true);
                        n
                    } else {
                        let node: LogicNode = Default::default();
                        let mut n = node.clone();
                        n.set_under_construction(true);
                        tree.replace_at_location(n, Loc(work_item.coord, work_item.level))
                            .expect("Terrain work given at a location that doesn't exist.");
                        node
                    };

                    self.priority_queues.queues[0].push(TerrainWorkRequest {
                        node,
                        descriptor: work_item.clone(),
                        noise: self.noise.get(
                            work_item.coord.x,
                            work_item.coord.z,
                            work_item.level,
                            work_item.target_lod,
                        ),
                    });
                    self.current_outstanding_work += 1;

                    if self.current_outstanding_work >= self.max_outstanding_work {
                        break 'work_list;
                    }
                }
            }
        }

        // log::debug!(
        //     "current_outstanding_work (after submission): {}",
        //     self.current_outstanding_work
        // );
    }

    pub fn clear_tentative_work(&mut self) {
        for work_list in self.tentative_work_buckets.iter_mut() {
            work_list.clear();
        }
    }

    /// NOTE! Ensure that current_outstanding_work is decremented by the caller for each item processed
    pub fn try_iter<'a>(&'a mut self) -> impl Iterator<Item = TerrainWork> + 'a {
        self.terrain_gen_return_recv.try_iter()
    }

    /// NOTE! Ensure that current_outstanding_work is decremented by the caller for each item processed
    pub fn iter<'a>(&'a mut self) -> impl Iterator<Item = TerrainWork> + 'a {
        self.terrain_gen_return_recv.iter()
    }
}
