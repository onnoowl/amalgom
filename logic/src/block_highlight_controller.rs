//! Controls everything about where the block_highlight will be placed
//!
//! Extra debug functionality like generating terrain within the volume of the block highlight are defined here. Also, various "modes" exist for moving what the block highlight selects

use crate::{
    terrain_manager::{
        GenerationDispatcher, TerrainManager, TerrainWorkDescriptor, TerrainWorkKind,
    },
    tree::LogicTree,
};
use base_utils::{ICoord, Loc, ROOT_LEVEL};
use game_utils::{block_highlight::BlockHighlight, camera::Camera, input::InputEvent};
use octree::{GetError, NotPresent};

use cgmath::InnerSpace;
use crossbeam::atomic::AtomicCell;
use std::{fmt::Debug, sync::Arc};
use winit::event::{ElementState, MouseScrollDelta, VirtualKeyCode};

#[derive(Debug)]
enum BlockHighlightMode {
    Intersect,
    FixedDist(Option<f32>),
}

#[derive(Debug)]
pub struct BlockHighlightController {
    /// Communicates a box to draw back to the render thread.
    /// Option is some when a change is made, None otherwise.
    pub block_highlight_cell: Arc<AtomicCell<Option<BlockHighlight>>>,
    pub block_highlight: BlockHighlight,

    target_level: u8,

    changed: bool,

    mode: BlockHighlightMode,
    pub lock_on_leafs: bool,
    hover_print: bool,
}

impl BlockHighlightController {
    pub fn new(block_highlight_cell: Arc<AtomicCell<Option<BlockHighlight>>>) -> Self {
        BlockHighlightController {
            block_highlight_cell,
            block_highlight: Default::default(),
            target_level: 0,
            changed: false,
            mode: BlockHighlightMode::Intersect,
            lock_on_leafs: true,
            hover_print: false,
        }
    }

    pub fn changed(&self) -> bool {
        self.changed
    }

    pub fn update(&mut self, tree: &LogicTree, camera: &Camera) {
        let last_box = self.block_highlight;

        match self.mode {
            BlockHighlightMode::Intersect => {
                let intersection = game_utils::intersection::ray_intersection(
                    &camera.controller.eye,
                    &(camera.cached_vectors.target - camera.controller.eye),
                    tree,
                    self.target_level as u32,
                    None,
                );

                match &intersection {
                    Some(intersection) => {
                        self.block_highlight.loc = Some(Loc(intersection.pos, intersection.level))
                    }
                    None => self.block_highlight.loc = None,
                }

                self.changed |= self.block_highlight != last_box;
                if let Some(intersection) = intersection {
                    if self.changed && self.hover_print {
                        let i = intersection;
                        log::debug!(
                            "Logic Thread Hover Info: {:?} lvl {:?}\n{:#?}",
                            i.pos,
                            i.level,
                            i.node
                        );
                    }
                }
            }
            BlockHighlightMode::FixedDist(ref mut dist) => {
                let tl = &mut self.target_level;
                let dist = dist.get_or_insert_with(|| {
                    if let Some(loc) = last_box.loc {
                        *tl = loc.1;
                        (loc.0.cast::<f32>().unwrap() - base_utils::p_to_v(&camera.controller.eye))
                            .magnitude()
                    } else {
                        BlockHighlightMode::FALLBACK_DIST
                    }
                });

                let coord = (((camera.cached_vectors.target - camera.controller.eye).normalize()
                    * (*dist))
                    + base_utils::p_to_v(&camera.controller.eye))
                .cast::<i64>()
                .unwrap();
                let mut level = self.target_level;

                let node = match tree.get_node(&coord, self.target_level as u32) {
                    Ok(node) => Some(node),
                    Err(e) => match e {
                        GetError::OutOfBounds(_) => None,
                        GetError::NotPresent(np) => {
                            if self.lock_on_leafs {
                                match np {
                                    NotPresent::IsUnloaded {
                                        depth: _,
                                        unloaded_node: _,
                                    } => None,
                                    NotPresent::IsHighLevelLeaf {
                                        depth,
                                        high_level_leaf_node,
                                    } => {
                                        level = (tree.root_level - depth) as u8;
                                        Some(high_level_leaf_node)
                                    }
                                }
                            } else {
                                None
                            }
                        }
                    },
                };

                let coord = tree.truncate_coord_at_level(&coord, level).ok();

                self.block_highlight.loc = coord.map(|some_coord| Loc(some_coord, level));

                self.changed |= self.block_highlight != last_box;
                if self.changed {
                    log::debug!(
                        "Logic Thread Hover Info: {:?} lvl {:?}\n{:#?}",
                        coord.unwrap_or_else(|| ICoord::new(-1, -1, -1)),
                        level,
                        node
                    );
                };
            }
        }
    }

    pub fn handle_input(&mut self, event: &InputEvent, terrain_manager: &mut TerrainManager) {
        self.changed |= match event {
            InputEvent::MouseWheel(wheel_event) => match wheel_event.delta {
                MouseScrollDelta::LineDelta(_, vertical) => {
                    if vertical > 0.0 {
                        self.level_up()
                    } else if vertical < 0.0 {
                        self.level_down()
                    } else {
                        false
                    }
                }
                MouseScrollDelta::PixelDelta(logical_position) => {
                    if logical_position.y > 0.0 {
                        self.level_up()
                    } else if logical_position.y < 0.0 {
                        self.level_down()
                    } else {
                        false
                    }
                }
            },
            InputEvent::KeyPress(kp) => {
                if kp.state == ElementState::Pressed {
                    match kp.virtual_keycode {
                        VirtualKeyCode::Down => self.level_down(),
                        VirtualKeyCode::Up => self.level_up(),
                        VirtualKeyCode::Comma => self.box_back(),
                        VirtualKeyCode::Period => self.box_forward(),
                        VirtualKeyCode::M => {
                            self.flip_mode();
                            true
                        }
                        VirtualKeyCode::G => self.regenerate(
                            &mut terrain_manager.generation_dispatcher,
                            &mut terrain_manager.tree,
                        ),
                        VirtualKeyCode::H => {
                            self.hover_print = !self.hover_print;
                            true
                        }
                        _ => false,
                    }
                } else {
                    false
                }
            }
            _ => false,
        };
    }

    /// Returns true when a mutation occurs
    fn level_down(&mut self) -> bool {
        if self.target_level > 0 {
            self.target_level -= 1;
            self.block_highlight.loc = None;
            true
        } else {
            false
        }
    }

    /// Returns true when a mutation occurs
    fn level_up(&mut self) -> bool {
        if self.target_level < ROOT_LEVEL {
            self.target_level += 1;
            self.block_highlight.loc = None;
            true
        } else {
            false
        }
    }

    fn box_forward(&mut self) -> bool {
        match self.mode {
            BlockHighlightMode::FixedDist(ref mut dist) => {
                *dist.get_or_insert(BlockHighlightMode::FALLBACK_DIST) +=
                    2.0f32.powi(self.target_level as i32);
                true
            }
            _ => false,
        }
    }

    fn box_back(&mut self) -> bool {
        match self.mode {
            BlockHighlightMode::FixedDist(ref mut dist) => {
                *dist.get_or_insert(BlockHighlightMode::FALLBACK_DIST) -=
                    2.0f32.powi(self.target_level as i32);
                true
            }
            _ => false,
        }
    }

    pub fn flush(&mut self) {
        if self.changed {
            self.block_highlight_cell.store(Some(self.block_highlight))
        }
        self.changed = false;
    }

    pub fn regenerate(
        &mut self,
        generation_dispatcher: &mut GenerationDispatcher,
        tree: &mut LogicTree,
    ) -> bool {
        if let Some(Loc(coord, level)) = self.block_highlight.loc {
            log::debug!(
                "Generating at ({:?}, {:?}, {:?}) lvl {:?}",
                coord.x,
                coord.y,
                coord.z,
                level
            );
            generation_dispatcher.clear_tentative_work();
            generation_dispatcher.push_tentative_work(
                TerrainWorkDescriptor {
                    kind: TerrainWorkKind::Generation,
                    coord,
                    level,
                    target_lod: 0.max(level - 5),
                },
                &cgmath::Point3::new(coord.x as f32, coord.y as f32, coord.z as f32),
                5, // TODO: buffer_depth_min_level should really be global so that we don't hard-code here.
            );
            generation_dispatcher.submit_tentative_work(tree);
            true
        } else {
            false
        }
    }

    pub fn flip_mode(&mut self) {
        match self.mode {
            BlockHighlightMode::Intersect => self.mode = BlockHighlightMode::FixedDist(None),
            BlockHighlightMode::FixedDist(_) => {
                if self.lock_on_leafs {
                    self.lock_on_leafs = false
                } else {
                    self.lock_on_leafs = true;
                    self.mode = BlockHighlightMode::Intersect
                }
            }
        }
    }
}

impl BlockHighlightMode {
    pub const FALLBACK_DIST: f32 = 1.0;
}
