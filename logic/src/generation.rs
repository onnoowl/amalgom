//! Terrain generation through random noise
//!
//! Stored in the commit history are many other methods of generation that proved to be slower

mod distorter;
mod height_cache;
mod noise_manager;
mod offset_perlin;
mod ridged_multi;

pub use noise_manager::NoiseManager;
pub use noise_manager::NoiseManagerHandle;

use crate::tree::*;
use base_utils::{ICoord, OCTAL_OFFSETS};
use octree::{
    traits::{HasID, HasMaxDataLOD, HasMaxDecLOD, HasNoUnloaded},
    OctNode,
};

use extend::ext;

// const SMALL_CAVE_FREQUENCY: f64 = 50.;
const BIG_CAVE_FREQUENCY: f64 = 150.; // noise noise can't be calculated on integers :(

#[ext(pub, name = NoiseGenerate)]
impl LogicNode {
    fn sample_noise(mut x: f64, mut y: f64, mut z: f64, noise: &NoiseManagerHandle) -> (u8, f64) {
        if let Some(d) = noise.distorter.as_ref() {
            let [nx, ny, nz] = d.distort(x, y, z);
            x = nx;
            y = ny;
            z = nz;
        }

        let height = noise.height.get(x, z);
        let dif_from_surface = y - height;

        // if dif_from_surface > 10. {
        //     return (0, 1.0);
        // }

        let density = noise.density_noise.get([
            x / BIG_CAVE_FREQUENCY,
            y / BIG_CAVE_FREQUENCY,
            z / BIG_CAVE_FREQUENCY,
        ]);
        // .max(noise.density_noise.get([
        //     x / SMALL_CAVE_FREQUENCY,
        //     y / SMALL_CAVE_FREQUENCY,
        //     z / SMALL_CAVE_FREQUENCY,
        // ]));

        if dif_from_surface > 0. {
            let threshold = 0.8 - (dif_from_surface - 4.0) / 7.;
            let diff = density - threshold;
            match diff {
                d if d > 0. => (0, diff),
                _ => (3, -1. * diff),
            }
        } else {
            let diff = density - 0.3;
            match diff {
                d if d > 0. => (0, diff),
                _ => (1, -1. * diff),
            }
        }
    }

    fn find_id_mode(blocks: &mut [Self; 8], data_level: u8) -> u8
    where
        Self: Sized,
    {
        let mut counts: Vec<(u8, u8)> = Default::default();

        for child in blocks.iter_mut() {
            let cid = match child {
                Self::Branch(b, children) => match b.get_id() {
                    Some(id) => id,
                    None => {
                        let nested_id = Self::find_id_mode(children, data_level - 1);
                        *b = LogicBranchData::new(Some(nested_id.into()), data_level);
                        nested_id
                    }
                },
                Self::Leaf(b) | Self::LODLeaf(b, _) => b.get_id().unwrap(),
                Self::Unloaded(_) => 0,
            };
            if cid == 2 {
                // grass
                return 2;
            }
            let mut found = false;
            for (id, count) in counts.iter_mut() {
                if *id == cid {
                    *count += 1;
                    if *count == 4 {
                        return cid;
                    }
                    found = true;
                    break;
                }
            }
            if !found {
                counts.push((cid, 0));
            }
        }
        let max = counts.into_iter().min_by_key(|(_k, v)| *v).unwrap();
        max.0
    }

    fn extend_with_id_and_then_noise(
        &mut self,
        target_lod: u8,
        level: u8,
        pos: &ICoord,
        noise: &NoiseManagerHandle,
        fill_id: u8,
        fill_target_level: u8,
    ) -> bool {
        if level < fill_target_level {
            self.extend_with_noise(target_lod, level, pos, noise)
        } else {
            match self {
                OctNode::Leaf(_) => false,
                OctNode::LODLeaf(l, lod) => {
                    let max_dec_lod = l.get_max_dec_lod();
                    if *lod <= target_lod && l.get_id().unwrap() == fill_id {
                        false
                    } else {
                        self.convert_to_id(level, target_lod, fill_id, max_dec_lod);
                        self.extend_with_id_and_then_noise(
                            target_lod,
                            level,
                            pos,
                            noise,
                            fill_id,
                            fill_target_level,
                        );
                        true
                    }
                }
                OctNode::Branch(b, children) => {
                    b.set_id(Some(fill_id));

                    let child_separation = 2_i64.pow(level as u32 - 1);
                    for (child, o) in children.iter_mut().zip(OCTAL_OFFSETS.iter()) {
                        child.extend_with_id_and_then_noise(
                            target_lod,
                            level - 1,
                            &(pos + child_separation * o),
                            noise,
                            fill_id,
                            fill_target_level,
                        );
                    }
                    self.mutation_hoist(target_lod);
                    true
                }
                OctNode::Unloaded(_) => {
                    self.convert_to_id(level, target_lod, fill_id, u8::MAX);
                    self.extend_with_id_and_then_noise(
                        target_lod,
                        level,
                        pos,
                        noise,
                        fill_id,
                        fill_target_level,
                    );
                    true
                }
            }
        }
    }

    fn extend_with_noise(
        &mut self,
        target_lod: u8,
        level: u8,
        pos: &ICoord,
        noise: &NoiseManagerHandle,
    ) -> bool {
        match self {
            OctNode::Leaf(_) => {
                false // Leaf is considered the highest detail possible
            }
            OctNode::LODLeaf(l, lod) => {
                // TODO: Populate it with a tree of blocks that already have the id that were originally hoisted
                // if *lod > target_lod {
                //     *self =
                //         Self::Branch((l.get_id().unwrap(), target_lod).into(), Default::default());
                //     self.extend_branch(target_lod, level, pos, noise)
                // } else {
                //     false
                // }

                if let Some(id) = l.get_id() {
                    let fill_lod = *lod;
                    self.extend_with_id_and_then_noise(target_lod, level, pos, noise, id, fill_lod)
                } else {
                    unreachable!("No id in LODLeaf");
                }
            }
            OctNode::Branch(b, _) => {
                b.get_max_data_lod() > target_lod
                    && self.extend_branch(target_lod, level, pos, noise)
            }
            OctNode::Unloaded(_) => {
                // *self = super::SparseOctalNode::Leaf(5.into());
                *self = Self::noise_recurse(
                    target_lod,
                    level,
                    noise,
                    pos.x as f64,
                    pos.y as f64,
                    pos.z as f64,
                );
                true
            }
        }
    }
}

#[ext(name = LogicNodeExtendBranch)]
impl LogicNode {
    fn extend_branch(
        &mut self,
        target_lod: u8,
        level: u8,
        pos: &ICoord,
        noise: &NoiseManagerHandle,
    ) -> bool {
        if let Some((_, children)) = self.get_branch_mut() {
            let spacing = 2u32.pow(level as u32 - 1) as i64;
            let mut made_changes = false;
            for (child, offset) in children.iter_mut().zip(OCTAL_OFFSETS.iter()) {
                made_changes |= child.extend_with_noise(
                    target_lod,
                    level - 1,
                    &(pos + offset * spacing),
                    noise,
                );
            }

            // TODO: Only need to re-hoist if specifically the id of the immediate children changed. Opportunity for enhancement
            if made_changes {
                self.mutation_hoist(target_lod);
            }

            made_changes
        } else {
            panic!("Calling extend_branch on not a branch");
        }
    }

    // Mutation hoist is used on a node that already exists. It mutates the node to hoist instead of returning it
    // TODO: mutation_hoist and initial_hoist and generation_hoist share some duplicated code
    fn mutation_hoist(&mut self, target_lod: u8) {
        if let Some((b, children)) = self.get_branch_mut() {
            let mut hoistable: bool = true;
            let mut bid = b.get_id();
            if let Some(id) = bid {
                // Hoist dirt along with grass
                if id == 3 {
                    bid = Some(2);
                }
            }
            let mut all_leaves = true;
            let mut no_unloaded = true;
            let mut max_dec_lod = 0;
            let mut max_data_lod = 0;
            for child in children.iter() {
                no_unloaded &= child.has_no_unloaded();
                match child {
                    Self::Leaf(l) => {
                        if let Some(id) = bid {
                            if l.block.id != id {
                                hoistable = false;
                                break;
                            }
                        } else {
                            bid = Some(l.block.id)
                        }
                        max_dec_lod = max_dec_lod.max(l.get_max_dec_lod());
                        continue;
                    }
                    Self::LODLeaf(l, data_lod) => {
                        if let Some(id) = bid {
                            if l.block.id != id {
                                hoistable = false;
                                break;
                            }
                        } else {
                            bid = Some(l.block.id)
                        }
                        max_dec_lod = max_dec_lod.max(l.get_max_dec_lod());
                        max_data_lod = max_data_lod.max(*data_lod)
                    }
                    _ => {
                        hoistable = false;
                        break;
                    }
                }
                all_leaves = false;
            }
            if hoistable {
                if all_leaves {
                    *self = Self::Leaf(LogicLeaf::new(bid.unwrap()))
                } else {
                    *self = Self::LODLeaf(
                        LogicLeaf::new_with_dec_lod(bid.unwrap(), max_dec_lod),
                        target_lod.min(max_data_lod),
                    )
                }
            } else {
                b.set_max_data_lod(b.get_max_data_lod().min(target_lod));
                b.set_has_no_unloaded(no_unloaded);
            }
        }
    }

    // Initial hoist is used when branch hasn't been set yet, which means we want to return either a Branch or Leaf
    fn generation_hoist(target_lod: u8, mut id: u8, children: [Self; 8]) -> Self
    where
        Self: Sized,
    {
        if id == 3 {
            // Hoist dirt along with grass
            id = 2;
        }
        let mut hoistable: bool = true;
        let mut all_leaves = true;
        let mut no_unloaded = true;
        let mut max_dec_lod = 0;
        let mut max_data_lod = 0;
        for child in children.iter() {
            no_unloaded &= child.has_no_unloaded();
            match child {
                Self::Leaf(l) => {
                    if l.block.id != id {
                        hoistable = false;
                        break;
                    }
                    max_dec_lod = max_dec_lod.max(l.get_max_dec_lod());
                }
                Self::LODLeaf(l, data_lod) => {
                    all_leaves = false;
                    if l.block.id != id {
                        hoistable = false;
                        break;
                    }
                    max_dec_lod = max_dec_lod.max(l.get_max_dec_lod());
                    max_data_lod = max_data_lod.max(*data_lod);
                }
                _ => {
                    hoistable = false;
                    all_leaves = false;
                    break;
                }
            }
        }
        if hoistable {
            if all_leaves {
                Self::Leaf(LogicLeaf::new(id))
            } else {
                Self::LODLeaf(
                    LogicLeaf::new_with_dec_lod(id, max_dec_lod),
                    target_lod.min(max_data_lod),
                )
            }
        } else {
            let mut bd = LogicBranchData::new(Some(id.into()), target_lod);
            bd.set_has_no_unloaded(no_unloaded);
            Self::Branch(bd, Box::new(children))
        }
    }

    fn convert_to_id(&mut self, level: u8, target_lod: u8, id: u8, max_dec_lod: u8) {
        if level == 0 {
            *self = Self::Leaf(LogicLeaf::new(id))
        } else if level == target_lod {
            *self = Self::LODLeaf(LogicLeaf::new_with_dec_lod(id, max_dec_lod), target_lod)
        } else {
            let mut new_branch = Self::Branch(
                LogicBranchData::new(Some(id.into()), target_lod),
                Default::default(),
            );
            new_branch.set_max_dec_lod(max_dec_lod);
            *self = new_branch;
        }
    }

    fn noise_recurse(
        target_lod: u8,
        level: u8,
        noise: &NoiseManagerHandle,
        x: f64,
        y: f64,
        z: f64,
    ) -> Self {
        let h_size = 2f64.powi(level as i32 - 1);
        let (id, confidence) = Self::sample_noise(x + h_size, y + h_size, z + h_size, noise);

        if (confidence / level as f64) < 2.9 && level > 0 || level >= 8 {
            if level > target_lod {
                // TODO: To make this cleaner, we should use OCTAL_OFFSETS here. Just need array intoIter
                let children = [
                    Self::noise_recurse(target_lod, level - 1, noise, x, y, z),
                    Self::noise_recurse(target_lod, level - 1, noise, x + h_size, y, z),
                    Self::noise_recurse(target_lod, level - 1, noise, x, y + h_size, z),
                    Self::noise_recurse(target_lod, level - 1, noise, x + h_size, y + h_size, z),
                    Self::noise_recurse(target_lod, level - 1, noise, x, y, z + h_size),
                    Self::noise_recurse(target_lod, level - 1, noise, x + h_size, y, z + h_size),
                    Self::noise_recurse(target_lod, level - 1, noise, x, y + h_size, z + h_size),
                    Self::noise_recurse(
                        target_lod,
                        level - 1,
                        noise,
                        x + h_size,
                        y + h_size,
                        z + h_size,
                    ),
                ];

                // Collapse blocks that are all the same into one
                Self::generation_hoist(target_lod, id, children)
            } else {
                Self::LODLeaf(LogicLeaf::new(id), target_lod)
            }
        } else {
            Self::Leaf(LogicLeaf::new(id))
        }
    }
}
