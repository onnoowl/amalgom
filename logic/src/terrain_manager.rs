//! Identifies new terrain to be generated and manages their placement

pub mod generation_dispatcher;
pub mod terrain_work;

use arrayvec::ArrayVec;
pub use generation_dispatcher::GenerationDispatcher;
pub use terrain_work::{TerrainWorkDescriptor, TerrainWorkKind};

use crate::{build_mesh::MakeMesh, generation::NoiseManager, tree::*};
use base_utils::{dimension::OcclusionPlane, p_to_v, Face, ICoord, Loc, OCTAL_OFFSETS, ROOT_LEVEL};
use game_utils::{
    should_lod_recurse, terrain_mesh::TerrainMesh, terrain_mesh::Vertex, thread_comm::StartupInfo,
    thread_comm::TerrainMeshMsg,
};
use octree::{traits::*, Address, OctNode};
use reporting::*;

use cgmath::Point3;
use std::sync::mpsc::Sender;

use self::terrain_work::TerrainWork;

pub struct TerrainManager {
    pub tree: LogicTree,
    pub generation_dispatcher: GenerationDispatcher,
    pub send_buffer: Sender<TerrainMeshMsg>,
    buffer_depth_and_min_level: u32,
}

impl TerrainManager {
    pub fn new(startup_info: &StartupInfo, send_buffer: Sender<TerrainMeshMsg>) -> Self {
        let hsize = 2i64.pow(ROOT_LEVEL as u32 - 1);
        let tree = LogicTree::new(
            LogicNode::Unloaded(Default::default()),
            ROOT_LEVEL as u32,
            p_to_v(&startup_info.player_spawn).cast::<i64>().unwrap()
                - ICoord::new(hsize, hsize, hsize),
        );

        Self {
            tree,
            generation_dispatcher: GenerationDispatcher::new(NoiseManager::default()),
            send_buffer,
            buffer_depth_and_min_level: startup_info.buffer_depth_and_min_level,
        }
    }

    pub fn get_buffer_depth_and_min_level(&self) -> u32 {
        self.buffer_depth_and_min_level
    }

    #[allow(dead_code)]
    pub fn with_fields(
        tree: LogicTree,
        generation_dispatcher: GenerationDispatcher,
        send_buffer: Sender<TerrainMeshMsg>,
        buffer_depth_and_min_level: u32,
    ) -> Self {
        Self {
            tree,
            generation_dispatcher,
            send_buffer,
            buffer_depth_and_min_level,
        }
    }

    fn lod_recurse(
        node: RecursionNode<'_>,
        coord: ICoord,
        level: u32,
        player_pos: &Point3<f32>,
        generation_dispatcher: &mut GenerationDispatcher,
        send_buffer: &Sender<TerrainMeshMsg>,
        buffer_depth_and_min_level: u32,
    ) -> Result<()> {
        if let RecursionNode::Some(n) = &node {
            if n.is_under_construction() {
                return Ok(());
            } // TODO create under construction hash for virtual recursion nodes
        }
        let bb = game_utils::intersection::get_bb(&coord.cast::<f32>().unwrap(), level);
        if should_lod_recurse(player_pos, &bb) && level > buffer_depth_and_min_level {
            let mut should_extend = false;
            let max_data_lod = node.get_max_data_lod();
            match node {
                RecursionNode::Some(LogicNode::Branch(b, children)) => {
                    if b.flags.has_no_unloaded()
                        && (level - max_data_lod as u32) <= buffer_depth_and_min_level
                    {
                        should_extend = true
                    } else {
                        for (index, child_coord) in
                            close_to_far_indices_and_coord(&coord, level, player_pos)
                        {
                            let child = &mut children[index];
                            Self::lod_recurse(
                                RecursionNode::Some(child),
                                child_coord,
                                level - 1,
                                player_pos,
                                generation_dispatcher,
                                send_buffer,
                                buffer_depth_and_min_level,
                            )?
                        }
                    }
                }
                RecursionNode::Some(LogicNode::Leaf(_) | LogicNode::LODLeaf(_, _)) => {
                    debug_ensure!(
                        max_data_lod != u8::MAX,
                        "Encountered node with max_data_lod of u8::MAX"
                    );
                    if (level - max_data_lod as u32) <= buffer_depth_and_min_level {
                        // It does not have enough detail, extend
                        should_extend = true
                    } else {
                        // It has enough detail (for this recursion depth at least)

                        // Ensure it has a buffer
                        Self::update_node_buffer(
                            node.unwrap(),
                            coord,
                            level,
                            max_data_lod,
                            send_buffer,
                        )?;

                        // Recurse into LODLeafs so we can make sure we can find generation/extension opportunities
                        if max_data_lod != 0 {
                            for (_, child_coord) in
                                close_to_far_indices_and_coord(&coord, level, player_pos)
                            {
                                Self::lod_recurse(
                                    RecursionNode::HighLevelLODLeaf(max_data_lod),
                                    child_coord,
                                    level - 1,
                                    player_pos,
                                    generation_dispatcher,
                                    send_buffer,
                                    buffer_depth_and_min_level,
                                )?;
                            }
                        }
                    }
                }
                RecursionNode::Some(LogicNode::Unloaded(_)) | RecursionNode::HighLevelUnloaded => {
                    for (_, child_coord) in
                        close_to_far_indices_and_coord(&coord, level, player_pos)
                    {
                        Self::lod_recurse(
                            RecursionNode::HighLevelUnloaded,
                            child_coord,
                            level - 1,
                            player_pos,
                            generation_dispatcher,
                            send_buffer,
                            buffer_depth_and_min_level,
                        )?;
                    }
                }
                RecursionNode::HighLevelLODLeaf(high_lod) => {
                    for (_, child_coord) in
                        close_to_far_indices_and_coord(&coord, level, player_pos)
                    {
                        Self::lod_recurse(
                            RecursionNode::HighLevelLODLeaf(high_lod),
                            child_coord,
                            level - 1,
                            player_pos,
                            generation_dispatcher,
                            send_buffer,
                            buffer_depth_and_min_level,
                        )?;
                    }
                }
            }
            if should_extend && !base_utils::CONFIG.no_extend {
                generation_dispatcher.push_tentative_work(
                    TerrainWorkDescriptor {
                        kind: TerrainWorkKind::Extension,
                        coord,
                        level: level as u8,
                        target_lod: (level - buffer_depth_and_min_level - 1) as u8,
                    },
                    player_pos,
                    buffer_depth_and_min_level,
                );
            }
        } else {
            let should_generate = match node {
                RecursionNode::Some(LogicNode::Leaf(_)) => false,
                RecursionNode::Some(LogicNode::LODLeaf(_, lod)) => {
                    (level - *lod as u32) < buffer_depth_and_min_level
                }
                RecursionNode::HighLevelLODLeaf(lod) => {
                    (level - lod as u32) < buffer_depth_and_min_level
                }
                RecursionNode::Some(LogicNode::Branch(b, _)) => {
                    let max_data_lod = b.get_max_data_lod();
                    if !b.has_no_unloaded()
                        && (max_data_lod == u8::MAX
                            || (level - max_data_lod as u32) < buffer_depth_and_min_level)
                    {
                        true
                    } else if !b.flags.has_buffer()
                        && (b.get_max_dec_lod() as u32) < level // Just to prevent subtract with overflow
                        && (level - b.get_max_dec_lod() as u32) >= buffer_depth_and_min_level
                    {
                        Self::update_node_buffer(
                            node.unwrap(),
                            coord,
                            level,
                            (level - buffer_depth_and_min_level) as u8,
                            &send_buffer,
                        )?;
                        false
                    } else {
                        false
                    }
                }
                RecursionNode::Some(LogicNode::Unloaded(_)) | RecursionNode::HighLevelUnloaded => {
                    true
                }
            };

            if should_generate && !base_utils::CONFIG.no_gen {
                generation_dispatcher.push_tentative_work(
                    TerrainWorkDescriptor {
                        kind: TerrainWorkKind::Generation,
                        coord,
                        level: level as u8,
                        target_lod: (level - buffer_depth_and_min_level) as u8,
                    },
                    player_pos,
                    buffer_depth_and_min_level,
                );
            }
        }
        Ok(())
    }

    pub fn lod_perspective(&mut self, player_pos: &Point3<f32>) -> Result<()> {
        self.generation_dispatcher.clear_tentative_work();

        self.tree.move_root_for_pos(player_pos);

        if !base_utils::CONFIG.no_lod_recurse {
            Self::lod_recurse(
                RecursionNode::Some(&mut self.tree.root),
                self.tree.root_coord,
                self.tree.root_level,
                player_pos,
                &mut self.generation_dispatcher,
                &self.send_buffer,
                self.buffer_depth_and_min_level,
            )?;
        }

        self.generation_dispatcher
            .submit_tentative_work(&mut self.tree);

        Ok(())
    }

    /// 1) Updates max_data_lod for chunk
    /// 2) Mutates node_packages to contain this chunk or neighboring chunks (as packages) that are ready for decoration
    fn process_chunk_change(
        tree: &mut LogicTree,
        node: &mut LogicNode,
        addr: &Address,
        coord: &ICoord,
        level: u32,
        extension: bool,
        node_packages: &mut Vec<(Address, ICoord, u8, Option<Face>)>,
        buffer_depth_and_min_level: u32,
    ) -> Result<()> {
        // Update branch parameters before insertion (necessary for max_data_lod to be correct before insertion)
        tree.recurse_at_address_and_update(&addr[0..addr.len() - 1], &|b, children, cur_level| {
            // Update max_data_lod
            let mut max_data_lod = children.iter().enumerate().fold(0, |max_so_far, (i, c)| {
                let child = if cur_level == level + 1 && i == *addr.last().unwrap() as usize {
                    &node
                } else {
                    c
                };
                let child_max = child.get_max_data_lod();
                max_so_far.max(child_max)
            });
            if max_data_lod == u8::MAX {
                max_data_lod = cur_level as u8;
            }
            b.set_max_data_lod(max_data_lod);
        });

        let node_max_data_lod = node.get_max_data_lod();
        let buffer_lod = node.get_buffer_lod(level, buffer_depth_and_min_level);

        let neighb_lods = tree.check_node_completion(
            Some(node),
            None,
            addr.clone(),
            coord,
            level,
            node_max_data_lod,
            buffer_lod,
            !extension,
            node_packages,
            buffer_depth_and_min_level,
        )?;

        if extension {
            node.package_at_data_depth(
                *coord,
                level,
                buffer_depth_and_min_level,
                node_packages,
                &tree.root_coord,
                tree.root_level,
                neighb_lods,
            );
        }

        Ok(())
    }

    /// Processes node packages and decorates them
    pub fn process_node_packages(
        tree: &mut LogicTree,
        send_buffer: &Sender<TerrainMeshMsg>,
        node_packages: Vec<(Address, ICoord, u8, Option<Face>)>,
        buffer_depth_and_min_level: u32,
    ) -> Result<()> {
        for (addr, coord, node_package_max_data_lod, _calling_face) in node_packages {
            let mut addr = &addr[..];
            let packaged_node = match tree.root.get_node_mut_with_address(&addr) {
                Ok(n) => n,
                Err(np) => match np {
                    octree::NotPresentMut::IsUnloaded {
                        depth: _,
                        unloaded_node: _,
                    } => {
                        log::error!("Packaged node is unloaded");
                        continue;
                    }
                    octree::NotPresentMut::IsHighLevelLeaf {
                        depth,
                        high_level_leaf_node,
                    } => {
                        addr = &addr[..depth as usize];
                        high_level_leaf_node
                    }
                },
            };

            let node_package_level = tree.root_level as u8 - addr.len() as u8;
            let old_max_dec_lod = packaged_node.get_max_dec_lod();
            // This doesn't update when a neighbor has changed
            let buffer_update_level = if old_max_dec_lod > node_package_max_data_lod {
                packaged_node.decorate(
                    &coord,
                    node_package_level,
                    old_max_dec_lod,
                    node_package_max_data_lod,
                );
                node_package_max_data_lod
            } else {
                old_max_dec_lod.max(node_package_level - buffer_depth_and_min_level as u8)
            };

            Self::update_node_buffer(
                packaged_node,
                coord,
                node_package_level as u32,
                buffer_update_level,
                send_buffer,
            )?;

            debug_ensure!(
                !matches!(packaged_node, LogicNode::Unloaded(_)),
                "A packaged node was unloaded"
            );

            debug_ensure!(
                packaged_node.get_max_dec_lod() <= node_package_max_data_lod,
                "actual = {}, desired = {}",
                packaged_node.get_max_dec_lod(),
                node_package_max_data_lod
            );

            if old_max_dec_lod > node_package_max_data_lod {
                tree.recurse_at_address_and_update(&addr, &|b, children, _cur_level| {
                    let max_dec_lod = children
                        .iter()
                        .map(|child| child.get_max_dec_lod())
                        .max()
                        .unwrap();
                    b.set_max_dec_lod(max_dec_lod);
                });
            }
        }

        Ok(())
    }

    pub fn process_finished_terrain(
        finished: TerrainWork,
        tree: &mut LogicTree,
        send_buffer: &Sender<TerrainMeshMsg>,
        buffer_depth_and_min_level: u32,
    ) -> Result<()> {
        let mut node = finished.node;
        let descriptor = finished.descriptor;

        let mut node_packages = Default::default();

        let mut address = Address::new();
        if let Ok(()) =
            tree.coord_to_address(&descriptor.coord, &mut address, descriptor.level as u32)
        {
            let is_extension = descriptor.kind.is_extension();

            Self::process_chunk_change(
                tree,
                &mut node,
                &address,
                &descriptor.coord,
                descriptor.level as u32,
                is_extension,
                &mut node_packages,
                buffer_depth_and_min_level,
            )?;

            // Update has_no_unloaded
            tree.replace_at_address_and_update(
                Some(node),
                &address,
                &|bd: &mut LogicBranchData, children: &[LogicNode], _| {
                    bd.set_has_no_unloaded(children.iter().all(|c: &LogicNode| c.has_no_unloaded()))
                },
            );

            Self::process_node_packages(
                tree,
                send_buffer,
                node_packages,
                buffer_depth_and_min_level,
            )?;
        } else {
            log::warn!("Extended node being inserted was out of bounds");
        }

        Ok(())
    }

    pub fn check_for_and_process_finished_terrain(&mut self, should_block: bool) -> Result<bool> {
        let mut processed_count = 0;
        let outstanding = self.generation_dispatcher.current_outstanding_work;

        if outstanding > 0 {
            // TODO: is there a way around using box here?
            let iter: Box<dyn Iterator<Item = TerrainWork>> = if should_block {
                Box::new(self.generation_dispatcher.iter())
            } else {
                Box::new(self.generation_dispatcher.try_iter())
            };
            for work_item in iter {
                processed_count += 1;

                let descriptor = work_item.descriptor.clone();

                let result: Result<()> = Self::process_finished_terrain(
                    work_item,
                    &mut self.tree,
                    &self.send_buffer,
                    self.buffer_depth_and_min_level,
                );
                result.with_context(|| {
                    format!("Failed while processing work item: {:?}", descriptor)
                })?;

                if processed_count >= outstanding {
                    break;
                    // nothing left to process, break loop
                    // not needed for non blocking try_iter, but needed for blocking iter
                }
            }
            self.generation_dispatcher.current_outstanding_work -= processed_count;
        }

        Ok(processed_count > 0)
    }

    fn update_node_buffer(
        node: &mut LogicNode,
        pos: ICoord,
        level: u32,
        lod: u8,
        send_buffer: &Sender<TerrainMeshMsg>,
    ) -> Result<()> {
        debug_ensure!(
            (lod as u32) <= level,
            "Buffer being created with level({}) < lod({})",
            level,
            lod
        );

        match node {
            OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => {
                if !l.has_buffer {
                    l.has_buffer = true
                } else {
                    return Ok(());
                }
            }
            OctNode::Branch(b, _) => {
                b.flags.set_has_buffer(true);
                b.max_dec_lod = lod
            }
            OctNode::Unloaded(_) => return Ok(()),
        }

        let mut vertex_data: [Vec<Vertex>; 6] = Default::default();
        let mut index_data: [Vec<u32>; 6] = Default::default();
        node.make_mesh(level as u8, lod, &pos, &mut vertex_data, &mut index_data);

        // Add occlusion planes
        let mut occluder_planes: ArrayVec<[OcclusionPlane; 6]> = Default::default();
        let scale = 2_i64.pow(level as u32);
        Face::variants().for_each(|face| {
            if node.is_face_solid(&face, level, lod as u32) {
                let mut verts: [ICoord; 4] = [ICoord::new(0, 0, 0); 4];
                base_utils::UV_FACE_VALS[face as usize]
                    .iter()
                    .zip(verts.iter_mut())
                    .for_each(|(Loc(p, _), vert)| *vert = pos + (p * scale));

                occluder_planes.push(OcclusionPlane::new(verts, face.into()))
            }
        });

        if !index_data.is_empty() {
            let new_buffer = TerrainMesh::new(vertex_data, index_data, occluder_planes);
            send_buffer
                .send(TerrainMeshMsg {
                    buf: Some(new_buffer),
                    loc: Loc(pos, level as u8),
                })
                .expect("Failed to send buffer");
        } else {
            send_buffer
                .send(TerrainMeshMsg {
                    buf: None,
                    loc: Loc(pos, level as u8),
                })
                .expect("Failed to send buffer");
        }
        Ok(())
    }
}

pub fn close_to_far_indices_and_coord(
    coord: &ICoord,
    level: u32,
    player_pos: &Point3<f32>,
) -> impl Iterator<Item = (usize, ICoord)> {
    let size = 2_u32.pow(level) as i64;
    let coord = *coord;
    close_to_far_indices(&coord, level, player_pos)
        .map(move |index| (index, coord + OCTAL_OFFSETS[index] * size / 2))
}

pub fn close_to_far_indices(
    coord: &ICoord,
    level: u32,
    player_pos: &Point3<f32>,
) -> impl Iterator<Item = usize> {
    let size = 2_u32.pow(level) as i64;
    let player_to_center = (player_pos.cast::<i64>().unwrap() * -1
        + coord
        + ICoord {
            x: size / 2,
            y: size / 2,
            z: size / 2,
        })
    .cast::<f32>()
    .unwrap();

    let sx = (player_to_center.x > 0.) as u8;
    let sy = (player_to_center.y > 0.) as u8;
    let sz = (player_to_center.z > 0.) as u8;

    (0..=7u8)
        .rev()
        .map(move |flip_bits| ((sx | (sy << 1) | (sz << 2)) ^ flip_bits) as usize)
}

pub enum RecursionNode<'a> {
    Some(&'a mut LogicNode),
    HighLevelLODLeaf(u8),
    HighLevelUnloaded,
}

impl<'a> RecursionNode<'a> {
    pub fn unwrap(self) -> &'a mut LogicNode {
        match self {
            RecursionNode::Some(node) => node,
            RecursionNode::HighLevelLODLeaf(_) => {
                panic!("unwrap() called on RecursionNode::HighLevelLODLeaf")
            }
            RecursionNode::HighLevelUnloaded => {
                panic!("unwrap called on RecursionNode::HighLevelUnloaded")
            }
        }
    }

    pub fn is_some(&self) -> bool {
        matches!(self, RecursionNode::Some(_))
    }
}

impl HasMaxDataLOD for RecursionNode<'_> {
    fn get_max_data_lod(&self) -> u8 {
        match self {
            RecursionNode::Some(node) => node.get_max_data_lod(),
            RecursionNode::HighLevelLODLeaf(high_lod) => *high_lod,
            RecursionNode::HighLevelUnloaded => u8::MAX,
        }
    }
    fn set_max_data_lod(&mut self, max_data_lod: u8) {
        match self {
            RecursionNode::Some(node) => node.set_max_data_lod(max_data_lod),
            RecursionNode::HighLevelLODLeaf(high_lod) => *high_lod = max_data_lod,
            RecursionNode::HighLevelUnloaded => {
                log::error!("Tried to set_max_data_lod on RecurseNode::HighLevelUnloaded.");
            }
        }
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//     use base_utils::OCTAL_OFFSETS;
//     use std::collections::HashMap;

//     #[derive(Debug, Clone, PartialEq, Hash, Eq)]
//     struct Key {
//         id: u8,
//         level: u8,
//         all_eq_to_id: bool,
//     }

//     fn all_equal_to_id(node: &LogicNode, id: u8) -> bool {
//         match node {
//             LogicNode::Leaf(l) | LogicNode::LODLeaf(l, _) => l.block.id == id,
//             LogicNode::Branch(_, children) => children.iter().all(|c| all_equal_to_id(c, id)),
//             LogicNode::Unloaded(_) => true,
//         }
//     }

//     fn recurse(
//         noise: &NoiseManager,
//         pos: ICoord,
//         level: u32,
//         node: &LogicNode,
//         data: &mut HashMap<Key, Vec<f64>>,
//     ) {
//         let h_size_f = 2f64.powi(level as i32 - 1);
//         let (id, confidence) = NoiseGenerate::sample_noise(
//             pos.x as f64 + h_size_f,
//             pos.y as f64 + h_size_f,
//             pos.z as f64 + h_size_f,
//             noise,
//         );

//         let all_eq_to_id = all_equal_to_id(node, id);

//         data.entry(Key {
//             id,
//             level: level as u8,
//             all_eq_to_id,
//         })
//         .or_default()
//         .push(confidence);

//         match node {
//             LogicNode::Leaf(_) | LogicNode::LODLeaf(_, _) => {}
//             LogicNode::Branch(_, children) => {
//                 let hsize_i = 2i64.pow(level - 1);
//                 for (index, offset) in (0..8).zip(OCTAL_OFFSETS.iter()) {
//                     recurse(
//                         noise,
//                         pos + offset * hsize_i,
//                         level - 1,
//                         &children[index],
//                         data,
//                     )
//                 }
//             }
//             LogicNode::Unloaded(_) => {}
//         }
//     }
// }
