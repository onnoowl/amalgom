use crate::lidar;
use animation::{Interpolate, Keyframe, NPodParams, NPodState, QVTransform};
use base_utils::{v_to_p, F32Coord};
use game_utils::thread_comm::{NPodID, NPodMsg};
use logic_tree::LogicTree;

use cgmath::{
    vec3, Deg, InnerSpace, Matrix3, Matrix4, Quaternion, Rad, Rotation, Rotation3, SquareMatrix,
    Vector3,
};
use collision::Frustum;
use std::{
    sync::{atomic::AtomicUsize, mpsc::Sender},
    time::{Duration, Instant},
};

// TODO, refactor to definition in thread_comm
const NPOD_ID_COUNTER: AtomicUsize = AtomicUsize::new(0);

/// This controller makes some assumptions about the leg ordering: `['Forward left', 'Forward right', 'Mid left', 'Mid right', 'Back left', 'Back right']`
pub struct TripodGaitController {
    id: NPodID,
    // params: &'static NPodParams<6>,
    channel: Sender<NPodMsg<6>>,

    pub last_npod_state: NPodState<6>,
    last_leading_foot: Option<LeftRight>,
    time_to_push: Instant,

    unscaled_max_reach: f32,

    pub config: TripodGaitConfig,

    pub target_walking_dir: Option<F32Coord>,

    new_foot: Option<(F32Coord, LeftRight)>,
}

#[derive(Debug, Clone)]
/// Updating any of these config items will have a real time affect on the npod.
pub struct TripodGaitConfig {
    /// blocks travelled per second
    pub speed: f32,

    /// What level of the tree should this npod walk on
    pub target_level: u32,

    /// Preferred angle of deflection for leg placement in radians. New foot placements will aim for this angle off of center
    pub preferred_angle_deflection: f32,

    /// How far should the robot try to reach with its feet, as a ratio of its max reach.
    /// e.g. a value of 1 will try to reach as far as it can.
    pub preferred_ratio_of_max_reach: f32,

    /// How high the body should be above its feet
    pub body_lift_height: f32,

    /// How high to lift its feet in between steps
    pub foot_lift_height: f32,

    /// How long the animations should lag behind the logic.
    /// If this is too short, there might be times that the animation halts, waiting for the logic thread to communicate
    pub lag_time: Duration,

    /// A f32 that must be in the range of (0, 1]
    /// When this f32 == 1.0, the animation will come to a full stop when it takes a step.
    /// When this f32 is close to 0.0, it will continue through the action in a more lerp-y kind of way
    pub slowdown_alpha: f32,
}

impl TripodGaitController {
    pub fn new(
        initial_state: NPodState<6>,
        params: &NPodParams<6>,
        channel: Sender<NPodMsg<6>>,
        config: TripodGaitConfig,
    ) -> Self {
        let id = NPodID(NPOD_ID_COUNTER.fetch_add(1, std::sync::atomic::Ordering::Relaxed));
        channel.send(NPodMsg::Spawn(id)).unwrap();

        let unscaled_max_reach = params.recommended_max_reach();

        channel
            .send(NPodMsg::SendKeyFrame(
                id,
                Keyframe {
                    state: initial_state.clone(),
                    time: Instant::now(),
                },
            ))
            .unwrap();

        Self {
            id,
            // params,
            last_npod_state: initial_state,
            last_leading_foot: None,
            channel,
            config,
            unscaled_max_reach,
            time_to_push: Instant::now(),
            target_walking_dir: None,
            new_foot: None,
        }
    }

    pub fn update(&mut self, cur_timestamp: Instant, tree: &LogicTree) {
        if let Some(walking_dir) = self.target_walking_dir {
            if cur_timestamp >= self.time_to_push {
                // Time to push the new foot!
                if let Some((new_foot, left_right)) = self.new_foot.take() {
                    // Add this foot placement to our state, and push the keyframes through the animation channel
                    // TODO use cur_timestamp, not a fresh instant
                    self.push_foot(new_foot, left_right, walking_dir, cur_timestamp);
                }

                // Begin computing the next foot now, push it later at time_to_push

                // Flip the foot
                let leading_foot = self
                    .last_leading_foot
                    .map(|lr| lr.flip())
                    .unwrap_or(LeftRight::Left); // left leg goes _first_ in the hokey pokey dance

                let ideal_foot_dir = self.get_ideal_foot_dir(leading_foot);

                let max_reach = self.unscaled_max_reach * self.last_npod_state.body.scale;

                // dbg!(ideal_foot_dir.0.rotate_vector(cgmath::vec3(0., 0., 1.)));

                let frustum = {
                    let transform = {
                        let trans =
                            Matrix4::from_translation(self.last_npod_state.body.translation);

                        let rot = Matrix4::from(
                            (Quaternion::from_arc(cgmath::vec3(0., 0., 1.), walking_dir, None)
                                * ideal_foot_dir.0
                                * Quaternion::from_angle_y(Deg(180.)))
                            .normalize(),
                        );

                        trans * rot
                    };

                    Frustum::from_matrix4(
                        create_forward_frustum(max_reach) * transform.invert().unwrap(),
                    )
                    .unwrap()
                };

                // Get the next foot placement
                if let Some(new_foot) = self.get_next_foot(
                    walking_dir,
                    self.last_npod_state.body.translation,
                    ideal_foot_dir,
                    self.config.target_level,
                    max_reach,
                    frustum,
                    tree,
                ) {
                    self.new_foot = Some((new_foot, leading_foot))
                } else {
                    log::error!("Found no place to put foot!");
                    return;
                }
            }
        }
    }

    fn get_next_foot(
        &self,
        walking_target_vec: F32Coord,
        body_pos: F32Coord,
        dir: (Quaternion<f32>, f32),
        target_level: u32,
        max_reach: f32,
        frustum: Frustum<f32>,
        tree: &LogicTree,
    ) -> Option<F32Coord> {
        let locs = lidar::lidar_n_points(
            max_reach,
            &[dir],
            target_level,
            tree,
            &v_to_p(&body_pos),
            &v_to_p(&(body_pos + walking_target_vec)),
            &frustum,
        );
        if locs.len() > 0 {
            let loc = locs[0];
            let hsize = 2f32.powi(loc.1 as i32) / 2.;
            return Some(loc.0.cast::<f32>().unwrap() + Vector3::new(hsize, hsize * 2., hsize));
        }
        return None;
    }

    pub fn push_foot(
        &mut self,
        pos: F32Coord,
        which_foot: LeftRight,
        walking_dir: Vector3<f32>,
        cur_timestamp: Instant,
    ) {
        let next_state = {
            // Copy the feet over, add the new foot, and then shuffle the feet forward
            let mut feet = self.last_npod_state.feet.clone();
            match which_foot {
                LeftRight::Left => {
                    feet[Foot::ForwardLeft as usize] = pos;
                    feet[Foot::MidRight as usize] = feet[Foot::ForwardRight as usize];
                    feet[Foot::BackLeft as usize] = feet[Foot::MidLeft as usize];
                }
                LeftRight::Right => {
                    feet[Foot::ForwardRight as usize] = pos;
                    feet[Foot::MidLeft as usize] = feet[Foot::ForwardLeft as usize];
                    feet[Foot::BackRight as usize] = feet[Foot::MidRight as usize];
                }
            };

            // Build ideal body pos/ori for each tripod

            let mut body = Self::compute_body_transform(
                &feet,
                self.config.body_lift_height,
                self.last_npod_state.body.scale,
                self.last_npod_state.body.rotation,
                walking_dir,
            );

            // If the fancy body positioning fails, just place the body in the +Y direction as a fallback
            if body.translation.x.is_nan()
                || body.translation.y.is_nan()
                || body.translation.z.is_nan()
            {
                log::error!("Could not compute body orientation, making it point forward");
                let body_pos = (feet.iter().sum::<F32Coord>() / 6.)
                    + self.config.body_lift_height * cgmath::vec3(0., 1., 0.);
                body = QVTransform::from_translation(body_pos);
            }

            NPodState {
                body,
                feet,
                ease_type: None,
            }
        };

        // Make an intermediate keyframe with all the legs lifted
        let mut half_state = self.last_npod_state.interpolate(&next_state, 0.5);
        let foot_lift_v = half_state.body.rotation.rotate_vector(Vector3::new(
            0.,
            self.config.foot_lift_height,
            0.,
        ));
        match which_foot {
            LeftRight::Left => {
                half_state.feet[Foot::ForwardLeft as usize] += foot_lift_v;
                half_state.feet[Foot::MidRight as usize] += foot_lift_v;
                half_state.feet[Foot::BackLeft as usize] += foot_lift_v;
            }
            LeftRight::Right => {
                half_state.feet[Foot::ForwardRight as usize] += foot_lift_v;
                half_state.feet[Foot::MidLeft as usize] += foot_lift_v;
                half_state.feet[Foot::BackRight as usize] += foot_lift_v;
            }
        };

        debug_assert!(next_state.body.translation.x.is_finite());

        // Calculate gait transition time (in millis) based on ideal how far we want to travel divided by our speed
        let dist =
            (next_state.body.translation - self.last_npod_state.body.translation).magnitude();

        // let half_gait_transition_time =
        //     Duration::from_secs_f32(((dist / self.config.speed) / 2.));
        let third_gait_transition_time = Duration::from_secs_f32((dist / self.config.speed) / 2.);

        // let t0 = cur_timestamp;
        let t1 = cur_timestamp + third_gait_transition_time;
        let t2 = t1 + third_gait_transition_time;
        // let t1 = cur_timestamp + half_gait_transition_time;
        // let t2 = t1 + half_gait_transition_time;
        self.time_to_push = t2;

        // TEMPORARY until processing gets faster and goes async
        // let mut key0 = Keyframe {
        //     state: self.last_npod_state.clone(),
        //     time: t0 + self.config.lag_time,
        // };

        let mut key1 = Keyframe {
            state: half_state,
            time: t1 + self.config.lag_time,
        };

        let mut key2 = Keyframe {
            state: next_state.clone(),
            time: t2 + self.config.lag_time,
        };

        // key0.state.ease_type = Some(animation::EaseType::Slowdown(self.config.slowdown_alpha));
        key1.state.ease_type = Some(animation::EaseType::Continue);
        key2.state.ease_type = Some(animation::EaseType::Slowdown(self.config.slowdown_alpha));

        // self.channel
        //     .send(NPodMsg::SendKeyFrame(self.id, key0))
        //     .unwrap();
        self.channel
            .send(NPodMsg::SendKeyFrame(self.id, key1))
            .unwrap();
        self.channel
            .send(NPodMsg::SendKeyFrame(self.id, key2))
            .unwrap();

        self.last_leading_foot = Some(which_foot);
        self.last_npod_state = next_state;
    }

    fn compute_body_transform(
        feet: &[F32Coord; 6],
        body_lift_height: f32,
        scale: f32,
        last_rotation: Quaternion<f32>,
        walking_dir: Vector3<f32>,
    ) -> QVTransform {
        let avg_left = ((feet[Foot::ForwardLeft as usize] - feet[Foot::ForwardRight as usize])
            .normalize()
            // + (feet[Foot::MidLeft as usize] - feet[Foot::MidRight as usize]).normalize()
            + (feet[Foot::BackLeft as usize] - feet[Foot::BackRight as usize]).normalize())
        .normalize();

        // TODO: should I ignore the middle legs?

        // Get a forward and back leg to use for determining which way our robot is facing
        let avg_forward = {
            let for_l = feet[Foot::ForwardLeft as usize];
            let bak_l = feet[Foot::BackLeft as usize];
            let for_r = feet[Foot::ForwardRight as usize];
            let bak_r = feet[Foot::BackRight as usize];

            let l = (for_l - bak_l).normalize();
            let r = (for_r - bak_r).normalize();

            (l + r).normalize()
        };

        let last_up = last_rotation.rotate_vector(vec3(0., 1., 0.));

        // The norm is derived from the average plane of our feet, derived from avg_left and avg_forward
        let mut up = avg_forward.cross(avg_left).normalize();

        if up.dot(last_up) < 0. {
            up *= -1.;
        }

        // The left and forward vectors are oriented to more closely match the specified `walking_dir`. This helps keep us on track.
        let left = up.cross(walking_dir).normalize(); //recompute left so its really perpendicular
        let forward = left.cross(up).normalize();

        let rot_matrix = Matrix3::from_cols(left, up, forward);

        let avg = feet.iter().sum::<Vector3<f32>>() / 6.;
        let translation = avg + body_lift_height * up;

        let mut rotation = Quaternion::<f32>::from(rot_matrix).normalize();

        if rotation.dot(last_rotation) < 0. {
            rotation *= -1.;
        }

        QVTransform {
            translation,
            scale,
            rotation,
        }
    }

    fn get_ideal_foot_dir(&self, leading_foot: LeftRight) -> (Quaternion<f32>, f32) {
        // Pick our "dir", or the quaternion/dist pair that describes where we want our foot to go ideally
        let ideal_dist = self.config.preferred_ratio_of_max_reach
            * self.unscaled_max_reach
            * self.last_npod_state.body.scale;

        //dbg!(ideal_dist);
        let ideal_foot_dir = {
            let side_rot = {
                let rot_sign = match leading_foot {
                    LeftRight::Left => 1.,
                    LeftRight::Right => -1.,
                };

                Quaternion::from_angle_y(Rad(rot_sign * self.config.preferred_angle_deflection))
            };

            let down_rot = {
                let down_angle = std::f32::consts::FRAC_PI_2
                    - (self.config.body_lift_height / ideal_dist).acos();
                debug_assert!(down_angle.is_finite());

                Quaternion::from_angle_x(Rad(down_angle))
            };

            // Rotate down, then sideways
            down_rot * side_rot
        };
        (ideal_foot_dir, ideal_dist)
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum LeftRight {
    Left,
    Right,
}

impl LeftRight {
    pub fn flip(&self) -> Self {
        match self {
            LeftRight::Left => LeftRight::Right,
            LeftRight::Right => LeftRight::Left,
        }
    }
}

#[repr(usize)]
enum Foot {
    ForwardLeft = 0,
    ForwardRight,
    MidLeft,
    MidRight,
    BackLeft,
    BackRight,
}

impl Drop for TripodGaitController {
    fn drop(&mut self) {
        self.channel.send(NPodMsg::Despawn(self.id)).unwrap();
    }
}

fn create_forward_frustum(max_reach: f32) -> Matrix4<f32> {
    // This is the FOV of the human eye (vertically), 0.75*pi radians
    cgmath::perspective(cgmath::Deg(150.), 1., 0.1, max_reach)
}
