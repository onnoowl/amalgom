use super::tripod_gait_controller::{TripodGaitConfig, TripodGaitController};
use crate::tree::*;
use animation::{param_zoo::big_ant, NPodState, QVTransform};
use base_utils::ICoord;
use game_utils::thread_comm::NPodMsg;

use cgmath::{vec3, Deg, InnerSpace, Quaternion, Rotation, Rotation3};
use std::{
    sync::mpsc::Sender,
    time::{Duration, Instant},
};

pub struct BigAnt {
    tripod: TripodGaitController,
    is_loaded: bool,
    spawn_time: Instant,
    circle_mode: bool,
}

impl BigAnt {
    pub fn new(channel: Sender<NPodMsg<6>>) -> Self {
        let config = TripodGaitConfig {
            speed: 10.,
            target_level: 0,
            preferred_angle_deflection: 0.2 * std::f32::consts::PI,
            preferred_ratio_of_max_reach: 1.0,
            body_lift_height: big_ant::LEG_LENGTH * 0.75,
            foot_lift_height: big_ant::LEG_LENGTH * 0.3,
            lag_time: Duration::from_millis(100),
            slowdown_alpha: 0.4,
        };

        //let pos = vec3(218., -24.5, 283.);
        let pos = vec3(-1., 65., 43.);
        let mut body = QVTransform::from_translation(
            pos + vec3(0., 1., 1.).normalize() * config.body_lift_height,
        );
        body.rotation = Quaternion::from_angle_x(Deg(45.));

        let p = pos - vec3(-0.0, 0., 0.);

        let mut feet = [p; 6];

        for frow in 0..=2 {
            for fcol in 0..=1 {
                feet[frow * 2 + fcol] =
                    p + (frow as f32 - 1.) * vec3(0., 2.0, -2.0) + fcol as f32 * vec3(-2.0, 0., 0.);
            }
        }

        let initial_state = NPodState {
            body,
            feet, //put all the feet at the same point, because spawning is a hard problem. TODO: spawn better
            ease_type: None,
        };

        Self {
            tripod: TripodGaitController::new(initial_state, &big_ant::PARAMS, channel, config),
            is_loaded: false,
            spawn_time: Instant::now(),
            circle_mode: true,
        }
    }

    pub fn update(&mut self, cur_timestamp: Instant, tree: &LogicTree, target_pos: Option<ICoord>) {
        if !self.is_loaded {
            // let loaded = tree.root.has_no_unloaded();

            // let loaded = !tree.root.is_under_construction()
            //     && match tree.get_node(
            //         &self
            //             .tripod
            //             .last_npod_state
            //             .body
            //             .translation
            //             .cast::<i64>()
            //             .unwrap(),
            //         0,
            //     ) {
            //         Ok(o) => o.get_unloaded().is_none(),
            //         Err(err) => match err {
            //             octree::GetError::OutOfBounds(_) => false,
            //             octree::GetError::NotPresent(np) => match np {
            //                 octree::NotPresent::IsUnloaded {
            //                     depth: _,
            //                     unloaded_node: _,
            //                 } => false,
            //                 octree::NotPresent::IsHighLevelLeaf {
            //                     depth: _,
            //                     high_level_leaf_node: hll,
            //                 } => !hll.is_under_construction(),
            //             },
            //         },
            //     };

            let loaded = self.spawn_time.elapsed() > Duration::from_secs(2);

            if loaded {
                log::info!("Terrain is loaded!");
            }

            self.is_loaded = loaded;
            return;
        }

        if let Some(target_pos) = target_pos {
            self.tripod.target_walking_dir = Some(
                target_pos.cast::<f32>().unwrap() - self.tripod.last_npod_state.body.translation,
            );
            self.circle_mode = false;
        }

        if self.circle_mode {
            self.tripod.target_walking_dir = Some(
                self.tripod
                    .last_npod_state
                    .body
                    .rotation
                    .rotate_vector(vec3(0.1, 0., 1.).normalize()),
            );
        }

        self.tripod.update(cur_timestamp, tree)
    }
}
