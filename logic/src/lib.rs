//! # Dependencies
//!
//! [game_utils](game_utils) - Utils for common functionality between the render and logic crates
//!
//! [octree](octree) - Houses the logic for the generic octree data structure
//!
//! [base_utils](base_utils) - Utils that don't require other crates from this project as dependencies

#![feature(try_blocks)]

pub mod block_highlight_controller;
pub mod build_mesh;
mod collision_cage;
pub mod controller_zoo;
pub mod generation;
pub mod lidar;
pub mod terrain_manager;

use base_utils::CONFIG;
use controller_zoo::big_ant_controller::BigAnt;
pub use logic_tree as tree;

use game_utils::{
    camera::Camera,
    input::{CurrentInputs, MouseClickEvent},
    thread_comm::{GrappleEvent, InteractionEvent, LogicComm, RenderStateNotification},
};
use reporting::*;

use std::time::Instant;

use crate::{
    block_highlight_controller::BlockHighlightController, terrain_manager::TerrainManager,
};

#[cfg(debug_assertions)]
use crate::tree::LogicTree;
#[cfg(debug_assertions)]
use game_utils::input::{InputEvent, KeyPressEvent};
#[cfg(debug_assertions)]
use generation::NoiseManager;
#[cfg(debug_assertions)]
use std::collections::VecDeque;
#[cfg(debug_assertions)]
use terrain_manager::GenerationDispatcher;
#[cfg(debug_assertions)]
use winit::event::{ElementState, VirtualKeyCode};

pub struct AmalgomLogic {
    /// Manages the terrain, spawning, extending, and buffer creation (buffers are send back to rendering thread)
    terrain_manager: TerrainManager,

    /// Communicates with the render thread
    logic_comm: LogicComm,

    /// Tracks currently pressed keys (updated by `input_event_recv` events)
    inputs: CurrentInputs,

    camera: Camera,

    block_highlight_controller: BlockHighlightController,

    #[cfg(debug_assertions)]
    stop_recursing: bool,

    #[cfg(debug_assertions)]
    past_terrains: VecDeque<LogicTree>,

    big_ant: Option<BigAnt>,
}

impl AmalgomLogic {
    pub fn new(logic_comm: LogicComm) -> Self {
        while logic_comm
            .renderer_state
            .recv()
            .expect("Failed to get startup message from render thread")
            != RenderStateNotification::Started
        {}

        // let current_time = std::time::SystemTime::now()
        //     .duration_since(std::time::UNIX_EPOCH)
        //     .expect("Failed to get time")
        //     .as_secs();

        // logic_comm
        //     .robot_keyframes_sender
        //     .send(Keyframe {
        //         state: canned_animation[0].clone(),
        //         time: current_time,
        //     })
        //     .expect("Failed to send keyframe");

        let terrain_manager = TerrainManager::new(
            &logic_comm.startup_info,
            logic_comm.terrain_bufs_sender.clone(),
        );

        let hexapod_sender = logic_comm.hexapod_sender.clone();
        let big_ant = if CONFIG.robot {
            Some(BigAnt::new(hexapod_sender))
        } else {
            None
        };

        let camera = logic_comm.camera_cell.take().unwrap();
        let block_highlight_cell = logic_comm.cursor_highlight_cell.clone();
        let mut this = Self {
            terrain_manager,
            logic_comm,

            inputs: Default::default(),

            camera,

            block_highlight_controller: BlockHighlightController::new(block_highlight_cell),

            #[cfg(debug_assertions)]
            stop_recursing: false,

            #[cfg(debug_assertions)]
            past_terrains: Default::default(),

            big_ant,
        };

        this.terrain_manager
            .lod_perspective(&this.camera.controller.eye)
            .unwrap();

        this
    }

    pub fn start_loop(&mut self) {
        loop {
            // Check for game shut down
            if self.logic_comm.renderer_state.try_recv() == Ok(RenderStateNotification::Stopping) {
                return;
            }

            let mut camera_changed = false;

            if let Some(c) = self.logic_comm.camera_cell.take() {
                self.camera = c;
                camera_changed = true;
            };

            for event in self.logic_comm.input_event_recv.try_iter() {
                // Update inputs
                self.inputs.process_input_event(&event);

                // Update BlockHighlight
                // #[cfg(debug_assertions)]
                // {
                //     log::debug!("{:?}", event);
                // }
                self.block_highlight_controller
                    .handle_input(&event, &mut self.terrain_manager);

                if event
                    == InputEvent::MouseClick(MouseClickEvent {
                        state: ElementState::Pressed,
                        button: winit::event::MouseButton::Left,
                    })
                {
                    if let Some(point) = game_utils::intersection::exact_ray_intersection(
                        &self.camera.controller.eye,
                        &(self.camera.cached_vectors.target - self.camera.controller.eye),
                        &self.terrain_manager.tree,
                        0,
                        None,
                    ) {
                        self.logic_comm
                            .interaction_events
                            .send(InteractionEvent::Grapple(GrappleEvent {
                                target: point,
                                event_time: Instant::now(),
                            }))
                            .unwrap();
                    }
                }

                // Toggle LOD Recurse when you hit L
                #[cfg(debug_assertions)]
                {
                    if let InputEvent::KeyPress(KeyPressEvent {
                        state: ElementState::Pressed,
                        virtual_keycode: key,
                    }) = event
                    {
                        match key {
                            VirtualKeyCode::Key0 => self.stop_recursing = !self.stop_recursing,
                            VirtualKeyCode::Left | VirtualKeyCode::Right => {
                                if let Some(tree) = self.past_terrains.front() {
                                    let bdml =
                                        self.terrain_manager.get_buffer_depth_and_min_level();
                                    replace_with::replace_with_or_abort(
                                        &mut self.terrain_manager,
                                        |tm| {
                                            TerrainManager::with_fields(
                                                tree.clone(),
                                                GenerationDispatcher::new(NoiseManager::default()),
                                                tm.send_buffer,
                                                bdml,
                                            )
                                        },
                                    );
                                    if key == VirtualKeyCode::Left {
                                        log::debug!("Going back in time");
                                        self.past_terrains.rotate_right(1);
                                    } else {
                                        log::debug!("Going forward in time");
                                        self.past_terrains.rotate_left(1);
                                    }
                                }
                            }
                            _ => (),
                        }
                    }
                }
            }

            let target_pos = match self.block_highlight_controller.block_highlight.loc {
                Some(loc) if self.inputs.pressed.t => {
                    let hsize = 2i64.pow(loc.1 as u32) / 2;
                    Some(loc.0 + cgmath::vec3(hsize, hsize, hsize))
                }
                _ => None,
            };

            // ROBOT PROCESSING
            if let Some(ba) = self.big_ant.as_mut() {
                ba.update(Instant::now(), &self.terrain_manager.tree, target_pos);
            }

            // block_highlight_changed might already be true because of key presses, in which case it still needs updating
            // but also update if the camera changed
            if camera_changed || self.block_highlight_controller.changed() {
                self.block_highlight_controller
                    .update(&self.terrain_manager.tree, &self.camera);

                self.block_highlight_controller.flush();
            }

            collision_cage::update_collision_cage(
                &mut self.logic_comm.collision_cage,
                &self.terrain_manager.tree,
            );

            #[cfg(debug_assertions)]
            {
                if self.stop_recursing {
                    continue;
                }
            }
            if let Err(e) = self
                .terrain_manager
                .check_for_and_process_finished_terrain(false)
                .map(|terrain_changed| -> Result<()> {
                    if camera_changed || terrain_changed {
                        self.terrain_manager
                            .lod_perspective(&self.camera.controller.eye)?;

                        #[cfg(debug_assertions)]
                        {
                            if let Some(num_copies) = base_utils::CONFIG.time_travel {
                                self.past_terrains
                                    .push_front(self.terrain_manager.tree.clone());
                                if self.past_terrains.len() > num_copies {
                                    self.past_terrains.pop_back();
                                }
                            }
                        }
                    }
                    Ok(())
                })
            {
                log::error!("{:?}", e);

                #[cfg(debug_assertions)]
                {
                    self.stop_recursing = true;

                    use rodio::source::Source;

                    let device = rodio::default_output_device().unwrap();
                    rodio::play_raw(
                        &device,
                        rodio::source::SineWave::new(523)
                            .take_duration(std::time::Duration::from_millis(500)),
                    );
                }
                #[cfg(not(debug_assertions))]
                {
                    panic!("{:?}", e);
                }
            }

            self.inputs.new_tick();
        }
    }
}
