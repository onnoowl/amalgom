use base_utils::{rwcube::RwCubeWriter, COLLISION_CAGE_SIZE};
use logic_tree::LogicTree;
use octree::{traits::IsAir, GetError, NotPresent};

pub fn update_collision_cage(
    collision_cage: &mut RwCubeWriter<bool, COLLISION_CAGE_SIZE>,
    tree: &LogicTree,
) {
    collision_cage.fill_in(|pos| match tree.get_block(pos) {
        Ok(leaf) => Some(!leaf.is_air()),
        Err(GetError::NotPresent(NotPresent::IsHighLevelLeaf {
            high_level_leaf_node,
            ..
        })) => Some(!high_level_leaf_node.get_leaf().unwrap().is_air()),
        _ => None,
    })
}
