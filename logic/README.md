# Logic

This crate has code for the game's mechanics that are independent from visuals. For example, things like player collisions will be found here. Eventually, what is in this crate will define what operations the server will be responsible for.

## Modules

[block_highlight_controller](src/block_highlight_controller.rs) - Controls everything about where the block_highlight will be placed

[build_mesh](src/build_mesh.rs) - Tools for gathering the vertices and indices required to draw something in the game

[flags](src/flags.rs) - Bit flag definitions for descriptors for a LogicNode

[generation](src/generation.rs) - Terrain generation through random noise

[terrain_manager](src/terrain_manager.rs) - Identifies new terrain to be generated and manages their placement

[tree](src/tree.rs) - Contains definitions for Logic-specific implementations of octree and octnode structs
