pub use anyhow::anyhow as err;
pub use anyhow::Context;

pub type Result<T> = anyhow::Result<T>;
pub type Error = anyhow::Error;

#[macro_export]
macro_rules! bail {
    ($($args:expr),*) => {
        Err(reporting::err!($($args),*))?
    };
}

#[macro_export]
macro_rules! ensure {
    ($cond:expr, $($args:expr),*) => {
        if !($cond) {
            reporting::bail!($($args),*)
        }
    };
}

#[macro_export]
macro_rules! debug_ensure {
    ($($args:expr),*) => {
        #[cfg(debug_assertions)]
        reporting::ensure!($($args),*)
    };
}
