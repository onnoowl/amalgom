# Amalgom

Amalgom is an early prototype for a voxel game engine.

## What's Different?


### Octrees & LOD

Amalgom uses octrees to store its world (with no chunks). This allows for powerful & dynamic level-of-detail (LOD) management that lets you see very distant mountains at 60 fps even on an average laptop. Terrain generation speed is also drastically improved, since terrain can be sampled at a low level of detail at further distances. Other benefits of octrees include low memory usage, low disk usage, and (eventually) performant ray tracing.

### WebGPU

Amalgom is made with WebGPU, which allows for excellent multiplatform native performance, while also making it possible to run Amalgom in the browser (work on this is still underway).

## Screenshots

![Screenshot of grassy hills](screenshots/screenshot1.png)

Grassy hills and LOD terrain using multi ridged noise. Blocks further away are physically larger, producing an LOD effect, and allowing for a render distance exponentially larger than Minecraft's.

![Screenshot of stony caves](screenshots/screenshot2.png)

Complex 3D caves using super simplex noise.

![Robot Walking GIF](https://media.giphy.com/media/YfWEGHqEjH64BLsANM/giphy.gif)

A hexapod robot performing a walking animation (see Active Work below)

Note: The current lighting system is temporary, and for debug use only.

## To Run

```
cargo run --release
```

## Architecture

Amalgom has a render thread, a logic thread, and a worker pool.

### Render Thread
Responsibilities:
1) Rendering
2) Camera movement/input
3) TODO: Collision detection

### Logic Thread
Responsibilities:
1) Core Game Logic
2) Terrain Loading and Management

### Worker Pool
Currently performs terrain generation work, but will eventually parallelize game logic work dispatched from the logic thread too.

## Active Work

### Robotics simulation

There is ongoing work to use this voxel engine as a simple robotics simulator. So far, we've built a LIDAR simulator, and a kinematics & inverse kinematics system that works for N-legged robots with 2 segment legs. The GIF above shows an early prototype performing a walking animation in mid air. The next step is to combine this work with the LIDAR simulation to dynamically determine foot placement.

## Building on Linux
The following packages must be installed to build on linux (at least on Ubuntu, tested on PopOS)
```
sudo apt install libasound2-dev libx11-dev
```
