//! # Dependencies
//!
//! [render](render) - Code for the game's visual rendering
//!
//! [logic](logic) - Code for the game's mechanics that are independent from visuals
mod logger;

use game_utils::thread_comm;
use logic::AmalgomLogic;

use std::thread;

fn main() {
    flexi_logger::Logger::with_env_or_str("debug,wgpu_core=warn,game_utils=debug,gfx=info,gfx_memory::heaps::heap=info,gfx_memory::heaps=none,naga::front::spv=warn,hotwatch=warn")
        .log_target(flexi_logger::LogTarget::Writer(Box::new(
            logger::Logger::new("debug.log"),
        )))
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    // let mut plugins = PluginManager::new();
    // unsafe {
    //     plugins
    //         .load_plugin("target/debug/libsample_plugin.dylib")
    //         .expect("Failed to load sample_plugin");
    // }

    let (render_comm, logic_comm) = thread_comm::new(Default::default());

    let thread = thread::spawn(|| {
        AmalgomLogic::new(logic_comm).start_loop();
    });

    render::framework::run::<render::renderer::AmalgomRenderer>("Amalgom", render_comm);

    thread.join().unwrap();

    log::logger().flush();
}
