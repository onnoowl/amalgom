// use chrono::naive::NaiveTime;
use flexi_logger::{writers::LogWriter, DeferredNow};
use log::{LevelFilter, Record};
use std::{
    // collections::VecDeque,
    fs::File,
    io::{BufWriter, Result, Write},
    path::Path,
    sync::Mutex,
};
// use wgpu::{CommandEncoder, Device, TextureFormat};

// const NUM_MESSAGES: usize = 80;

// pub struct ScreenLogger {
//     // recent: VecDeque<OwnedText>,
// // glyph_brush: GlyphBrush<(), ab_glyph::FontArc>,
// // cached_section: Option<OwnedSection>,
// }

// impl ScreenLogger {
//     pub fn new(device: &Device, format: TextureFormat) -> Arc<Mutex<Self>> {
//         // let font = ab_glyph::FontArc::try_from_slice(include_bytes!("fonts/Inconsolata.ttf"))
//         //     .expect("Load font");

//         // let glyph_brush = GlyphBrushBuilder::using_font(font)
//         //     // .cache_redraws(false) // TODO?
//         //     .texture_filter_method(wgpu::FilterMode::Nearest)
//         //     .build(&device, format);

//         Arc::new(Mutex::new(ScreenLogger {
//             // recent: Default::default(),
//             // glyph_brush,
//             // cached_section: None,
//         }))
//     }

//     fn write(&mut self, time: &NaiveTime, record: &Record) {
//         if record.level() == log::Level::Trace {
//             return;
//         }

//         // self.recent.push_front(
//         //     OwnedText::new(format!(
//         //         "{} {}:{} -- {}\n",
//         //         time,
//         //         record.level(),
//         //         record.target(),
//         //         record.args()
//         //     ))
//         //     .with_color(match record.level() {
//         //         log::Level::Error => [1.0, 0.4, 0.3, 1.0],
//         //         log::Level::Warn => [1.0, 0.55, 0.2, 1.0],
//         //         log::Level::Info => [0.6, 1.0, 0.6, 1.0],
//         //         log::Level::Debug => [0.9, 0.9, 0.9, 1.0],
//         //         log::Level::Trace => [0.7, 0.7, 0.7, 1.0],
//         //     }),
//         // );
//         // if self.recent.len() > NUM_MESSAGES {
//         //     self.recent.pop_back();
//         // }

//         // self.cached_section = None;
//     }

//     pub fn draw(
//         &mut self,
//         device: &Device,
//         encoder: &mut CommandEncoder,
//         frame: &wgpu::SwapChainFrame,
//         width: u32,
//         height: u32,
//     ) {
//         // if self.cached_section.is_none() {
//         //     self.cached_section = Some(OwnedSection {
//         //         screen_position: (10.0, 10.0),
//         //         text: self.recent.iter().map(|t| t.clone()).collect(),
//         //         ..OwnedSection::default()
//         //     });
//         // }
//         // let section = self.cached_section.as_ref().unwrap();

//         // self.glyph_brush.queue(section.to_borrowed());

//         // self.glyph_brush
//         //     .draw_queued(&device, encoder, &frame.view, width, height)
//         //     .unwrap();
//     }
// }

pub struct Logger {
    file_log: Mutex<BufWriter<File>>,
    // screen_log: Arc<Mutex<ScreenLogger>>,
}

impl Logger {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        Logger {
            file_log: Mutex::new(BufWriter::new(
                File::create(path).expect("Failed to create logging file."),
            )),
            // screen_log,
        }
    }
}

impl LogWriter for Logger {
    fn write(&self, now: &mut DeferredNow, record: &Record) -> Result<()> {
        let time = now.now().time();
        // self.screen_log.lock().unwrap().write(&time, record);

        // Print to STDOUT too
        println!(
            "{} {}:{} -- {}",
            time,
            record.level(),
            record.target(),
            record.args()
        );

        writeln!(
            self.file_log.lock().unwrap(),
            "{} {}:{} -- {}",
            time,
            record.level(),
            record.target(),
            record.args()
        )
    }

    fn flush(&self) -> Result<()> {
        self.file_log.lock().unwrap().flush()
    }

    fn max_log_level(&self) -> LevelFilter {
        LevelFilter::Trace
    }
}
