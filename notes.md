# Fast Lighting
The following lighting techniques can be summed together.

## Sun Lighting

### Sun-to-Block Lighting

### Sun-to-Pixel lighting

## Torch Lighting

### Torch-to-Block lighting
Minecraft style?

### Torch-to-Pixel lighting
Too expensive?

# Storage

- Dense octal trees?
    - Chunks and super chunks?
- 3D rotating dense indirection table?
    - Table of chunk location triples (x, y, z) to Box pointers.
        - Avoids hashmap lookup or something slower like that
    -

# Marketplace

## Transparent Plugin Data Accessing
- Plugins already need to declare what data they read/write too...
- This could be published on the market place, so that users know what kind of shit the plugin does.
- This could help prevent malicious plugins, and aid in auditing.

# Gameplay

## Dragons!!!

## Robust NPC system

## Robust questing system



Straight forward boolean logic:
    4 bytes per octal node
    3 &&s per side
    6 sides
    So 24 ops per octal


8 & bitmasks on subnodes
7 &s to combine
So 15 ops total
1 byte per octal node (only need the 4 bits)

//up, down, left, right, front, back
//udlrfb
& 010101 //drb
& 010110 //drf
& 011001 //dlb
& 011010 //dlf
& 100101 //urb
& 100110 //urf
& 101001 //ulb
& 101010 //ulf

## Combat
- Directional melee and blocking.
-