#![feature(test)]

extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    use noise::{NoiseFn, SuperSimplex};
    use simdnoise::NoiseBuilder;

    #[bench]
    fn noise_lib(b: &mut Bencher) {
        let simplex = SuperSimplex::new(0);

        b.iter(|| for _ in 0..(64 * 64 * 64) {simplex.get([42.4, 37.7, 2.8]); });
    }

    #[bench]
    fn simdnoise_lib(b: &mut Bencher) {
        b.iter(|| NoiseBuilder::gradient_3d(64, 64, 64).generate_scaled(0.0,1.0) );
    }
}
