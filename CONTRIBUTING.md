# Style

All files follow a common order, with each item separated by whitespace:
1) `mod` declarations
2) `use` statements for the submodules
3) `use` statements for other Amalgom code (in the current crate, or other crates)
4) `use` statements for external code
5) `const` and `type` declarations
6) The main code body

For example:
```rust
mod distorter; // mod declarations
mod noise_manager;
mod ridged_multi;

pub use noise_manager::NoiseManager; // use statements for the submodules

use crate::tree::LogicLeaf; // use statements for other Amalgom code
use base_utils::{ICoord, OCTAL_OFFSETS};
use octree::{
    traits::{HasID, HasMaxDataLOD, HasMode, HasNoUnloaded},
    SparseOctalNode,
};

use extend::ext; // use statements for external code
use std::fmt::Debug;

const BIG_CAVE_FREQUENCY: f64 = 150.; // const and type declarations
const HEIGHT_SCALAR: f64 = 70.;

// ... Rest of code here
```

Merged imports are preferred over split imports.
