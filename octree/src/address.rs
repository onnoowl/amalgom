//! Defines the structure and functions for addresses that represent a location in an octree.
//!
//! An address is made up of 16 u8s. Starting from the root node, each byte represents the index of the next child to recurse into. Fot the order of these indices, see OCTAL_OFFSETS.

use crate::error::*;
use arrayvec::ArrayVec;
use base_utils::{ICoord, UCoord};

/// This "addresses" into the tree, and indicates the path of pointer traversals to get to a node.
/// An address is only valid as long as the root node doesn't move. If the root node moves, this will point to an unexpected node.
pub type Address = ArrayVec<[u8; 16]>;

pub fn out_of_bounds(offset: &UCoord, size: u64) -> bool {
    offset.x >= size || offset.y >= size || offset.z >= size
}

pub fn out_of_bounds_check(offset: &UCoord, size: u64) -> Result<(), OutOfBounds> {
    if out_of_bounds(offset, size) {
        Err(OutOfBounds())
    } else {
        Ok(())
    }
}

pub fn offset_to_address(
    offset: &UCoord,
    address: &mut Address,
    this_nodes_level: u32,
    target_level: u32,
) -> Result<(), OutOfBounds> {
    out_of_bounds_check(offset, 2u64.pow(this_nodes_level))?;
    offset_to_address_unchecked(offset, address, this_nodes_level, target_level);
    Ok(())
}

/// Offset must fit within the bounds of size, where size = 2^root_level
/// Use `out_of_bounds(...)` to check.
pub fn offset_to_address_unchecked(
    offset: &UCoord,
    address: &mut Address,
    this_nodes_level: u32,
    target_level: u32,
) {
    debug_assert_eq!(offset.x >> this_nodes_level, 0);
    debug_assert_eq!(offset.y >> this_nodes_level, 0);
    debug_assert_eq!(offset.z >> this_nodes_level, 0);
    address.clear();
    for i in ((target_level as u64)..(this_nodes_level as u64)).rev() {
        address.push(
            (((offset.x >> i) & 1) | (((offset.y >> i) & 1) << 1) | (((offset.z >> i) & 1) << 2))
                as u8,
        );
    }
}

pub fn coord_to_address(
    coord: &ICoord,
    base_coord: &ICoord,
    address: &mut Address,
    this_nodes_level: u32,
    target_level: u32,
) -> Result<(), OutOfBounds> {
    let offset = &(coord - base_coord).cast::<u64>().ok_or(OutOfBounds())?;
    offset_to_address(offset, address, this_nodes_level, target_level)?;
    Ok(())
}

// out_of_bounds tests
#[cfg(test)]
mod tests {

    use super::*;

    // This is the runner used by all test cases
    // It will convert boolean values depicting the values of the clauses
    // into the desired inputs to achieve those values for the clauses
    // xout - whether the x value of the offset should be out of bounds
    // yout - whether the y value of the offset should be out of bounds
    // zout - whether the z value of the offset should be out of bounds
    // expected - the expected predicate given these inputs
    fn out_of_bounds_runner(xout: bool, yout: bool, zout: bool, expected: bool) {
        let xoffset = (xout as u64) * 100;
        let yoffset = (yout as u64) * 100;
        let zoffset = (zout as u64) * 100;
        let offset: UCoord = UCoord::new(xoffset, yoffset, zoffset);
        assert_eq!(expected, out_of_bounds(&offset, 10_u64));
    }

    // x as major clause
    //  minor clause y = false
    //  minor clause z = false
    // Major clause = true so x will be out of bounds.
    // Predicate will return true
    #[test]
    fn out_of_bounds_test_x_t() {
        out_of_bounds_runner(true, false, false, true);
    }

    // x as major clause
    //  minor clause y = false
    //  minor clause z = false
    // Major clause = false so no dimensions will be out of bounds.
    // Predicate will return false
    #[test]
    fn out_of_bounds_test_x_f() {
        out_of_bounds_runner(false, false, false, false);
    }

    // y as major clause
    //  minor clause x = false
    //  minor clause z = false
    // Major clause = true so y will be out of bounds.
    // Predicate will return true
    #[test]
    fn out_of_bounds_test_y_t() {
        out_of_bounds_runner(false, true, false, true);
    }

    // y as major clause
    //  minor clause x = false
    //  minor clause z = false
    // Major clause = false so no dimensions will be out of bounds.
    // Predicate will return false
    #[test]
    fn out_of_bounds_test_y_f() {
        out_of_bounds_runner(false, false, false, false);
    }

    // z as major clause
    //  minor clause x = false
    //  minor clause y = false
    // Major clause = true so z will be out of bounds.
    // Predicate will return true
    #[test]
    fn out_of_bounds_test_z_t() {
        out_of_bounds_runner(false, false, true, true);
    }

    // z as major clause
    //  minor clause x = false
    //  minor clause y = false
    // Major clause = false so no dimensions will be out of bounds.
    // Predicate will return false
    #[test]
    fn out_of_bounds_test_z_f() {
        out_of_bounds_runner(false, false, false, false);
    }
}
