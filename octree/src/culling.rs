//! Handles culling-related structures and functions to mark faces on octnodes that have non-air neighbors.
//!
//! This module is currently only used within the logic crate.

use crate::{
    traits::HasFaceData, traits::HasMode, traits::IsAir, LeafOrBranchDataMut::*, Occlusion,
    OctNode, QuadTree,
};
use base_utils::Face;

use bitflags::bitflags;
use std::{borrow::Cow, fmt::Debug};

#[derive(Debug, Default, Copy, Clone, PartialEq)]
pub struct CullingInfo {
    visible_sides: SidesBitFlags,
}

#[derive(Debug, Clone, PartialEq)]
pub enum FaceData {
    Compact {
        culling_info: CullingInfo,
        lighting_info: BlockLightingData,
    },
    QuadTree(Box<[QuadTree; 6]>),
}

impl Default for FaceData {
    fn default() -> Self {
        FaceData::Compact {
            culling_info: Default::default(),
            lighting_info: Default::default(),
        }
    }
}

impl FaceData {
    pub fn is_compact(&self) -> bool {
        match self {
            FaceData::Compact {
                culling_info: _,
                lighting_info: _,
            } => true,
            FaceData::QuadTree(_) => false,
        }
    }

    pub fn is_quadtree(&self) -> bool {
        !self.is_compact()
    }

    pub fn get_side(&self, face: &Face) -> Cow<'_, QuadTree> {
        match self {
            FaceData::Compact {
                culling_info,
                lighting_info,
            } => {
                if culling_info.visible_sides.is_visible(*face) {
                    Cow::Owned(QuadTree::Leaf(Occlusion::Visible(
                        lighting_info.get_side(face),
                    )))
                } else {
                    Cow::Owned(QuadTree::Leaf(Occlusion::Occluded))
                }
            }
            FaceData::QuadTree(quads) => Cow::Borrowed(&quads[*face as usize]),
        }
    }

    pub fn is_side_visible(&self, face: &Face) -> bool {
        match self {
            FaceData::Compact {
                culling_info,
                lighting_info: _,
            } => {
                if culling_info.visible_sides.is_visible(*face) {
                    return true;
                }
                return false;
            }
            FaceData::QuadTree(quads) => {
                return quads[*face as usize].is_ever_visible();
            }
        }
    }

    pub fn mark_side_visible(&mut self, face: &Face) {
        match self {
            FaceData::Compact {
                culling_info,
                lighting_info: _,
            } => {
                culling_info.visible_sides |= SidesBitFlags::from(*face);
            }
            FaceData::QuadTree(quads) => {
                quads[*face as usize] = QuadTree::Leaf(Occlusion::Visible(Default::default()));
            }
        }
    }

    pub fn get_quadtrees_or_upgrade(&mut self) -> &mut [QuadTree; 6] {
        if self.is_compact() {
            self.upgrade_to_quadtree();
        }
        match self {
            FaceData::Compact {
                culling_info: _,
                lighting_info: _,
            } => unreachable!(),
            FaceData::QuadTree(q) => q,
        }
    }

    pub fn upgrade_to_quadtree(&mut self) {
        match self {
            FaceData::Compact {
                culling_info,
                lighting_info,
            } => {
                *self = FaceData::QuadTree(Box::new([
                    QuadTree::from({
                        let face = Face::from(0u8);
                        (
                            culling_info.visible_sides.is_visible(face),
                            lighting_info.get_side(&face),
                        )
                    }),
                    QuadTree::from({
                        let face = Face::from(1u8);
                        (
                            culling_info.visible_sides.is_visible(face),
                            lighting_info.get_side(&face),
                        )
                    }),
                    QuadTree::from({
                        let face = Face::from(2u8);
                        (
                            culling_info.visible_sides.is_visible(face),
                            lighting_info.get_side(&face),
                        )
                    }),
                    QuadTree::from({
                        let face = Face::from(3u8);
                        (
                            culling_info.visible_sides.is_visible(face),
                            lighting_info.get_side(&face),
                        )
                    }),
                    QuadTree::from({
                        let face = Face::from(4u8);
                        (
                            culling_info.visible_sides.is_visible(face),
                            lighting_info.get_side(&face),
                        )
                    }),
                    QuadTree::from({
                        let face = Face::from(5u8);
                        (
                            culling_info.visible_sides.is_visible(face),
                            lighting_info.get_side(&face),
                        )
                    }),
                ]))
            }
            FaceData::QuadTree(_) => (),
        }
    }
}

#[derive(Debug, Default, Clone, PartialEq)]
pub struct BlockLightingData {}

impl BlockLightingData {
    pub fn get_side(&self, _face: &Face) -> QuadLightingData {
        QuadLightingData::default() // TODO
    }
}

#[derive(Debug, Default, Clone, PartialEq)]
pub struct QuadLightingData {}

impl CullingInfo {
    pub fn has_neighbor(&self, f: Face) -> bool {
        !self.visible_sides.is_visible(f)
    }
}

fn cull_leaf_and_leaf<L: HasFaceData + IsAir>(
    leaf1: &mut L,
    leaf2: &mut L,
    dir_of_1: &Face,
    dir_of_2: &Face,
) {
    match (leaf1.is_air(), leaf2.is_air()) {
        (true, false) => {
            leaf2.get_face_data_mut().mark_side_visible(dir_of_1);
        }
        (false, true) => {
            leaf1.get_face_data_mut().mark_side_visible(dir_of_2);
        }
        _ => {}
    }
}

/// `face` is the side of the branch to mark as mark_visible
pub fn branch_mark_side_visible<
    L: Debug + HasFaceData + IsAir,
    B: Debug + HasMode,
    U: Debug + Default,
>(
    branch: &mut B,
    children: &mut [OctNode<L, B, U>],
    face: &Face,
) {
    branch
        .get_mode_mut()
        .map(|m| m.get_face_data_mut().mark_side_visible(face));

    face.get_zipped_octal_and_quad()
        .for_each(
            |(octal_index, _quad_index)| match &mut children[octal_index] {
                OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => {
                    l.get_face_data_mut().mark_side_visible(face)
                }
                OctNode::Branch(b, c) => branch_mark_side_visible(b, c.as_mut(), face),
                OctNode::Unloaded(_) => {} //do nothing
            },
        )
}

/// `face` is the side of the leaf to build the quad tree on
fn quadtree_recurse<L: Debug + HasFaceData + IsAir, B: Debug + HasMode, U: Debug + Default>(
    branch_data: &mut B,
    children: &mut [OctNode<L, B, U>],
    quad: &mut QuadTree,
    face: &Face,
) {
    let (quad_mode, quad_children) = quad.make_branch_and_get_mut();
    if let Some(branch_mode) = branch_data.get_mode() {
        if branch_mode.is_air() || {
            // Or if any on the lower level are air
            // This fixes culling bugs for quad trees at LOD transitions
            // TODO: Maybe only do this on actual borders, rather then all the time?
            face.flip()
                .get_zipped_octal_and_quad()
                .any(|(child_offset, _)| match &children[child_offset] {
                    OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => l.is_air(),
                    OctNode::Branch(bd, _) => bd.get_mode().map(|m| m.is_air()).unwrap_or(false),
                    OctNode::Unloaded(_) => {
                        log::error!("Found unloaded in quadtree_recurse");
                        true
                    }
                })
        } {
            *quad_mode = Some(Occlusion::Visible(Default::default()))
        } else {
            *quad_mode = Some(Occlusion::Occluded)
        } // todo make this work with LOD transitions
    }

    face.flip().get_zipped_octal_and_quad().for_each(
        |(octal_index, quad_index)| match &mut children[octal_index] {
            OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => {
                if l.is_air() {
                    quad_children[quad_index] =
                        QuadTree::Leaf(Occlusion::Visible(Default::default()))
                }
            }
            OctNode::Branch(b_d, c) => {
                quadtree_recurse(b_d, c.as_mut(), &mut quad_children[quad_index], face)
            }
            OctNode::Unloaded(_) => {} //do nothing
        },
    );

    if let QuadTree::Leaf(first_leaf) = &quad_children[0] {
        // We must ensure that the quad mode equals the quad leaves before attempting hoisting
        let equals_quad_mode = if let Some(some_quad_mode) = quad_mode.as_ref() {
            first_leaf == some_quad_mode
        } else {
            true
        };

        if equals_quad_mode && quad_children[1..].iter().all(|q| q == &quad_children[0]) {
            *quad = quad_children[0].take(); //hoisting
        }
    }
}

fn cull_leaf_and_branch<L: Debug + HasFaceData + IsAir, B: Debug + HasMode, U: Debug + Default>(
    leaf_data: &mut L,
    branch_data: &mut B,
    children: &mut [OctNode<L, B, U>],
    dir_of_leaf: &Face,
    dir_of_branch: &Face,
) {
    if leaf_data.is_air() {
        branch_mark_side_visible(branch_data, children, dir_of_leaf);
    } else {
        let quad =
            &mut leaf_data.get_face_data_mut().get_quadtrees_or_upgrade()[*dir_of_branch as usize];
        quadtree_recurse(branch_data, children, quad, dir_of_branch)
    }
}

/// Unlike much of the culling here, this one is not bi-directional
fn cull_mode_and_branch<
    M: HasFaceData + IsAir,
    L: Debug + HasFaceData + IsAir,
    B: Debug + HasMode,
    U: Debug + Default,
>(
    mode: &mut M,
    other_branch_mode: &M,
    other_branch_children: &[OctNode<L, B, U>],
    dir_of_other_branch: &Face,
) {
    if other_branch_mode.is_air()
        || dir_of_other_branch.flip().get_zipped_octal_and_quad().any(
            |(octal_index, _quad_index)| match &other_branch_children[octal_index] {
                OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => l.is_air(),
                OctNode::Branch(b, _) => b.get_mode().map(|m| m.is_air()) == Some(true), // TODO, think about this harder
                OctNode::Unloaded(_) => false,
            },
        )
    {
        mode.get_face_data_mut()
            .mark_side_visible(dir_of_other_branch)
    }
}

fn cull_branch_and_branch<
    L: Debug + HasFaceData + IsAir,
    B: Debug + HasMode,
    U: Debug + Default,
>(
    branch1: &mut B,
    children1: &mut [OctNode<L, B, U>],
    branch2: &mut B,
    children2: &mut [OctNode<L, B, U>],
    dir_of_1: &Face,
    dir_of_2: &Face,
) {
    // TODO: What if these are not both some? We could still do mode culling based on the children
    if let (Some(mode1), Some(mode2)) = (branch1.get_mode_mut(), branch2.get_mode_mut()) {
        // cull_leaf_and_leaf(mode1, mode2, dir_of_1, dir_of_2); // old behavior
        cull_mode_and_branch(mode1, mode2, children2, dir_of_2);
        cull_mode_and_branch(mode2, mode1, children1, dir_of_1);
    }
    dir_of_2.get_zip_indices().for_each(|(index1, index2)| {
        cull_pair(&mut children1[index1], &mut children2[index2], dir_of_2)
    })
}

/// `face` is child2's relation to child1 from child1's perspective
pub fn cull_pair<L: Debug + HasFaceData + IsAir, B: Debug + HasMode, U: Debug + Default>(
    node1: &mut OctNode<L, B, U>,
    node2: &mut OctNode<L, B, U>,
    dir_of_node2_from_node1: &Face,
) {
    let dir_of_2 = *dir_of_node2_from_node1; // node2's direction from node1's perspective
    let dir_of_1 = dir_of_node2_from_node1.flip(); //node1's direction from node2's perspective

    match (node1.get_data_mut(), node2.get_data_mut()) {
        (Some(data1), Some(data2)) => match (data1, data2, dir_of_1, dir_of_2) {
            (LeafDataMut(leaf1), LeafDataMut(leaf2), _, _) => {
                cull_leaf_and_leaf(leaf1, leaf2, &dir_of_1, &dir_of_2)
            }
            (LeafDataMut(leaf), BranchDataMut((branch, children)), dir_of_leaf, dir_of_branch)
            | (BranchDataMut((branch, children)), LeafDataMut(leaf), dir_of_branch, dir_of_leaf) => {
                cull_leaf_and_branch(
                    leaf,
                    branch,
                    children.as_mut(),
                    &dir_of_leaf,
                    &dir_of_branch,
                )
            }
            (BranchDataMut((branch1, children1)), BranchDataMut((branch2, children2)), _, _) => {
                cull_branch_and_branch(
                    branch1,
                    children1.as_mut(),
                    branch2,
                    children2.as_mut(),
                    &dir_of_1,
                    &dir_of_2,
                )
            }
        },
        (None, _) | (_, None) => {} // Unloadeds
    }
}

impl<L: Debug + HasFaceData + IsAir, B: Debug + HasMode, U: Debug + Default> OctNode<L, B, U> {
    pub fn internal_culling(&mut self) {
        if let OctNode::Branch(_, children) = self {
            for pairing_dim in 0..=2u8 {
                // //non_pairing dimension a
                for np_dim_a in 0..=1u8 {
                    //non_pairing dimension b
                    for np_dim_b in 0..=1u8 {
                        let index1 = match pairing_dim {
                            0 => (np_dim_a << 2) | (np_dim_b << 1),
                            1 => (np_dim_a << 2) | (np_dim_b),
                            2 => (np_dim_a << 1) | (np_dim_b),
                            _ => unreachable!(),
                        };
                        let index2 = index1 ^ (1 << pairing_dim);
                        let (slice1, slice2) = children.split_at_mut(index2 as usize);
                        let child1 = &mut slice1[index1 as usize];
                        let child2 = &mut slice2[0];

                        let face = Face::from_two_indices(index1, index2).unwrap();
                        cull_pair(child1, child2, &face);
                    }
                }
            }
            for c in children.iter_mut() {
                c.internal_culling()
            }
        }
    }
}

bitflags! {
    pub struct SidesBitFlags: u8 {
        const NONE   = 0b00000000;
        const FRONT  = 0b00000001;
        const BACK   = 0b00000010;
        const LEFT   = 0b00000100;
        const RIGHT  = 0b00001000;
        const TOP    = 0b00010000;
        const BOTTOM = 0b00100000;
    }
}

impl SidesBitFlags {
    /// Check if the bit for a particular face is "visible"
    pub fn is_visible(&self, f: Face) -> bool {
        *self & SidesBitFlags::from(f) != SidesBitFlags::NONE
    }

    pub fn sides_for_oct_index(oct_index: u8) -> SidesBitFlags {
        let x = if oct_index & 0b001 != 0 {
            SidesBitFlags::LEFT
        } else {
            SidesBitFlags::RIGHT
        };
        let y = if oct_index & 0b010 != 0 {
            SidesBitFlags::TOP
        } else {
            SidesBitFlags::BOTTOM
        };
        let z = if oct_index & 0b100 != 0 {
            SidesBitFlags::BACK
        } else {
            SidesBitFlags::FRONT
        };

        x | y | z
    }
}

impl From<Face> for SidesBitFlags {
    fn from(f: Face) -> Self {
        match f {
            Face::Front => SidesBitFlags::FRONT,
            Face::Back => SidesBitFlags::BACK,
            Face::Left => SidesBitFlags::LEFT,
            Face::Right => SidesBitFlags::RIGHT,
            Face::Top => SidesBitFlags::TOP,
            Face::Bottom => SidesBitFlags::BOTTOM,
        }
    }
}

impl Default for SidesBitFlags {
    fn default() -> Self {
        SidesBitFlags::NONE
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct IsOnEdge(SidesBitFlags);

impl IsOnEdge {
    pub fn new() -> Self {
        Self(SidesBitFlags::all())
    }

    pub fn is_still_on_edge(&self) -> bool {
        self.0 != SidesBitFlags::NONE
    }

    #[must_use]
    pub fn descend(self, oct_index: u8) -> Self {
        Self(self.0 & SidesBitFlags::sides_for_oct_index(oct_index))
    }
}
