//! Defines the nodes that can exist on an octree.
//!
//!They come in four variants:
//!
//! - Leaf -> Leaf that is considered infinite precision. u8 represents the level of this leaf +1 since depth = level+1.
//! - LODLeaf -> Leaf that is an approximation of the real content. u8 is always >=1 and represents how this leaf has been hoisted
//! - Branch -> Stores the 8 octal children
//! - Unloaded -> No content. Dead end.

pub mod leaf_or_branch_data;

pub use leaf_or_branch_data::{
    LeafOrBranchData::{self, *},
    LeafOrBranchDataMut::{self, *},
};

use crate::{address::*, error::*, traits::*};
use base_utils::UCoord;

use derivative::Derivative;
#[allow(unused_imports)]
use log::*;
use std::fmt::Debug;

#[derive(Derivative, Clone, PartialEq)]
#[derivative(Debug)]
pub enum OctNode<L: Debug, B: Debug, U: Debug + Default> {
    Leaf(L),
    LODLeaf(L, u8),
    Branch(
        B,
        #[derivative(Debug = "ignore")] Box<[OctNode<L, B, U>; 8]>,
    ),
    Unloaded(U),
}

use OctNode::*;

impl<L: Debug, B: Debug, U: Debug + Default> OctNode<L, B, U> {
    pub fn is_unloaded(&self) -> bool {
        match self {
            Unloaded(_) => true,
            _ => false,
        }
    }

    pub fn take(&mut self) -> Self {
        let mut result = Default::default();
        std::mem::swap(self, &mut result);
        result
    }

    pub fn get_data(&self) -> Option<LeafOrBranchData<L, B, U>> {
        match self {
            Leaf(l) | LODLeaf(l, _) => Some(LeafData(l)),
            Branch(b, c) => Some(BranchData((b, c))),
            Unloaded(_) => None,
        }
    }

    pub fn get_data_mut(&mut self) -> Option<LeafOrBranchDataMut<L, B, U>> {
        match self {
            Leaf(l) | LODLeaf(l, _) => Some(LeafDataMut(l)),
            Branch(b, c) => Some(BranchDataMut((b, c))),
            Unloaded(_) => None,
        }
    }

    pub fn get_leaf(&self) -> Option<&L> {
        match self {
            Leaf(l) | LODLeaf(l, _) => Some(l),
            Branch(_, _) => None,
            Unloaded(_) => None,
        }
    }

    pub fn get_leaf_mut(&mut self) -> Option<&mut L> {
        match self {
            Leaf(l) | LODLeaf(l, _) => Some(l),
            Branch(_, _) => None,
            Unloaded(_) => None,
        }
    }

    pub fn get_unloaded(&self) -> Option<&U> {
        match self {
            Leaf(_) | LODLeaf(_, _) | Branch(_, _) => None,
            Unloaded(u) => Some(u),
        }
    }

    pub fn get_unloaded_mut(&mut self) -> Option<&mut U> {
        match self {
            Leaf(_) | LODLeaf(_, _) | Branch(_, _) => None,
            Unloaded(u) => Some(u),
        }
    }

    pub fn into_leaf(self) -> Option<L> {
        match self {
            Leaf(l) | LODLeaf(l, _) => Some(l),
            Branch(_, _) => None,
            Unloaded(_) => None,
        }
    }

    pub fn get_branch(&self) -> Option<(&B, &[OctNode<L, B, U>; 8])> {
        match self {
            Leaf(_) | LODLeaf(_, _) => None,
            Branch(b, c) => Some((b, c)),
            Unloaded(_) => None,
        }
    }

    pub fn get_branch_mut(&mut self) -> Option<(&mut B, &mut [OctNode<L, B, U>; 8])> {
        match self {
            Leaf(_) | LODLeaf(_, _) => None,
            Branch(b, c) => Some((b, c)),
            Unloaded(_) => None,
        }
    }

    pub fn into_branch(self) -> Option<(B, Box<[OctNode<L, B, U>; 8]>)> {
        match self {
            Leaf(_) | LODLeaf(_, _) => None,
            Branch(b, c) => Some((b, c)),
            Unloaded(_) => None,
        }
    }

    pub fn get_block(
        &self,
        offset: &UCoord,
        this_nodes_level: u32,
    ) -> Result<&L, GetError<'_, L, B, U>> {
        self.get_node(offset, this_nodes_level, 0).map(|node|
            node.get_leaf().expect("Didn't get a leaf when retrieving level 0 block node. SparseOctalTree was constructed incorrectly.")
        )
    }

    pub fn get_block_mut(
        &mut self,
        offset: &UCoord,
        this_nodes_level: u32,
    ) -> Result<&mut L, GetMutError<'_, L, B, U>> {
        self.get_node_mut(offset, this_nodes_level, 0).map(|node|
            node.get_leaf_mut().expect("Didn't get a leaf when retrieving level 0 block node. SparseOctalTree was constructed incorrectly.")
        )
    }

    pub fn get_node(
        &self,
        offset: &UCoord,
        this_nodes_level: u32,
        target_level: u32,
    ) -> Result<&Self, GetError<'_, L, B, U>> {
        let mut addr = Address::new();
        offset_to_address(offset, &mut addr, this_nodes_level, target_level)?;
        Ok(self.get_node_with_address(&addr)?)
    }

    pub fn get_node_mut(
        &mut self,
        offset: &UCoord,
        this_nodes_level: u32,
        target_level: u32,
    ) -> Result<&mut Self, GetMutError<'_, L, B, U>> {
        let mut addr = Address::new();
        offset_to_address(offset, &mut addr, this_nodes_level, target_level)?;
        Ok(self.get_node_mut_with_address(&addr)?)
    }

    pub fn get_node_with_address(&self, addr: &[u8]) -> Result<&Self, NotPresent<'_, L, B, U>> {
        let mut node = self;
        for (depth, which_branch) in addr.iter().enumerate() {
            match node {
                Leaf(_) | LODLeaf(_, _) => {
                    // TODO: This error should only be returned if we ran into a hoisted leaf
                    // In the case of a normal leaf, we should have a different error. Invalid address or LOD'd chunk error maybe
                    return Err(NotPresent::IsHighLevelLeaf {
                        depth: depth as u32,
                        high_level_leaf_node: node,
                    });
                }
                Branch(_, ptrs) => {
                    node = &ptrs[*which_branch as usize];
                }
                Unloaded(_) => {
                    return Err(NotPresent::IsUnloaded {
                        depth: depth as u32,
                        unloaded_node: node,
                    })
                }
            }
        }
        Ok(node)
    }

    pub fn get_node_mut_with_address(
        &mut self,
        addr: &[u8],
    ) -> Result<&mut Self, NotPresentMut<'_, L, B, U>> {
        let mut node = self;
        for (depth, which_branch) in addr.iter().enumerate() {
            match node {
                Leaf(_) | LODLeaf(_, _) => {
                    // TODO: See TODO in get_node_with_address. Same deal.
                    return Err(NotPresentMut::IsHighLevelLeaf {
                        depth: depth as u32,
                        high_level_leaf_node: node,
                    });
                }
                Branch(_, ptrs) => {
                    node = &mut ptrs[*which_branch as usize];
                }
                Unloaded(_) => {
                    return Err(NotPresentMut::IsUnloaded {
                        depth: depth as u32,
                        unloaded_node: node,
                    })
                }
            }
        }
        Ok(node)
    }
}

impl<B: Debug, U: Debug + Default> OctNode<B, B, U> {
    /// When the leaf and branch data are the same, use this getter to get the common data leaf/branch (lb) data
    pub fn get_lb_data(&self) -> Option<&B> {
        match self {
            Leaf(l) | LODLeaf(l, _) => Some(l),
            Branch(b, _) => Some(b),
            Unloaded(_) => None,
        }
    }

    /// When the leaf and branch data are the same, use this getter to get the common data leaf/branch (lb) data
    pub fn get_lb_data_mut(&mut self) -> Option<&mut B> {
        match self {
            Leaf(l) | LODLeaf(l, _) => Some(l),
            Branch(b, _) => Some(b),
            Unloaded(_) => None,
        }
    }
}

impl<L: Debug, B: Debug + BranchNew<Node = OctNode<L, B, U>>, U: Debug + Default> OctNode<L, B, U> {
    pub fn with_unloaded_children(level: u8) -> Self {
        let children: [OctNode<L, B, U>; 8] = Default::default();
        OctNode::Branch(B::new_branch(level, &children), Box::new(children))
    }
}

impl<
        L: Debug,
        B: Debug + ModeFromNode<Node = OctNode<L, B, U>> + BranchNew<Node = OctNode<L, B, U>>,
        U: Debug + Default,
    > OctNode<L, B, U>
where
    Self: Clone,
{
    /// Adds a new node in at the given address, adding new unloaded nodes and branches where appropriate to allow for the insertion.
    /// Will replace leaf nodes with branch nodes, and break up unloadeds & LODLeafs as necessary
    /// If new_node is None, no replacement is done. This can be useful to just do a recursive update.
    pub fn replace_at_address(
        &mut self,
        new_node: Option<OctNode<L, B, U>>,
        addr: &[u8],
        current_level: u32,
    ) {
        self.replace_at_address_and_update(new_node, addr, current_level, &|_, _, _| (), None)
    }

    /// Adds a new node in at the given address, adding new unloaded nodes and branches where appropriate to allow for the insertion.
    /// Will replace leaf nodes with branch nodes, and break up unloadeds & LODLeafs as necessary
    /// Will call the provided closure (after insertion, from the bottom of the tree upward) for each branch to update any metadata for the new insertion
    /// Provide a `filler_mode` of None. It is used in the recursion to give modes to broken up LODLeafs.
    /// If new_node is None, no replacement is done. This can be useful to just do a recursive update.
    pub fn replace_at_address_and_update<F: Fn(&mut B, &[OctNode<L, B, U>], u32)>(
        &mut self,
        new_node: Option<OctNode<L, B, U>>,
        addr: &[u8],
        current_level: u32,
        update: &F,
        mut filler_mode: Option<Self>,
    ) {
        if addr.is_empty() {
            if let Some(node) = new_node {
                *self = node;
            }

            // Call update if we're a branch
            if let Branch(ref mut branch_data, children) = self {
                update(branch_data, children.as_ref(), current_level);
            }
        } else {
            match self {
                Branch(ref mut branch_data, children) => {
                    let child = &mut children[addr[0] as usize];
                    child.replace_at_address_and_update(
                        new_node,
                        &addr[1..],
                        current_level - 1,
                        update,
                        filler_mode,
                    );

                    update(branch_data, children.as_ref(), current_level);
                }
                ref not_a_branch => {
                    match not_a_branch {
                        Leaf(_) | LODLeaf(_, _) => {
                            filler_mode = Some(self.clone());
                        }
                        Unloaded(_) => {}
                        Branch(_, _) => unreachable!(),
                    }

                    // Turn self into a branch (with proper filler mode data)
                    match &filler_mode {
                        Some(mode_node) => {
                            *self =
                                OctNode::Branch(B::mode_from_node(mode_node), Default::default());
                        }
                        None => {
                            *self = OctNode::with_unloaded_children(current_level as u8);
                        }
                    }
                    // Then call again to recurse into the branch
                    self.replace_at_address_and_update(
                        new_node,
                        &addr[..],
                        current_level,
                        update,
                        filler_mode,
                    );
                }
            }
        }
    }

    pub fn recurse_at_address_and_update<F: Fn(&mut B, &[OctNode<L, B, U>], u32)>(
        &mut self,
        addr: &[u8],
        current_level: u32,
        update: &F,
    ) {
        self.replace_at_address_and_update(None, addr, current_level, update, None);
    }
}

impl<L: Debug + Default + BranchNew<Node = OctNode<L, L, U>>, U: Debug + Default> OctNode<L, L, U> {
    /// Turns leaves into branches as it searches to reach address.
    /// If the target node (whose data is replaced) is a branch, `update` will be called for that branch too.
    /// Ordinarily, `update` is called for each parent branch as it recurses back up the tree.
    pub fn update_data_at_branch_or_leaf<TU: FnOnce(&mut Self), RBU: Fn(&mut L, &[Self])>(
        &mut self,
        addr: &[u8],
        target_update: TU,
        recursive_branch_update: &RBU,
    ) {
        if addr.is_empty() {
            target_update(self);

            if let Branch(b, children) = self {
                recursive_branch_update(b, children.as_ref())
            }
        } else {
            match self {
                Branch(ref mut branch_data, children) => {
                    let child = &mut children[addr[0] as usize];
                    child.update_data_at_branch_or_leaf(
                        &addr[1..],
                        target_update,
                        recursive_branch_update,
                    );

                    recursive_branch_update(branch_data, children.as_ref())
                }
                Leaf(_) | LODLeaf(_, _) => {
                    let l = match self.take() {
                        Leaf(l) | LODLeaf(l, _) => l,
                        _ => unreachable!(),
                    };
                    *self = Branch(l, Default::default());
                    self.update_data_at_branch_or_leaf(
                        &addr,
                        target_update,
                        recursive_branch_update,
                    );
                }
                Unloaded(_) => {
                    *self = OctNode::with_unloaded_children(u8::MAX);
                    self.update_data_at_branch_or_leaf(
                        &addr,
                        target_update,
                        recursive_branch_update,
                    );
                    //recall on self to traverse
                }
            }
        }
    }
}

impl<L: Debug, B: Debug, U: Debug + Default> Default for OctNode<L, B, U> {
    fn default() -> Self {
        OctNode::Unloaded(Default::default())
    }
}
