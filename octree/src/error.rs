//! Defines the possible errors returned when doing octree operations.
//!
//! For example, that the address was out of bounds or that a higher level leaf was found before indicated address was reached.

use super::OctNode;

use std::fmt::Debug;
use thiserror::Error;

#[derive(Debug, Error)]
#[error("Data was out of bounds for this tree")]
pub struct OutOfBounds();

#[derive(Debug, Error)]
/// Note, `depth` here represents the depth from the node the search was made from.
/// In this sense, `depth` is the inverse of `level`, which counts from the bottom of the tree.
pub enum NotPresent<'a, L: Debug, B: Debug, U: Debug + Default> {
    #[error("IsUnloaded: The node being fetched was truncated in this tree at depth {depth:?}")]
    IsUnloaded {
        depth: u32,
        unloaded_node: &'a OctNode<L, B, U>,
    },
    #[error("IsHighLevelLeaf: The node being fetched was a high level leaf at depth {depth:?}, and couldn't be accessed directly")]
    IsHighLevelLeaf {
        depth: u32,
        high_level_leaf_node: &'a OctNode<L, B, U>,
    },
}

#[derive(Debug, Error)]
/// Note, `depth` here represents the depth from the node the search was made from.
/// In this sense, `depth` is the inverse of `level`, which counts from the bottom of the tree.
pub enum NotPresentMut<'a, L: Debug, B: Debug, U: Debug + Default> {
    #[error("IsUnloaded: The node being fetched was truncated in this tree at depth {depth:?}")]
    IsUnloaded {
        depth: u32,
        unloaded_node: &'a mut OctNode<L, B, U>,
    },
    #[error("IsHighLevelLeaf: The node being fetched was a high level leaf at depth {depth:?}, and couldn't be accessed directly")]
    IsHighLevelLeaf {
        depth: u32,
        high_level_leaf_node: &'a mut OctNode<L, B, U>,
    },
}

#[derive(Debug, Error)]
pub enum GetError<'a, L: Debug, B: Debug, U: Debug + Default> {
    #[error("OutOfBounds({0})")]
    OutOfBounds(#[from] OutOfBounds),
    #[error("NotPresent({0})")]
    NotPresent(NotPresent<'a, L, B, U>),
}

#[derive(Debug, Error)]
pub enum GetMutError<'a, L: Debug, B: Debug, U: Debug + Default> {
    #[error("OutOfBounds({0})")]
    OutOfBounds(#[from] OutOfBounds),
    #[error("NotPresent({0})")]
    NotPresentMut(NotPresentMut<'a, L, B, U>),
}

impl<'a, L: Debug, B: Debug, U: Debug + Default> From<NotPresent<'a, L, B, U>>
    for GetError<'a, L, B, U>
{
    fn from(e: NotPresent<'a, L, B, U>) -> Self {
        GetError::NotPresent(e)
    }
}

impl<'a, L: Debug, B: Debug, U: Debug + Default> From<NotPresentMut<'a, L, B, U>>
    for GetMutError<'a, L, B, U>
{
    fn from(e: NotPresentMut<'a, L, B, U>) -> Self {
        GetMutError::NotPresentMut(e)
    }
}
