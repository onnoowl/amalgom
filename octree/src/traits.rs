//! Houses traits that define certain specific behavior for an octree or octnode.
//!
//! Most of these pertain to the logic implementation. For example, HasID refers to an octnode's block id, which is implemented for LogicNodes, but not for RenderNodes.

use crate::culling::FaceData;

pub trait ModeFromNode {
    type Node;

    fn mode_from_node(node: &Self::Node) -> Self;
}

pub trait BranchNew {
    type Node: Sized;
    fn new_branch(level: u8, children: &[Self::Node]) -> Self;
}

pub trait HasUnderConstruction {
    fn is_under_construction(&self) -> bool;
    fn set_under_construction(&mut self, under_construction: bool);
}

pub trait HasMaxDataLOD {
    fn get_max_data_lod(&self) -> u8;
    fn set_max_data_lod(&mut self, max_data_lod: u8);
}

pub trait HasMaxDecLOD {
    fn get_max_dec_lod(&self) -> u8;
    fn set_max_dec_lod(&mut self, max_dec_lod: u8);
}

pub trait HasBuffer {
    fn get_has_buffer(&self) -> bool;
    fn set_has_buffer(&mut self, val: bool);
}

pub trait HasMode {
    type ModeType: HasFaceData + IsAir + HasID;

    fn get_mode(&self) -> Option<&Self::ModeType>;
    fn get_mode_mut(&mut self) -> Option<&mut Self::ModeType>;
}

pub trait HasID {
    fn get_id(&self) -> Option<u8>;
    fn set_id(&mut self, id: Option<u8>);
}

pub trait HasNoUnloaded {
    fn has_no_unloaded(&self) -> bool;
    fn set_has_no_unloaded(&mut self, has_no_unloaded: bool);
}

// Culling traits

pub trait HasFaceData {
    fn get_face_data(&self) -> &FaceData;
    fn get_face_data_mut(&mut self) -> &mut FaceData;
}

pub trait IsAir {
    fn is_air(&self) -> bool;
}
