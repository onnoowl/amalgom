//! Houses functions for octrees which is basically a wrapper around an octnode that also stores some metadata.
//!
//! For example, one of the functions here is get_node, because it needs the context of the root node to function.

use super::traits::ModeFromNode;
use crate::{address::*, error::*, traits::BranchNew, OctNode};
use base_utils::{offset_to_index, ICoord, Loc, OCTAL_OFFSETS, TREE_MOVE_THRESHOLD};

use cgmath::Point3;
use std::fmt::Debug;

#[derive(Debug, Clone, PartialEq)]
pub struct Octree<L: Debug, B: Debug, U: Debug + Default> {
    pub root: OctNode<L, B, U>,
    pub root_level: u32,
    pub root_coord: ICoord,
}

impl<L: Debug, B: Debug, U: Debug + Default> Octree<L, B, U> {
    pub fn new(root: OctNode<L, B, U>, root_level: u32, root_coord: ICoord) -> Self {
        Self {
            root,
            root_level,
            root_coord,
        }
    }

    pub fn get_block(&self, coord: &ICoord) -> Result<&L, GetError<'_, L, B, U>> {
        self.get_node(&coord, 0).map(|node|
            node.get_leaf().expect("Didn't get a leaf when retrieving level 0 block node. SparseOctalTree was constructed incorrectly.")
        )
    }
    pub fn get_block_mut(&mut self, coord: &ICoord) -> Result<&mut L, GetMutError<'_, L, B, U>> {
        self.get_node_mut(&coord, 0).map(|node|
            node.get_leaf_mut().expect("Didn't get a leaf when retrieving level 0 block node. SparseOctalTree was constructed incorrectly.")
        )
    }

    pub fn get_node(
        &self,
        coord: &ICoord,
        level: u32,
    ) -> Result<&OctNode<L, B, U>, GetError<'_, L, B, U>> {
        let offset = (coord - self.root_coord)
            .cast::<u64>()
            .ok_or(OutOfBounds())?;
        self.root.get_node(&offset, self.root_level, level)
    }

    pub fn get_node_mut(
        &mut self,
        coord: &ICoord,
        level: u32,
    ) -> Result<&mut OctNode<L, B, U>, GetMutError<'_, L, B, U>> {
        let offset = (coord - self.root_coord)
            .cast::<u64>()
            .ok_or(OutOfBounds())?;
        self.root.get_node_mut(&offset, self.root_level, level)
    }

    pub fn coord_to_address(
        &self,
        coord: &ICoord,
        address: &mut Address,
        target_level: u32,
    ) -> Result<(), OutOfBounds> {
        let offset = &(coord - self.root_coord)
            .cast::<u64>()
            .ok_or(OutOfBounds())?;
        offset_to_address(offset, address, self.root_level, target_level)?;
        Ok(())
    }

    pub fn truncate_coord_at_level(
        &self,
        coord: &ICoord,
        target_level: u8,
    ) -> Result<ICoord, OutOfBounds> {
        let mask = u64::MAX.overflowing_shl(target_level as u32).0;
        let offset = &(coord - self.root_coord)
            .cast::<u64>()
            .ok_or(OutOfBounds())?;

        let offset = cgmath::Vector3::<u64>::new(offset.x & mask, offset.y & mask, offset.z & mask);
        Ok(offset.cast::<i64>().unwrap() + self.root_coord)
    }
}

impl<L: Debug, B: Debug + BranchNew<Node = OctNode<L, B, U>>, U: Debug + Default> Octree<L, B, U> {
    pub fn move_root_for_pos(&mut self, pos: &Point3<f32>) {
        let world_size = 2_u32.pow(self.root_level as u32) as f64;
        let ratios = (pos.cast::<i64>().unwrap() - self.root_coord)
            .cast::<f64>()
            .unwrap()
            / world_size;

        let mut mover = |dir: (usize, bool)| {
            if dir.1 {
                self.root_coord[dir.0] += world_size as i64 / 2;
            } else {
                self.root_coord[dir.0] -= world_size as i64 / 2;
            }

            let (_, mut children) = self.root.take().into_branch().unwrap();
            let mut new_root_children: [OctNode<L, B, U>; 8] = Default::default();

            for (j, offset) in OCTAL_OFFSETS.iter().enumerate() {
                let mut flipped_offset = *offset;
                flipped_offset[dir.0] ^= 1; //flip it
                let dest_index = offset_to_index(flipped_offset);

                if offset[dir.0] == dir.1 as i64 {
                    new_root_children[dest_index] = children[j].take();
                } else {
                    new_root_children[dest_index] =
                        OctNode::<L, B, U>::Unloaded(Default::default());
                }
            }

            self.root = OctNode::Branch(
                B::new_branch(self.root_level as u8, &new_root_children),
                Box::new(new_root_children),
            );
        };

        for i in 0..=2 {
            if ratios[i] < TREE_MOVE_THRESHOLD {
                mover((i, false));
            } else if ratios[i] > 1. - TREE_MOVE_THRESHOLD {
                mover((i, true));
            }
        }
    }
}

impl<
        L: Debug,
        B: Debug + ModeFromNode<Node = OctNode<L, B, U>> + BranchNew<Node = OctNode<L, B, U>>,
        U: Debug + Default,
    > Octree<L, B, U>
where
    OctNode<L, B, U>: Clone,
{
    pub fn replace_at_location(
        &mut self,
        new_node: OctNode<L, B, U>,
        location: Loc,
    ) -> Result<(), OutOfBounds> {
        self.replace_at_location_and_update(new_node, location, |_, _, _| ())
    }

    pub fn replace_at_location_and_update<F: Fn(&mut B, &[OctNode<L, B, U>], u32)>(
        &mut self,
        new_node: OctNode<L, B, U>,
        location: Loc,
        update: F,
    ) -> Result<(), OutOfBounds> {
        let mut address = Address::new();
        coord_to_address(
            &location.0,
            &self.root_coord,
            &mut address,
            self.root_level,
            location.1 as u32,
        )?;
        self.replace_at_address_and_update(Some(new_node), &address, &update);
        Ok(())
    }

    /// Adds a new node in at the given address, adding new unloaded nodes and branches where appropriate to allow for the insertion.
    /// Will replace leaf nodes with branch nodes, and break up unloadeds & LODLeafs as necessary
    /// If new_node is None, no replacement is done. This can be useful to just do a recursive update.
    pub fn replace_at_address(&mut self, new_node: Option<OctNode<L, B, U>>, addr: &[u8]) {
        self.root
            .replace_at_address(new_node, addr, self.root_level)
    }

    /// Adds a new node in at the given address, adding new unloaded nodes and branches where appropriate to allow for the insertion.
    /// Will replace leaf nodes with branch nodes, and break up unloadeds & LODLeafs as necessary
    /// Will call the provided closure (after insertion, from the bottom of the tree upward) for each branch to update any metadata for the new insertion
    /// Provide a `filler_mode` of None. It is used in the recursion to give modes to broken up LODLeafs.
    /// If new_node is None, no replacement is done. This can be useful to just do a recursive update.
    pub fn replace_at_address_and_update<F: Fn(&mut B, &[OctNode<L, B, U>], u32)>(
        &mut self,
        new_node: Option<OctNode<L, B, U>>,
        addr: &[u8],
        update: &F,
    ) {
        self.root
            .replace_at_address_and_update(new_node, addr, self.root_level, update, None);
    }

    pub fn recurse_at_address_and_update<F: Fn(&mut B, &[OctNode<L, B, U>], u32)>(
        &mut self,
        addr: &[u8],
        update: &F,
    ) {
        self.root
            .replace_at_address_and_update(None, addr, self.root_level, update, None);
    }
}
