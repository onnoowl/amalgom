use crate::OctNode;
use std::fmt::Debug;

/// This is View style struct for nodes, returned by node.get_data()
pub enum LeafOrBranchData<'a, L: Debug, B: Debug, U: Debug + Default> {
    LeafData(&'a L),
    BranchData((&'a B, &'a [OctNode<L, B, U>; 8])),
}

/// This is View style struct for nodes, returned by node.get_data_mut()
pub enum LeafOrBranchDataMut<'a, L: Debug, B: Debug, U: Debug + Default> {
    LeafDataMut(&'a mut L),
    BranchDataMut((&'a mut B, &'a mut [OctNode<L, B, U>; 8])),
}
