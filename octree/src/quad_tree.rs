//! Quad trees are used to break down the culling information of a large octnode that is next to several smaller octnodes.
//!
//! Quadtrees exist that only have a single "quad" when either one of the other faces on the octnode needs a quadtree and we can't have all the culling info in a "compact" byte or when there are smaller voxel neighbors, but all the culling info is the same and can be "hoisted" into one value instead of four.

use super::culling::QuadLightingData;

#[derive(Debug, Clone, PartialEq)]
pub enum Occlusion {
    Occluded,
    Visible(QuadLightingData),
}

impl Occlusion {
    pub fn is_occluded(&self) -> bool {
        match self {
            Occlusion::Occluded => true,
            Occlusion::Visible(_) => false,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum QuadTree {
    Leaf(Occlusion),
    Branch(Option<Occlusion>, Box<[QuadTree; 4]>),
}

impl QuadTree {
    pub fn is_leaf(&self) -> bool {
        match self {
            QuadTree::Leaf(_) => true,
            QuadTree::Branch(_, _) => false,
        }
    }

    pub fn is_leaf_or_mode_occluded(&self) -> bool {
        match self {
            QuadTree::Leaf(occlusion) => occlusion.is_occluded(),
            QuadTree::Branch(occlusion, _) => {
                occlusion.as_ref().map(|o| o.is_occluded()).unwrap_or(false)
            }
        }
    }

    /// Also checks mode data
    pub fn is_ever_visible(&self) -> bool {
        match self {
            QuadTree::Leaf(occlusion) => !occlusion.is_occluded(),
            QuadTree::Branch(mode, children) => {
                mode.as_ref().map(|o| !o.is_occluded()) == Some(true)
                    || children.iter().any(|c| c.is_ever_visible())
            }
        }
    }

    pub fn get_occlusion(&self) -> Option<&Occlusion> {
        match self {
            QuadTree::Leaf(occlusion) => Some(occlusion),
            QuadTree::Branch(opt_occlusion, _) => opt_occlusion.as_ref(),
        }
    }

    pub fn with_occluded_children() -> Self {
        QuadTree::Branch(Default::default(), Default::default())
    }

    pub fn make_branch_and_get_mut(&mut self) -> (&mut Option<Occlusion>, &mut [QuadTree]) {
        *self = Self::with_occluded_children();
        match self {
            QuadTree::Branch(mode_lighting, children) => (mode_lighting, children.as_mut()),
            QuadTree::Leaf(_) => unreachable!(),
        }
    }

    pub fn take(&mut self) -> QuadTree {
        let mut quad = Default::default();
        std::mem::swap(self, &mut quad);
        quad
    }
}

impl Default for QuadTree {
    fn default() -> Self {
        QuadTree::Leaf(Occlusion::Occluded)
    }
}

impl From<(bool, QuadLightingData)> for QuadTree {
    fn from(t: (bool, QuadLightingData)) -> Self {
        if t.0 {
            QuadTree::Leaf(Occlusion::Visible(t.1))
        } else {
            QuadTree::Leaf(Occlusion::Occluded)
        }
    }
}
