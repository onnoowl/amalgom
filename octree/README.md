# Octree

This crate houses the logic for the octree data structure. The functions here are generic enough to be applied to both the render thread's octree and the logic thread's octree.

Click [here](https://en.wikipedia.org/wiki/Octree) to learn what an octree is.

## Modules

[address](src/address.rs) - Defines the structure and functions for addresses that represent a location in an octree.

[culling](src/culling.rs) - Handles culling-related structures and functions to mark faces on octnodes that have non-air neighbors.

[error](src/error.rs) - Defines the possible errors returned when doing octree operations.

[octnode](src/octnode.rs) - Defines the nodes that can exist on an octree.

[octree](src/octree.rs) - Houses functions for octrees which is basically a wrapper around an octnode that also stores some metadata.

[quad_tree](src/quad_tree) - Quad trees are used to break down the culling information of a large octnode that is next to several smaller octnodes.

[traits](src/traits.rs) - Houses traits that define certain specific behavior for an octree or octnode.
