use rayon::prelude::*;
use std::process::Command;

fn main() {
    println!("cargo:rerun-if-changed=src/renderer/shaders/");
    std::fs::read_dir("src/renderer/shaders/")
        .unwrap()
        .collect::<Vec<_>>()
        .into_par_iter().for_each(|entry|
    {
        let entry = entry.unwrap();
        let p = entry.path();
        let p_str = p.to_str().unwrap();
        if p.is_file() && p_str.ends_with(".vert") || p_str.ends_with(".frag") {
            println!("cargo:rerun-if-changed={}", p_str);
            let output = Command::new("glslangValidator")
                .arg("-V")
                .arg(&p_str)
                .arg("-o")
                .arg(format!("{}.spv", p_str))
                .output();

            match output {
                Ok(out) => {
                    if !out.status.success() {
                        panic!("{}", String::from_utf8(out.stdout).unwrap());
                    }
                }
                Err(e) => match e.kind() {
                    std::io::ErrorKind::NotFound => println!(
                        "cargo:warning=\"Failed to compile {} with `glslangValidator`. Is it installed?\"", p_str
                    ),
                    _ => {
                        panic!("{}", e);
                    }
                },
            }
        }
    });
}
