# Render

Code for the game's visual rendering

## Modules

[framework](src/framework.rs) - Structure for generic game loop

[renderer](src/renderer.rs) - What is called by framework.rs to relay stages of the game loop and orchestrate a frame draw

[terrain_visualizer](src/terrain_visualizer.rs) - Octree for rendering that stores chunk meshes as leafs
