//! Octree for rendering that stores chunk meshes as leafs
//!
//! This tree receives vertices and indices of terrain from the logic tree and decides when to turn these into gpu buffers based on the player's camera

pub mod render_tree;
pub mod terrain_buffer;

use cgmath::{Matrix4, Point3, Vector2, Vector3};
pub use render_tree::*;
pub use terrain_buffer::{FaceBuffer, TerrainBuffer};

use crate::{
    renderer::{
        camera_manager::CameraManager,
        passes::terrain_pass::{MaybeVisual, TerrainVisual},
    },
    {occlusion_culling, occlusion_culling::OcclusionTiles},
};
use base_utils::{p_to_v, ICoord, OCTAL_OFFSETS, ROOT_LEVEL};
use game_utils::{
    should_lod_recurse,
    thread_comm::{StartupInfo, TerrainMeshMsg},
};
use octree::{Address, OctNode, OutOfBounds};

use collision::Frustum;
use std::{
    sync::{mpsc::Receiver, Arc},
    time::{Duration, Instant},
};
use wgpu::{Device, Queue};

use self::terrain_buffer::OpacityAndFaceBuffer;

pub struct TerrainVisualizer {
    pub tree: RenderTree,
    recv_buffer: Receiver<TerrainMeshMsg>,

    pub buffers: Vec<Arc<FaceBuffer>>,
    pub opacity_buffers: Vec<OpacityAndFaceBuffer>,
    memory_used: usize,
    pub last_memory_used: usize,

    buffer_depth_and_min_level: u32,

    time: Option<Instant>,
    last_player_pos: Option<Point3<f32>>,

    occlusion_tiles: OcclusionTiles,
}

const ZERO_VERT: Vector3<f32> = Vector3::<f32> {
    x: 0.,
    y: 0.,
    z: 0.,
};
const DEFAULT_BB_VERTS: [Vector3<f32>; 8] = [
    ZERO_VERT, ZERO_VERT, ZERO_VERT, ZERO_VERT, ZERO_VERT, ZERO_VERT, ZERO_VERT, ZERO_VERT,
];

impl TerrainVisualizer {
    pub fn new(startup_info: &StartupInfo, recv_buffer: Receiver<TerrainMeshMsg>) -> Self {
        let hsize = 2i64.pow(ROOT_LEVEL as u32 - 1);
        let tree = RenderTree::new(
            OctNode::Unloaded(()),
            ROOT_LEVEL as u32,
            p_to_v(&startup_info.player_spawn).cast::<i64>().unwrap()
                - ICoord::new(hsize, hsize, hsize),
        );

        Self {
            tree,
            recv_buffer,
            buffers: Vec::new(),
            opacity_buffers: Vec::new(),
            memory_used: 0,
            last_memory_used: 0,
            buffer_depth_and_min_level: startup_info.buffer_depth_and_min_level,
            time: None,
            last_player_pos: None,

            occlusion_tiles: OcclusionTiles::default(),
        }
    }

    pub fn update(
        &mut self,
        camera_manager: &CameraManager,
        _camera_changed: bool,
        device: &Device,
        queue: &Queue,
    ) {
        self.last_memory_used = self.memory_used;

        self.process_finished_buffer_generations();

        self.lod_perspective(
            &camera_manager.camera.controller.eye,
            &camera_manager.camera.cached_matrices.frustum,
            &camera_manager.camera.cached_matrices.view_proj,
            device,
            queue,
        );

        // if self.memory_used != self.last_memory_used {
        //     log::debug!(
        //         "# Buffers: {}, Memory Used: {}GB",
        //         self.buffers.len(),
        //         self.memory_used as f32 / 1024_f32.powi(3)
        //     );
        // }

        // #[cfg(debug_assertions)]
        // self.occlusion_tiles.debug_print();
    }

    fn lod_perspective(
        &mut self,
        player_pos: &Point3<f32>,
        frustum: &Frustum<f32>,
        view_proj_matrix: &Matrix4<f32>,
        device: &Device,
        queue: &Queue,
    ) -> usize {
        self.buffers.clear();
        self.opacity_buffers.clear();
        self.occlusion_tiles.reset();

        self.tree.move_root_for_pos(player_pos);

        let time = self.time.get_or_insert_with(Instant::now);
        let cur_time = Instant::now();
        let mut time_change = cur_time - *time;
        *time = cur_time;

        if self.last_player_pos.as_ref() == Some(player_pos) {
            time_change = Duration::new(0, 0); // Halt all animations
        }
        self.last_player_pos = Some(*player_pos);

        Self::lod_recurse(
            &mut self.tree.root,
            self.tree.root_coord,
            self.tree.root_level,
            player_pos,
            frustum,
            view_proj_matrix,
            &mut self.buffers,
            &mut self.opacity_buffers,
            device,
            queue,
            self.buffer_depth_and_min_level,
            AncestorAnimating::None,
            cur_time,
            time_change,
            &mut self.occlusion_tiles,
            false,
        );

        0
    }

    fn lod_recurse(
        node: &mut RenderNode,
        pos: ICoord,
        level: u32,
        player_pos: &Point3<f32>,
        frustum: &Frustum<f32>,
        view_proj_matrix: &Matrix4<f32>,
        bufs: &mut Vec<Arc<FaceBuffer>>,
        opacity_bufs: &mut Vec<OpacityAndFaceBuffer>,
        device: &Device,
        queue: &Queue,
        buffer_depth_and_min_level: u32,
        ancestor_animating: AncestorAnimating,
        cur_time: Instant,
        time_change: Duration,
        occlusion_tiles: &mut OcclusionTiles,
        subtree_was_culled_last_frame: bool,
    ) {
        let size = 2_u32.pow(level) as i64;
        let bb = game_utils::intersection::get_bb(&pos.cast::<f32>().unwrap(), level);

        // First frustum cull
        let mut should_cull =
            frustum.contains::<collision::Aabb3<f32>>(&bb) == collision::Relation::Out;

        // Then occlusion cull
        if !should_cull {
            let mut verts = DEFAULT_BB_VERTS;
            for (out_v, in_v) in verts.iter_mut().zip(bb.to_corners().iter()) {
                let projected = view_proj_matrix * in_v.to_homogeneous().truncate().extend(1.0);
                *out_v = projected.truncate() / projected.w;
                *out_v = out_v.map(|c| ((c + 1.) / 2.).max(0.).min(1.));
            }

            let mut min_x = f32::MAX;
            let mut min_y = f32::MAX;
            let mut max_x = f32::MIN;
            let mut max_y = f32::MIN;
            let mut min_z = f32::MAX;
            for v in &mut verts {
                min_x = min_x.min(v.x);
                max_x = max_x.max(v.x);
                min_y = min_y.min(v.y);
                max_y = max_y.max(v.y);
                min_z = min_z.min(v.z);
            }
            let min_corner = Vector2::new(
                (min_x * (occlusion_culling::WIDTH as f32 - 1.)) as usize,
                (min_y * (occlusion_culling::HEIGHT as f32 - 1.)) as usize,
            );
            let max_corner = Vector2::new(
                (max_x * (occlusion_culling::WIDTH as f32 - 1.)) as usize,
                (max_y * (occlusion_culling::HEIGHT as f32 - 1.)) as usize,
            );

            should_cull = occlusion_tiles.check_bounding_box_occluded(min_corner, max_corner, min_z)
        }

        if should_cull {
            if let Some(data) = node.get_lb_data_mut() {
                data.was_culled_last_frame = true;
            }

            // Forcibly recurse anyway if the ancestor is animating to make sure the animations are computed together and in sync
            if !ancestor_animating.is_animating() {
                return;
            }
        }

        // If our ancestor is animating, and we're not, snap us visible or invisible (depending on the parent)
        // Then let the normal logic below put us into an animating state
        if let Some(data) = node.get_lb_data_mut() {
            if data.visual.is_some()
                && ancestor_animating.is_animating()
                && !matches!(data.anim, BufVisAnim::Animating(_, _, _))
            {
                match ancestor_animating {
                    AncestorAnimating::FadingIn(_) => {
                        data.anim.finish();
                        data.anim = BufVisAnim::Static(BufVisibility::Normal)
                    }
                    AncestorAnimating::FadingOut(_) => {
                        data.anim.finish();
                        data.anim = BufVisAnim::Static(BufVisibility::SeeThrough)
                    }
                    _ => (),
                }
            }
        }

        match node {
            RenderNode::Branch(branch_data, children) => {
                let can_recurse =
                    level > buffer_depth_and_min_level && branch_data.all_children_have_bufs;
                let wants_to_recurse = can_recurse && should_lod_recurse(player_pos, &bb);

                let mut this_node_animating = AncestorAnimating::None;

                if branch_data.visual.is_some() {
                    this_node_animating.or_equals((&branch_data.anim).into());

                    branch_data.update_anim(
                        !wants_to_recurse && !ancestor_animating.fading_in(),
                        cur_time,
                        time_change,
                        branch_data.all_children_not_animating,
                    );

                    if subtree_was_culled_last_frame {
                        // This is the first frame we're in view, we can skip through our animation
                        branch_data.anim.finish_appearing();
                    }

                    if let AncestorAnimating::FadingIn(dur) | AncestorAnimating::FadingOut(dur) =
                        ancestor_animating
                    {
                        branch_data.anim.try_sync_animation_dur(dur, cur_time);
                    }

                    // Set before and after to give children the chance to set their proper state and finish their anims
                    this_node_animating.or_equals((&branch_data.anim).into());

                    if !should_cull {
                        Self::push_branch_data(
                            branch_data,
                            device,
                            queue,
                            view_proj_matrix,
                            bufs,
                            opacity_bufs,
                            &pos,
                            size,
                            player_pos,
                            level,
                            buffer_depth_and_min_level,
                            occlusion_tiles,
                        );
                    }
                }

                let see_through = matches!(
                    branch_data.anim,
                    BufVisAnim::Static(BufVisibility::SeeThrough)
                );

                // Don't recurse if an ancestor AND us are animating, don't want to have nested AncesstorAnimating behavior
                if !(this_node_animating.is_animating() && ancestor_animating.is_animating())
                        // Recurse if we have no buffer (e.g. the root)
                    && (branch_data.visual.is_none()
                        // Recurse if our children have bufs and we are animating/see-through
                        || (can_recurse && (this_node_animating.is_animating() || see_through))
                        // Recurse if any children are animating, reccurse
                        || !branch_data.all_children_not_animating)
                {
                    // branch_data.buf.map(|buf| self.memory_used -= buf.alloc_size);
                    // branch_data.buf = None; //clear the buffer, we don't need it

                    let player_to_center = (player_pos.cast::<i64>().unwrap() * -1
                        + pos
                        + ICoord {
                            x: size / 2,
                            y: size / 2,
                            z: size / 2,
                        })
                    .cast::<f32>()
                    .unwrap();

                    let sx = (player_to_center.x > 0.) as u8;
                    let sy = (player_to_center.y > 0.) as u8;
                    let sz = (player_to_center.z > 0.) as u8;

                    for flip_bits in (0..=7u8).rev() {
                        let index = ((sx | (sy << 1) | (sz << 2)) ^ flip_bits) as usize;
                        Self::lod_recurse(
                            &mut children[index],
                            pos + OCTAL_OFFSETS[index] * size / 2,
                            level - 1,
                            player_pos,
                            frustum,
                            view_proj_matrix,
                            bufs,
                            opacity_bufs,
                            device,
                            queue,
                            buffer_depth_and_min_level,
                            this_node_animating.or(ancestor_animating),
                            cur_time,
                            time_change,
                            occlusion_tiles,
                            subtree_was_culled_last_frame || branch_data.was_culled_last_frame,
                        )
                    }

                    // Update all_children_have_bufs, because recursing might have dropped buffers
                    branch_data.all_children_have_bufs =
                        Self::compute_all_children_have_bufs(children.as_ref());
                    branch_data.all_children_not_animating =
                        Self::compute_all_children_not_animating(children.as_ref());
                }
            }
            RenderNode::Leaf(leaf_data) => {
                if leaf_data.visual.is_some() {
                    leaf_data.update_anim(
                        !ancestor_animating.fading_in(),
                        cur_time,
                        time_change,
                        true,
                    );

                    if subtree_was_culled_last_frame {
                        // This is the first frame we're in view, we can skip through our animation
                        leaf_data.anim.finish();
                    }

                    if let AncestorAnimating::FadingIn(dur) | AncestorAnimating::FadingOut(dur) =
                        ancestor_animating
                    {
                        leaf_data.anim.try_sync_animation_dur(dur, cur_time);
                    }

                    if !should_cull {
                        Self::push_branch_data(
                            leaf_data,
                            device,
                            queue,
                            view_proj_matrix,
                            bufs,
                            opacity_bufs,
                            &pos,
                            size,
                            player_pos,
                            level,
                            buffer_depth_and_min_level,
                            occlusion_tiles,
                        );
                    }
                }
            }
            _ => return,
        };

        if let Some(data) = node.get_lb_data_mut() {
            data.was_culled_last_frame = false;
        }
    }

    fn process_finished_buffer_generations(&mut self) -> bool {
        let mut made_updates = false;
        let memory_used = &mut self.memory_used;

        for buf_msg in self.recv_buffer.try_iter() {
            made_updates = true;

            let mut address = Address::new();
            if let Err(OutOfBounds()) =
                self.tree
                    .coord_to_address(&buf_msg.loc.0, &mut address, buf_msg.loc.1 as u32)
            {
                // Buffer generated from before root move, just throw it away
                continue;
            }

            if let Some(buf) = buf_msg.buf.as_ref() {
                *memory_used += buf.alloc_size;
            }

            self.tree.root.update_data_at_branch_or_leaf(
                &address,
                |target_node| {
                    let visual = match buf_msg.buf {
                        Some(inner_tbuf) => MaybeVisual::Some(TerrainVisual::new(inner_tbuf)),
                        None => MaybeVisual::Air,
                    };
                    match target_node {
                        OctNode::Leaf(d) | OctNode::LODLeaf(d, _) | OctNode::Branch(d, _) => {
                            d.new_visual(visual)
                        }
                        OctNode::Unloaded(_) => {
                            *target_node = OctNode::Leaf(RenderBranchData {
                                visual,
                                all_children_have_bufs: false,
                                all_children_not_animating: true,
                                anim: Default::default(),
                                waiting_new_visual: None,
                                was_culled_last_frame: true,
                            })
                        }
                    };
                },
                &|b, children| {
                    b.all_children_have_bufs = Self::compute_all_children_have_bufs(children)
                },
            );
        }
        made_updates
    }

    fn compute_all_children_have_bufs(children: &[RenderNode]) -> bool {
        children.iter().all(|c| match c {
            OctNode::Leaf(bd) | OctNode::LODLeaf(bd, _) | OctNode::Branch(bd, _) => {
                !bd.visual.is_none() || bd.all_children_have_bufs
            }
            OctNode::Unloaded(_) => false,
        })
    }

    fn compute_all_children_not_animating(children: &[RenderNode]) -> bool {
        children.iter().all(|c| match c {
            OctNode::Leaf(bd) | OctNode::LODLeaf(bd, _) => {
                bd.visual.is_none() || !matches!(bd.anim, BufVisAnim::Animating(_, _, _))
            }
            OctNode::Branch(bd, _) => {
                !matches!(bd.anim, BufVisAnim::Animating(_, _, _)) && bd.all_children_not_animating
            }
            OctNode::Unloaded(_) => true,
        })
    }

    fn push_branch_data(
        data: &mut RenderBranchData,
        device: &Device,
        queue: &Queue,
        view_proj_matrix: &Matrix4<f32>,
        bufs: &mut Vec<Arc<FaceBuffer>>,
        opacity_bufs: &mut Vec<OpacityAndFaceBuffer>,
        pos: &ICoord,
        size: i64,
        player_pos: &Point3<f32>,
        level: u32,
        buffer_depth_and_min_level: u32,
        occlusion_tiles: &mut OcclusionTiles,
    ) {
        match &mut data.visual {
            MaybeVisual::Some(terrain_visual) => match &mut data.anim {
                BufVisAnim::Static(BufVisibility::Normal) => {
                    // Only register occlusion planes for fully visible chunks
                    if occlusion_tiles.below_plane_counter_limit() {
                        for plane in &terrain_visual.mesh.occlusion_planes {
                            occlusion_tiles.register_occluding_plane(plane, view_proj_matrix);
                        }
                    }

                    Self::push_terrain_visual_with_opacity(
                        device,
                        queue,
                        bufs,
                        opacity_bufs,
                        terrain_visual,
                        pos,
                        size,
                        player_pos,
                        1.,
                        level - buffer_depth_and_min_level,
                    );
                }
                BufVisAnim::Static(BufVisibility::SeeThrough) => (),
                BufVisAnim::Animating(_, opacity, _) => {
                    Self::push_terrain_visual_with_opacity(
                        device,
                        queue,
                        bufs,
                        opacity_bufs,
                        terrain_visual,
                        pos,
                        size,
                        player_pos,
                        *opacity,
                        level - buffer_depth_and_min_level,
                    );
                }
                BufVisAnim::AnimatingFromLastBuf {
                    last_buf,
                    last_buf_opacity,
                    opacity,
                    acc_time: _,
                } => {
                    Self::push_terrain_visual_with_opacity(
                        device,
                        queue,
                        bufs,
                        opacity_bufs,
                        terrain_visual,
                        pos,
                        size,
                        player_pos,
                        *opacity,
                        level - buffer_depth_and_min_level,
                    );
                    match last_buf.as_mut() {
                        MaybeVisual::Some(last_visual) => {
                            Self::push_terrain_visual_with_opacity(
                                device,
                                queue,
                                bufs,
                                opacity_bufs,
                                last_visual,
                                pos,
                                size,
                                player_pos,
                                *last_buf_opacity,
                                level - buffer_depth_and_min_level,
                            );
                        }
                        MaybeVisual::Air => {}
                        MaybeVisual::None => {}
                    }
                }
                BufVisAnim::Spawning(opacity, _) => {
                    Self::push_terrain_visual_with_opacity(
                        device,
                        queue,
                        bufs,
                        opacity_bufs,
                        terrain_visual,
                        pos,
                        size,
                        player_pos,
                        *opacity,
                        level - buffer_depth_and_min_level,
                    );
                }
            },
            MaybeVisual::Air => (),
            MaybeVisual::None => (),
        }
    }

    fn push_terrain_visual_with_opacity(
        device: &Device,
        queue: &Queue,
        bufs: &mut Vec<Arc<FaceBuffer>>,
        opacity_bufs: &mut Vec<OpacityAndFaceBuffer>,
        terrain_visual: &mut TerrainVisual,
        pos: &ICoord,
        size: i64,
        player_pos: &Point3<f32>,
        opacity: f32,
        block_level: u32,
    ) {
        if opacity == 1. {
            terrain_visual
                .get_or_create_visible_face_bufs(device, queue, pos, size, player_pos)
                .into_iter()
                .for_each(|face_buf| bufs.push(face_buf))
        } else if opacity != 0. {
            terrain_visual
                .get_or_create_visible_face_bufs(device, queue, pos, size, player_pos)
                .into_iter()
                .for_each(|face_buffer| {
                    let block_size = 2_u32.pow(block_level) as f32;

                    opacity_bufs.push(OpacityAndFaceBuffer {
                        face_buffer,
                        opacity,
                        pos: pos.cast::<f32>().unwrap(),
                        block_size,
                    })
                })
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum AncestorAnimating {
    None,
    FadingIn(Duration),
    FadingOut(Duration),
}

impl AncestorAnimating {
    fn fading_in(&self) -> bool {
        match self {
            AncestorAnimating::FadingIn(_) => true,
            AncestorAnimating::FadingOut(_) => false,
            AncestorAnimating::None => false,
        }
    }

    // fn fading_out(&self) -> bool {
    //     match self {
    //         AncestorAnimating::StillFadingIn => false,
    //         AncestorAnimating::StillFadingOut => true,
    //         AncestorAnimating::StartedFadingIn => false,
    //         AncestorAnimating::StartedFadingOut => true,
    //         AncestorAnimating::None => false,
    //     }
    // }

    fn is_animating(&self) -> bool {
        *self != AncestorAnimating::None
    }

    fn or(&self, other: Self) -> Self {
        if *self == AncestorAnimating::None {
            return other;
        } else {
            return *self;
        }
    }

    fn or_equals(&mut self, other: Self) {
        *self = self.or(other)
    }

    // fn set_child_anim(&self, anim: &mut BufVisAnim, cur_time: Instant) {
    //     match self {
    //         AncestorAnimating::None => {}
    //         AncestorAnimating::FadingIn(dur) => {
    //             *anim = BufVisAnim::Animating(BufVisibility::Normal, 0., *dur);
    //             anim.update_opacity(cur_time);
    //         }
    //         AncestorAnimating::FadingOut(dur) => {
    //             *anim = BufVisAnim::Animating(BufVisibility::SeeThrough, 0., *dur);
    //             anim.update_opacity(cur_time);
    //         }
    //     }
    // }
}

impl From<&BufVisAnim> for AncestorAnimating {
    fn from(anim: &BufVisAnim) -> Self {
        use AncestorAnimating::*;
        match anim {
            BufVisAnim::Static(_) => None,

            BufVisAnim::Animating(BufVisibility::Normal, _, dur) => FadingOut(*dur),

            BufVisAnim::Animating(BufVisibility::SeeThrough, _, dur) => FadingIn(*dur),

            BufVisAnim::AnimatingFromLastBuf {
                last_buf: _,
                last_buf_opacity: _,
                opacity: _,
                acc_time: _,
            } => None,
            BufVisAnim::Spawning(_, _) => None,
        }
    }
}
