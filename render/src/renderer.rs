//! What is called by framework.rs to relay stages of the game loop and orchestrate a frame draw

pub mod camera_manager;
pub mod passes;
pub mod player;
pub mod shaders;

use base_utils::CONFIG;
use camera_manager::CameraManager;
use passes::{
    block_highlight_pass::BlockHighlightPass, ssao_pass::SSAOPass, terrain_pass::TerrainPass,
};
use shaders::Shaders;

use crate::framework::{self, Spawner};
use crate::terrain_visualizer::TerrainVisualizer;
use game_utils::{
    input::{CurrentInputs, InputEvent},
    thread_comm::RenderComm,
    thread_comm::RenderStateNotification,
};

use log::*;
use std::io::Write;
use std::{convert::TryFrom, time::Instant};
use wgpu::{Device, PipelineLayout, RenderPipeline, ShaderModule, TextureFormat, TextureView};
use winit::{dpi::PhysicalPosition, event::WindowEvent, window::Window};

use self::{passes::robot_pass::RobotPass, player::Player};

pub struct AmalgomRenderer {
    terrain: TerrainVisualizer,

    terrain_pass: TerrainPass,
    opacity_terrain_pass: TerrainPass,
    ssao_pass: SSAOPass,
    debug_pass: BlockHighlightPass,
    robot_pass: Option<RobotPass>,

    camera_manager: CameraManager,

    player: Player,

    forward_depth: wgpu::TextureView,

    inputs: CurrentInputs,

    render_comm: RenderComm,

    cursor_reset: bool,

    last_time: Instant,
}

impl framework::Renderer for AmalgomRenderer {
    fn init(
        config: &wgpu::SurfaceConfiguration,
        _adapter: &wgpu::Adapter,
        device: &Device,
        queue: &wgpu::Queue,
        window: &winit::window::Window,
        mut render_comm: RenderComm,
    ) -> Self {
        std::io::stdout().flush().unwrap();
        let shaders = Shaders::compile(device);

        let player = Player::new(render_comm.startup_info.player_spawn, &mut render_comm);

        let camera_manager = CameraManager::new(device, config, &player, &mut render_comm);

        let forward_depth = Self::create_depth_buffer(device, config.width, config.height);

        let debug_pass =
            BlockHighlightPass::new(device, config, &shaders, &camera_manager, &mut render_comm);
        let robot_pass = if CONFIG.robot {
            Some(RobotPass::new(
                device,
                config,
                &shaders,
                &camera_manager,
                &mut render_comm,
            ))
        } else {
            None
        };
        let ssao_pass = SSAOPass::new(device, config, &shaders, &camera_manager);
        let terrain_pass = TerrainPass::new(
            device,
            queue,
            config,
            &shaders,
            &camera_manager,
            &ssao_pass,
            false,
        );
        let opacity_terrain_pass = TerrainPass::new(
            device,
            queue,
            config,
            &shaders,
            &camera_manager,
            &ssao_pass,
            true,
        );

        let terrain = TerrainVisualizer::new(
            &render_comm.startup_info, // TODO sync spawn position with TerrainManager in a better way
            render_comm.terrain_bufs_recv.take().unwrap(),
        );

        // Done
        let amalgom = AmalgomRenderer {
            terrain,

            terrain_pass,
            opacity_terrain_pass,
            ssao_pass,
            debug_pass,
            robot_pass,

            camera_manager,

            player,

            forward_depth,

            inputs: CurrentInputs::default(),

            render_comm,

            cursor_reset: true,

            last_time: Instant::now(),
        };

        window.set_visible(true);
        window.set_cursor_visible(false);

        amalgom
            .render_comm
            .renderer_state
            .send(RenderStateNotification::Started)
            .expect("Failed to send startup message to logic thread");

        amalgom
    }

    fn update_window_event(&mut self, event: WindowEvent, window: &Window) {
        if let Ok(e) = InputEvent::try_from(&event) {
            self.render_comm
                .input_event_sender
                .send(e)
                .expect("Failed to send event in channel");

            self.inputs.process_window_event(&event);

            if self.inputs.just_pressed.f10 || self.inputs.just_pressed.f12 {
                if window.fullscreen().is_some() {
                    window.set_fullscreen(None);
                } else {
                    window.set_fullscreen(Some(winit::window::Fullscreen::Borderless(
                        window.current_monitor(),
                    )));
                }
            }

            self.terrain_pass.update_input(&self.inputs);
            self.opacity_terrain_pass.update_input(&self.inputs);
        }
    }

    fn update_mouse(&mut self, input: (f64, f64)) {
        if self.cursor_reset {
            self.cursor_reset = false;
        } else {
            self.camera_manager.update_mouse(input);
        }
    }

    fn update_frame(&mut self, device: &wgpu::Device, queue: &wgpu::Queue) {
        let this_time = Instant::now();
        let dt = (this_time - self.last_time).as_secs_f32().min(5.0 / 30.0);

        self.player
            .update_tick(&self.inputs, &mut self.camera_manager.camera, dt);

        let camera_changed = self.camera_manager.update_tick(queue);

        self.terrain
            .update(&self.camera_manager, camera_changed, device, queue);

        self.debug_pass
            .update(device, queue, &self.terrain.tree, &self.inputs);

        if let Some(rp) = self.robot_pass.as_mut() {
            rp.update(device, queue, &self.terrain.tree);
        }

        self.terrain_pass.update(&queue);
        self.opacity_terrain_pass.update(&queue);

        self.inputs.new_tick();
        self.last_time = this_time;
    }

    fn resize(
        &mut self,
        config: &wgpu::SurfaceConfiguration,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        window: &winit::window::Window,
    ) {
        let size = window.inner_size();
        let center = PhysicalPosition::new(f64::from(size.width) / 2., f64::from(size.height) / 2.);
        if let Err(error) = window.set_cursor_position(center) {
            error!("Failed to set cursor position: {}", error);
        } else {
            self.cursor_reset = true;
        }

        if let Err(error) = window.set_cursor_grab(winit::window::CursorGrabMode::Confined) {
            error!("Failed to grab cursor: {}", error);
        }

        self.forward_depth = Self::create_depth_buffer(device, size.width, size.height);

        self.ssao_pass
            .resize(size.width, size.height, device, &self.camera_manager);
        self.terrain_pass.resize(
            size.width,
            size.height,
            device,
            &self.camera_manager,
            &self.ssao_pass,
        );
        self.opacity_terrain_pass.resize(
            size.width,
            size.height,
            device,
            &self.camera_manager,
            &self.ssao_pass,
        );

        self.camera_manager.camera.controller.aspect = config.width as f32 / config.height as f32;
        self.camera_manager.process_changes(queue);
    }

    fn render(
        &mut self,
        view: &wgpu::TextureView,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        _spawner: &Spawner,
    ) {
        // Change lod
        let mut encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Render Encoder"),
        });

        if self.terrain_pass.debug_lighting_toggle {
            self.ssao_pass
                .render(view, device, &mut encoder, &self.terrain.buffers);
        }

        self.terrain_pass.render(
            view,
            device,
            &mut encoder,
            &self.terrain.buffers,
            &self.forward_depth,
            &self.camera_manager,
        );
        self.opacity_terrain_pass.render(
            view,
            device,
            &mut encoder,
            &self.terrain.opacity_buffers,
            &self.forward_depth,
            &self.camera_manager,
        );

        if let Some(rp) = self.robot_pass.as_mut() {
            rp.render(view, device, &mut encoder, &self.forward_depth);
        }

        self.debug_pass
            .render(view, device, &mut encoder, &self.forward_depth);

        queue.submit(std::iter::once(encoder.finish()));
    }
}

impl AmalgomRenderer {
    pub const DEPTH_FORMAT: TextureFormat = TextureFormat::Depth32Float;
    pub const DATA_FORMAT: TextureFormat = TextureFormat::Rgba32Float;

    pub fn create_depth_buffer(device: &Device, width: u32, height: u32) -> TextureView {
        device
            .create_texture(&wgpu::TextureDescriptor {
                size: wgpu::Extent3d {
                    width,
                    height,
                    depth_or_array_layers: 1,
                },
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: AmalgomRenderer::DEPTH_FORMAT,
                usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
                label: Some("Depth Buffer"),
            })
            .create_view(&Default::default())
    }

    /// Has usage OUTPUT_ATTACHMENT | SAMPLED, format: DATA_FORMAT
    pub fn create_data_texture(
        device: &Device,
        width: u32,
        height: u32,
        label: Option<&'static str>,
    ) -> TextureView {
        device
            .create_texture(&wgpu::TextureDescriptor {
                size: wgpu::Extent3d {
                    width,
                    height,
                    depth_or_array_layers: 1,
                },
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: AmalgomRenderer::DATA_FORMAT,
                usage: wgpu::TextureUsages::RENDER_ATTACHMENT
                    | wgpu::TextureUsages::COPY_SRC
                    | wgpu::TextureUsages::TEXTURE_BINDING,
                label,
            })
            .create_view(&Default::default())
    }

    pub fn create_simple_pipeline_for_verts(
        config: &wgpu::SurfaceConfiguration,
        device: &wgpu::Device,
        pipeline_layout: &PipelineLayout,
        vert: &ShaderModule,
        frag: &ShaderModule,
        label: &'static str,
        with_opacity_blending: bool,
    ) -> RenderPipeline {
        device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: Some(pipeline_layout),
            vertex: wgpu::VertexState {
                module: vert,
                entry_point: "main",
                buffers: &[wgpu::VertexBufferLayout {
                    array_stride: std::mem::size_of::<game_utils::terrain_mesh::Vertex>()
                        as wgpu::BufferAddress,
                    step_mode: wgpu::VertexStepMode::Vertex,
                    attributes: &[
                        wgpu::VertexAttribute {
                            format: wgpu::VertexFormat::Float32x3,
                            offset: 0,
                            shader_location: 0,
                            //pos
                        },
                        wgpu::VertexAttribute {
                            format: wgpu::VertexFormat::Uint32,
                            offset: 4 * 3,
                            shader_location: 1,
                            // corner: u2,
                            // level: u6,
                            // tex_palette: u8,
                            // tex_id: u8,
                            // face: u8,
                        },
                    ],
                }],
            },
            fragment: Some(wgpu::FragmentState {
                module: frag,
                entry_point: "main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format,
                    blend: if with_opacity_blending {
                        Some(wgpu::BlendState {
                            color: wgpu::BlendComponent {
                                src_factor: wgpu::BlendFactor::SrcAlpha,
                                dst_factor: wgpu::BlendFactor::OneMinusSrcAlpha,
                                operation: wgpu::BlendOperation::Add,
                            },
                            alpha: wgpu::BlendComponent {
                                src_factor: wgpu::BlendFactor::One,
                                dst_factor: wgpu::BlendFactor::One,
                                operation: wgpu::BlendOperation::Max,
                            },
                        })
                    } else {
                        Some(wgpu::BlendState::REPLACE)
                    },
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }), // TODO: get rid of frag shader for normal shader? It can be "flat"
            primitive: wgpu::PrimitiveState {
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: None,
                strip_index_format: None,
                topology: wgpu::PrimitiveTopology::TriangleList,
                polygon_mode: wgpu::PolygonMode::Fill,
                ..Default::default()
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: AmalgomRenderer::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: Default::default(),
                bias: wgpu::DepthBiasState {
                    constant: 0,
                    slope_scale: 0.0,
                    clamp: 0.0,
                },
            }),
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            label: Some(label),
            multiview: None
        })
    }
}

impl Drop for AmalgomRenderer {
    fn drop(&mut self) {
        self.render_comm
            .renderer_state
            .send(RenderStateNotification::Stopping)
            .expect("Failed to send stopping message to logic thread");
    }
}
