use base_utils::F32Coord;
use cgmath::MetricSpace;
use derivative::Derivative;
use std::sync::Arc;
use zerocopy::AsBytes;

use crate::renderer::camera_manager::CameraManager;

const DEPTH_CHANGE_PROPORTION: f32 = 0.5;

#[derive(Derivative)]
#[derivative(Debug)]
pub struct FaceBuffer {
    #[derivative(Debug = "ignore")]
    pub vertices: wgpu::Buffer,
    #[derivative(Debug = "ignore")]
    pub indices: wgpu::Buffer,
    pub indices_len: u32,
}

#[derive(Debug, Default)]
pub struct TerrainBuffer {
    pub buffs: [Option<Arc<FaceBuffer>>; 6],
}

pub struct OpacityAndFaceBuffer {
    pub face_buffer: Arc<FaceBuffer>,
    pub opacity: f32,
    pub pos: F32Coord,
    pub block_size: f32,
}

pub trait FaceBufferRender {
    fn render<'a, 's: 'a>(
        &'s self,
        rpass: &mut wgpu::RenderPass<'a>,
        camera_manager: &CameraManager,
    );
}

impl FaceBufferRender for Arc<FaceBuffer> {
    fn render<'a, 's: 'a>(
        &'s self,
        rpass: &mut wgpu::RenderPass<'a>,
        _camera_manager: &CameraManager,
    ) {
        rpass.set_index_buffer(self.indices.slice(..), wgpu::IndexFormat::Uint32);
        rpass.set_vertex_buffer(0, self.vertices.slice(..));
        rpass.draw_indexed(0..self.indices_len, 0, 0..1);
    }
}

impl FaceBufferRender for OpacityAndFaceBuffer {
    fn render<'a, 's: 'a>(
        &'s self,
        rpass: &mut wgpu::RenderPass<'a>,
        camera_manager: &CameraManager,
    ) {
        let depth_change_size = {
            // We could be more accurate by centering this, but it doesn't really matter.
            let p1 = self.pos;
            let p2 = self.pos + camera_manager.camera.cached_vectors.look_dir * self.block_size;

            let p1 = camera_manager.camera.cached_matrices.view_proj * p1.extend(1.0);
            let p2 = camera_manager.camera.cached_matrices.view_proj * p2.extend(1.0);
            let p1_proj = p1.xyz() / p1.w;
            let p2_proj = p2.xyz() / p2.w;

            DEPTH_CHANGE_PROPORTION * p2_proj.distance(p1_proj) * (1. - self.opacity)
        };

        rpass.set_push_constants(wgpu::ShaderStages::VERTEX, 0, depth_change_size.as_bytes());
        rpass.set_push_constants(wgpu::ShaderStages::FRAGMENT, 4, self.opacity.as_bytes());
        rpass.set_index_buffer(
            self.face_buffer.indices.slice(..),
            wgpu::IndexFormat::Uint32,
        );
        rpass.set_vertex_buffer(0, self.face_buffer.vertices.slice(..));
        rpass.draw_indexed(0..self.face_buffer.indices_len, 0, 0..1);
    }
}
