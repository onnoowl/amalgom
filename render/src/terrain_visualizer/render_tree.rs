use std::time::{Duration, Instant};

use octree::{traits::BranchNew, OctNode, Octree};
use BufVisAnim::*;
use BufVisibility::*;

use crate::renderer::passes::terrain_pass::MaybeVisual;

pub type RenderTree = Octree<RenderBranchData, RenderBranchData, ()>;
pub type RenderNode = OctNode<RenderBranchData, RenderBranchData, ()>;

const ANIMATION_DURATION: Duration = Duration::from_millis(2000);
const SPAWN_DURATION: Duration = Duration::from_millis(500);

#[derive(Default, Debug)]
pub struct RenderBranchData {
    pub visual: MaybeVisual,
    pub all_children_have_bufs: bool,
    pub all_children_not_animating: bool,
    pub anim: BufVisAnim,
    pub waiting_new_visual: Option<MaybeVisual>,
    pub was_culled_last_frame: bool,
}

impl BranchNew for RenderBranchData {
    type Node = RenderNode;
    fn new_branch(_: u8, _: &[Self::Node]) -> Self {
        return Default::default();
    }
}

#[derive(Debug)]
pub enum BufVisAnim {
    Static(BufVisibility),
    Animating(BufVisibility, f32, Duration),
    AnimatingFromLastBuf {
        last_buf: Box<MaybeVisual>,
        last_buf_opacity: f32,
        opacity: f32,
        acc_time: Duration,
    },
    Spawning(f32, Instant),
}

impl RenderBranchData {
    pub fn update_anim(
        &mut self,
        should_be_visible: bool,
        cur_time: Instant,
        time_change: Duration,
        allowed_to_switch_to_animating: bool,
    ) {
        if self.waiting_new_visual.is_some() {
            if matches!(&mut self.anim, Static(Normal))
                || matches!(&mut self.anim, Spawning(_, start_time) if cur_time - *start_time > SPAWN_DURATION)
            {
                // if allowed_to_switch_to_animating {
                let mut tmp = self.waiting_new_visual.take().unwrap();
                std::mem::swap(&mut tmp, &mut self.visual);
                self.anim = AnimatingFromLastBuf {
                    last_buf: Box::new(tmp),
                    last_buf_opacity: 1.0,
                    opacity: 0.,
                    acc_time: Duration::ZERO,
                }
                // }
            }
            if matches!(&mut self.anim, Static(SeeThrough)) {
                self.visual = self.waiting_new_visual.take().unwrap()
            }
        }

        match &mut self.anim {
            Static(state) => {
                if allowed_to_switch_to_animating {
                    match (state, should_be_visible) {
                        (Normal, false) => self.anim = Animating(Normal, 1., Duration::ZERO),
                        (SeeThrough, true) => self.anim = Animating(SeeThrough, 0., Duration::ZERO),
                        _ => (),
                    }
                }
            }
            Animating(_, _, acc_time) => {
                *acc_time += time_change;
                self.anim.update_opacity(cur_time);
            }
            BufVisAnim::AnimatingFromLastBuf {
                last_buf: _,
                last_buf_opacity: _,
                opacity: _,
                acc_time,
            } => {
                *acc_time += time_change;
                self.anim.update_opacity(cur_time);
            }
            Spawning(_, _) => {
                self.anim.update_opacity(cur_time);
            }
        }
    }
}

impl BufVisAnim {
    pub fn update_opacity(&mut self, cur_time: Instant) {
        match self {
            Static(_) => {}
            Animating(starting_state, opacity, acc_time) => {
                if *acc_time > ANIMATION_DURATION {
                    self.finish()
                } else {
                    let acc_time = acc_time.as_secs_f32();
                    let total_duration = ANIMATION_DURATION.as_secs_f32();
                    let half = total_duration / 2.;
                    *opacity = match starting_state {
                        Normal => normal_to_seethrough(acc_time, half),
                        SeeThrough => seethrough_to_normal(acc_time, half),
                    }
                }
            }
            BufVisAnim::AnimatingFromLastBuf {
                last_buf: _,
                last_buf_opacity,
                opacity,
                acc_time,
            } => {
                if *acc_time > ANIMATION_DURATION {
                    self.finish()
                } else {
                    let acc_time = acc_time.as_secs_f32();
                    let total_duration = ANIMATION_DURATION.as_secs_f32();
                    let half = total_duration / 2.;
                    *last_buf_opacity = normal_to_seethrough(acc_time, half);
                    *opacity = seethrough_to_normal(acc_time, half);
                }
            }
            Spawning(opacity, start_time) => {
                let dif = cur_time - *start_time;
                if dif > ANIMATION_DURATION {
                    self.finish()
                } else {
                    *opacity = (dif.as_secs_f32() / SPAWN_DURATION.as_secs_f32())
                        .min(1.0)
                        .max(0.0)
                }
            }
        }
    }

    /// Force the animation into its finished state
    pub fn finish(&mut self) {
        match self {
            Static(_) => {}

            Animating(starting_state, _, _) => *self = Static(starting_state.flip()),

            BufVisAnim::AnimatingFromLastBuf {
                last_buf: _,
                last_buf_opacity: _,
                opacity: _,
                acc_time: _,
            } => *self = Static(Normal),

            Spawning(_, _) => *self = Static(BufVisibility::Normal),
        }
    }

    /// Force the animations from invis to visible to finish
    pub fn finish_appearing(&mut self) {
        match self {
            Static(_) => {}

            Animating(BufVisibility::SeeThrough, _, _) => *self = Static(BufVisibility::Normal),
            Animating(BufVisibility::Normal, _, _) => (),

            BufVisAnim::AnimatingFromLastBuf {
                last_buf: _,
                last_buf_opacity: _,
                opacity: _,
                acc_time: _,
            } => *self = Static(Normal),

            Spawning(_, _) => *self = Static(BufVisibility::Normal),
        }
    }

    pub fn try_sync_animation_dur(&mut self, dur: Duration, cur_time: Instant) {
        match self {
            Static(_) => {}
            Animating(_, _, self_dur) => {
                *self_dur = dur;
                self.update_opacity(cur_time);
            }
            AnimatingFromLastBuf {
                last_buf: _,
                last_buf_opacity: _,
                opacity: _,
                acc_time: _,
            } => {}
            Spawning(_, _) => {}
        }
    }
}

impl Default for BufVisAnim {
    fn default() -> Self {
        Spawning(0., Instant::now())
    }
}

impl RenderBranchData {
    pub fn new_visual(&mut self, new_visual: MaybeVisual) {
        if self.visual.is_some() {
            self.waiting_new_visual = Some(new_visual);
        } else {
            self.visual = new_visual;
            self.anim = Static(SeeThrough)
        }
    }
}

fn normal_to_seethrough(time_dif: f32, half: f32) -> f32 {
    // doesn't start disappaering until second half of animation
    if time_dif < half {
        1.
    } else {
        (1. - ((time_dif - half) / half))
            .min(1.0) // clamp to 0-1 range
            .max(0.0)
    }
}

fn seethrough_to_normal(time_dif: f32, half: f32) -> f32 {
    // starts appearing in first half of animation
    if time_dif < half {
        (time_dif / half)
            .min(1.0) // clamp to 0-1 range
            .max(0.0)
    } else {
        1.
    }
}

#[derive(Debug)]
pub enum BufVisibility {
    Normal,
    SeeThrough,
}

impl BufVisibility {
    fn flip(&self) -> Self {
        match self {
            Normal => SeeThrough,
            SeeThrough => Normal,
        }
    }
}
