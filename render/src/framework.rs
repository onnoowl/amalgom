//! Structure for generic game loop
//!
//! Handles device specific operations like which monitor the game will be displayed on
//! Mainly taken from boilerplate wgpu

use game_utils::thread_comm::RenderComm;

#[cfg(not(target_arch = "wasm32"))]
use winit::{
    dpi::PhysicalPosition,
    event::{self, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::Fullscreen,
};

#[allow(dead_code)]
pub fn cast_slice<T>(data: &[T]) -> &[u8] {
    use std::{mem::size_of, slice::from_raw_parts};

    unsafe { from_raw_parts(data.as_ptr() as *const u8, data.len() * size_of::<T>()) }
}

#[allow(dead_code)]
pub enum ShaderStages {
    Vertex,
    Fragment,
    Compute,
}

pub trait Renderer: 'static + Sized {
    fn optional_features() -> wgpu::Features {
        wgpu::Features::empty()
    }
    fn required_features() -> wgpu::Features {
        wgpu::Features::PUSH_CONSTANTS | wgpu::Features::POLYGON_MODE_LINE | wgpu::Features::SPIRV_SHADER_PASSTHROUGH
        // TODO: The NON_FILL is for wireframes, but isn't well supported. We need to remove this
    }
    fn required_limits() -> wgpu::Limits {
        let mut ret = wgpu::Limits::default();
        ret.max_push_constant_size = 128; // needs to match bloch_highlight_pass
        return ret;
    }
    fn init(
        config: &wgpu::SurfaceConfiguration,
        adapter: &wgpu::Adapter,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        window: &winit::window::Window,
        render_comm: RenderComm,
    ) -> Self;
    fn resize(
        &mut self,
        config: &wgpu::SurfaceConfiguration,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        window: &winit::window::Window,
    );
    fn update_mouse(&mut self, motion: (f64, f64));
    fn update_window_event(&mut self, event: WindowEvent, window: &winit::window::Window);
    fn update_frame(&mut self, device: &wgpu::Device, queue: &wgpu::Queue);
    fn render(
        &mut self,
        view: &wgpu::TextureView,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        spawner: &Spawner,
    );
}

struct Setup {
    window: winit::window::Window,
    event_loop: EventLoop<()>,
    instance: wgpu::Instance,
    size: winit::dpi::PhysicalSize<u32>,
    surface: wgpu::Surface,
    adapter: wgpu::Adapter,
    device: wgpu::Device,
    queue: wgpu::Queue,
}

async fn setup<E: Renderer>(title: &str) -> Setup {
    #[cfg(all(not(target_arch = "wasm32"), feature = "subscriber"))]
    {
        let chrome_tracing_dir = std::env::var("WGPU_CHROME_TRACE");
        wgpu::util::initialize_default_subscriber(
            chrome_tracing_dir.as_ref().map(std::path::Path::new).ok(),
        );
    };

    #[cfg(target_arch = "wasm32")]
    console_log::init().expect("could not initialize logger");

    let event_loop = EventLoop::new();
    let mut builder = winit::window::WindowBuilder::new();
    builder = builder.with_title(title);

    let monitor = event_loop.available_monitors().last().unwrap();
    builder = builder
        .with_fullscreen(Some(Fullscreen::Borderless(Some(monitor))))
        .with_visible(false);

    #[cfg(windows_OFF)] // TODO
    {
        use winit::platform::windows::WindowBuilderExtWindows;
        builder = builder.with_no_redirection_bitmap(true);
    }
    let window = builder.build(&event_loop).unwrap();

    log::info!("Initializing the surface...");

    let backend = wgpu::util::backend_bits_from_env().unwrap_or(wgpu::Backends::PRIMARY);

    let instance = wgpu::Instance::new(backend);
    let (size, surface) = unsafe {
        let size = window.inner_size();
        let surface = instance.create_surface(&window);
        (size, surface)
    };

    // Grabbing Cursor
    if let Err(error) = window.set_cursor_grab(winit::window::CursorGrabMode::Confined) {
        log::error!("Failed to grab cursor: {}", error);
    }
    let center = PhysicalPosition::new(f64::from(size.width) / 2., f64::from(size.height) / 2.);
    if let Err(error) = window.set_cursor_position(center) {
        log::error!("Failed to set cursor position: {}", error);
    }

    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        })
        .await
        .unwrap();

    let optional_features = E::optional_features();
    let required_features = E::required_features();
    let adapter_features = adapter.features();
    assert!(
        adapter_features.contains(required_features),
        "Adapter does not support required features for this example: {:?}",
        required_features - adapter_features
    );

    let needed_limits = E::required_limits();

    let trace_dir = std::env::var("WGPU_TRACE");
    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                features: (optional_features & adapter_features) | required_features,
                limits: needed_limits,
                label: None,
            },
            trace_dir.ok().as_ref().map(std::path::Path::new),
        )
        .await
        .unwrap();

    Setup {
        window,
        event_loop,
        instance,
        size,
        surface,
        adapter,
        device,
        queue,
    }
}

fn start<E: Renderer>(
    Setup {
        window,
        event_loop,
        instance,
        size,
        surface,
        adapter,
        device,
        queue,
    }: Setup,
    render_comm: RenderComm,
) {
    let spawner = Spawner::new();

    let mut config = wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        // TODO: Allow srgb unconditionally
        format: if cfg!(target_arch = "wasm32") {
            wgpu::TextureFormat::Bgra8Unorm
        } else {
            wgpu::TextureFormat::Bgra8UnormSrgb
        },
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Mailbox,
        alpha_mode: wgpu::CompositeAlphaMode::Opaque
    };
    surface.configure(&device, &config);

    log::info!("Initializing amalgom...");
    let mut renderer = E::init(&config, &adapter, &device, &queue, &window, render_comm);

    log::info!("Entering render loop...");
    event_loop.run(move |event, _, control_flow| {
        let _ = (&instance, &adapter); // force ownership by the closure
        *control_flow = if cfg!(feature = "metal-auto-capture") {
            ControlFlow::Exit
        } else {
            ControlFlow::Poll
        };
        match event {
            event::Event::RedrawEventsCleared => {
                #[cfg(not(target_arch = "wasm32"))]
                spawner.run_until_stalled();

                window.request_redraw();
            }
            event::Event::WindowEvent {
                event:
                    WindowEvent::Resized(size)
                    | WindowEvent::ScaleFactorChanged {
                        new_inner_size: &mut size,
                        ..
                    },
                ..
            } => {
                log::info!("Resizing to {:?}", size);
                config.width = size.width;
                config.height = size.height;
                renderer.resize(&config, &device, &queue, &window);
                surface.configure(&device, &config);
            }
            event::Event::WindowEvent {
                event: WindowEvent::Focused(false),
                ..
            } => {
                // TODO: Re-enable after upgrade to wgpu 0.7
                // window.set_minimized(true);
            }
            event::Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    input:
                        event::KeyboardInput {
                            virtual_keycode: Some(event::VirtualKeyCode::Escape),
                            state: event::ElementState::Pressed,
                            ..
                        },
                    ..
                }
                | WindowEvent::CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                }
                _ => {
                    renderer.update_window_event(event, &window);
                }
            },
            event::Event::RedrawRequested(_) => {
                renderer.update_frame(&device, &queue);
                let frame = match surface.get_current_texture() {
                    Ok(frame) => frame,
                    Err(_) => {
                        surface.configure(&device, &config);
                        surface
                            .get_current_texture()
                            .expect("Failed to acquire next surface texture!")
                    }
                };
                let view = frame
                    .texture
                    .create_view(&wgpu::TextureViewDescriptor::default());

                renderer.render(&view, &device, &queue, &spawner);

                frame.present();

                #[cfg(target_arch = "wasm32")]
                {
                    if let Some(offscreen_canvas_setup) = &offscreen_canvas_setup {
                        let image_bitmap = offscreen_canvas_setup
                            .offscreen_canvas
                            .transfer_to_image_bitmap()
                            .expect("couldn't transfer offscreen canvas to image bitmap.");
                        offscreen_canvas_setup
                            .bitmap_renderer
                            .transfer_from_image_bitmap(&image_bitmap);

                        log::info!("Transferring OffscreenCanvas to ImageBitmapRenderer");
                    }
                }
            }
            event::Event::DeviceEvent { event, .. } => {
                if let event::DeviceEvent::MouseMotion { delta } = event {
                    renderer.update_mouse(delta);
                }
            }
            _ => {}
        }
    })
}

#[cfg(not(target_arch = "wasm32"))]
pub struct Spawner<'a> {
    executor: async_executor::LocalExecutor<'a>,
}

#[cfg(not(target_arch = "wasm32"))]
impl<'a> Spawner<'a> {
    fn new() -> Self {
        Self {
            executor: async_executor::LocalExecutor::new(),
        }
    }

    #[allow(dead_code)]
    pub fn spawn_local(&self, future: impl futures::Future<Output = ()> + 'a) {
        self.executor.spawn(future).detach();
    }

    fn run_until_stalled(&self) {
        while self.executor.try_tick() {}
    }
}

#[cfg(target_arch = "wasm32")]
pub struct Spawner {}

#[cfg(target_arch = "wasm32")]
impl Spawner {
    fn new() -> Self {
        Self {}
    }

    #[allow(dead_code)]
    pub fn spawn_local(&self, future: impl Future<Output = ()> + 'static) {
        wasm_bindgen_futures::spawn_local(future);
    }
}

#[cfg(not(target_arch = "wasm32"))]
pub fn run<E: Renderer>(title: &str, render_comm: RenderComm) {
    let setup = pollster::block_on(setup::<E>(title));
    start::<E>(setup, render_comm);
}

#[cfg(target_arch = "wasm32")]
pub fn run<E: Example>(title: &str, render_comm: RendererComm) {
    let title = title.to_owned();
    wasm_bindgen_futures::spawn_local(async move {
        let setup = setup::<E>(&title).await;
        start::<E>(setup, render_comm);
    });
}

// This allows treating the framework as a standalone example,
// thus avoiding listing the example names in `Cargo.toml`.
#[allow(dead_code)]
fn main() {}
