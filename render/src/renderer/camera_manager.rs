use game_utils::{
    camera::{Camera, CameraController},
    thread_comm::RenderComm,
};

use crossbeam::atomic::AtomicCell;
use std::sync::Arc;
use wgpu::{util::DeviceExt, Buffer};
use zerocopy::AsBytes;

use super::player::Player;

/// Manages the camera, its updates (from keyboard, mouse, and from physics),
/// as well as communication with the relevant GPU buffers and the logic thread
pub struct CameraManager {
    pub camera: Camera,
    pub camera_cell: Arc<AtomicCell<Option<Camera>>>,
    pub mouse_sensitivity: f32,
    last_camera_controller: Option<CameraController>,

    pub view_proj_uniform_buf: Arc<Buffer>,
    pub view_uniform_buf: Arc<Buffer>,
    pub proj_uniform_buf: Arc<Buffer>,
}

impl CameraManager {
    pub fn new(
        device: &wgpu::Device,
        config: &wgpu::SurfaceConfiguration,
        player: &Player,
        render_comm: &mut RenderComm,
    ) -> Self {
        let camera = Camera::new(
            &player.get_eye_pos(),
            config.width as f32 / config.height as f32,
        );

        render_comm.camera_cell.store(Some(camera.clone()));

        let view_proj_uniform_buf = {
            let mx_total = camera.cached_matrices.view_proj;
            let mx_ref: &[f32; 16] = mx_total.as_ref();
            Arc::new(
                device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    contents: mx_ref.as_bytes(),
                    usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                    label: Some("Camera View Proj Uniform"),
                }),
            )
        };

        let view_uniform_buf = {
            let mx_total = camera.cached_matrices.view;
            let mx_ref: &[f32; 16] = mx_total.as_ref();
            Arc::new(
                device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    contents: mx_ref.as_bytes(),
                    usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                    label: Some("Camera View Uniform"),
                }),
            )
        };

        let proj_uniform_buf = {
            let mx_total = camera.cached_matrices.projection;
            let mx_ref: &[f32; 16] = mx_total.as_ref();
            Arc::new(
                device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    contents: mx_ref.as_bytes(),
                    usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                    label: Some("Camera Proj Uniform"),
                }),
            )
        };

        CameraManager {
            camera,
            camera_cell: render_comm.camera_cell.clone(),
            last_camera_controller: None,
            mouse_sensitivity: 0.001,
            view_proj_uniform_buf,
            view_uniform_buf,
            proj_uniform_buf,
        }
    }

    pub fn update_mouse(&mut self, input: (f64, f64)) {
        self.camera.controller.yaw -= input.0 as f32 * self.mouse_sensitivity;
        self.camera.controller.pitch += input.1 as f32 * self.mouse_sensitivity;
    }

    /// Returns true if the camera changed this tick
    pub fn update_tick(&mut self, queue: &wgpu::Queue) -> bool {
        if self.last_camera_controller.as_ref() != Some(&self.camera.controller) {
            self.process_changes(queue);
            self.last_camera_controller = Some(self.camera.controller.clone());

            true
        } else {
            false
        }
    }

    pub fn process_changes(&mut self, queue: &wgpu::Queue) {
        self.camera.update_cached_vectors();
        self.camera.update_cached_matrices();
        self.update_uniform_buffers(queue);
        self.camera_cell.store(Some(self.camera.clone()));
    }

    fn update_uniform_buffers(&mut self, queue: &wgpu::Queue) {
        {
            let mx_total = self.camera.cached_matrices.view_proj;
            let mx_ref: &[f32; 16] = mx_total.as_ref();

            queue.write_buffer(&self.view_proj_uniform_buf, 0, mx_ref.as_bytes());
        }
        {
            let mx_total = self.camera.cached_matrices.view;
            let mx_ref: &[f32; 16] = mx_total.as_ref();

            queue.write_buffer(&self.view_uniform_buf, 0, mx_ref.as_bytes());
        }
        {
            let mx_total = self.camera.cached_matrices.projection;
            let mx_ref: &[f32; 16] = mx_total.as_ref();

            queue.write_buffer(&self.proj_uniform_buf, 0, mx_ref.as_bytes());
        }
    }
}
