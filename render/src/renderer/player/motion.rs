use base_utils::{
    live_reload::LiveReload, live_reload_load, rwcube::RwCubeReader, COLLISION_CAGE_SIZE,
};
use cgmath::{EuclideanSpace, InnerSpace, Point3, Vector3};
use game_utils::{camera::CameraVectors, input::CurrentInputs};

use serde::Deserialize;

use super::{collision_resolver::CollisionResolver, grappling_hook::GrapplingHook};

/// Manages the motion of the camera (player physics, movement, grappling hook stuff, etc.)
pub struct Motion {
    pub consts: LiveReload<MotionConsts>,

    pub values: MotionValues,

    pub collision_cage: RwCubeReader<bool, COLLISION_CAGE_SIZE>,
}

pub struct MotionValues {
    pub velocity: Vector3<f32>,
    pub is_grappling: bool,
    pub is_crouched: bool,
    pub is_on_ground: bool,
    pub noclip: bool,
}

pub struct MotionCtxt<'a, 'm> {
    pub next_loc: Point3<f32>,

    pub loc: &'a Point3<f32>,
    pub last_vel: &'a Vector3<f32>,

    pub dt: f32,

    pub consts: &'m MotionConsts,
    pub values: &'m mut MotionValues,

    pub inputs: &'a CurrentInputs,
    pub vectors: &'a CameraVectors,
}

#[derive(Debug, Clone, Deserialize)]
pub struct MotionConsts {
    pub noclip_speed: f32,
    pub noclip_fast_speed: f32,
    pub walking_speed: f32, // In minecraft you can walk 20 blocks in 4.5 seconds
    pub sprinting_speed: f32,
    pub crouch_speed: f32,

    pub ground_accel_ratio_of_speed: f32,
    pub hanging_in_air_accel: f32,
    pub ground_friction_coeff: f32,
    pub jump_vel_delta: f32,
    pub gravity_accel: f32,

    pub player_eye_height: f32,
    pub player_width: f32,
    pub player_height: f32,
    pub player_crouch_height: f32,

    pub grapple_max_accel_while_lengthening: f32,
    pub grapple_max_accel_whie_shortening: f32,
    pub grapple_max_vel: f32,
    pub grapple_anti_grav_ratio: f32,
    pub grapple_hanging_in_air_accel: f32,
    pub grapple_pump_accel: f32,
    pub grapple_pump_stamina_drain: f32,
}

impl Motion {
    pub fn new(collision_cage: RwCubeReader<bool, COLLISION_CAGE_SIZE>) -> Self {
        Self {
            consts: live_reload_load!("motion_consts.ron"),
            values: MotionValues {
                is_grappling: false,
                is_crouched: false,
                is_on_ground: false,
                noclip: false,
                velocity: Vector3::new(0., 0., 0.),
            },
            collision_cage,
        }
    }

    pub fn update_tick(
        &mut self,
        loc: &mut Point3<f32>,
        inputs: &CurrentInputs,
        vectors: &CameraVectors,
        gh: &mut GrapplingHook,
        dt: f32,
    ) {
        self.consts.check_reload();

        let last_vel = self.values.velocity;

        let mut m = MotionCtxt {
            next_loc: *loc,
            loc: &loc,
            last_vel: &last_vel,
            dt,
            consts: &*self.consts,
            values: &mut self.values,
            inputs,
            vectors,
        };

        {
            if inputs.just_pressed.n {
                m.values.noclip = !m.values.noclip;
                log::info!("Noclip set to {}", m.values.noclip);
                m.values.is_on_ground = false;
            }

            gh.check_status(&mut m);
            Self::update_vel_with_friction(&mut m);
            Self::update_vel_with_wasd_movement(&mut m);
            Self::update_vel_with_grav(&mut m);
            gh.update_tick(&mut m);
            m.next_loc += m.values.velocity * m.dt;
            CollisionResolver::resolve_collisions(&mut m, self.collision_cage.read_handle());

            self.collision_cage.request_cube_adjust_around(
                &m.loc.cast::<i64>().unwrap().to_vec(),
                Some(&m.values.velocity),
            )
        }

        *loc = m.next_loc;
    }

    pub fn update_vel_with_friction(m: &mut MotionCtxt) {
        if m.values.noclip {
            m.values.velocity *= 0.;
        } else if m.values.is_on_ground && !m.values.is_grappling {
            m.values.velocity *= m.consts.ground_friction_coeff.powf(m.dt * 60.0);
        }
    }

    pub fn build_wasd_vector(
        inputs: &CurrentInputs,
        vectors: &CameraVectors,
        use_true_forward: bool,
    ) -> Vector3<f32> {
        let mut wasd_vec = Vector3::new(0., 0., 0.);

        if inputs.pressed.w {
            if use_true_forward {
                wasd_vec += vectors.look_dir;
            } else {
                wasd_vec += vectors.forward_xz;
            }
        }
        if inputs.pressed.s {
            if use_true_forward {
                wasd_vec -= vectors.look_dir;
            } else {
                wasd_vec -= vectors.forward_xz;
            }
        }
        if inputs.pressed.d {
            wasd_vec += vectors.right_xz;
        }
        if inputs.pressed.a {
            wasd_vec -= vectors.right_xz;
        }

        if inputs.pressed.lControl {
            wasd_vec -= vectors.up;
        }

        if inputs.pressed.space {
            wasd_vec += vectors.up;
        }

        wasd_vec
    }

    pub fn update_vel_with_wasd_movement(m: &mut MotionCtxt) {
        let lshift = m.inputs.pressed.lShift;
        let lctrl = m.inputs.pressed.lControl;

        let speed = if m.values.noclip {
            m.values.is_crouched = false;
            m.values.is_grappling = false;
            if lshift {
                m.consts.noclip_fast_speed
            } else {
                m.consts.noclip_speed
            }
        } else {
            if lctrl {
                m.values.is_crouched = true;

                m.consts.crouch_speed
            } else {
                m.values.is_crouched = false;

                if lshift {
                    m.consts.sprinting_speed
                } else {
                    m.consts.walking_speed
                }
            }
        };

        let non_norm_wasd = Self::build_wasd_vector(&m.inputs, &m.vectors, false);
        let wasd_vec = if m.values.noclip {
            base_utils::safe_normalize(&non_norm_wasd)
        } else {
            base_utils::safe_normalize(&Vector3::new(non_norm_wasd.x, 0.0, non_norm_wasd.z))
        };

        if m.values.noclip {
            m.values.velocity = wasd_vec * speed;
        } else {
            let previous_mag = m.values.velocity.xz().magnitude();

            let accel = if m.values.is_on_ground {
                speed * m.consts.ground_accel_ratio_of_speed
            } else {
                if m.values.is_grappling {
                    speed * m.consts.grapple_hanging_in_air_accel
                } else {
                    speed * m.consts.hanging_in_air_accel
                }
            };

            m.values.velocity += wasd_vec * accel * m.dt;

            let new_mag = m.values.velocity.xz().magnitude();

            let speed_limit = previous_mag.max(speed);
            if new_mag > speed_limit {
                m.values.velocity.x *= speed_limit / new_mag;
                m.values.velocity.z *= speed_limit / new_mag;
            }
        }

        if m.values.is_on_ground {
            if m.inputs.pressed.space {
                m.values.velocity.y = m.consts.jump_vel_delta;
            }
        }
    }

    pub fn update_vel_with_grav(m: &mut MotionCtxt) {
        if !m.values.is_on_ground && !m.values.noclip {
            m.values.velocity -= Vector3::unit_y() * m.consts.gravity_accel * m.dt;
        }
    }
}
