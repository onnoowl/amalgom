use std::time::Instant;

use base_utils::F32Coord;
use cgmath::{EuclideanSpace, InnerSpace, Vector2, Vector3, Zero};
use game_utils::thread_comm::GrappleEvent;

use super::motion::{Motion, MotionCtxt};

#[derive(Debug, Default)]
pub struct GrapplingHook {
    state: Option<HookState>,
}

#[derive(Debug)]
struct HookState {
    hook_loc: F32Coord,

    _init_time: Instant,

    // When you pump a swing, you tilt yourself forward and back
    // This mimicks that by giving you a "stamina"
    swing_pumper: Vector2<f32>,
}

impl GrapplingHook {
    pub fn grapple_event(&mut self, e: GrappleEvent) {
        self.state = Some(e.into());
    }

    pub fn check_status(&mut self, m: &mut MotionCtxt) -> () {
        if let Some(_) = &self.state {
            if m.inputs.just_pressed.right_mouse || m.values.noclip {
                self.state = None;
                m.values.is_grappling = false;
            } else {
                m.values.is_grappling = true;
            }
        }
    }

    pub fn update_tick(&mut self, m: &mut MotionCtxt) -> () {
        if let Some(s) = &mut self.state {
            let grapple_vec = s.hook_loc - m.next_loc.to_vec();
            let grapple_dir = grapple_vec.normalize();

            let vel_mag_along_dir = m.values.velocity.dot(grapple_dir);

            let wasd_vec =
                base_utils::safe_normalize(&Motion::build_wasd_vector(&m.inputs, &m.vectors, true));

            // TODO, have a max xz vel, and a max y vel, and interpolate between the speed limits based on the angle.
            let ideal_vel = if vel_mag_along_dir > 0.0 {
                wasd_vec * m.consts.grapple_max_vel
            } else {
                wasd_vec * m.values.velocity.magnitude()
            };

            // Accel us to perfectly line up with the ideal vel, if possible
            let delta = ideal_vel - m.values.velocity;
            let delta_mag_along_dir = delta.dot(grapple_dir);

            if delta_mag_along_dir > 0.0 {
                let mut accel = delta_mag_along_dir * grapple_dir; // todo, replace with project_on fun
                let accel_mag = delta_mag_along_dir;

                let max_accel = if vel_mag_along_dir > 0.0 {
                    m.consts.grapple_max_accel_whie_shortening * m.dt
                } else {
                    m.consts.grapple_max_accel_while_lengthening * m.dt
                };

                if accel_mag > max_accel {
                    accel *= max_accel / accel_mag;
                }

                m.values.velocity += accel;
            }

            if wasd_vec != Vector3::zero() {
                // == Anti Grav ==
                let delta_y = ideal_vel.y - m.values.velocity.y;
                if delta_y > 0.0 {
                    let max_accel =
                        m.consts.grapple_anti_grav_ratio * m.consts.gravity_accel * m.dt;

                    let accel = Vector3::new(0.0, delta_y.min(max_accel), 0.0);

                    m.values.velocity += accel;
                }

                // == Slight WASD Mid-Air Acceleration ==
                //    Best used for swinging

                // let angle = grapple_dir.dot(Vector3::unit_y()).acos();
                // if angle < std::f32::consts::FRAC_PI_2 {
                //     // Lies in [0, 1], and will be close to 0 out the bounds, and close to 1 in the "swining sweet spot" at 45 degrees off vertical
                //     let how_close_to_swing_sweetspot = (2.0 * angle).sin();

                //     let allowed_swing_vector = {
                //         let v = grapple_dir.xz().normalize() * how_close_to_swing_sweetspot;
                //         Vector3::new(v[0], 0.0, v[1])
                //     };

                //     let mut delta = ideal_vel - m.values.velocity;
                //     delta.y = 0.0;
                // }

                let wasd_vec_2d = base_utils::safe_normalize(&wasd_vec.xz());
                let swing_pump_delta = wasd_vec_2d - s.swing_pumper;
                let swing_pump_delta_mag = swing_pump_delta.magnitude();

                let drain_mag = m.consts.grapple_pump_stamina_drain * m.dt;

                if swing_pump_delta_mag < drain_mag {
                    s.swing_pumper = wasd_vec_2d;
                } else {
                    s.swing_pumper += (swing_pump_delta / swing_pump_delta_mag) * drain_mag;

                    let swing_pumper_mag = s.swing_pumper.magnitude();

                    let swing_power = 1.0 - swing_pumper_mag;

                    let accel = wasd_vec_2d * m.consts.grapple_pump_accel * swing_power * m.dt;
                    let accel = Vector3::new(accel[0], 0.0, accel[1]);
                    m.values.velocity += accel;
                }
            } else {
                if base_utils::safe_normalize(&m.values.velocity.xz()).dot(s.swing_pumper) < 0.0 {
                    // Restore swing pumper power

                    let swing_pumper_mag = s.swing_pumper.magnitude();

                    if swing_pumper_mag < m.consts.grapple_pump_stamina_drain * m.dt {
                        s.swing_pumper = Vector2::zero();
                    } else {
                        s.swing_pumper -=
                            m.consts.grapple_pump_stamina_drain * m.dt * s.swing_pumper
                                / swing_pumper_mag;
                    }
                }
            }
            dbg!(s.swing_pumper);
        }
    }
}

impl From<GrappleEvent> for HookState {
    fn from(e: GrappleEvent) -> Self {
        HookState {
            hook_loc: e.target,
            _init_time: e.event_time,
            swing_pumper: Vector2::zero(),
        }
    }
}
