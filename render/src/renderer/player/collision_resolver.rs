use base_utils::{rwcube::RwCubeReadHandle, ICoord};
use cgmath::{EuclideanSpace, InnerSpace};

use super::motion::MotionCtxt;

pub struct CollisionResolver;

pub struct SingleResolveResult {
    /// Had any collisions
    had_any_collisions: bool,

    /// Had any out of bounds
    had_any_oob: bool,

    oob_coord: Option<ICoord>,
}

impl CollisionResolver {
    pub fn resolve_collisions<const N: usize>(m: &mut MotionCtxt, cage: RwCubeReadHandle<bool, N>) {
        let final_next_loc = m.next_loc;
        let total_delta = final_next_loc - m.loc;
        let total_dist = total_delta.dot(total_delta).sqrt();
        let total_delta_dir = total_delta / total_dist;
        let step_dist = 0.5;
        let mut t = 0.0;

        let signs = [0, 1, 2].map(|dim| if total_delta[dim] > 0.0 { 1.0 } else { -1.0 });

        let mut last_good_pos = *m.loc;

        while t < total_dist {
            t += step_dist;
            if t >= total_dist {
                t = total_dist;
                m.next_loc = final_next_loc;
            } else {
                m.next_loc = base_utils::v_to_p(&(total_delta_dir * t + m.loc.to_vec()));
            }

            let result = Self::single_resolve_collisions(m, &cage, signs);

            if !result.had_any_oob {
                last_good_pos = m.next_loc;
            } else {
                log::info!("OOB at {:?}, player: {:?}", result.oob_coord, m.next_loc);
                m.next_loc = last_good_pos;
                m.values.velocity = *m.last_vel;
                break;
            }

            if result.had_any_collisions {
                break;
            }
        }

        // Failsafe "make sure you don't fall through the world" code
        if !m.values.is_on_ground {
            if cage.read(&m.next_loc.cast::<i64>().unwrap().to_vec()) == Some(true) {
                m.values.is_on_ground = true;
            }
        }
    }

    pub fn single_resolve_collisions<const N: usize>(
        m: &mut MotionCtxt,
        cage: &RwCubeReadHandle<bool, N>,
        signs: [f32; 3],
    ) -> SingleResolveResult {
        let mut result = SingleResolveResult {
            had_any_collisions: false,
            had_any_oob: false,
            oob_coord: None,
        };

        if !m.values.noclip {
            let mut entries = [0, 1, 2].map(|dim| Entry {
                dim,
                sign: signs[dim],
                resolved: false,
                result: None,
            });

            for _iteration in 0..3 {
                for e in &mut entries {
                    if !e.resolved {
                        e.result = Some(Self::get_collision_correction(m, &cage, e.dim, e.sign))
                    }
                }

                let closest_entry = entries
                    .iter_mut()
                    .filter(|e| !e.resolved)
                    .min_by(|a, b| a.partial_cmp(b).unwrap());
                if let Some(e) = closest_entry {
                    e.resolved = true;
                    match e.result.as_ref().unwrap() {
                        Collision::Some(dif) => {
                            result.had_any_collisions = true;

                            m.next_loc[e.dim] -= dif;
                            m.values.velocity[e.dim] = 0.0;
                        }
                        Collision::OutOfBounds(oob_coord) => {
                            result.had_any_oob = true;
                            result.oob_coord = Some(*oob_coord);
                        }
                        Collision::None => (),
                    }
                } else {
                    break;
                }
            }

            if !m.values.noclip {
                match entries[1].result.as_ref().unwrap() {
                    Collision::Some(dif) => {
                        m.values.is_on_ground = *dif <= 0.0;
                    }
                    Collision::None => m.values.is_on_ground = false,
                    Collision::OutOfBounds(_) => m.values.is_on_ground = false,
                }
            }
        }

        result
    }

    pub fn get_collision_correction<const N: usize>(
        m: &mut MotionCtxt,
        cage: &RwCubeReadHandle<bool, N>,
        dim: usize,
        sign: f32,
    ) -> Collision {
        let player_dims = [
            m.consts.player_width,
            m.consts.player_height,
            m.consts.player_width,
        ];

        let mut center = m.next_loc;
        center.y += m.consts.player_height / 2.0;

        let offset = sign * player_dims[dim] / 2.0;

        let mut player_point = center;
        player_point[dim] += offset + sign * E;

        let dim_a = (dim + 1) % 3;
        let dim_b = (dim + 2) % 3;

        let offset_a = player_dims[dim_a] / 2.0;
        let offset_b = player_dims[dim_b] / 2.0;

        let test_coords = [(-1, -1), (-1, 1), (1, -1), (1, 1)].map(|(sign_a, sign_b)| {
            let mut p = player_point;
            p[dim_a] += sign_a as f32 * (offset_a - E);
            p[dim_b] += sign_b as f32 * (offset_b - E);
            p.map(f32::floor).cast::<i64>().unwrap().to_vec()
        });

        for i in 0..4 {
            let test_coord = test_coords[i];

            // Don't test the same teset coord more than once
            if test_coords[0..i].iter().all(|c| c != &test_coord) {
                if let Some(block_exists) = cage.read(&test_coord) {
                    if block_exists {
                        let mut adjacent_to_test_coord = test_coord;
                        adjacent_to_test_coord[dim] -= sign as i64;
                        if let Some(adjacent_block_exists) = cage.read(&adjacent_to_test_coord) {
                            if adjacent_block_exists {
                                continue; // We're not comparing against a real exposed face, this is an inner seam
                            }
                        } else {
                            return Collision::OutOfBounds(adjacent_to_test_coord);
                        }

                        let boundary = (test_coord[dim] + if sign == 1.0 { 0 } else { 1 }) as f32;

                        let dif = (center[dim] + offset) - boundary;

                        return Collision::Some(dif);
                    }
                } else {
                    return Collision::OutOfBounds(test_coord);
                }
            }
        }

        Collision::None
    }
}

#[derive(Debug, Clone)]
/// Partially orders like: `small_sqrd_dist_to_face < big_sqrd_dist_to_face < Collision::None < Collision::OutOfBounds < resolved_entry`
struct Entry {
    dim: usize,
    sign: f32,
    resolved: bool,
    result: Option<Collision>,
}

#[derive(Debug, Clone)]
pub enum Collision {
    Some(f32),
    None,
    OutOfBounds(ICoord),
}

const E: f32 = 0.01; // use this to test slightly inside boundaries, avoid rounding issues

impl PartialEq for Entry {
    fn eq(&self, other: &Self) -> bool {
        self.partial_cmp(other) == Some(std::cmp::Ordering::Equal)
    }
}

impl PartialOrd for Entry {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        use std::cmp::Ordering::*;

        if self.resolved && other.resolved {
            return Some(Equal);
        }
        if self.resolved {
            return Some(Greater);
        }
        if other.resolved {
            return Some(Less);
        }

        if let (Some(a), Some(b)) = (self.result.as_ref(), other.result.as_ref()) {
            match (a, b) {
                (Collision::Some(a), Collision::Some(b)) => a.abs().partial_cmp(&b.abs()),
                (Collision::None, Collision::Some { .. }) => Some(Greater),
                (Collision::Some { .. }, Collision::None) => Some(Less),
                (Collision::None, Collision::None) => Some(Equal),

                (Collision::OutOfBounds(_), Collision::OutOfBounds(_)) => Some(Equal),
                (_, Collision::OutOfBounds(_)) => Some(Greater),
                (Collision::OutOfBounds(_), _) => Some(Less),
            }
        } else {
            return None;
        }
    }
}
