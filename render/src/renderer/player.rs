mod collision_resolver;
pub mod grappling_hook;
pub mod motion;

use std::sync::mpsc::Receiver;

use cgmath::Point3;
use game_utils::{
    camera::Camera,
    input::CurrentInputs,
    thread_comm::{InteractionEvent, RenderComm},
};
use grappling_hook::GrapplingHook;

use self::motion::Motion;

pub struct Player {
    /// Really the location of their feet
    pub pos: Point3<f32>,

    pub grappling_hook: GrapplingHook,

    pub motion: Motion,

    interaction_events: Receiver<InteractionEvent>,
}

impl Player {
    pub fn new(pos: Point3<f32>, render_comm: &mut RenderComm) -> Self {
        let motion = Motion::new(render_comm.collision_cage.take().unwrap());

        Self {
            pos,
            motion,
            grappling_hook: GrapplingHook::default(),
            interaction_events: render_comm.interaction_events.take().unwrap(),
        }
    }

    /// Modifies the camera with the latest player position
    pub fn update_tick(&mut self, inputs: &CurrentInputs, camera: &mut Camera, dt: f32) {
        for e in self.interaction_events.try_iter() {
            match e {
                InteractionEvent::Grapple(e) => self.grappling_hook.grapple_event(e),
            }
        }

        self.motion.update_tick(
            &mut self.pos,
            inputs,
            &camera.cached_vectors,
            &mut self.grappling_hook,
            dt,
        );

        camera.controller.eye = self.get_eye_pos();
    }

    pub fn get_eye_pos(&self) -> Point3<f32> {
        let eye_height = if self.motion.values.is_crouched {
            self.motion.consts.player_crouch_height
        } else {
            self.motion.consts.player_eye_height
        };
        let mut eye_pos = self.pos;
        eye_pos.y += eye_height;
        return eye_pos;
    }
}
