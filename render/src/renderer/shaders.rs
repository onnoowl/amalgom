use rayon::prelude::*;
use wgpu::include_spirv_raw;

pub struct Shaders {
    pub line: VSFSPair,
    pub robot: VSFSPair,
    pub normal: VSFSPair,
    pub position: VSFSPair,
    pub ssao: VSFSPair,
    pub terrain: VSFSPair,
    pub opacity_terrain: VSFSPair,
}

impl Shaders {
    pub fn compile(device: &wgpu::Device) -> Self {
        let mut shaders = vec![
            include_spirv_raw!("shaders/line.vert.spv"),
            include_spirv_raw!("shaders/line.frag.spv"),
            include_spirv_raw!("shaders/robot.vert.spv"),
            include_spirv_raw!("shaders/robot.frag.spv"),
            include_spirv_raw!("shaders/normal.vert.spv"),
            include_spirv_raw!("shaders/normal.frag.spv"),
            include_spirv_raw!("shaders/position.vert.spv"),
            include_spirv_raw!("shaders/position.frag.spv"),
            include_spirv_raw!("shaders/ssao.vert.spv"),
            include_spirv_raw!("shaders/ssao.frag.spv"),
            include_spirv_raw!("shaders/terrain.vert.spv"),
            include_spirv_raw!("shaders/terrain.frag.spv"),
            include_spirv_raw!("shaders/opacity_terrain.vert.spv"),
            include_spirv_raw!("shaders/opacity_terrain.frag.spv"),
        ]
        .into_par_iter()
        .map(|shader_src| {
            // TODO: Re-enable validation once wgpu/naga fixed push contant validation error
            // shader_src.flags.remove(wgpu::ShaderFlags::VALIDATION);
            unsafe { Some(device.create_shader_module_spirv(&shader_src)) }
        })
        .collect::<Vec<_>>();

        if let [line_vs, line_fs, robot_vs, robot_fs, norm_vs, norm_fs, pos_vs, pos_fs, ssao_vs, ssao_fs, terrain_vs, terrain_fs, opacity_terrain_vs, opacity_terrain_fs] =
            &mut shaders[..]
        {
            Shaders {
                line: VSFSPair {
                    vs: line_vs.take().unwrap(),
                    fs: line_fs.take().unwrap(),
                },
                robot: VSFSPair {
                    vs: robot_vs.take().unwrap(),
                    fs: robot_fs.take().unwrap(),
                },
                normal: VSFSPair {
                    vs: norm_vs.take().unwrap(),
                    fs: norm_fs.take().unwrap(),
                },
                position: VSFSPair {
                    vs: pos_vs.take().unwrap(),
                    fs: pos_fs.take().unwrap(),
                },
                ssao: VSFSPair {
                    vs: ssao_vs.take().unwrap(),
                    fs: ssao_fs.take().unwrap(),
                },
                terrain: VSFSPair {
                    vs: terrain_vs.take().unwrap(),
                    fs: terrain_fs.take().unwrap(),
                },
                opacity_terrain: VSFSPair {
                    vs: opacity_terrain_vs.take().unwrap(),
                    fs: opacity_terrain_fs.take().unwrap(),
                },
            }
        } else {
            panic!("Did not match the correct number of shaders");
        }
    }
}

pub struct VSFSPair {
    pub vs: wgpu::ShaderModule,
    pub fs: wgpu::ShaderModule,
}
