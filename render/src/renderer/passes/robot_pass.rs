#![allow(dead_code)]
use crate::{
    renderer::{camera_manager::CameraManager, shaders::Shaders},
    terrain_visualizer::RenderTree,
};
use animation::{
    param_zoo::{ant, big_ant},
    KChain, Keyframes, NPodMapping, NPodParams, NPodState,
};

use bytemuck::{Pod, Zeroable};
use cgmath::Matrix4;
use game_utils::thread_comm::{NPodMsg, RenderComm};

use std::{sync::mpsc::Receiver, time::Instant};
use wgpu::{util::DeviceExt, BufferSize};
use zerocopy::AsBytes;

#[derive(Debug)]
struct Ant {
    pub frames: Keyframes<NPodState<{ Self::N }>>,
    pub kchain: KChain<{ Self::K }>,
}

impl Ant {
    const N: usize = ant::N; //6
    const K: usize = 13;

    const PARAMS: NPodParams<{ Self::N }> = ant::PARAMS;
    const MAPPING: NPodMapping<{ Self::N }, { Self::K }> = NPodMapping::linear_mapping();
    const DEFAULT: Self = Self {
        frames: Keyframes::None,
        kchain: KChain::init_with_npod(&Self::PARAMS, &Self::MAPPING),
    };
}

impl Default for Ant {
    fn default() -> Self {
        Self::DEFAULT
    }
}
#[derive(Debug)]
struct BigAnt {
    pub frames: Keyframes<NPodState<{ Self::N }>>,
    pub kchain: KChain<{ Self::K }>,
}

impl BigAnt {
    const N: usize = big_ant::N; //6
    const K: usize = 13;

    const PARAMS: NPodParams<{ Self::N }> = big_ant::PARAMS;
    const MAPPING: NPodMapping<{ Self::N }, { Self::K }> = NPodMapping::linear_mapping();
    const DEFAULT: Self = Self {
        frames: Keyframes::None,
        kchain: KChain::init_with_npod(&Self::PARAMS, &Self::MAPPING),
    };
}

impl Default for BigAnt {
    fn default() -> Self {
        Self::DEFAULT
    }
}

pub struct RobotPass {
    keyframe_channel: Receiver<NPodMsg<{ Ant::N }>>,
    keyframes: Keyframes<NPodState<{ Ant::N }>>,
    walking_bot: BigAnt,

    bind_group: wgpu::BindGroup,
    pipeline: wgpu::RenderPipeline,
    body_v_buf: wgpu::Buffer,
    leg_v_buf: wgpu::Buffer,
    index_buf: wgpu::Buffer,
    index_len: u32,
}

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
struct Vertex {
    _pos: [f32; 3],
    _color: [f32; 3],
    _norm: [f32; 3],
}

fn vertex(pos: [i8; 3], norm: [i8; 3]) -> Vertex {
    Vertex {
        _pos: [pos[0] as f32, pos[1] as f32, pos[2] as f32],
        _color: [0.9 as f32, 0.25 as f32, 0.0 as f32],
        _norm: [norm[0] as f32, norm[1] as f32, norm[2] as f32],
    }
}

fn create_vertices() -> (Vec<Vertex>, Vec<u16>) {
    let vertex_data = [
        // front (0, 0, 1)
        vertex([-1, 0, 1], [0, 0, 1]),
        vertex([1, 0, 1], [0, 0, 1]),
        vertex([1, 1, 1], [0, 0, 1]),
        vertex([-1, 1, 1], [0, 0, 1]),
        // back (0, 0, -1)
        vertex([-1, 1, -1], [0, 0, -1]),
        vertex([1, 1, -1], [0, 0, -1]),
        vertex([1, 0, -1], [0, 0, -1]),
        vertex([-1, 0, -1], [0, 0, -1]),
        // left (1, 0, 0)
        vertex([1, 0, -1], [1, 0, 0]),
        vertex([1, 1, -1], [1, 0, 0]),
        vertex([1, 1, 1], [1, 0, 0]),
        vertex([1, 0, 1], [1, 0, 0]),
        // right (-1, 0, 0)
        vertex([-1, 0, 1], [-1, 0, 0]),
        vertex([-1, 1, 1], [-1, 0, 0]),
        vertex([-1, 1, -1], [-1, 0, 0]),
        vertex([-1, 0, -1], [-1, 0, 0]),
        // top (0, 1, 0)
        vertex([1, 1, -1], [0, 1, 0]),
        vertex([-1, 1, -1], [0, 1, 0]),
        vertex([-1, 1, 1], [0, 1, 0]),
        vertex([1, 1, 1], [0, 1, 0]),
        // bottom (0, -1, 0)
        vertex([1, 0, 1], [0, -1, 0]),
        vertex([-1, 0, 1], [0, -1, 0]),
        vertex([-1, 0, -1], [0, -1, 0]),
        vertex([1, 0, -1], [0, -1, 0]),
    ];

    let index_data: &[u16] = &[
        0, 1, 2, 2, 3, 0, // top
        4, 5, 6, 6, 7, 4, // bottom
        8, 9, 10, 10, 11, 8, // right
        12, 13, 14, 14, 15, 12, // left
        16, 17, 18, 18, 19, 16, // front
        20, 21, 22, 22, 23, 20, // back
    ];

    (vertex_data.to_vec(), index_data.to_vec())
}

impl RobotPass {
    pub fn new(
        device: &wgpu::Device,
        config: &wgpu::SurfaceConfiguration,
        shaders: &Shaders,
        camera_manager: &CameraManager,
        render_comm: &mut RenderComm,
    ) -> Self {
        let (vertex_data, index_data) = create_vertices();

        // Box Vertices
        let body_verts: Vec<Vertex> = vertex_data
            .iter()
            .cloned()
            .map(|mut v| {
                v._pos[0] *= big_ant::WIDTH / 2.;
                v._pos[1] *= big_ant::LEG_LENGTH / 20.;
                v._pos[2] *= big_ant::LENGTH / 2.;
                return v;
            })
            .collect();

        let body_v_buf = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            contents: bytemuck::cast_slice(&body_verts),
            usage: wgpu::BufferUsages::VERTEX,
            label: Some("Robot Pass Box Vertices"),
        });

        let leg_verts: Vec<Vertex> = vertex_data
            .iter()
            .cloned()
            .map(|mut v| {
                v._pos[0] *= big_ant::LEG_LENGTH / 50.;
                v._pos[1] *= big_ant::LEG_LENGTH;
                v._pos[2] *= big_ant::LEG_LENGTH / 50.;
                return v;
            })
            .collect();

        let leg_v_buf = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            contents: bytemuck::cast_slice(&leg_verts),
            usage: wgpu::BufferUsages::VERTEX,
            label: Some("Robot Pass Box Vertices"),
        });

        let index_buf = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            contents: bytemuck::cast_slice(&index_data),
            usage: wgpu::BufferUsages::INDEX,
            label: Some("Robot Pass Box Vertices"),
        });

        // Bind Group Layout
        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[wgpu::BindGroupLayoutEntry {
                binding: 0, // view proj
                visibility: wgpu::ShaderStages::VERTEX,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: BufferSize::new(64),
                }, // TODO, make true?
                count: None,
            }],
            label: Some("Robot Pass Bind Group Layout"),
        });

        // Pipeline Layout
        let pushc_size_bytes = 2 * std::mem::size_of::<Matrix4<f32>>() as u32;
        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[wgpu::PushConstantRange {
                stages: wgpu::ShaderStages::VERTEX,
                range: 0..pushc_size_bytes,
            }],
            label: Some("Robot Pass Pipeline Layout"),
        });

        // Pipeline
        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shaders.robot.vs,
                entry_point: "main",
                buffers: &[wgpu::VertexBufferLayout {
                    array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
                    step_mode: wgpu::VertexStepMode::Vertex,
                    attributes: &[
                        wgpu::VertexAttribute {
                            offset: 0,
                            shader_location: 0,
                            format: wgpu::VertexFormat::Float32x3,
                        },
                        wgpu::VertexAttribute {
                            offset: std::mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                            shader_location: 1,
                            format: wgpu::VertexFormat::Float32x3,
                        },
                        wgpu::VertexAttribute {
                            offset: 2 * std::mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                            shader_location: 2,
                            format: wgpu::VertexFormat::Float32x3,
                        },
                    ],
                }],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shaders.robot.fs,
                entry_point: "main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }), // TODO: get rid of frag shader for normal shader? It can be "flat"
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: wgpu::PolygonMode::Fill,
                ..Default::default()
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: crate::renderer::AmalgomRenderer::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: Default::default(),
                bias: wgpu::DepthBiasState {
                    constant: 0,
                    slope_scale: 0.0,
                    clamp: 0.0,
                },
            }),
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            label: Some("Robot Pass Pipeline"),
            multiview: None
        });

        // Bind Group
        let bind_group = create_bind_group(
            device,
            &bind_group_layout,
            camera_manager.view_proj_uniform_buf.as_ref(),
        );

        let index_len = index_data.len() as u32;
        println!("{:?}", index_len);

        RobotPass {
            keyframe_channel: render_comm.hexapod_recv.take().unwrap(),
            keyframes: Keyframes::None,
            walking_bot: BigAnt::DEFAULT,
            bind_group,
            pipeline,
            body_v_buf,
            leg_v_buf,
            index_buf,
            index_len,
        }
    }

    pub fn update(&mut self, _device: &wgpu::Device, _queue: &wgpu::Queue, _tree: &RenderTree) {
        let current_time = Instant::now();

        // Read from thread comms to update keyframe and use to update buffers
        // Actually, might want to let render fn update buffers.
        // TODO: For now, use canned animation

        if self.keyframes.needs_next(current_time) {
            if let Ok(pod_msg) = self.keyframe_channel.try_recv() {
                match pod_msg {
                    NPodMsg::Spawn(_) => {}
                    NPodMsg::Despawn(_) => {}
                    NPodMsg::SendKeyFrame(_, kf) => {
                        self.keyframes.push(kf);
                    }
                }
            }
        }

        // if self.keyframes == Keyframes::None {
        //     self.keyframes.push(kf);
        // } else if self.keyframes.needs_next(current_time) {
        //     self.keyframes.push(Keyframe {
        //         state: self.canned_animation.next().unwrap(),
        //         time: current_time + Duration::from_millis(200),
        //     });
        // }

        match self
            .keyframes
            .ease_npod_pose(current_time, &BigAnt::PARAMS, &BigAnt::MAPPING)
        {
            Ok(pose) => {
                self.walking_bot.kchain.set_pose(&pose);
                self.walking_bot.kchain.update();
            }
            Err(e) => log::error!("{:?}", e),
        }
    }

    pub fn render(
        &self,
        view: &wgpu::TextureView,
        _device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        depth_tex: &wgpu::TextureView,
    ) {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Load,
                    store: true,
                },
            })],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: depth_tex,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Load,
                    store: true,
                }),
                stencil_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Load,
                    store: true,
                }),
            }),
        });
        rpass.set_pipeline(&self.pipeline);
        rpass.set_index_buffer(self.index_buf.slice(..), wgpu::IndexFormat::Uint32);
        rpass.set_bind_group(0, &self.bind_group, &[]);

        if self.walking_bot.kchain.joints[0]
            .cached_cumulative_rotation
            .is_some()
        {
            for (i, joint) in self.walking_bot.kchain.joints.iter().enumerate() {
                // TODO: update push constant
                let mat: Matrix4<f32> = joint.cached_cumulative_transform.unwrap();
                let mx_ref: &[f32; 16] = mat.as_ref();
                let bytes = unsafe { mx_ref.as_bytes().align_to::<u8>().1 };
                rpass.set_push_constants(
                    wgpu::ShaderStages::VERTEX,
                    0, // offset
                    // wgpu wants us to pass as a &[u32]
                    // it wants this because push constants need to be 4 byte
                    // aligned (which ours is bc it's a &[f32])
                    bytes,
                );
                let mat: Matrix4<f32> = joint.cached_cumulative_rotation.unwrap().into();
                let mx_ref: &[f32; 16] = mat.as_ref();
                let bytes = unsafe { mx_ref.as_bytes().align_to::<u8>().1 };
                rpass.set_push_constants(
                    wgpu::ShaderStages::VERTEX,
                    64, // offset
                    // wgpu wants us to pass as a &[u32]
                    // it wants this because push constants need to be 4 byte
                    // aligned (which ours is bc it's a &[f32])
                    bytes,
                );
                if i == 0 {
                    // The body
                    rpass.set_vertex_buffer(0, self.body_v_buf.slice(..));
                } else {
                    rpass.set_vertex_buffer(0, self.leg_v_buf.slice(..));
                }
                rpass.draw_indexed(0..self.index_len, 0, 0..1);
            }
        }
    }
}

fn create_bind_group(
    device: &wgpu::Device,
    bind_group_layout: &wgpu::BindGroupLayout,
    view_proj_uniform_buf: &wgpu::Buffer,
) -> wgpu::BindGroup {
    device.create_bind_group(&wgpu::BindGroupDescriptor {
        layout: bind_group_layout,
        entries: &[wgpu::BindGroupEntry {
            binding: 0,
            resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                buffer: view_proj_uniform_buf,
                offset: 0,
                size: core::num::NonZeroU64::new(64),
            }),
        }],
        label: Some("Robot Pass Bind Group"),
    })
}
