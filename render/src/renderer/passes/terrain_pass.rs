use super::ssao_pass::SSAOPass;
use crate::{
    renderer::{camera_manager::CameraManager, shaders::Shaders, AmalgomRenderer},
    terrain_visualizer::FaceBuffer,
    terrain_visualizer::{terrain_buffer::FaceBufferRender, TerrainBuffer},
};
use arrayvec::ArrayVec;
use base_utils::ICoord;
use cgmath::Point3;
use game_utils::{input::CurrentInputs, terrain_mesh::TerrainMesh};

use image::GenericImageView;
use std::sync::Arc;
use wgpu::{util::DeviceExt, Buffer, BufferSize, Device};
use zerocopy::AsBytes;

pub struct TerrainPass {
    blocks_texture_view: wgpu::TextureView,
    blocks_texture_sampler: wgpu::Sampler,
    bind_group: wgpu::BindGroup,
    bind_group_layout: wgpu::BindGroupLayout,
    pipeline: wgpu::RenderPipeline,

    /// This bool toggles SSAO atm, but it lives here since the buffer is used in the terrain pass.
    pub debug_lighting_toggle: bool,
    debug_lighting_toggle_buf: Buffer,
    debug_lighting_toggle_buf_needs_update: bool,

    opacity_pass: bool,
}

impl TerrainPass {
    pub fn new(
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        config: &wgpu::SurfaceConfiguration,
        shaders: &Shaders,
        camera_manager: &CameraManager,
        ssao_pass: &SSAOPass,
        opacity_pass: bool,
    ) -> Self {
        // Create the texture for blocks
        let diffuse_bytes = include_bytes!("../images/atlas.png");
        let diffuse_image = image::load_from_memory(diffuse_bytes).unwrap();
        let diffuse_rgba = diffuse_image.as_rgba8().unwrap();
        let dimensions = diffuse_image.dimensions();
        let texture_extent = wgpu::Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth_or_array_layers: 1,
        };
        let blocks_texture = device.create_texture(&wgpu::TextureDescriptor {
            size: texture_extent,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            label: Some("Blocks Texture for Terrain Pass"),
        });
        let blocks_texture_view = blocks_texture.create_view(&Default::default());
        queue.write_texture(
            blocks_texture.as_image_copy(),
            &diffuse_rgba,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(std::num::NonZeroU32::new(4 * dimensions.0).unwrap()),
                rows_per_image: None,
            },
            texture_extent,
        );

        // Sampler
        let blocks_texture_sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: -100.0,
            lod_max_clamp: 100.0,
            compare: None,
            anisotropy_clamp: None, //Some(NonZeroU8::new(1).unwrap()), // TODO: Figure out what this is, and what it should be
            border_color: None,
            label: Some("Block Texture Sampler"),
        });

        // Debug Lighting Toggle Uniform Buffer
        let debug_lighting_toggle_buf = {
            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                contents: [0u8; wgpu::COPY_BUFFER_ALIGNMENT as usize].as_bytes(),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                label: Some("Debug Lighting Toggle"),
            })
        };

        // Bind Group Layout
        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0, // view proj
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(64),
                    }, // TODO, make true?
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1, // mesh textures
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 2, //mesh texture sampler
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 3, // ssao texture
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 4, //ssao sampler
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 5, // lighting debug info
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(wgpu::COPY_BUFFER_ALIGNMENT),
                    },
                    count: None,
                },
            ],
            label: Some("Terrain Pass Bind Group Layout"),
        });

        // Bind Group
        let bind_group = Self::create_bind_group(
            device,
            &bind_group_layout,
            &camera_manager.view_proj_uniform_buf,
            &blocks_texture_view,
            &blocks_texture_sampler,
            &ssao_pass.out_tex,
            &ssao_pass.sampler,
            &debug_lighting_toggle_buf,
        );

        let opacity_push_constants = [
            wgpu::PushConstantRange {
                stages: wgpu::ShaderStages::VERTEX,
                range: 0..4, //f32
            },
            wgpu::PushConstantRange {
                stages: wgpu::ShaderStages::FRAGMENT,
                range: 4..8, //f32
            },
        ];
        let push_constants = if opacity_pass {
            &opacity_push_constants[..]
        } else {
            &[]
        };

        // Pipeline Layout
        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: push_constants,
            label: Some("Terrain Pass Pipeline Layout"),
        });

        // Pipeline
        let pipeline = if opacity_pass {
            AmalgomRenderer::create_simple_pipeline_for_verts(
                config,
                device,
                &pipeline_layout,
                &shaders.opacity_terrain.vs,
                &shaders.opacity_terrain.fs,
                "Opacity Terrain Pass Pipeline",
                true,
            )
        } else {
            AmalgomRenderer::create_simple_pipeline_for_verts(
                config,
                device,
                &pipeline_layout,
                &shaders.terrain.vs,
                &shaders.terrain.fs,
                "Terrain Pass Pipeline",
                false,
            )
        };

        TerrainPass {
            blocks_texture_view,
            blocks_texture_sampler,
            bind_group,
            bind_group_layout,
            pipeline,
            debug_lighting_toggle: false,
            debug_lighting_toggle_buf,
            debug_lighting_toggle_buf_needs_update: false,
            opacity_pass,
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn create_bind_group(
        device: &wgpu::Device,
        bind_group_layout: &wgpu::BindGroupLayout,
        view_proj_uniform_buf: &Buffer,
        blocks_texture_view: &wgpu::TextureView,
        blocks_texture_sampler: &wgpu::Sampler,
        ssao_ambient_out_tex: &wgpu::TextureView,
        ssao_sampler: &wgpu::Sampler,
        debug_lighting_toggle_buf: &Buffer,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: view_proj_uniform_buf,
                        offset: 0,
                        size: core::num::NonZeroU64::new(64), //TODO: make size None?
                    }),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::TextureView(&blocks_texture_view),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: wgpu::BindingResource::Sampler(&blocks_texture_sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: wgpu::BindingResource::TextureView(&ssao_ambient_out_tex),
                },
                wgpu::BindGroupEntry {
                    binding: 4,
                    resource: wgpu::BindingResource::Sampler(&ssao_sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 5,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: debug_lighting_toggle_buf,
                        offset: 0,
                        size: None,
                    }),
                },
            ],
            label: Some("Terrain Pass Bind Group"),
        })
    }

    pub fn update_input(&mut self, inputs: &CurrentInputs) {
        if inputs.just_pressed.l {
            self.debug_lighting_toggle = !self.debug_lighting_toggle;
            self.debug_lighting_toggle_buf_needs_update = true
        }
    }

    pub fn update(&mut self, queue: &wgpu::Queue) {
        let mut data = [0_u8; wgpu::COPY_BUFFER_ALIGNMENT as usize];
        data[0] = self.debug_lighting_toggle as u8;

        if self.debug_lighting_toggle_buf_needs_update {
            queue.write_buffer(&self.debug_lighting_toggle_buf, 0, &data.as_bytes());
        }
    }

    pub fn resize(
        &mut self,
        _width: u32,
        _height: u32,
        device: &wgpu::Device,
        camera_manager: &CameraManager,
        ssao_pass: &SSAOPass,
    ) {
        self.bind_group = Self::create_bind_group(
            device,
            &self.bind_group_layout,
            &camera_manager.view_proj_uniform_buf,
            &self.blocks_texture_view,
            &self.blocks_texture_sampler,
            &ssao_pass.out_tex,
            &ssao_pass.sampler,
            &self.debug_lighting_toggle_buf,
        )
    }

    pub fn render<B: FaceBufferRender>(
        &self,
        view: &wgpu::TextureView,
        _device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        buffs: &[B],
        depth_tex: &wgpu::TextureView,
        camera_manager: &CameraManager,
    ) {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: if self.opacity_pass {
                        wgpu::LoadOp::Load
                    } else {
                        wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        })
                    },
                    store: true,
                },
            })],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: depth_tex,
                depth_ops: Some(wgpu::Operations {
                    load: if self.opacity_pass {
                        wgpu::LoadOp::Load
                    } else {
                        wgpu::LoadOp::Clear(1.0)
                    },
                    store: true,
                }),
                stencil_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(0),
                    store: true,
                }),
            }),
        });
        rpass.set_pipeline(&self.pipeline);

        rpass.set_bind_group(0, &self.bind_group, &[]);
        for buf in buffs.iter() {
            buf.render(&mut rpass, camera_manager);
        }
    }
}

#[derive(Debug)]
pub struct TerrainVisual {
    pub mesh: TerrainMesh,
    pub buffer: TerrainBuffer,
}

impl TerrainVisual {
    pub fn new(mesh: TerrainMesh) -> Self {
        Self {
            mesh,
            buffer: Default::default(),
        }
    }

    pub fn get_or_create_visible_face_bufs(
        &mut self,
        device: &Device,
        queue: &wgpu::Queue,
        pos: &ICoord,
        size: i64,
        player_pos: &Point3<f32>,
    ) -> ArrayVec<[Arc<FaceBuffer>; 6]> {
        let mut visible_face_buffers = ArrayVec::new();
        let faces = base_utils::calculate_visible_faces(&pos, size, player_pos);
        for face in faces.iter() {
            let f = *face as usize;
            if self.buffer.buffs[f].is_none() {
                // log::debug!("Creating a buffer");
                // Create it
                let vbuf = device.create_buffer(&wgpu::BufferDescriptor {
                    size: self.mesh.vertices[f].as_bytes().len() as u64,
                    usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
                    label: Some("Terrain Vertex Buffer"),
                    mapped_at_creation: false, //TODO? Which is better?
                });
                let ibuf = device.create_buffer(&wgpu::BufferDescriptor {
                    size: self.mesh.indices[f].as_bytes().len() as u64,
                    usage: wgpu::BufferUsages::INDEX | wgpu::BufferUsages::COPY_DST,
                    label: Some("Terrain Index Buffer"),
                    mapped_at_creation: false, //TODO? Which is better?
                });

                queue.write_buffer(&vbuf, 0, self.mesh.vertices[f].as_bytes());
                queue.write_buffer(&ibuf, 0, self.mesh.indices[f].as_bytes());

                self.buffer.buffs[f] = Some(Arc::new(FaceBuffer {
                    vertices: vbuf,
                    indices: ibuf,
                    indices_len: self.mesh.indices[f].len() as u32,
                }));
            }
            visible_face_buffers.push(self.buffer.buffs[f].as_ref().unwrap().clone());
        }
        visible_face_buffers
    }
}

#[derive(Debug)]
pub enum MaybeVisual {
    Some(TerrainVisual),
    Air,
    None,
}

impl Default for MaybeVisual {
    fn default() -> Self {
        MaybeVisual::None
    }
}

impl MaybeVisual {
    pub fn is_none(&self) -> bool {
        match self {
            MaybeVisual::Some(_) => false,
            MaybeVisual::Air => false,
            MaybeVisual::None => true,
        }
    }
    pub fn is_some(&self) -> bool {
        match self {
            MaybeVisual::Some(_) => true,
            MaybeVisual::Air => false,
            MaybeVisual::None => false,
        }
    }
}
