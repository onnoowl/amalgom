use crate::{
    renderer::{camera_manager::CameraManager, shaders::Shaders},
    terrain_visualizer::RenderTree,
};
use base_utils::{Loc, QUAD_OFFSETS};
use game_utils::{
    block_highlight::BlockHighlight, block_highlight::BlockHighlightMsg, input::CurrentInputs,
    thread_comm::RenderComm,
};

use arrayvec::ArrayVec;
use cgmath::Matrix4;
use crossbeam::atomic::AtomicCell;
use std::{
    collections::HashMap,
    sync::{mpsc::Receiver, Arc},
};
use wgpu::{util::DeviceExt, BufferSize};
use zerocopy::AsBytes;

pub struct BlockHighlightPass {
    cursor_highlight_cell: Arc<AtomicCell<Option<BlockHighlight>>>,
    other_highlights_channel: Receiver<BlockHighlightMsg>,

    should_draw_cursor_highlight: bool,

    bind_group_layout: wgpu::BindGroupLayout,
    pipeline: wgpu::RenderPipeline,
    vertex_buf: wgpu::Buffer,

    cursor_highlight: BlockHighlightBufAndBind,
    other_highlights: HashMap<Loc, BlockHighlightBufAndBind>,

    view_proj_uniform_buf: Arc<wgpu::Buffer>,

    hover_print: bool,
}

pub struct BlockHighlightBufAndBind {
    uniform: wgpu::Buffer,
    bind_group: wgpu::BindGroup,
}

impl BlockHighlightPass {
    pub fn new(
        device: &wgpu::Device,
        config: &wgpu::SurfaceConfiguration,
        shaders: &Shaders,
        camera_manager: &CameraManager,
        render_comm: &mut RenderComm,
    ) -> Self {
        // Box Vertices
        let vertex_buf = {
            let mut lines = ArrayVec::<[f32; 12 * 2 * 4]>::new(); //4 bytes not 3 (for 16 byte padding)

            // for each x, y, and z axis
            for line_axis in 0usize..=2 {
                // iterate over a quad (where the line_axis dim is fixed)
                for (np_dim_a, np_dim_b) in QUAD_OFFSETS.iter() {
                    let np_dim_a = *np_dim_a as f32;
                    let np_dim_b = *np_dim_b as f32;
                    let coord1 = match line_axis {
                        0 => [0f32, np_dim_a, np_dim_b, 0f32],
                        1 => [np_dim_a, 0f32, np_dim_b, 0f32],
                        2 => [np_dim_a, np_dim_b, 0f32, 0f32],
                        _ => unreachable!(),
                    };

                    // coord1 has line_axis dim set to zero, but coord2 has it at one, making a line.
                    let mut coord2 = coord1;
                    coord2[line_axis] = 1.0;

                    lines.try_extend_from_slice(&coord1).unwrap();
                    lines.try_extend_from_slice(&coord2).unwrap();
                }
            }
            debug_assert!(lines.len() == lines.capacity());

            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                contents: lines.as_bytes(),
                usage: wgpu::BufferUsages::VERTEX,
                label: Some("Debug Pass Box Vertices"),
            })
        };

        // Bind Group Layout
        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0, // view proj
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(64),
                    }, // TODO, make true?
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1, // block_highlight_uniform
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(64),
                    }, // TODO, make true?
                    count: None,
                },
            ],
            label: Some("Debug Pass Bind Group Layout"),
        });

        // Pipeline Layout
        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
            label: Some("Debug Pass Pipeline Layout"),
        });

        // Pipeline
        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shaders.line.vs,
                entry_point: "main",
                buffers: &[wgpu::VertexBufferLayout {
                    array_stride: std::mem::size_of::<game_utils::terrain_mesh::Vertex>()
                        as wgpu::BufferAddress,
                    step_mode: wgpu::VertexStepMode::Vertex,
                    attributes: &[wgpu::VertexAttribute {
                        format: wgpu::VertexFormat::Float32x3,
                        offset: 0,
                        shader_location: 0,
                        //pos
                    }],
                }],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shaders.line.fs,
                entry_point: "main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }), // TODO: get rid of frag shader for normal shader? It can be "flat"
            primitive: wgpu::PrimitiveState {
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                strip_index_format: None,
                topology: wgpu::PrimitiveTopology::LineList,
                polygon_mode: wgpu::PolygonMode::Line,
                ..Default::default()
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            label: Some("Debug Pass Pipeline"),
            multiview: None
        });

        let cursor_highlight = {
            // Uniform
            let uniform_buf = {
                let zeroes: [f32; 16] = Default::default();
                device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    contents: zeroes.as_bytes(),
                    usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                    label: Some("Debug Pass Box Transform Uniform"),
                })
            };

            // Bind Group
            let bind_group = create_bind_group(
                device,
                &bind_group_layout,
                camera_manager.view_proj_uniform_buf.as_ref(),
                &uniform_buf,
            );

            BlockHighlightBufAndBind {
                uniform: uniform_buf,
                bind_group,
            }
        };

        BlockHighlightPass {
            cursor_highlight_cell: render_comm.cursor_highlight_cell.clone(),
            other_highlights_channel: render_comm.other_block_highlights.take().unwrap(),
            should_draw_cursor_highlight: false,
            bind_group_layout,
            pipeline,
            vertex_buf,
            cursor_highlight,
            other_highlights: Default::default(),
            view_proj_uniform_buf: camera_manager.view_proj_uniform_buf.clone(),
            hover_print: false,
        }
    }

    pub fn update(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        tree: &RenderTree,
        inputs: &CurrentInputs,
    ) {
        if inputs.just_pressed.h {
            self.hover_print = !self.hover_print;
        }

        if let Some(block_highlight) = self.cursor_highlight_cell.take() {
            if let Some(loc) = block_highlight.loc {
                let Loc(coord, level) = loc;

                if self.hover_print {
                    let node = tree.get_node(&coord, level as u32);
                    if match &node {
                        Ok(_) => true,
                        Err(e) => match e {
                            octree::GetError::OutOfBounds(_) => false,
                            octree::GetError::NotPresent(np) => match np {
                                octree::NotPresent::IsUnloaded {
                                    depth: _,
                                    unloaded_node: _,
                                } => false,
                                octree::NotPresent::IsHighLevelLeaf {
                                    depth: _,
                                    high_level_leaf_node: _,
                                } => true,
                            },
                        },
                    } {
                        log::debug!("Render Thread Hover Info: {:?}", node);
                    }
                }

                self.should_draw_cursor_highlight = true;

                let mat: Matrix4<f32> = loc.into();
                let mx_ref: &[f32; 16] = mat.as_ref();

                queue.write_buffer(&self.cursor_highlight.uniform, 0, mx_ref.as_bytes());
            } else {
                self.should_draw_cursor_highlight = false;
            }
        }

        for other_highlight_msg in self.other_highlights_channel.try_iter() {
            match other_highlight_msg {
                BlockHighlightMsg::Add(loc) => {
                    let uniform_buf = {
                        let mat: Matrix4<f32> = loc.into();
                        let mx_ref: &[f32; 16] = mat.as_ref();
                        device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                            contents: mx_ref.as_bytes(),
                            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                            label: Some("Debug Pass Box Transform Uniform"),
                        })
                    };

                    let bind_group = self.create_bind_group(device, &uniform_buf);

                    let buf_and_bind = BlockHighlightBufAndBind {
                        uniform: uniform_buf,
                        bind_group,
                    };

                    self.other_highlights.insert(loc, buf_and_bind);
                }
                BlockHighlightMsg::Remove(loc) => {
                    self.other_highlights.remove(&loc);
                }
            }
        }
    }

    pub fn render(
        &self,
        view: &wgpu::TextureView,
        _device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        _depth_tex: &wgpu::TextureView,
    ) {
        if self.should_draw_cursor_highlight || !self.other_highlights.is_empty() {
            let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });
            rpass.set_pipeline(&self.pipeline);
            rpass.set_vertex_buffer(0, self.vertex_buf.slice(..));

            if self.should_draw_cursor_highlight {
                rpass.set_bind_group(0, &self.cursor_highlight.bind_group, &[]);
                rpass.draw(0..(12 * 2), 0..1);
            }

            for (
                _loc,
                BlockHighlightBufAndBind {
                    uniform: _,
                    bind_group,
                },
            ) in self.other_highlights.iter()
            {
                rpass.set_bind_group(0, bind_group, &[]);
                rpass.draw(0..(12 * 2), 0..1);
            }
        }
    }

    fn create_bind_group(&self, device: &wgpu::Device, uniform: &wgpu::Buffer) -> wgpu::BindGroup {
        create_bind_group(
            device,
            &self.bind_group_layout,
            self.view_proj_uniform_buf.as_ref(),
            uniform,
        )
    }
}

fn create_bind_group(
    device: &wgpu::Device,
    bind_group_layout: &wgpu::BindGroupLayout,
    view_proj_uniform_buf: &wgpu::Buffer,
    uniform: &wgpu::Buffer,
) -> wgpu::BindGroup {
    device.create_bind_group(&wgpu::BindGroupDescriptor {
        layout: bind_group_layout,
        entries: &[
            wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                    buffer: view_proj_uniform_buf,
                    offset: 0,
                    size: core::num::NonZeroU64::new(64),
                }),
            },
            wgpu::BindGroupEntry {
                binding: 1,
                resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                    buffer: uniform,
                    offset: 0,
                    size: core::num::NonZeroU64::new(64),
                }),
            },
        ],
        label: Some("Debug Pass Bind Group"),
    })
}
