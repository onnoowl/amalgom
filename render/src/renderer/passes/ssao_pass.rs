mod normal_pass;
mod position_pass;

use normal_pass::NormalPass;
use position_pass::PositionPass;

use crate::{
    renderer::{camera_manager::CameraManager, shaders::Shaders, AmalgomRenderer},
    terrain_visualizer::FaceBuffer,
};

use cgmath::{InnerSpace, Vector3};
use std::{num::NonZeroU8, sync::Arc};
use wgpu::{util::DeviceExt, BufferSize};
use zerocopy::AsBytes;

const NUM_NOISE: u64 = 4;
const NUM_SAMPLES: u64 = 8;

pub struct SSAOPass {
    position_pass: PositionPass,
    normal_pass: NormalPass,

    pub sampler: wgpu::Sampler, //for position, and normals
    depth_tex: wgpu::TextureView,

    pub out_tex: wgpu::TextureView,

    bind_group: wgpu::BindGroup,
    bind_group_layout: wgpu::BindGroupLayout,

    pipeline: wgpu::RenderPipeline,

    samples_buf: wgpu::Buffer,
    noise_buf: wgpu::Buffer,
}

impl SSAOPass {
    const RESOLUTION_DIVISOR: u32 = 4;

    pub fn new(
        device: &wgpu::Device,
        config: &wgpu::SurfaceConfiguration,
        shaders: &Shaders,
        camera_manager: &CameraManager,
    ) -> Self {
        // Sub Passes
        let position_pass = PositionPass::new(device, config, shaders, camera_manager);
        let normal_pass = NormalPass::new(device, config, shaders, camera_manager);

        // Samples Buffer
        let samples_buf = {
            let mut v_samples = Vec::<f32>::with_capacity(4 * NUM_SAMPLES as usize);
            if NUM_SAMPLES == 1 {
                let v = Vector3::<f32>::new(1.0, 0.0, 1.0).normalize();
                v_samples.push(v.x);
                v_samples.push(v.y);
                v_samples.push(v.z);
                v_samples.push(0.0);
            } else if NUM_SAMPLES == 4 {
                let mut vecs: [Vector3<f32>; 4] = [
                    Vector3::new(1.0, 0.0, 1.0),
                    Vector3::new(0.0, 1.0, 1.0),
                    Vector3::new(-1.0, 0.0, 1.0),
                    Vector3::new(0.0, -1.0, 1.0),
                ];
                for v in vecs.iter_mut() {
                    v.normalize();
                    v_samples.push(v.x);
                    v_samples.push(v.y);
                    v_samples.push(v.z);
                    v_samples.push(0.0);
                }
            } else {
                for i in 0..NUM_SAMPLES {
                    let mut sample = Vector3::<f32>::new(
                        rand::random::<f32>() * 2.0 - 1.0,
                        rand::random::<f32>() * 2.0 - 1.0,
                        rand::random::<f32>() * 0.9 + 0.1,
                    )
                    .normalize();

                    let rand = rand::random::<f32>();
                    sample *= rand;

                    let mut scale = (i as f32) / (NUM_SAMPLES as f32);
                    scale = (scale * scale) * 0.9 + 0.1; //lerp between 0.1 and 1.0 with t = scale^2
                    sample *= scale;

                    // debug!("Sample: {:?}", sample);

                    v_samples.push(sample.x);
                    v_samples.push(sample.y);
                    v_samples.push(sample.z);
                    v_samples.push(0.0);
                }
            }
            let v_samples_bytes = v_samples[..].as_bytes();
            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                contents: v_samples_bytes,
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST, // TODO: Remove COPY_DST?
                label: Some("SSAO Samples Uniform"),
            })
        };

        // Noise Buffer
        let noise_buf = {
            let mut v_noise = Vec::<f32>::with_capacity(4 * NUM_NOISE as usize);
            if NUM_NOISE == 1 {
                v_noise.push(1.0);
                v_noise.push(1.0);
                v_noise.push(1.0);
                v_noise.push(0.0);
            } else {
                for _ in 0..NUM_NOISE {
                    let noise = Vector3::<f32>::new(
                        rand::random::<f32>() * 2.0 - 1.0,
                        rand::random::<f32>() * 2.0 - 1.0,
                        0.0,
                    );

                    // debug!("Noise: {:?}", noise);

                    v_noise.push(noise.x);
                    v_noise.push(noise.y);
                    v_noise.push(noise.z);
                    v_noise.push(0.0);
                }
            }
            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                contents: v_noise[..].as_bytes(),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST, // TODO: Remove COPY_DST?
                label: Some("SSAO Noise Uniform"),
            })
        };

        // Sampler
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: -100.0,
            lod_max_clamp: 100.0,
            compare: None,
            anisotropy_clamp: Some(NonZeroU8::new(1).unwrap()), // TODO: Figure out what this is, and what it should be
            border_color: None,
            label: Some("SSAO Texture Sampler"),
        });

        // Bind Group Layout
        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0, //view projection
                    visibility: wgpu::ShaderStages::VERTEX | wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(64),
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1, //projection
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(64),
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 2, //t_Position
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 3, //s_Position
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 4, //t_Normal
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 5, //s_Normal
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 6, //samples
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(4 * NUM_SAMPLES),
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 7, //noise
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(4 * NUM_NOISE),
                    },
                    count: None,
                },
            ],
            label: Some("SSAO Bind Group Layout"),
        });

        // Bind Group
        let bind_group = Self::create_bind_group(
            &bind_group_layout,
            device,
            camera_manager,
            &position_pass,
            &normal_pass,
            &sampler,
            &samples_buf,
            &noise_buf,
        );

        // Pipeline Layout
        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
            label: Some("SSAO Pipeline Layout"),
        });

        // Pipeline
        let pipeline = AmalgomRenderer::create_simple_pipeline_for_verts(
            config,
            device,
            &pipeline_layout,
            &shaders.ssao.vs,
            &shaders.ssao.fs,
            "SSAO Pipeline",
            false,
        );

        // Depth
        let depth_tex = AmalgomRenderer::create_depth_buffer(
            device,
            config.width / Self::RESOLUTION_DIVISOR,
            config.height / Self::RESOLUTION_DIVISOR,
        );

        let out_tex = AmalgomRenderer::create_data_texture(
            device,
            config.width / Self::RESOLUTION_DIVISOR,
            config.height / Self::RESOLUTION_DIVISOR,
            Some("SSAO Output"),
        );

        SSAOPass {
            position_pass,
            normal_pass,
            sampler,
            depth_tex,
            out_tex,
            bind_group,
            bind_group_layout,
            pipeline,
            samples_buf,
            noise_buf,
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn create_bind_group(
        bind_group_layout: &wgpu::BindGroupLayout,
        device: &wgpu::Device,
        camera_manager: &CameraManager,
        position_pass: &PositionPass,
        normal_pass: &NormalPass,
        sampler: &wgpu::Sampler,
        samples_buf: &wgpu::Buffer,
        noise_buf: &wgpu::Buffer,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: camera_manager.view_proj_uniform_buf.as_ref(),
                        offset: 0,
                        size: core::num::NonZeroU64::new(64),
                    }),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: camera_manager.proj_uniform_buf.as_ref(),
                        offset: 0,
                        size: core::num::NonZeroU64::new(64),
                    }),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: wgpu::BindingResource::TextureView(&position_pass.out_tex),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: wgpu::BindingResource::Sampler(sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 4,
                    resource: wgpu::BindingResource::TextureView(&normal_pass.out_tex),
                },
                wgpu::BindGroupEntry {
                    binding: 5,
                    resource: wgpu::BindingResource::Sampler(sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 6,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: samples_buf,
                        offset: 0,
                        size: None,
                    }),
                },
                wgpu::BindGroupEntry {
                    binding: 7,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: noise_buf,
                        offset: 0,
                        size: None,
                    }),
                },
            ],
            label: Some("SSAO Bind Group"),
        })
    }

    pub fn resize(
        &mut self,
        width: u32,
        height: u32,
        device: &wgpu::Device,
        camera_manager: &CameraManager,
    ) {
        self.normal_pass.resize(width, height, device);
        self.position_pass.resize(width, height, device);

        self.bind_group = Self::create_bind_group(
            &self.bind_group_layout,
            device,
            camera_manager,
            &self.position_pass,
            &self.normal_pass,
            &self.sampler,
            &self.samples_buf,
            &self.noise_buf,
        )
    }

    pub fn render(
        &self,
        view: &wgpu::TextureView,
        device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        buffers: &[Arc<FaceBuffer>],
    ) {
        self.normal_pass
            .render(view, device, encoder, buffers, &self.depth_tex);
        self.position_pass
            .render(view, device, encoder, buffers, &self.depth_tex);

        {
            let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &self.out_tex,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.0,
                            g: 0.0,
                            b: 0.0,
                            a: 0.0,
                        }),
                        store: true,
                    },
                })],
                depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                    view: &self.depth_tex,
                    depth_ops: Some(wgpu::Operations {
                        load: wgpu::LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: Some(wgpu::Operations {
                        load: wgpu::LoadOp::Clear(0),
                        store: true,
                    }),
                }),
            });
            rpass.set_pipeline(&self.pipeline);
            rpass.set_bind_group(0, &self.bind_group, &[]);
            for buf in buffers.iter() {
                rpass.set_index_buffer(buf.indices.slice(..), wgpu::IndexFormat::Uint32);
                rpass.set_vertex_buffer(0, buf.vertices.slice(..));
                rpass.draw_indexed(0..buf.indices_len, 0, 0..1);
            }
        }
    }
}
