use super::SSAOPass;
use crate::{
    renderer::{camera_manager::CameraManager, shaders::Shaders, AmalgomRenderer},
    terrain_visualizer::FaceBuffer,
};

use std::sync::Arc;
use wgpu::BufferSize;

pub struct NormalPass {
    pub pipeline: wgpu::RenderPipeline,
    pub out_tex: wgpu::TextureView,
    pub bind_group: wgpu::BindGroup,
}

impl NormalPass {
    pub fn new(
        device: &wgpu::Device,
        config: &wgpu::SurfaceConfiguration,
        shaders: &Shaders,
        camera_manager: &CameraManager,
    ) -> Self {
        // Bind Group Layout
        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0, //view projection
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(64),
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1, //view
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: BufferSize::new(64),
                    },
                    count: None,
                },
            ],
            label: Some("Normal Pass Bind Group Layout"),
        });

        // Bind Group
        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: camera_manager.view_proj_uniform_buf.as_ref(),
                        offset: 0,
                        size: core::num::NonZeroU64::new(64),
                    }),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                        buffer: camera_manager.view_uniform_buf.as_ref(),
                        offset: 0,
                        size: core::num::NonZeroU64::new(64),
                    }),
                },
            ],
            label: Some("Normal Pass Bind Group"),
        });

        // Pipeline Layout
        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
            label: Some("Normal Pass Pipeline Layout"),
        });

        // Pipeline
        let pipeline = AmalgomRenderer::create_simple_pipeline_for_verts(
            config,
            device,
            &pipeline_layout,
            &shaders.normal.vs,
            &shaders.normal.fs,
            "Normal Pass Pipeline",
            false,
        );

        // Output Texture
        let out_tex = Self::create_out_tex(config.width, config.height, device);

        NormalPass {
            pipeline,
            out_tex,
            bind_group,
        }
    }

    fn create_out_tex(
        screen_width: u32,
        screen_height: u32,
        device: &wgpu::Device,
    ) -> wgpu::TextureView {
        AmalgomRenderer::create_data_texture(
            device,
            screen_width / SSAOPass::RESOLUTION_DIVISOR,
            screen_height / SSAOPass::RESOLUTION_DIVISOR,
            Some("Normal Pass Output"),
        )
    }

    pub fn resize(&mut self, width: u32, height: u32, device: &wgpu::Device) {
        self.out_tex = Self::create_out_tex(width, height, device)
    }

    pub fn render(
        &self,
        _view: &wgpu::TextureView,
        _device: &wgpu::Device,
        encoder: &mut wgpu::CommandEncoder,
        buffers: &[Arc<FaceBuffer>],
        depth_tex: &wgpu::TextureView,
    ) {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &self.out_tex,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color {
                        r: 0.0,
                        g: 0.0,
                        b: 0.0,
                        a: 0.0,
                    }),
                    store: true,
                },
            })],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: depth_tex,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(1.0),
                    store: true,
                }),
                stencil_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(0),
                    store: true,
                }),
            }),
        });
        rpass.set_pipeline(&self.pipeline);
        rpass.set_bind_group(0, &self.bind_group, &[]);
        for terrain_buf in buffers.iter() {
            rpass.set_index_buffer(terrain_buf.indices.slice(..), wgpu::IndexFormat::Uint32);
            rpass.set_vertex_buffer(0, terrain_buf.vertices.slice(..));
            rpass.draw_indexed(0..terrain_buf.indices_len, 0, 0..1);
        }
    }
}
