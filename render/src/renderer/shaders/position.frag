#version 450

layout(location = 0) in vec4 v_Position;
layout(location = 0) out vec4 fragColor;

void main() {
  fragColor = v_Position;
}
