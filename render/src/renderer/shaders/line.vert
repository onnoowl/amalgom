#version 450

layout(location = 0) in vec3 a_Pos;

layout(set = 0, binding = 0) uniform ViewPerspective {
    mat4 u_ViewPerspective;
};

layout(set = 0, binding = 1) uniform ModelTransform {
    mat4 u_Model;
};

void main() {
    gl_Position = u_ViewPerspective * u_Model * vec4(a_Pos, 1.0);
}
