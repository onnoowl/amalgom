#version 450

layout(location = 0) flat in vec3 v_Normal;
layout(location = 1) flat in vec2 v_TexCoordBase;
layout(location = 2) in vec2 v_TexCoordPercent;
layout(location = 0) out vec4 o_Target;
layout(set = 0, binding = 1) uniform texture2D t_Color;
layout(set = 0, binding = 2) uniform sampler s_Color;
layout(set = 0, binding = 3) uniform texture2D t_Ssao;
layout(set = 0, binding = 4) uniform sampler s_Ssao;

layout(set = 0, binding = 5) uniform DebugLighting {
    bool debug_lighting_toggle;
};

void main() {
    vec3 ssao_occlusion = vec3(1.0, 1.0, 1.0);
    if (debug_lighting_toggle) {
        vec2 ssaoTexSize  = textureSize(sampler2D(t_Ssao, s_Ssao), 0).xy;
        vec2 ssaoTexCoord = gl_FragCoord.xy / ssaoTexSize / 4;
        ssao_occlusion = texture(sampler2D(t_Ssao, s_Ssao), ssaoTexCoord).xyz;
    }

    vec2 texCoord = v_TexCoordBase + mod(v_TexCoordPercent, 1.0) / 16.0;
    vec4 tex = texture(sampler2D(t_Color, s_Color), texCoord);
    float mag = length(texCoord-vec2(0.5));
    vec3 texture = mix(tex.xyz, vec3(0.0), mag*mag); // TODO: use tex.w?

    float direct = clamp(dot(v_Normal, normalize(vec3(0.4, 0.9, -0.2))), 0.0, 1.0);
    float ambient = 0.1;
    float lighting = clamp(direct + ambient, 0.0, 1.0);
    o_Target = vec4(texture * lighting * ssao_occlusion, tex.w);
}
