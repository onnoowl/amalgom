#version 450

layout(location=0) in vec3 v_color;
layout(location=1) in vec3 v_norm;
layout(location = 0) out vec4 fragColor;

void main() {
  float direct = clamp(dot(v_norm, normalize(vec3(0.4, 0.9, -0.2))), 0.0, 1.0);
  float ambient = 0.4;
  float lighting = clamp(direct + ambient, 0.0, 1.0);

  fragColor = vec4(v_color * lighting, 1.0);
}
