#version 450

layout(location = 0) in vec3 a_Pos;
layout(location = 1) in uint a_Meta;
layout(location = 0) out flat vec4 v_Normal;

layout(set = 0, binding = 0) uniform ViewPerspective {
    mat4 u_ViewPerspective;
};
layout(set = 0, binding = 1) uniform View {
    mat4 u_View;
};

void main() {
    uint a_Face = a_Meta & 0xff;
    if (a_Face == 0) { //front
        v_Normal = vec4(0.0, 0.0, -1.0, 1.0);
    } else if (a_Face == 1) { //back
        v_Normal = vec4(0.0, 0.0, 1.0, 1.0);
    } else if (a_Face == 2) { //left
        v_Normal = vec4(1.0, 0.0, 0.0, 1.0);
    } else if (a_Face == 3) { //right
        v_Normal = vec4(-1.0, 0.0, 0.0, 1.0);
    } else if (a_Face == 4) { //top
        v_Normal = vec4(0.0, 1.0, 0.0, 1.0);
    } else if (a_Face == 5) { //bottom
        v_Normal = vec4(0.0, -1.0, 0.0, 1.0);
    } else {
        v_Normal = vec4(0.0, 0.0, 0.0, 0.0);
    }

    mat3 normalMatrix = transpose(inverse(mat3(u_View)));
	v_Normal = vec4(normalMatrix * v_Normal.xyz, 1.0);

    gl_Position = u_ViewPerspective * vec4(a_Pos, 1.0);
}
