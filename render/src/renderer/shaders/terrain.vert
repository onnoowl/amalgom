#version 450

layout(location = 0) in vec3 a_Pos;
layout(location = 1) in uint a_Meta;
    // Meta:
    // corner_and_level: u8, //corner: u2, level: u6
    // tex_palette: u8,      //tex_palette is currently unused
    // tex_id: u8,
    // face: u8,

layout(location = 0) flat out vec3 v_Normal;
layout(location = 1) flat out vec2 v_TexCoordBase;
layout(location = 2) out vec2 v_TexCoordPercent;

layout(set = 0, binding = 0) uniform Locals {
    mat4 u_ViewPerspective;
};

void main() {
    uint a_corner_and_level = a_Meta >> 24;
    // uint a_tex_palette = (a_Meta & 0x00ff0000) >> 16;
    uint a_tex_id = (a_Meta & 0x0000ff00) >> 8;
    uint a_Face = a_Meta & 0x000000ff;
    uint a_corner = a_corner_and_level >> 6;
    uint level = a_corner_and_level & 0x3f;

    if (a_Face == 0) { //front
        v_Normal = vec3(0.0, 0.0, -1.0);
    } else if (a_Face == 1) { //back
        v_Normal = vec3(0.0, 0.0, 1.0);
    } else if (a_Face == 2) { //left
        v_Normal = vec3(1.0, 0.0, 0.0);
    } else if (a_Face == 3) { //right
        v_Normal = vec3(-1.0, 0.0, 0.0);
    } else if (a_Face == 4) { //top
        v_Normal = vec3(0.0, 1.0, 0.0);
    } else if (a_Face == 5) { //bottom
        v_Normal = vec3(0.0, -1.0, 0.0);
    } else {
        v_Normal = vec3(0.0, 0.0, 0.0);
    }

    uint tex_x = a_tex_id % 16;
    uint tex_y = a_tex_id / 16;
    v_TexCoordBase = vec2((float(tex_x)) / 16.0, (float(tex_y)) / 16.0);

    v_TexCoordPercent = vec2(float(a_corner & 1), float(a_corner >> 1));
    v_TexCoordPercent *= pow(2, level);

    gl_Position = u_ViewPerspective * vec4(a_Pos, 1.0);
}
