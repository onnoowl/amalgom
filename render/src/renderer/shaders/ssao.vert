#version 450

layout(location = 0) in vec3 a_Pos;
layout(location = 1) in uint a_Meta;

layout(set = 0, binding = 0) uniform ViewPerspective {
    mat4 u_ViewPerspective;
};

void main() {
    gl_Position = u_ViewPerspective * vec4(a_Pos, 1.0);
}
