#version 450

layout(location = 0) in vec3 a_Pos;
layout(location=1) in vec3 a_color;
layout(location=2) in vec3 a_norm;
layout(location=0) out vec3 v_color;
layout(location=1) out vec3 v_norm;

layout(set = 0, binding = 0) uniform ViewPerspective {
    mat4 u_ViewPerspective;
};

layout(push_constant) uniform ModelTransform {
    mat4 u_Model;
    mat4 u_ModelInvTrans;
};

void main() {
    v_color = a_color;
    v_norm = (u_ModelInvTrans * vec4(a_norm, 1.0)).xyz;
    gl_Position = u_ViewPerspective * u_Model * vec4(a_Pos, 1.0);
}
