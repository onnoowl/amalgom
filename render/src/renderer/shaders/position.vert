#version 450

layout(location = 0) in vec3 a_Pos;
layout(location = 1) in uint a_Meta;
layout(location = 0) out vec4 v_Position;

layout(set = 0, binding = 0) uniform ViewPerspective {
    mat4 u_ViewPerspective;
};

layout(set = 0, binding = 1) uniform View {
    mat4 u_View;
};

void main() {
    v_Position = u_View * vec4(a_Pos, 1.0);
    gl_Position = u_ViewPerspective * vec4(a_Pos, 1.0);
}
