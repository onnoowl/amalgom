// Adapted and modified from https://github.com/lettier/3d-game-shaders-for-beginners/blob/master/demonstration/shaders/fragment/ssao.frag

#version 450

#define NUM_NOISE   4
#define NUM_SAMPLES 8

layout(set = 0, binding = 1) uniform Perspective {
    mat4 u_Perspective;
};

layout(set = 0, binding = 2) uniform texture2D t_Position;
layout(set = 0, binding = 3) uniform sampler s_Position;
layout(set = 0, binding = 4) uniform texture2D t_Normal;
layout(set = 0, binding = 5) uniform sampler s_Normal;

layout(set = 0, binding = 6) uniform Samples {
    mediump vec4 samples[NUM_SAMPLES];
};
layout(set = 0, binding = 7) uniform Noise {
    mediump vec4 noise[NUM_NOISE];
};
//layout( column_major std140

layout(location = 0) out vec4 fragColor;

void main() {
  float radius    = 3.5;
  float bias      = 0.01;
  float magnitude = 0.5;
  float contrast  = 1.0;

  fragColor = vec4(1);

  vec2 texSize  = textureSize(sampler2D(t_Position, s_Position), 0).xy;
  vec2 texCoord = gl_FragCoord.xy / texSize;

  vec4 position = texture(sampler2D(t_Position, s_Position), texCoord);
  if (position.a <= 0) { return; }

  vec3 normal = normalize(texture(sampler2D(t_Normal, s_Normal), texCoord).xyz);

  int  noiseS = int(sqrt(NUM_NOISE));
  int  noiseX = int(gl_FragCoord.x - 0.5) % noiseS;
  int  noiseY = int(gl_FragCoord.y - 0.5) % noiseS;
  vec3 random = noise[noiseX + (noiseY * noiseS)].xyz;

  vec3 tangent  = normalize(random - normal * dot(random, normal));
  vec3 binormal = cross(tangent, normal);
  mat3 tbn      = mat3(tangent, binormal, normal);

  float occlusion = NUM_SAMPLES;

  for (int i = 0; i < NUM_SAMPLES; ++i) {
    vec3 v_sample = tbn * samples[i].xyz;
         v_sample = position.xyz + v_sample * radius;

    vec4 offset      = vec4(v_sample, 1);
         offset      = u_Perspective * offset;
         offset.xyz /= offset.w;
         offset.x    = offset.x * 0.5 + 0.5;
         offset.y   = offset.y * -0.5 + 0.5;

    vec4 offsetPosition = texture(sampler2D(t_Position, s_Position), offset.xy);

    float occluded = 0;
    if (v_sample.z + bias >= offsetPosition.z) { occluded = 0; } else { occluded = 1; }

    float intensity =
      smoothstep
        ( 0
        , 1
        ,   radius
          / abs(position.z - offsetPosition.z)
        );
    occluded  *= intensity;
    occlusion -= occluded;
  }

  occlusion /= NUM_SAMPLES;
  occlusion  = pow(occlusion, magnitude);
  occlusion  = clamp(contrast * (occlusion - 0.5) + 0.5, 0.0, 1.0);

  fragColor = vec4(vec3(occlusion), position.a);
}
