use arrayvec::ArrayVec;
use base_utils::dimension::OcclusionPlane;
use cgmath::{Matrix4, Vector2, Vector3};
use std::fmt::Debug;

pub const WIDTH: usize = 32;
pub const HEIGHT: usize = WIDTH * 3 / 4; //24
const MAX_REGISTERED_PLANES: usize = 4;
// const TILE_WIDTH: f32 = 1.0 / (WIDTH as f32); //screen space spans from 0 to 1
// const TILE_HEIGHT: f32 = 1.0 / (HEIGHT as f32); //screen space spans from 0 to 1

const DEFAULT_TILE: Tile = Tile::NoOcclusion;
const DEFAULT_TILE_ROW: [Tile; WIDTH] = [DEFAULT_TILE; WIDTH];
const DEFAULT_TILES: [[Tile; WIDTH]; HEIGHT] = [DEFAULT_TILE_ROW; HEIGHT];

const TOTAL_MAX_PLANES: u32 = 100;

pub struct OcclusionTiles {
    tiles: Box<[[Tile; WIDTH]; HEIGHT]>,

    plane_counter: u32,

    debug_print_counter: u32,
}

enum Tile {
    NoOcclusion,
    FullyOccluded(f32),
    BoundaryCase(BoundaryCase),
}

struct BoundaryCase {
    registered_planes: ArrayVec<[RegisteredPlane; MAX_REGISTERED_PLANES]>,
}

#[allow(dead_code)]
struct RegisteredPlane {
    projected_plane: OcclusionPlane,
    partial_coverage: PartialCoverage,
}

impl OcclusionTiles {
    /// Returns true if occluded
    pub fn check_bounding_box_occluded(
        &self,
        min_corner: Vector2<usize>,
        max_corner: Vector2<usize>,
        closest_z: f32,
    ) -> bool {
        for y in min_corner.y..=max_corner.y {
            for x in min_corner.x..=max_corner.x {
                if let Tile::FullyOccluded(depth) = self.get(x, y) {
                    if closest_z < *depth {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        true
    }

    pub fn check_chunk_occluded(
        &self,
        min_corner: Vector2<usize>,
        max_corner: Vector2<usize>,
        closest_z: f32,
    ) -> bool {
        for y in min_corner.y..=max_corner.y {
            for x in min_corner.x..=max_corner.x {
                if let Tile::FullyOccluded(depth) = self.get(x, y) {
                    if closest_z < *depth {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        true
    }

    fn get(&self, x: usize, y: usize) -> &Tile {
        &self.tiles[y][x]
    }

    fn get_mut(&mut self, x: usize, y: usize) -> &mut Tile {
        &mut self.tiles[y][x]
    }

    pub fn reset(&mut self) {
        *self.tiles = DEFAULT_TILES;

        self.plane_counter = 0;
    }

    pub fn below_plane_counter_limit(&self) -> bool {
        self.plane_counter < TOTAL_MAX_PLANES
    }

    /// Plane must be in screen space, i.e. each coordinate from 0 to 1
    pub fn register_occluding_plane(
        &mut self,
        plane: &OcclusionPlane,
        view_projection_matrix: &Matrix4<f32>,
    ) {
        let projected_plane = plane.project_to_screen_space(view_projection_matrix);
        // println!("{:?}", plane);

        self.plane_counter += 1;

        for v in &projected_plane.verts {
            if v.z < 0. || v.z > 1. {
                return;
            }
        }

        for (x, y, furthest_depth, coverage) in rasterize_to_tile(&projected_plane.verts) {
            let tile = self.get_mut(x, y);

            // We register planes front to back, so if it's already occluded, move on
            if !matches!(tile, Tile::FullyOccluded(_)) {
                match coverage {
                    RasterCoverage::FullyCovers => *tile = Tile::FullyOccluded(furthest_depth),
                    RasterCoverage::PartialCoverage(partial_coverage) => {
                        let registry = if let Tile::BoundaryCase(boundary) = tile {
                            &mut boundary.registered_planes
                        } else {
                            // Turn into boundary case, then match to get the reference back to the list
                            *tile = Tile::BoundaryCase(BoundaryCase {
                                registered_planes: ArrayVec::new(),
                            });
                            if let Tile::BoundaryCase(BoundaryCase {
                                ref mut registered_planes,
                            }) = tile
                            {
                                registered_planes
                            } else {
                                unreachable!()
                            }
                        };

                        if registry.remaining_capacity() > 0 {
                            let registration = RegisteredPlane {
                                projected_plane: projected_plane.clone(),
                                partial_coverage,
                            };
                            registry.push(registration);
                        }
                    }
                }
            }
        }
    }

    pub fn resolve_boundary_cases(&mut self) {
        for x in 0..WIDTH {
            for y in 0..HEIGHT {
                let tile = self.get_mut(x, y);
                if let Tile::BoundaryCase(_boundary_case) = tile {
                    *tile = Tile::NoOcclusion; // TODO
                }
            }
        }
    }

    // #[cfg(debug_assertions)]
    pub fn debug_print(&mut self) {
        if self.debug_print_counter == 0 {
            println!("{} planes\n{:?}", self.plane_counter, self);
            self.debug_print_counter = 30;
        } else {
            self.debug_print_counter -= 1;
        }
    }
}

impl Default for OcclusionTiles {
    fn default() -> Self {
        OcclusionTiles {
            tiles: Box::new(DEFAULT_TILES),

            plane_counter: 0,

            debug_print_counter: 0,
        }
    }
}

pub enum RasterCoverage {
    FullyCovers,
    PartialCoverage(PartialCoverage),
}

pub enum PartialCoverage {
    EdgeInTile(f32),
    VertexInTile(f32),
}

pub fn rasterize_to_tile(
    verts: &[Vector3<f32>; 4],
) -> impl Iterator<Item = (usize, usize, f32, RasterCoverage)> {
    let wrapped_verts = [verts[0], verts[1], verts[2], verts[3], verts[0]];

    let mut max_z = f32::MIN;
    for v in &wrapped_verts[0..4] {
        if v.z > max_z {
            max_z = v.z;
        }
    }

    let mut min_y = f32::MAX;
    let mut max_y = f32::MIN;

    for v in verts {
        if v.y < min_y {
            min_y = v.y;
        }
        if v.y > max_y {
            max_y = v.y;
        }
    }

    let min_y = (min_y.max(0.) * (HEIGHT - 1) as f32) as usize;
    let max_y = (max_y.min(1.) * (HEIGHT - 1) as f32) as usize;

    (min_y..=max_y).into_iter().flat_map(move |y_row_num| {
        let top_y = y_row_num as f32 / (HEIGHT - 1) as f32;
        let bot_y = (y_row_num + 1) as f32 / (HEIGHT - 1) as f32;

        // This has an edge case that we miss, if there's a sharp spikey edge of a quad WITHIN top_y and bot_y, we might miss a good portion of it in the rasterization because the far tip is beyond the intersections we get at the top_y and bot_y points
        let top_xs = intersections(top_y, &wrapped_verts);
        let bot_xs = intersections(bot_y, &wrapped_verts);

        let outer_min_x = top_xs.min_x.min(bot_xs.min_x);
        let outer_max_x = top_xs.max_x.max(bot_xs.max_x);

        let inner_min_x = top_xs.min_x.max(bot_xs.min_x);
        let inner_max_x = top_xs.max_x.min(bot_xs.max_x);

        let outer_min_x = (outer_min_x * (WIDTH - 1) as f32) as usize;
        let outer_max_x = (outer_max_x * (WIDTH - 1) as f32) as usize;

        let inner_min_x = f32::ceil(inner_min_x * (WIDTH - 1) as f32) as usize;
        let inner_max_x = (inner_max_x * (WIDTH - 1) as f32) as usize; // Make these very exclusive bounds

        (outer_min_x..=outer_max_x)
            .into_iter()
            .map(move |x_col_num| {
                if x_col_num >= inner_min_x && x_col_num <= inner_max_x {
                    (x_col_num, y_row_num, max_z, RasterCoverage::FullyCovers)
                } else {
                    (
                        x_col_num,
                        y_row_num,
                        max_z,
                        RasterCoverage::PartialCoverage(PartialCoverage::EdgeInTile(max_z)),
                    )
                }
            })
    })
}

fn intersections(y: f32, wrapped_verts: &[Vector3<f32>; 5]) -> IntersectResult {
    let mut result = IntersectResult {
        min_x: f32::MAX,
        max_x: f32::MIN,
    };

    for i in 0..4 {
        let v1 = wrapped_verts[i];
        let v2 = wrapped_verts[i + 1];

        let xdif = v2.x - v1.x;
        let ydif = v2.y - v1.y;

        if ydif != 0. {
            let x = (xdif / ydif) * (y - v1.y) + v1.x;

            if x >= v1.x.min(v2.x) && x <= v1.x.max(v2.x) {
                if x < result.min_x {
                    result.min_x = x;
                }
                if x > result.max_x {
                    result.max_x = x;
                }
            }
        } else {
            let max_x = v1.x.max(v2.x);
            let min_x = v1.x.min(v2.x);
            if min_x < result.min_x {
                result.min_x = min_x;
            }
            if max_x > result.max_x {
                result.max_x = max_x;
            }
        }

        // (y - y1) = (y2 - y1)/(x2 - x1) * (x - x1)
        // y = given_y

        // (given_y - y1) = (y2-y1)/(x2-x1) * (x - x1)
        // (x2-x1)/(y2-y1) * (given_y - y1) = (x - x1)
        // (x2-x1)/(y2-y1) * (given_y - y1) + x1 = x
    }

    result.min_x = result.min_x.max(0.);
    result.max_x = result.max_x.min(1.);

    result
}

struct IntersectResult {
    min_x: f32,
    max_x: f32,
}

impl Debug for OcclusionTiles {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self.tiles.iter().rev() {
            for tile in row {
                match tile {
                    Tile::NoOcclusion => write!(f, " ")?,
                    Tile::FullyOccluded(_d) => write!(f, "#")?,
                    Tile::BoundaryCase(_) => write!(f, "+")?,
                }
            }
            write!(f, "\n")?
        }
        Ok(())
    }
}
