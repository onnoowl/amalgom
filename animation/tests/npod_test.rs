use animation::*;

use cgmath::{assert_ulps_eq, vec3, Deg, InnerSpace, One, Quaternion, Rotation3};

#[derive(Debug)]
struct TestBiped {
    pub frames: Keyframes<NPodState<{ Self::N }>>,
    pub kchain: KChain<{ Self::K }>,
}

impl TestBiped {
    const N: usize = 2;
    const K: usize = 5;
    const PARAMS: NPodParams<{ Self::N }> = NPodParams {
        legs_config: [
            LegConfig {
                base_pos: vec3(0.2, 0., 0.),
                upper_seg_vec: vec3(0., -1., 0.),
                lower_seg_vec: vec3(0., -1.5, 0.),
                bend_dir: vec3(0., 1., 0.),
            },
            LegConfig {
                base_pos: vec3(-0.3, 0., 0.),
                upper_seg_vec: vec3(0., -1., 0.),
                lower_seg_vec: vec3(0., -1.5, 0.),
                bend_dir: vec3(0., 1., 0.),
            },
        ],
    };
    const MAPPING: NPodMapping<{ Self::N }, { Self::K }> = NPodMapping::linear_mapping();
    const DEFAULT: Self = TestBiped {
        frames: Keyframes::None,
        kchain: KChain::init_with_npod(&Self::PARAMS, &Self::MAPPING),
    };
}

impl Default for TestBiped {
    fn default() -> Self {
        Self::DEFAULT
    }
}

#[test]
fn all_cached_cumulative_transform_some_after_update() {
    let mut bot = TestBiped::DEFAULT;
    bot.kchain.update();
    for joint in bot.kchain.joints.iter() {
        assert!(joint.cached_cumulative_transform.is_some())
    }
}

fn check_foot_placement_with_state(state: NPodState<{ TestBiped::N }>) {
    let mut bot = TestBiped::DEFAULT;

    // compute pose that puts the legs where they should be
    let pose = state
        .compute_npod_pose(&TestBiped::PARAMS, &TestBiped::MAPPING)
        .unwrap();

    bot.kchain.set_pose(&pose);
    bot.kchain.update();

    dbg!(&bot.kchain);

    for (i, expected_foot_pos) in state.feet.iter().enumerate() {
        dbg!(expected_foot_pos);

        let conf = &TestBiped::PARAMS.legs_config[i];

        // Get the transforms

        let body_trans = bot.kchain.joints[0].cached_cumulative_transform.unwrap();

        let upper_trans = bot
            .kchain
            .get_leg(i, &LegSegment::Upper, &TestBiped::MAPPING)
            .cached_cumulative_transform
            .unwrap();

        let lower_trans = bot
            .kchain
            .get_leg(i, &LegSegment::Lower, &TestBiped::MAPPING)
            .cached_cumulative_transform
            .unwrap();

        // Apply the transforms

        let real_shldr_pos = body_trans.transform_vec(conf.base_pos);
        let real_knee_pos = upper_trans.transform_vec(conf.upper_seg_vec);
        let real_foot_pos = lower_trans.transform_vec(conf.lower_seg_vec);

        dbg!(real_shldr_pos, real_knee_pos, real_foot_pos);

        // Test

        // Check upper leg len
        assert_ulps_eq!(
            (real_knee_pos - real_shldr_pos).magnitude(),
            conf.upper_seg_vec.magnitude(),
            epsilon = 0.00001
        );
        // Check lower leg len
        assert_ulps_eq!(
            (real_foot_pos - real_knee_pos).magnitude(),
            conf.lower_seg_vec.magnitude(),
            epsilon = 0.00001
        );
        // Check for it where we expect
        assert_ulps_eq!(real_foot_pos, *expected_foot_pos, epsilon = 0.00001);
    }
}

#[test]
fn simple_foot_placement() {
    let state = NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 0.),
            scale: 1.,
            rotation: Quaternion::one(),
        },

        // list desired leg positions
        feet: [vec3(0.5, 0., 0.5), vec3(-0.5, 0.5, 0.5)],

        ease_type: None,
    };

    check_foot_placement_with_state(state);
}

#[test]
fn rotated_foot_placement() {
    let state = NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 0.), //15.),
            scale: 1.,
            rotation: Quaternion::from_axis_angle(vec3(0., 0., 1.), Deg(90.)).normalize(),
        },

        // list desired leg positions
        feet: [vec3(0., 0.2, 1.5), vec3(0., -0.3, 1.5)],

        ease_type: None,
    };

    check_foot_placement_with_state(state);
}

#[test]
fn trans_foot_placement() {
    let state = NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 15.),
            scale: 1.,
            rotation: Quaternion::one(),
        },

        // list desired leg positions
        feet: [vec3(0.5, 0., 15.5), vec3(-0.5, 0.5, 15.5)],

        ease_type: None,
    };

    check_foot_placement_with_state(state);
}

#[test]
fn trans_and_rot_foot_placement() {
    let state = NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 15.),
            scale: 1.,
            rotation: Quaternion::from_axis_angle(vec3(0., 0., 1.), Deg(90.)).normalize(),
        },

        // list desired leg positions
        feet: [vec3(0.5, 0., 15.5), vec3(-0.5, 0.5, 15.5)],

        ease_type: None,
    };

    check_foot_placement_with_state(state);
}

// TODO: test leg placement that aligns with the leg_bend_dir, messing with the cross product
