use crate::KChain;

trait HasKChain<const K: usize> {
    // const K: usize;

    fn get_kchain(&self) -> &KChain<K>;
}

// pub trait Rig {
//     fn update_pose(&self, pose: Pose);

//     // fn get_joints(&self) -> &[Joint];

//     fn iter_joints(&self) -> dyn Iterator<Item = Joint>;
// }

// impl<T: HasKChain> Rig for T {
//     fn update_pose(&self, pose: Pose) -> {
//         self.bot.kchain.set_pose(&pose);
//         self.bot.kchain.update();
//     }
//     ...
// }
