use crate::{
    pose::Pose,
    transform::{QVTransform, Transform},
    LegSegment, NPodMapping,
};

use cgmath::{Matrix4, One, Quaternion};

pub type JointIndex = usize;
pub const ROOT_JOINT_PARENT_INDEX: JointIndex = usize::MAX;
pub const BLANK_JOINT_PARENT_INDEX: JointIndex = usize::MAX - 1;

/// This is a K(inematics) Chain. K here is also the length of the chain
/// Each joint's parent must come BEFORE it in the chain
#[derive(Debug, Clone)]
pub struct KChain<const K: usize> {
    pub joints: [Joint; K],
}

impl<const K: usize> KChain<K> {
    pub fn get_joint_index(&self, joint_name: &str) -> Option<JointIndex> {
        for (index, joint) in self.joints.iter().enumerate() {
            if joint.name.map(|s| s == joint_name) == Some(true) {
                return Some(index as JointIndex);
            }
        }

        None
    }

    pub fn get(&self, index: JointIndex) -> &Joint {
        &self.joints[index]
    }

    pub fn get_mut(&mut self, index: JointIndex) -> &mut Joint {
        &mut self.joints[index]
    }

    pub fn get_leg<const N: usize>(
        &self,
        leg: usize,
        segment: &LegSegment,
        mapping: &NPodMapping<N, K>,
    ) -> &Joint {
        &self.joints[mapping.get_leg_index(leg, segment)]
    }

    pub fn get_leg_mut<const N: usize>(
        &mut self,
        leg: usize,
        segment: &LegSegment,
        mapping: &NPodMapping<N, K>,
    ) -> &Joint {
        &mut self.joints[mapping.get_leg_index(leg, segment)]
    }

    /// Overwrites existing rotations with the desired pose
    /// Note, `update` must be called after this for changes to take place in the `cached_cumulative_transform`
    pub fn set_pose(&mut self, pose: &Pose<K>) {
        self.joints[0].transform.translation = pose.root_translation;
        self.joints[0].transform.scale = pose.root_scale;
        for (joint, rotation) in self.joints.iter_mut().zip(pose.joint_rotations.iter()) {
            joint.transform.set_rotation(*rotation)
        }
    }

    /// Applies a pose on top of the existing pose, multiplying the quaternions together
    /// It adds in the root translation, and multiplies in the root scaling
    /// Note, `update` must be called after this for changes to take place in the `cached_cumulative_transform`
    pub fn apply_pose(&mut self, pose: &Pose<K>) {
        self.joints[0].transform.translation += pose.root_translation;
        self.joints[0].transform.scale *= pose.root_scale;
        for (joint, rotation) in self.joints.iter_mut().zip(pose.joint_rotations.iter()) {
            joint.transform.rotation = joint.transform.rotation * *rotation
        }
    }

    /// Updates the cached cumulative transforms within each joint.
    /// Will make `cached_cumulative_transform` `Some` for every non-blank joint. Blank joints are skipped.
    pub fn update(&mut self) {
        for i in 0..K {
            let joint = &mut self.joints[i];
            match joint.parent_index {
                ROOT_JOINT_PARENT_INDEX => {
                    joint.cached_cumulative_transform = Some(joint.transform.to_matrix());
                    joint.cached_cumulative_rotation = Some(joint.transform.rotation);
                }
                BLANK_JOINT_PARENT_INDEX => (), // skip blank joints, they are placeholders
                parent_index => {
                    debug_assert!(
                        self.joints[parent_index].parent_index != BLANK_JOINT_PARENT_INDEX,
                        "`parent_index` pointed to a blank joint."
                    );
                    self.joints[i].cached_cumulative_transform = Some(
                        self.joints[parent_index].cached_cumulative_transform.expect(
                            "`cached_cumulative_transform` was none. `parent_index` in all joints must point to before their own position to avoid this."
                        ) * self.joints[i].transform.to_matrix()
                    );
                    self.joints[i].cached_cumulative_rotation = Some(
                        self.joints[parent_index].cached_cumulative_rotation.expect(
                            "`cached_cumulative_rotation` was none. `parent_index` in all joints must point to before their own position to avoid this."
                        ) * self.joints[i].transform.rotation
                    )
                }
            }
        }
    }

    /// Sets all rotations in the KChain to identity
    /// Does NOT clear any translations, including the root translation (which is technically part of a pose)
    /// Note, `update` must be called after this for changes to take place in the `cached_cumulative_transform`
    /// This will leave every `cached_cumulative_transform` value in the chain as Some
    pub fn clear_pose(&mut self) {
        for joint in self.joints.iter_mut() {
            joint.transform.set_rotation(Quaternion::one())
        }
    }

    /// Clears the cached cumulative transforms within each joint.
    pub fn clear_cache(&mut self) {
        for joint in self.joints.iter_mut() {
            joint.cached_cumulative_transform = None
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Joint {
    pub name: Option<&'static str>,

    /// The parent of a joint must come before it in the chain
    pub parent_index: JointIndex,

    /// This represents the transform for this joint.
    /// Its position will likely be fixed, and represents this joints offset from its parent.
    /// Its rotation can be changed by applying Poses
    pub transform: QVTransform,

    /// Calling `update` on the kchain populates this field with cumulative transforms.
    /// These transforms can be applied directly to graphical representations to put them where they need to be in the world coordinate space
    /// This value _will_ be `Some` after `update` is called on the KChain, unless this is a blank Joint (i.e. `parent_index` equals `BLANK_JOINT_PARENT_INDEX`)
    pub cached_cumulative_transform: Option<Matrix4<f32>>,

    /// Calling `update` on the kchain populates this field with cumulative rotations.
    /// These rotations can be turned into Matrix4s, and can serve as the inverse transpose of the transform matrix
    /// This value _will_ be `Some` after `update` is called on the KChain, unless this is a blank Joint (i.e. `parent_index` equals `BLANK_JOINT_PARENT_INDEX`)
    pub cached_cumulative_rotation: Option<Quaternion<f32>>,
}

impl Joint {
    /// Blank joints are skipped by `update` and do nothing.
    /// This is useful for initialization, or leaving space for future joints.
    pub const fn blank() -> Self {
        Self {
            name: None,
            parent_index: BLANK_JOINT_PARENT_INDEX,
            transform: QVTransform::const_identity(),
            cached_cumulative_transform: None,
            cached_cumulative_rotation: None,
        }
    }

    pub const fn root() -> Self {
        Self {
            name: None,
            parent_index: ROOT_JOINT_PARENT_INDEX,
            transform: QVTransform::const_identity(),
            cached_cumulative_transform: None,
            cached_cumulative_rotation: None,
        }
    }

    pub fn named_root() -> Self {
        Self {
            name: Some("root"),
            parent_index: ROOT_JOINT_PARENT_INDEX,
            transform: QVTransform::const_identity(),
            cached_cumulative_transform: None,
            cached_cumulative_rotation: None,
        }
    }

    pub const fn with_parent(parent_index: JointIndex) -> Self {
        Self {
            name: None,
            parent_index,
            transform: QVTransform::const_identity(),
            cached_cumulative_transform: None,
            cached_cumulative_rotation: None,
        }
    }

    pub fn is_root(&self) -> bool {
        self.parent_index == ROOT_JOINT_PARENT_INDEX
    }

    pub fn is_blank(&self) -> bool {
        self.parent_index == BLANK_JOINT_PARENT_INDEX
    }
}

impl Default for Joint {
    fn default() -> Self {
        Self::blank()
    }
}
