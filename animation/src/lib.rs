//! The main way to interact with this crate is through a `KChain` and `Keyframes`.
//! `Keyframes` can interpolate state, and can be used to create a `Pose`. Poses can then be applied to KChains.
//! For "NPods", you'll want a `Keyframes<NPodState<N>>`. The function `interpolate_npod_pose` can then provide a pose that can be used on a KChain.

#![feature(const_precise_live_drops)]
#![feature(const_fn_floating_point_arithmetic)]
#![feature(div_duration)]

pub mod interpolate;
pub mod kchain;
pub mod keyframes;
pub mod npod;
pub mod param_zoo;
pub mod pose;
pub mod rig;
pub mod transform;

pub use interpolate::*;
pub use kchain::*;
pub use keyframes::*;
pub use npod::*;
pub use pose::*;
pub use transform::*;
