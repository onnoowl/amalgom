use crate::{LegConfig, NPodParams};
use cgmath::{vec3, Vector3};

pub const N: usize = 6;
pub const LEG_LENGTH: f32 = 10.0;
pub const UPPER_SEG: Vector3<f32> = vec3(0.0, LEG_LENGTH, 0.0);
pub const LOWER_SEG: Vector3<f32> = vec3(0.0, LEG_LENGTH, 0.0);
pub const WIDTH: f32 = 2.0;
pub const LENGTH: f32 = 8.0;
pub const BEND_DIR_Y: f32 = (WIDTH + LENGTH) / 1.5;
pub const PARAMS: NPodParams<{ N }> = NPodParams {
    legs_config: [
        // Forward left
        LegConfig {
            base_pos: vec3(WIDTH / 2., 0., LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: vec3(WIDTH / 2., BEND_DIR_Y, LENGTH / 2.),
        },
        // Forward right
        LegConfig {
            base_pos: vec3(-WIDTH / 2., 0., LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: vec3(-WIDTH / 2., BEND_DIR_Y, LENGTH / 2.),
        },
        // Mid left
        LegConfig {
            base_pos: vec3(WIDTH / 2., 0., 0.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: vec3(WIDTH / 2., BEND_DIR_Y, 0.),
        },
        // Mid right
        LegConfig {
            base_pos: vec3(-WIDTH / 2., 0., 0.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: vec3(-WIDTH / 2., BEND_DIR_Y, 0.),
        },
        // Back left
        LegConfig {
            base_pos: vec3(WIDTH / 2., 0., -LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: vec3(WIDTH / 2., BEND_DIR_Y, -LENGTH / 2.),
        },
        // Back right
        LegConfig {
            base_pos: vec3(-WIDTH / 2., 0., -LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: vec3(-WIDTH / 2., BEND_DIR_Y, -LENGTH / 2.),
        },
    ],
};
