use crate::{EaseType, LegConfig, NPodParams, NPodState, QVTransform};
use base_utils::F32Coord;
use cgmath::{vec3, Quaternion, Vector3};

// #[derive(Debug)]
// struct Ant {
//     pub frames: Keyframes<NPodState<{ Self::N }>>,
//     pub kchain: KChain<{ Self::K }>,
// }

// impl Rig for Ant {
//     const N: usize = ant::N; //6
//     const K: usize = 13;

//     const PARAMS: NPodParams<{ Self::N }> = ant::PARAMS;
//     const MAPPING: NPodMapping<{ Self::N }, { Self::K }> = NPodMapping::linear_mapping();
//     const DEFAULT: Self = Ant {
//         frames: Keyframes::None,
//         kchain: KChain::init_with_npod(&Self::PARAMS, &Self::MAPPING),
//     };
// }

// impl Default for Ant {
//     fn default() -> Self {
//         Self::DEFAULT
//     }
// }

pub const N: usize = 6;
pub const UPPER_SEG: Vector3<f32> = vec3(0.0, 1.0, 0.0);
pub const LOWER_SEG: Vector3<f32> = vec3(0.0, 1.0, 0.0);
pub const BEND_DIR: Vector3<f32> = vec3(0.0, 1.0, 0.0);
pub const WIDTH: f32 = 1.0;
pub const LENGTH: f32 = 2.0;
pub const PARAMS: NPodParams<{ N }> = NPodParams {
    legs_config: [
        // Forward left
        LegConfig {
            base_pos: vec3(WIDTH / 2., 0., LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: BEND_DIR,
        },
        // Forward right
        LegConfig {
            base_pos: vec3(-WIDTH / 2., 0., LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: BEND_DIR,
        },
        // Mid left
        LegConfig {
            base_pos: vec3(WIDTH / 2., 0., 0.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: BEND_DIR,
        },
        // Mid right
        LegConfig {
            base_pos: vec3(-WIDTH / 2., 0., 0.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: BEND_DIR,
        },
        // Back left
        LegConfig {
            base_pos: vec3(WIDTH / 2., 0., -LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: BEND_DIR,
        },
        // Back right
        LegConfig {
            base_pos: vec3(-WIDTH / 2., 0., -LENGTH / 2.),
            upper_seg_vec: UPPER_SEG,
            lower_seg_vec: LOWER_SEG,
            bend_dir: BEND_DIR,
        },
    ],
};

/// Ideal max spacing between feet
pub const FEET_WIDTH: f32 = 2.5 + WIDTH;

/// Ideal max spacing between feet length wise
pub const FEET_LENGTH: f32 = 1.0 + LENGTH;

pub const BODY_LIFT_HEIGHT: f32 = 0.75;

pub const FOOT_LIFT_HEIGHT: f32 = 0.8;

/// Neutral standing pos
const STANDING: [F32Coord; N] = [
    vec3(FEET_WIDTH / 2., -BODY_LIFT_HEIGHT, FEET_LENGTH / 2.),
    vec3(-FEET_WIDTH / 2., -BODY_LIFT_HEIGHT, FEET_LENGTH / 2.),
    vec3(FEET_WIDTH / 2., -BODY_LIFT_HEIGHT, 0.),
    vec3(-FEET_WIDTH / 2., -BODY_LIFT_HEIGHT, 0.),
    vec3(FEET_WIDTH / 2., -BODY_LIFT_HEIGHT, -FEET_LENGTH / 2.),
    vec3(-FEET_WIDTH / 2., -BODY_LIFT_HEIGHT, -FEET_LENGTH / 2.),
];
// how far forward (and back) legs reach
pub const STRIDE: f32 = 0.6;
const STRIDE_V: Vector3<f32> = vec3(0., 0., STRIDE);
// vector for lifting legs
const LIFT_V: Vector3<f32> = vec3(0., FOOT_LIFT_HEIGHT, 0.);

const SLOWDOWN_ALPHA: f32 = 0.8;

/// temp, for debug
pub const CANNED_ANIMATION: [NPodState<{ self::N }>; 4] = [
    NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 0.),
            scale: 1.,
            rotation: Quaternion::from_sv(1., cgmath::vec3(0., 0., 0.)),
        },

        // list desired leg positions
        feet: [
            add(STANDING[0], STRIDE_V),
            sub(STANDING[1], STRIDE_V),
            sub(STANDING[2], STRIDE_V),
            add(STANDING[3], STRIDE_V),
            add(STANDING[4], STRIDE_V),
            sub(STANDING[5], STRIDE_V),
        ],

        ease_type: Some(EaseType::Slowdown(SLOWDOWN_ALPHA)),
    },
    NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 0.),
            scale: 1.,
            rotation: Quaternion::from_sv(1., cgmath::vec3(0., 0., 0.)),
        },

        // list desired leg positions
        feet: [
            STANDING[0],
            add(STANDING[1], LIFT_V),
            add(STANDING[2], LIFT_V),
            STANDING[3],
            STANDING[4],
            add(STANDING[5], LIFT_V),
        ],

        ease_type: Some(EaseType::Continue),
    },
    NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 0.),
            scale: 1.,
            rotation: Quaternion::from_sv(1., cgmath::vec3(0., 0., 0.)),
        },

        // list desired leg positions
        feet: [
            sub(STANDING[0], STRIDE_V),
            add(STANDING[1], STRIDE_V),
            add(STANDING[2], STRIDE_V),
            sub(STANDING[3], STRIDE_V),
            sub(STANDING[4], STRIDE_V),
            add(STANDING[5], STRIDE_V),
        ],

        ease_type: Some(EaseType::Slowdown(SLOWDOWN_ALPHA)),
    },
    NPodState {
        // translate and rotate the body to make it harder
        body: QVTransform {
            translation: vec3(0., 0., 0.),
            scale: 1.,
            rotation: Quaternion::from_sv(1., cgmath::vec3(0., 0., 0.)),
        },

        // list desired leg positions
        feet: [
            add(STANDING[0], LIFT_V),
            STANDING[1],
            STANDING[2],
            add(STANDING[3], LIFT_V),
            add(STANDING[4], LIFT_V),
            STANDING[5],
        ],

        ease_type: Some(EaseType::Continue),
    },
];

const fn add(a: F32Coord, b: F32Coord) -> F32Coord {
    F32Coord::new(a.x + b.x, a.y + b.y, a.z + b.z)
}
const fn sub(a: F32Coord, b: F32Coord) -> F32Coord {
    F32Coord::new(a.x - b.x, a.y - b.y, a.z - b.z)
}
