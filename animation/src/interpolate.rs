use std::f32::consts::FRAC_PI_2 as HPI;

pub trait Interpolate: Clone {
    /// Returns a new value approximately equal to `self * (1. - amount) + other * amount`.
    fn interpolate(&self, other: &Self, amount: f32) -> Self;
}

pub trait Ease: Interpolate {
    fn get_ease_type(&self) -> EaseType;

    fn ease(&self, other: &Self, amount: f32) -> Self {
        let amount = match (self.get_ease_type(), other.get_ease_type()) {
            (EaseType::Slowdown(alpha1), EaseType::Slowdown(alpha2)) => {
                ease_in_out(amount, alpha1, alpha2)
            }
            (EaseType::Slowdown(alpha), EaseType::Continue) => ease_in(amount, alpha),
            (EaseType::Continue, EaseType::Slowdown(alpha)) => ease_out(amount, alpha),
            (EaseType::Continue, EaseType::Continue) => amount,
        };
        self.interpolate(other, amount)
    }
}

fn ease_in_out(amt: f32, alpha1: f32, alpha2: f32) -> f32 {
    (1. - amt) * ease_in(amt, alpha1) + amt * ease_out(amt, alpha2)
}

fn ease_in(amt: f32, mut alpha: f32) -> f32 {
    if alpha <= 0. {
        alpha = 0.01;
    } else if alpha > 1. {
        alpha = 1.;
    }
    1. - (HPI * alpha * (1. - amt)).sin() / (HPI * alpha).sin()
}

fn ease_out(amt: f32, mut alpha: f32) -> f32 {
    if alpha <= 0. {
        alpha = 0.01;
    } else if alpha > 1. {
        alpha = 1.;
    }
    (HPI * alpha * amt).sin() / (HPI * alpha).sin()
}

/// Whether or not the interpolation should come to a smooth stop at this keyframe, or continuously glide straight through it
#[derive(Clone, Debug, PartialEq, Copy)]
pub enum EaseType {
    /// With Slowdown, the robot will slow or stop at this keyframe before continuing
    /// f32 must be in the range of (0, 1]
    /// When this f32 == 1.0, the animation will come to a full stop for this key frame.
    /// When this f32 is close to 0.0, it will act more like `Continue`
    Slowdown(f32),

    /// With Continue, the animation will smoothly continue through this keyframe at speed
    Continue,
}

impl<T> Interpolate for T
where
    T: Clone + std::ops::Add<Self, Output = Self>,
    for<'a> &'a T: std::ops::Mul<f32, Output = Self>,
{
    fn interpolate(&self, other: &Self, amount: f32) -> Self {
        (self * (1. - amount)) + other * amount
    }
}
