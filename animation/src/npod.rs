use std::time::Instant;

use crate::{
    interpolate::Interpolate,
    kchain::JointIndex,
    keyframes::Keyframes,
    pose::Pose,
    transform::{QVTransform, Transform},
    Ease, EaseType, Joint, KChain,
};
use base_utils::F32Coord;

use cgmath::{InnerSpace, Quaternion, Rad, Rotation, Rotation3};
use float_ord::FloatOrd;

impl<const N: usize> Keyframes<NPodState<N>> {
    /// Interpolates the NPodState from the keyframes, then computes the pose that will make the robot put its legs in the desired place
    pub fn interpolate_npod_pose<const K: usize>(
        &self,
        cur_timestamp: Instant,
        params: &NPodParams<N>,
        mapping: &NPodMapping<N, K>,
    ) -> Result<Pose<K>, NPodInterpolatePoseErr> {
        self.interpolate_state(cur_timestamp)
            .ok_or(NPodInterpolatePoseErr::NoKeyframe)
            .and_then(|state| {
                state
                    .compute_npod_pose(params, mapping)
                    .ok_or(NPodInterpolatePoseErr::LegPosUnreachable)
            })
    }
}

impl<const N: usize> Keyframes<NPodState<N>> {
    /// Eases the NPodState from the keyframes, then computes the pose that will make the robot put its legs in the desired place
    pub fn ease_npod_pose<const K: usize>(
        &self,
        cur_timestamp: Instant,
        params: &NPodParams<N>,
        mapping: &NPodMapping<N, K>,
    ) -> Result<Pose<K>, NPodInterpolatePoseErr> {
        self.ease_state(cur_timestamp)
            .ok_or(NPodInterpolatePoseErr::NoKeyframe)
            .and_then(|state| {
                state
                    .compute_npod_pose(params, mapping)
                    .ok_or(NPodInterpolatePoseErr::LegPosUnreachable)
            })
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct NPodState<const N: usize> {
    /// The transform for the body itself
    pub body: QVTransform,

    /// The positions each foot should be at
    pub feet: [F32Coord; N],

    pub ease_type: Option<EaseType>,
}

/// This defines things like leg length, and leg positions on the body
/// This is needed by the logic and render sides
/// NOTE: Typical controller logic assumes that the legs are configured so that +Y is up, and +Z is "forward" for the npod
pub struct NPodParams<const N: usize> {
    /// Starting coordinates of legs relative to root
    /// The coordinate system assumes +Y is up, +Z is forward
    pub legs_config: [LegConfig; N],
}

pub struct LegConfig {
    /// The position of the leg's beginning.
    pub base_pos: F32Coord,

    /// A vector representing the direction and magnitude of the upper segment of the leg
    pub upper_seg_vec: F32Coord,

    /// A vector representing the direction and magnitude of the lower segment of the leg
    pub lower_seg_vec: F32Coord,

    /// This is relative to the unmodified skeleton, so "up" or outward would be great for spiders. "Forward" would be better for quadrupeds or bipeds.
    pub bend_dir: F32Coord,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LegSegment {
    Upper = 0,
    Lower,
}

impl<const N: usize> NPodState<N> {
    /// Returns None when a leg position is unreachable due to being too far
    pub fn compute_npod_pose<const K: usize>(
        &self,
        params: &NPodParams<N>,
        mapping: &NPodMapping<N, K>,
    ) -> Option<Pose<K>> {
        // TODO: isolate which ".normalize()" calls are most important in order to try and optimize
        let mut pose = Pose::<K>::default();

        pose.root_translation = self.body.get_translation();
        pose.root_scale = self.body.scale;
        pose.joint_rotations[0] = self.body.get_rotation();

        for i in 0..N {
            let config = &params.legs_config[i];
            let start_pos = self.body.transform_vec(config.base_pos);
            let leg_end = self.feet[i];
            let target_vector = leg_end - start_pos;
            let target_vector_mag = target_vector.magnitude();
            let target_vector_norm = target_vector / target_vector_mag;

            let leg_bend_dir_norm = self
                .body
                .rotation
                .rotate_vector(config.bend_dir.normalize())
                .normalize();

            let bend_axis = leg_bend_dir_norm.cross(target_vector_norm).normalize();
            if cgmath::ulps_eq!(bend_axis, cgmath::vec3(0., 0., 0.)) {
                // TODO, find other perpendicular vector
                log::warn!("`leg_bend_dir_norm` was equal to `target_vector_norm`");
                return None;
            }

            let upper_seg_mag = config.upper_seg_vec.magnitude();
            let lower_seg_mag = config.lower_seg_vec.magnitude();

            // ## Get desired angles ##

            // See law of cosines: https://mathworld.wolfram.com/LawofCosines.html

            // The angle between the target_vector and the ideal upper_seg
            let ideal_upper_seg_angle = ((target_vector_mag.powi(2) + upper_seg_mag.powi(2)
                - lower_seg_mag.powi(2))
                / (2. * target_vector_mag * upper_seg_mag))
                .acos();

            // The angle between the target vector and the ideal lower_seg
            let ideal_lower_seg_angle = ((target_vector_mag.powi(2) - upper_seg_mag.powi(2)
                + lower_seg_mag.powi(2))
                / (2. * target_vector_mag * lower_seg_mag))
                .acos();

            if ideal_upper_seg_angle.is_nan() || ideal_lower_seg_angle.is_nan() {
                return None;
            }

            // ## Upper segment ##

            // Find the desired orientation for the upper_seg
            // Since `body_rot * DESIRED_UPPER_SEG_ROT * config.upper_seg_vec == Quat(bend_axis, -ideal_upper_seg_angle) * target_vector_norm`
            // Then we solve for `DESIRED_UPPER_SEG_ROT` by computing
            // `DESIRED_UPPER_SEG_ROT = body_rot^-1 * Quat(bend_axis, -ideal_upper_seg_angle) * target_vector_norm
            let desired_upper_seg_norm = (self.body.rotation.invert()
                * Quaternion::<f32>::from_axis_angle(bend_axis, Rad(-ideal_upper_seg_angle))
                    .normalize())
            .rotate_vector(target_vector_norm)
            .normalize();

            // Compute the quaternion that gets us to the desired vector
            let upper_seg_quat = Quaternion::<f32>::from_arc(
                config.upper_seg_vec.normalize(),
                desired_upper_seg_norm,
                None,
            )
            .normalize();

            // ## Lower segment ##

            // Find the desired orientation for the lower_seg
            // Since `body_rot * upper_seg_rot * DESIRED_LOWER_SEG_ROT * config.lower_seg_vec == Quat(bend_axis, ideal_lower_seg_angle) * target_vector_norm`
            // Then we solve for `DESIRED_LOWER_SEG_ROT` by computing
            // `DESIRED_LOWER_SEG_ROT = upper_seg_rot^-1 * body_rot^-1 * Quat(bend_axis, ideal_lower_seg_angle) * target_vector_norm
            let desired_lower_seg_norm = (upper_seg_quat.invert()
                * self.body.rotation.invert()
                * Quaternion::<f32>::from_axis_angle(bend_axis, Rad(ideal_lower_seg_angle))
                    .normalize())
            .rotate_vector(target_vector_norm)
            .normalize();

            // Compute the quaternion that gets us to the desired vector
            let lower_seg_quat = Quaternion::<f32>::from_arc(
                config.lower_seg_vec.normalize(),
                desired_lower_seg_norm,
                None,
            )
            .normalize();

            // ## Store results in pose ##

            pose.joint_rotations[mapping.get_leg_index(i, &LegSegment::Upper)] = upper_seg_quat;
            pose.joint_rotations[mapping.get_leg_index(i, &LegSegment::Lower)] = lower_seg_quat;
        }

        Some(pose)
    }
}

/// This defines how the body and legs map to joints in the KChain
/// This is needed by just the render side, not the logic
/// N is the number of legs, and K is the size of the KChain
pub struct NPodMapping<const N: usize, const K: usize> {
    legs: [(JointIndex, JointIndex); N],
}

impl<const N: usize, const K: usize> NPodMapping<N, K> {
    /// This is a const fn that does bounds checking at compile time to verify the mapping
    /// Each element of `legs` should correspond to a leg.
    /// The first element in the tuple is for LegSegment::Upper, the second is for LegSegment::Lower.
    /// JointIndices should correspond to the relevant joints in the KChain
    /// N is the number of legs, and K is the size of the KChain
    pub const fn new(legs: [(JointIndex, JointIndex); N]) -> Self {
        let mut i = 0_usize;
        // for loops not allowed in const fns yet
        while i < N {
            let (upper_i, lower_i) = legs[i];

            if upper_i >= K || lower_i >= K {
                panic!("Invalid JointIndex. Please ensure values are < K")
            }

            // TODO: Use below code when formatting in panics is implemented in Rust
            // if upper_i >= K {
            //     panic!(
            //         "Upper JoinIndex (first in tuple) at index={} was {}, but must be < {}",
            //         i, upper_i, K
            //     );
            // } else if lower_i >= K {
            //     panic!(
            //         "Lower JoinIndex (second in tuple) at index={} was {}, but must be < {}",
            //         i, upper_i, K
            //     );
            // }
            i += 1;
        }
        NPodMapping { legs }
    }

    /// Only works for NPodMapping<N, K> where 2*N = K.
    /// The default linear mapping assigns joints as follows:
    /// Joint 1: Leg 1 upper segment
    /// Joint 2: Leg 1 lower segment
    /// Joint 3: Leg 2 upper segment
    /// Joint 4: Leg 2 lower segment
    /// Joint 5: Leg 3 upper segment
    /// Joint 6: Leg 3 lower segment
    /// and so on. Joint 0 is always the root.
    /// This is equivalent to NPodMapping::new([(1, 2), (3, 4), (5, 6), ...])
    pub const fn linear_mapping() -> Self {
        if 2 * N + 1 != K {
            panic!("2*N+1 must equal K to use a default linear mapping");
        }
        let mut legs = [(0, 0); N];
        let mut n = 0;
        while n < N {
            legs[n] = (2 * n + 1, 2 * n + 2);
            n += 1;
        }
        Self::new(legs)
    }

    pub const fn get_leg_index(&self, leg_num: usize, segment: &LegSegment) -> JointIndex {
        if leg_num >= N {
            panic!("leg_num must be less than N");
        }
        let (upper, lower) = self.legs[leg_num];
        match segment {
            LegSegment::Upper => upper,
            LegSegment::Lower => lower,
        }
    }
}

impl<const N: usize, const K: usize> Default for NPodMapping<N, K> {
    fn default() -> Self {
        Self::linear_mapping()
    }
}

impl<const K: usize> KChain<K> {
    /// Initializes a KChain with blank joints, adds a root, and then
    /// sets the `transform.translation` and `parent_index` of the leg joints appropriately based on the `params` and `mapping`
    pub const fn init_with_npod<const N: usize>(
        params: &NPodParams<N>,
        mapping: &NPodMapping<N, K>,
    ) -> Self {
        let joints = {
            // Initialize with "root" as starter value everywhere
            let mut data: [Joint; K] = [Joint::blank(); K];
            data[0] = Joint::root();
            let mut n = 0;
            while n < N {
                let conf = &params.legs_config[n];
                let ui = mapping.get_leg_index(n, &LegSegment::Upper);
                let li = mapping.get_leg_index(n, &LegSegment::Lower);
                data[ui].parent_index = 0;
                data[li].parent_index = ui;

                data[ui].transform.translation = conf.base_pos;

                //normal vector add isn't a const fn, need to add manually
                data[li].transform.translation = conf.upper_seg_vec;

                n += 1;
            }
            data
        };

        KChain { joints }
    }
}

impl<const N: usize> Interpolate for NPodState<N> {
    fn interpolate(&self, other: &Self, amount: f32) -> Self {
        // Can't collect into arrays yet, so let's start with all zeros, then replace with real data.
        let mut legs = [F32Coord {
            x: 0.,
            y: 0.,
            z: 0.,
        }; N];
        for i in 0..N {
            legs[i] = self.feet[i].interpolate(&other.feet[i], amount);
        }
        NPodState {
            body: self.body.interpolate(&other.body, amount),
            feet: legs,
            ease_type: None,
        }
    }
}

impl<const N: usize> Ease for NPodState<N> {
    fn get_ease_type(&self) -> EaseType {
        self.ease_type.unwrap_or(EaseType::Continue)
    }
}

impl LegConfig {
    pub fn max_reach(&self) -> f32 {
        self.base_pos.magnitude() + self.upper_seg_vec.magnitude() + self.lower_seg_vec.magnitude()
    }
}

impl<const N: usize> NPodParams<N> {
    pub fn recommended_max_reach(&self) -> f32 {
        // find the `rmr` (recommended max reach)
        // If each leg is placed `rmr` distance away from the previous, then there will be at most `(N-1)*rmr` distance between the furthest legs.
        // So, `(N-1)*rmr` should be <= `2*min_leg_length`, so that we know the legs can span this distance
        // So `rmr = 2*min_leg_len / (N-1)`

        let min_leg_len = self
            .legs_config
            .iter()
            .map(|lc| FloatOrd(lc.max_reach()))
            .min()
            .unwrap()
            .0;

        // 2. * min_leg_len / (N - 1) as f32
        min_leg_len
    }
}

#[derive(thiserror::Error, Debug)]
pub enum NPodInterpolatePoseErr {
    #[error("No keyframes have been added yet for this animation")]
    NoKeyframe,
    #[error("The leg positions specified by the NPodState are unreachable, and are either too far, or directly in the `leg_bend_dir_norm` direction")]
    LegPosUnreachable,
}
