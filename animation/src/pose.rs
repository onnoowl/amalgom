use cgmath::{One, Quaternion, Vector3};

pub struct Pose<const K: usize> {
    pub joint_rotations: [Quaternion<f32>; K],
    pub root_translation: Vector3<f32>,
    pub root_scale: f32,
}

impl<const K: usize> Default for Pose<K> {
    fn default() -> Self {
        Pose {
            joint_rotations: [Quaternion::one(); K],
            root_translation: Vector3 {
                x: 0.,
                y: 0.,
                z: 0.,
            },
            root_scale: 1.,
        }
    }
}
