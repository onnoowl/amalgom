use crate::interpolate::{Ease, Interpolate};

use std::time::Instant;

#[derive(Debug, Clone, PartialEq)]
pub struct Keyframe<S> {
    pub state: S,
    pub time: Instant,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Keyframes<S> {
    None,
    One(Keyframe<S>),
    Two {
        next: Keyframe<S>,
        last: Keyframe<S>,
    },
}

impl<S> Keyframes<S> {
    /// Keyframes only holds two keyframes at a time, the two that its actively interpolating between
    /// Use `needs_next` to see if another keyframe needs to be pushed.
    pub fn push(&mut self, frame: Keyframe<S>) {
        use Keyframes::*;

        *self = match self.take() {
            None => One(frame),
            One(last) => Two { next: frame, last },
            Two {
                next: last,
                last: _,
            } => Two { next: frame, last },
        }
    }

    /// Returns true if the elapsed time is ahead of the `next` keyframe, meaning we need the next keyframe.
    /// Also returns true if we don't have two keyframes yet.
    pub fn needs_next(&self, cur_timestamp: Instant) -> bool {
        match self {
            Keyframes::None => true,
            Keyframes::One(_) => true,
            Keyframes::Two { next, last: _ } => cur_timestamp >= next.time,
        }
    }

    fn take(&mut self) -> Self {
        let mut val = Keyframes::None;
        std::mem::swap(&mut val, self);
        val
    }
}

impl<S: Interpolate> Keyframes<S> {
    /// Interpolate the state between keyframes given a timestamp
    pub fn interpolate_state(&self, cur_timestamp: Instant) -> Option<S> {
        match self {
            Keyframes::None => None,
            Keyframes::One(kf) => Some(kf.state.clone()),
            Keyframes::Two { next, last } => {
                let elapsed = cur_timestamp.saturating_duration_since(last.time);
                let amount = elapsed.div_duration_f32(next.time - last.time).min(1.);
                Some(last.state.interpolate(&next.state, amount as f32))
            }
        }
    }
}

impl<S: Ease> Keyframes<S> {
    /// Ease the state between keyframes given a timestamp
    pub fn ease_state(&self, cur_timestamp: Instant) -> Option<S> {
        match self {
            Keyframes::None => None,
            Keyframes::One(kf) => Some(kf.state.clone()),
            Keyframes::Two { next, last } => {
                let elapsed = cur_timestamp.saturating_duration_since(last.time);
                let amount = elapsed.div_duration_f32(next.time - last.time).min(1.);
                Some(last.state.ease(&next.state, amount as f32))
            }
        }
    }
}

impl<S: Interpolate> Default for Keyframes<S> {
    fn default() -> Self {
        Self::None
    }
}
