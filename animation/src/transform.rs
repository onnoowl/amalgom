use crate::interpolate::Interpolate;

use cgmath::{
    InnerSpace, Matrix3, Matrix4, One, Quaternion, Rotation, SquareMatrix, Vector3, VectorSpace,
    Zero,
};

pub trait Transform: Copy + Interpolate {
    fn identity() -> Self;
    fn concat(self, other: Self) -> Self;
    fn inverse(self) -> Self;
    fn transform_vec(self, v: Vector3<f32>) -> Vector3<f32>;
    fn to_matrix(self) -> Matrix4<f32>;
    fn from_matrix(m: Matrix4<f32>) -> Self;
    fn set_rotation(&mut self, rotation: Quaternion<f32>);
    fn get_rotation(self) -> Quaternion<f32>;
    fn set_translation(&mut self, translation: Vector3<f32>);
    fn get_translation(self) -> Vector3<f32>;
}

/// Transformation represented by separate scaling, translation, and rotation factors.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct QVTransform {
    /// Translation
    pub translation: Vector3<f32>,

    /// Uniform scale factor.
    pub scale: f32,

    /// Rotation
    pub rotation: Quaternion<f32>,
}

impl Transform for QVTransform {
    fn identity() -> QVTransform {
        QVTransform {
            translation: Vector3::zero(),
            scale: 1.0,
            rotation: Quaternion::one(),
        }
    }

    fn set_rotation(&mut self, rotation: Quaternion<f32>) {
        self.rotation = rotation;
    }

    fn get_rotation(self) -> Quaternion<f32> {
        self.rotation
    }

    fn set_translation(&mut self, translation: Vector3<f32>) {
        self.translation = translation;
    }

    fn get_translation(self) -> Vector3<f32> {
        self.translation
    }

    fn concat(self, other: QVTransform) -> QVTransform {
        QVTransform {
            rotation: self.rotation * other.rotation,
            scale: self.scale * other.scale,
            translation: self.translation + other.translation,
        }
    }

    fn inverse(self) -> QVTransform {
        QVTransform {
            rotation: self.rotation.invert(),
            scale: self.scale,
            translation: self.translation,
        }
    }

    // TODO: Piston does rot, trans, then scale. Should we?
    fn transform_vec(self, v: Vector3<f32>) -> Vector3<f32> {
        let v = v * self.scale;
        let v = &self.rotation.rotate_vector(v);
        v + self.translation
    }

    fn to_matrix(self) -> Matrix4<f32> {
        let mut m: Matrix4<f32> = self.rotation.into();

        m[3][0] = self.translation[0];
        m[3][1] = self.translation[1];
        m[3][2] = self.translation[2];

        m * Matrix4::<f32>::from_scale(self.scale)
    }

    /// TODO: Caution, this does not currently honor `scale`
    fn from_matrix(_m: Matrix4<f32>) -> QVTransform {
        todo!();

        // TODO: I think row major / col major on this is swapped

        // // matrix_to_quaternion(&m);
        // let rotation = Matrix3::from_cols(
        //     Vector3::new(m.x[0], m.x[1], m.x[2]),
        //     Vector3::new(m.y[0], m.y[1], m.y[2]),
        //     Vector3::new(m.z[0], m.z[1], m.z[2]),
        // )
        // .into();

        // let translation = [m[0][3], m[1][3], m[2][3]];

        // QVTransform {
        //     rotation: rotation,
        //     scale: 1.0,
        //     translation: translation.into(),
        // }
    }
}

impl Interpolate for QVTransform {
    fn interpolate(&self, other: &QVTransform, amount: f32) -> QVTransform {
        QVTransform {
            translation: (&self.translation).lerp(other.translation, amount),
            scale: interpolation::lerp(&self.scale, &other.scale, &amount),
            rotation: self.rotation.nlerp(other.rotation, amount).normalize(),
        }
    }
}

impl QVTransform {
    pub const fn const_identity() -> Self {
        Self {
            translation: cgmath::vec3(0., 0., 0.),
            scale: 1.,
            rotation: Quaternion::from_sv(1., cgmath::vec3(0., 0., 0.)),
        }
    }

    pub fn from_translation(translation: Vector3<f32>) -> Self {
        QVTransform {
            translation,
            scale: 1.0,
            rotation: Quaternion::one(),
        }
    }
}

impl Transform for Matrix4<f32> {
    fn identity() -> Matrix4<f32> {
        Matrix4::one()
    }

    fn set_rotation(&mut self, _rotation: Quaternion<f32>) {
        todo!();

        // TODO: I think row major / col major on this is swapped

        // let rotation: Matrix3<f32> = rotation.into();

        // self[0][0] = rotation[0][0];
        // self[1][0] = rotation[1][0];
        // self[2][0] = rotation[2][0];

        // self[0][1] = rotation[0][1];
        // self[1][1] = rotation[1][1];
        // self[2][1] = rotation[2][1];

        // self[0][2] = rotation[0][2];
        // self[1][2] = rotation[1][2];
        // self[2][2] = rotation[2][2];
    }

    fn get_rotation(self) -> Quaternion<f32> {
        Matrix3::from_cols(
            Vector3::new(self.x[0], self.x[1], self.x[2]),
            Vector3::new(self.y[0], self.y[1], self.y[2]),
            Vector3::new(self.z[0], self.z[1], self.z[2]),
        )
        .into()
    }

    fn set_translation(&mut self, translation: Vector3<f32>) {
        self[3][0] = translation[0];
        self[3][1] = translation[1];
        self[3][2] = translation[2];
    }

    fn get_translation(self) -> Vector3<f32> {
        [self[3][0], self[3][1], self[3][2]].into()
    }

    fn concat(self, other: Matrix4<f32>) -> Matrix4<f32> {
        self * other
    }

    fn inverse(self) -> Matrix4<f32> {
        self.invert().unwrap()
    }

    // fn lerp(self, other: Matrix4<f32>, parameter: f32) -> Matrix4<f32> {
    //     VectorSpace::lerp(self, other, parameter)
    // }

    fn transform_vec(self, v: Vector3<f32>) -> Vector3<f32> {
        (self * v.extend(1.)).truncate()
    }

    fn to_matrix(self) -> Matrix4<f32> {
        self
    }

    fn from_matrix(m: Matrix4<f32>) -> Matrix4<f32> {
        m
    }
}

pub trait FromTransform<T: Transform> {
    fn from_transform(t: T) -> Self;
}

impl<T: Transform> FromTransform<T> for Matrix4<f32> {
    fn from_transform(t: T) -> Matrix4<f32> {
        t.to_matrix()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use cgmath::*;

    #[test]
    fn from_axis_angle() {
        let p1 = vec3(1., 0., 0.);
        let quat = Quaternion::from_axis_angle(vec3(0., 1., 0.), Rad(std::f32::consts::FRAC_PI_2))
            .normalize();
        let p2 = quat.rotate_vector(p1);

        assert_ulps_eq!(p2, vec3(0., 0., -1.));
    }

    #[test]
    fn from_arc() {
        let p1 = vec3(1., 0., 0.);
        let quat = Quaternion::from_arc(vec3(0., 0., 1.), vec3(1., 0., 0.), None).normalize();
        let p2 = quat.rotate_vector(p1);

        assert_ulps_eq!(p2, vec3(0., 0., -1.));
    }

    #[test]
    fn matrix_translate() {
        let trans1 = QVTransform {
            translation: vec3(4., 5., 6.),
            scale: 1.,
            rotation: Quaternion::one(),
        };
        let mat1 = trans1.to_matrix();
        let mat2 = Matrix4::<f32>::from_translation(vec3(4., 5., 6.));
        assert_ulps_eq!(mat1, mat2);

        let p1 = vec3(1., 2., 3.);
        let p2 = mat1.transform_vec(p1);

        assert_ulps_eq!(p2, vec3(5., 7., 9.));
    }
}
