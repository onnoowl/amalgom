//! # Dependencies
//! [octree](octree) - Houses the logic for the generic octree data structure
//!
//! [base_utils](base_utils) - Utils that don't require other crates from this project as dependencies
#![feature(concat_idents)]
#![feature(new_uninit)]

pub mod block_highlight;
pub mod camera;
pub mod events;
pub mod input;
pub mod intersection;
pub mod plugins;
pub mod prelude;
pub mod terrain_mesh;
pub mod thread_comm;
pub mod thread_pool;
pub use logic_tree as tree;

use base_utils::LOD_MULT;
use cgmath::{self, Point3};
use collision::Contains;

pub fn should_lod_recurse(perspective: &Point3<f32>, bb: &collision::Aabb3<f32>) -> bool {
    let scaled = intersection::scale_bb_by(&bb, LOD_MULT);
    scaled.contains(perspective)
}
