//! Defines communication structure between render thread and logic thread

use crate::{
    block_highlight::BlockHighlight, block_highlight::BlockHighlightMsg, camera::Camera,
    input::InputEvent, terrain_mesh::TerrainMesh,
};
use animation::{Keyframe, NPodState};
use base_utils::{
    rwcube::{RwCubeReader, RwCubeWriter},
    F32Coord, Loc, COLLISION_CAGE_SIZE,
};

use cgmath::Point3;
use crossbeam::atomic::AtomicCell;
use std::{
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc,
    },
    time::Instant,
};

pub fn new(startup_info: StartupInfo) -> (RenderComm, LogicComm) {
    let (terrain_bufs_sender, terrain_bufs_recv) = channel();
    let (hexapod_sender, hexapod_recv) = channel();
    let (input_event_sender, input_event_recv) = channel();
    let (renderer_state_send, renderer_state_recv) = channel();

    let camera_cell: Arc<AtomicCell<_>> = Default::default();
    let cursor_highlight_cell: Arc<AtomicCell<_>> = Default::default();
    let (other_highlight_send, other_highlight_recv) = channel();

    let (cc_writer, cc_reader) = base_utils::rwcube::new_rwcube();

    let (interaction_events_sender, interaction_event_receiver) = channel();

    (
        RenderComm {
            startup_info: startup_info.clone(),
            terrain_bufs_recv: Some(terrain_bufs_recv),
            hexapod_recv: Some(hexapod_recv),
            input_event_sender,
            camera_cell: camera_cell.clone(),
            cursor_highlight_cell: cursor_highlight_cell.clone(),
            other_block_highlights: Some(other_highlight_recv),
            renderer_state: renderer_state_send,
            collision_cage: Some(cc_reader),
            interaction_events: Some(interaction_event_receiver),
        },
        LogicComm {
            startup_info,
            terrain_bufs_sender,
            hexapod_sender,
            input_event_recv,
            camera_cell,
            cursor_highlight_cell: cursor_highlight_cell,
            other_block_highlights: other_highlight_send,
            renderer_state: renderer_state_recv,
            collision_cage: cc_writer,
            interaction_events: interaction_events_sender,
        },
    )
}

pub struct LogicComm {
    pub startup_info: StartupInfo,

    pub terrain_bufs_sender: Sender<TerrainMeshMsg>,

    pub hexapod_sender: Sender<NPodMsg<6>>,

    /// Communicates key presses, mouse button presses, and mouse scrolls from the render thread to here
    pub input_event_recv: Receiver<InputEvent>,

    /// Communicates the current camera from the render thread to here
    /// Option is some when a change is made, None otherwise.
    pub camera_cell: Arc<AtomicCell<Option<Camera>>>,

    pub cursor_highlight_cell: Arc<AtomicCell<Option<BlockHighlight>>>,
    pub other_block_highlights: Sender<BlockHighlightMsg>,

    pub renderer_state: Receiver<RenderStateNotification>,

    pub collision_cage: RwCubeWriter<bool, COLLISION_CAGE_SIZE>,

    pub interaction_events: Sender<InteractionEvent>,
}

pub struct RenderComm {
    pub startup_info: StartupInfo,

    pub terrain_bufs_recv: Option<Receiver<TerrainMeshMsg>>,

    pub hexapod_recv: Option<Receiver<NPodMsg<6>>>,

    pub input_event_sender: Sender<InputEvent>,

    pub camera_cell: Arc<AtomicCell<Option<Camera>>>,

    pub cursor_highlight_cell: Arc<AtomicCell<Option<BlockHighlight>>>,
    pub other_block_highlights: Option<Receiver<BlockHighlightMsg>>,

    pub renderer_state: Sender<RenderStateNotification>,

    pub collision_cage: Option<RwCubeReader<bool, COLLISION_CAGE_SIZE>>,

    pub interaction_events: Option<Receiver<InteractionEvent>>,
}

#[derive(Debug, Clone)]
pub struct StartupInfo {
    pub player_spawn: Point3<f32>,
    pub buffer_depth_and_min_level: u32,
}

impl Default for StartupInfo {
    fn default() -> Self {
        Self {
            player_spawn: Point3::new(0., 35., 0.),
            buffer_depth_and_min_level: 5,
        }
    }
}

pub struct TerrainMeshMsg {
    pub buf: Option<TerrainMesh>,
    pub loc: Loc,
}

#[derive(Debug, PartialEq, Clone)]
pub enum RenderStateNotification {
    Started,
    Stopping,
}

#[derive(Debug, PartialEq, Clone)]
pub enum NPodMsg<const N: usize> {
    Spawn(NPodID),
    Despawn(NPodID),
    SendKeyFrame(NPodID, Keyframe<NPodState<N>>),
}

#[derive(Debug, PartialEq, Clone, Hash, Eq, Ord, PartialOrd, Copy)]
pub struct NPodID(pub usize);

pub enum InteractionEvent {
    /// Occurs when a player clicks with their grappling hook
    Grapple(GrappleEvent),
}

pub struct GrappleEvent {
    pub target: F32Coord,

    pub event_time: Instant,
}
