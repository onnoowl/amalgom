//! Legion wrappers for defining events and listeners

pub mod client;
pub mod queue;
pub mod server;

use legion::borrow::{AtomicRefCell, Ref};
use legion::prelude::*;
use std::{fmt::Debug, sync::Arc};

/// An Event. Structs following this trait should hold relevant Event data.
pub trait Event: Clone + Debug {
    type EventListenerSet: EventListenerSet;
    type ListenerManager: ListenerManager;
}

/// A simple wrapper around a ListenerSet that knows what Event type it's associated with.
/// It also knows how to fetch itself from a ListenerManager.
pub trait EventListenerSet {
    type Event: Event;
    type ListenerManager: ListenerManager;

    fn get_from_manager(man: &mut Self::ListenerManager) -> &mut Self;
    fn process_event(
        &mut self,
        world: &mut legion::world::World,
        resources: &mut legion::prelude::Resources,
        e: Self::Event,
    );
}

/// A cell type that can be handed out to event handlers. It will be updated by the ListenerManager
/// to point toward the actual event data before the event handler's system is called.
pub struct EventCell<E: Event> {
    inner: Arc<AtomicRefCell<Option<E>>>,
}

impl<E: Event> Clone for EventCell<E> {
    fn clone(&self) -> Self {
        EventCell {
            inner: self.inner.clone(),
        }
    }
}

impl<E: Event> EventCell<E> {
    fn new() -> Self {
        EventCell {
            inner: Arc::new(AtomicRefCell::new(None)),
        }
    }

    pub fn get(&self) -> Ref<'_, E> {
        self.inner
            .try_get()
            .expect("Runtime borrow error occured trying to get EventData from EventCell")
            .map(|e| e.as_ref().expect("EventData failed to get initialized"))
    }

    fn set(&self, event: E) {
        self.inner
            .try_get_mut()
            .expect("Runtime borrow error occured trying to get_mut EventData from EventCell")
            .replace(event);
    }
}

#[doc(hidden)]
#[macro_export]
// Helper macro for defining events, since it's fairly verbose and repetitive.
macro_rules! define_events {
    ($enum_name:ident $manager_name:ident $({$(#[$doc:meta])* $struct_or_enum:ident $event_type:ident $type_defn:tt, $on_name:tt}),*) => {
        paste::item! {
            $(
                pub struct [<$event_type ListenerSet>] {
                    listeners: $crate::events::ListenerSet,
                    event_cell: $crate::events::EventCell<$event_type>,
                }

                impl [<$event_type ListenerSet>] {

                    fn new(name: &'static str) -> Self {
                        [<$event_type ListenerSet>] {
                            listeners: $crate::events::ListenerSet::new(name),
                            event_cell: $crate::events::EventCell::<$event_type>::new()
                        }
                    }

                    pub fn add_listener<
                        // T: 'static,  //might be useful for debugging (via legion SystemId), but adds unnecessary complexity to API
                        F: FnOnce(legion::prelude::SystemBuilder, $crate::events::EventCell<$event_type>) -> Box<dyn legion::prelude::Schedulable>,
                    >(
                        &mut self,
                        f: F,
                    ) {
                        let event_cell = self.event_cell.clone();
                        let builder = legion::prelude::SystemBuilder::<()>::new(
                            legion_systems::SystemId::of::<()>(Some(self.listeners.name.to_string()))
                        );
                        self.listeners.add_listener_system(f(builder, event_cell));
                    }
                }

                impl $crate::events::EventListenerSet for [<$event_type ListenerSet>] {
                    type Event = $event_type;
                    type ListenerManager = $manager_name;

                    fn get_from_manager(man: &mut $manager_name) -> &mut Self {
                        &mut man. $on_name
                    }

                    fn process_event(&mut self, world: &mut legion::world::World, resources: &mut legion::prelude::Resources, e: Self::Event) {
                        self.event_cell.set(e);
                        self.listeners.get_executor().run_systems(world, resources)
                    }
                }

                $(#[$doc])*
                #[derive(Debug, Clone)]
                pub $struct_or_enum $event_type $type_defn

                impl $crate::events::Event for $event_type {
                    type EventListenerSet = [<$event_type ListenerSet>];
                    type ListenerManager = $manager_name;
                }

            )*

            pub struct $manager_name {
                $(
                    pub $on_name: [<$event_type ListenerSet>],
                )*
            }

            impl $crate::events::ListenerManager for $manager_name {
                fn new() -> Self {
                    $manager_name {
                        $(
                            $on_name: [<$event_type ListenerSet>]::new(stringify!($on_name)),
                        )*
                    }
                }
            }

            pub enum $enum_name {
                $(
                    $event_type($event_type),
                )*
            }
        }
    };
}

/// Contains a set of "listeners", which are actually `legion` systems (which implement the `Scheduable` trait).
/// When the set of listeners is called, an `Executor` is made, which finds the best way to
/// run the systems/listeners in parallel.
pub struct ListenerSet {
    name: &'static str,
    building_vec: Option<Vec<Box<dyn Schedulable>>>, // only one of these will
    built_executor: Option<Executor>,                // ever be `Some` at a time
}

impl ListenerSet {
    pub fn new(name: &'static str) -> Self {
        ListenerSet {
            name,
            building_vec: None,
            built_executor: None,
        }
    }

    /// Adds a listener. This should be a `legion::system::System`, which can be created using `legion::system::SystemBuilder`.
    fn add_listener_system(&mut self, system: Box<dyn Schedulable>) {
        let mut listeners = self
            .built_executor
            .take()
            .map(|e| e.into_vec())
            .unwrap_or_else(|| self.building_vec.take().unwrap_or_else(Vec::new));
        listeners.push(system);
        self.building_vec.replace(listeners);
    }

    /// Gets (or builds and caches) an `Executor` for this set of ListenerSet.
    /// Executors automatically identify parallelism opportunities.
    /// Executors are a list of Systems that have been grouped and analyzed how best to run simultaneously.
    pub fn get_executor(&mut self) -> &mut Executor {
        let vec = self.building_vec.take().unwrap_or_else(Vec::new);
        self.built_executor
            .get_or_insert_with(|| Executor::new(vec))
    }
}

/// A `ListenerManager` holds `EventListenerSet`s for every possible type of event.
pub trait ListenerManager {
    fn new() -> Self;
    // fn set_registering_plugin_name(&mut self, name: String);
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::atomic::{AtomicBool, Ordering};
    use std::sync::Arc;

    #[test]
    fn basic_event() {
        env_logger::from_env(env_logger::Env::default().default_filter_or("debug")).init();

        let it_ran = Arc::new(AtomicBool::new(false));
        let it_ran_clone = it_ran.clone();

        let mut m = client::ClientListenerManager::new();
        m.on_keypress.add_listener(|builder, event| {
            builder.build(move |_commands, _world, _resource, _queries| {
                log::info!("{:?}", &*event.get());

                it_ran_clone.store(true, Ordering::SeqCst);
            })
        });

        let u = Universe::new();
        let mut world = u.create_world();
        let mut resources = Resources::default();

        m.on_keypress
            .process_event(&mut world, &mut resources, client::KeypressEvent { key: 0 });
        assert!(it_ran.load(Ordering::SeqCst));
    }
}
