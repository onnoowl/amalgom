//! Struct and helper functions for data pertaining to the player's perspective, or camera

use cgmath::{InnerSpace, Matrix4, Point3, Rotation, Rotation3, Vector3};
use collision::Frustum;

#[derive(Debug, Clone)]
pub struct Camera {
    /// The core controlling values of the camera
    pub controller: CameraController,

    /// Derived vectors orienting the camera
    pub cached_vectors: CameraVectors,

    /// Derived matrices (view, proj, frustum)
    pub cached_matrices: CameraMatrices,
}

#[derive(Debug, Clone, PartialEq)]
/// These are the core values that define and "control" the camera.
/// Everything else in `Camera` is derived from these "controlling" values
pub struct CameraController {
    pub eye: Point3<f32>,

    pub yaw: f32,
    pub pitch: f32,

    pub aspect: f32,
    pub fovy: f32,
    pub znear: f32,
    pub zfar: f32,
}

#[derive(Debug, Clone)]
pub struct CameraVectors {
    pub up: Vector3<f32>,
    pub target: Point3<f32>,
    pub look_dir: Vector3<f32>,
    pub forward_xz: Vector3<f32>,
    pub right_xz: Vector3<f32>,
}

impl CameraVectors {
    pub fn new(control: &CameraController) -> Self {
        const UNIT_VEC: Vector3<f32> = Vector3::<f32>::new(0., 0., 1.);

        let yaw_rot = cgmath::Basis3::<f32>::from_angle_y(cgmath::Rad(control.yaw));
        let pitch_rot = cgmath::Basis3::<f32>::from_angle_x(cgmath::Rad(control.pitch));

        let look_dir = yaw_rot.rotate_vector(pitch_rot.rotate_vector(UNIT_VEC));

        let up = Vector3::unit_y();
        let forward_xz = Vector3::new(look_dir.x, 0., look_dir.z).normalize();

        Self {
            up,
            target: control.eye + look_dir,
            look_dir,
            forward_xz,
            right_xz: forward_xz.cross(up),
        }
    }
}

#[derive(Debug, Clone)]
pub struct CameraMatrices {
    pub view: Matrix4<f32>,
    pub projection: Matrix4<f32>,
    pub view_proj: Matrix4<f32>,

    /// Technically not a matrix I suppose
    pub frustum: Frustum<f32>,
}

impl CameraMatrices {
    pub fn new(control: &CameraController, vectors: &CameraVectors) -> Self {
        let view = cgmath::Matrix4::look_at(control.eye, vectors.target, vectors.up);
        let projection = OPENGL_TO_WGPU_MATRIX
            * cgmath::perspective(
                cgmath::Deg(control.fovy),
                control.aspect,
                control.znear,
                control.zfar,
            );
        let view_proj = projection * view;
        let frustum = collision::Frustum::from_matrix4(view_proj).unwrap();

        CameraMatrices {
            view,
            projection,
            view_proj,
            frustum,
        }
    }
}

impl Camera {
    pub fn new(pos: &Point3<f32>, aspect: f32) -> Self {
        Self::new_with_target(pos, &(pos.x, pos.y, pos.z + 1.).into(), aspect)
    }

    pub fn new_with_target(pos: &Point3<f32>, target: &Point3<f32>, aspect: f32) -> Self {
        let look_dir = (target - pos).normalize();
        let pitch = (-look_dir.y).asin();
        let yaw = look_dir.x.atan2(look_dir.z);

        let fovy: f32 = 60.0;
        let znear: f32 = 0.1;
        let zfar: f32 = 2.0_f32.powi(14);

        let cam_control = CameraController {
            eye: *pos,
            aspect,
            yaw,
            pitch,
            fovy,
            znear,
            zfar,
        };
        let cached_vectors = CameraVectors::new(&cam_control);
        let cached_matrices = CameraMatrices::new(&cam_control, &cached_vectors);

        Self {
            controller: cam_control,
            cached_vectors,
            cached_matrices,
        }
    }

    pub fn update_cached_vectors(&mut self) {
        self.cached_vectors = CameraVectors::new(&self.controller);
    }

    pub fn update_cached_matrices(&mut self) {
        self.cached_matrices = CameraMatrices::new(&self.controller, &self.cached_vectors);
    }
}

#[rustfmt::skip]
#[allow(unused)]
pub const OPENGL_TO_WGPU_MATRIX: cgmath::Matrix4<f32> = cgmath::Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

/// A simplified read-only camera information struct that can be made from a normal camera
pub struct CameraInfo {
    pub eye: Point3<f32>,
    pub up: Vector3<f32>,
    pub target: Point3<f32>,
    pub forward_xz: Vector3<f32>,
    pub right_xz: Vector3<f32>,
    pub frustum: Frustum<f32>,
}

impl From<Camera> for CameraInfo {
    fn from(c: Camera) -> Self {
        Self {
            eye: c.controller.eye,
            up: c.cached_vectors.up,
            target: c.cached_vectors.target,
            forward_xz: c.cached_vectors.forward_xz,
            right_xz: c.cached_vectors.right_xz,
            frustum: c.cached_matrices.frustum,
        }
    }
}
