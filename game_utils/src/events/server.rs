use crate::define_events;

define_events! {
    ServerEvent
    ServerListenerManager
    {
        /// Not yet implemented
        /// Triggered when a player joins
        struct PlayerJoinEvent {
            pub player_name: String,
        },
        on_player_join
    }
}
