use crate::events::{client::ClientEvent, server::ServerEvent};

use std::sync::mpsc::{self, Receiver, Sender};

pub struct EventQueue {
    client_events_recv: Receiver<ClientEvent>,
    server_events_recv: Receiver<ServerEvent>,
    client_events_send: Sender<ClientEvent>,
    server_events_send: Sender<ServerEvent>,
}

impl Default for EventQueue {
    fn default() -> Self {
        let (client_events_send, client_events_recv) = mpsc::channel::<ClientEvent>();
        let (server_events_send, server_events_recv) = mpsc::channel::<ServerEvent>();
        EventQueue {
            client_events_recv,
            server_events_recv,
            client_events_send,
            server_events_send,
        }
    }
}

impl EventQueue {
    pub fn client_events_recv(&mut self) -> &mut Receiver<ClientEvent> {
        &mut self.client_events_recv
    }
    pub fn server_events_recv(&mut self) -> &mut Receiver<ServerEvent> {
        &mut self.server_events_recv
    }
    pub fn client_events_send(&self) -> Sender<ClientEvent> {
        self.client_events_send.clone()
    }
    pub fn server_events_send(&self) -> Sender<ServerEvent> {
        self.server_events_send.clone()
    }
}
