use crate::define_events;

define_events! {
    ClientEvent
    ClientListenerManager
    {
        /// Triggered when a keyboard key is pressed.
        struct KeypressEvent {
            pub key: u32,
        },
        on_keypress
    },
    {
        /// Not yet implemented
        struct ClickEvent {
            pub x: u32,
            pub y: u32
        },
        on_click
    }
}
