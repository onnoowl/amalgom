//! Collision logic using bounding boxes and octrees
use octree::{traits::IsAir, OctNode, Octree};

use base_utils::{p_to_v, v_to_p, F32Coord, ICoord, OCTAL_OFFSETS};
use cgmath::{InnerSpace, Point3, Vector3};
use collision::{prelude::*, Aabb3, Ray};
use num_traits::cast::FromPrimitive;
use std::fmt::Debug;

pub fn get_bb<S: cgmath::BaseNum + FromPrimitive>(
    pos: &Vector3<S>,
    this_nodes_level: u32,
) -> Aabb3<S> {
    let size = S::from_u32(2u32.pow(this_nodes_level)).unwrap();
    Aabb3::new(
        v_to_p(pos),
        v_to_p(&(pos + Vector3::<S>::new(size, size, size))),
    )
}

/// Expands the bb by epsilon when creating it. Equivalent to get_bb then using expand_bb_by.
pub fn get_bb_with_epsilon<S: cgmath::BaseNum + FromPrimitive>(
    pos: &Vector3<S>,
    this_nodes_level: u32,
    epislon: S,
) -> Aabb3<S> {
    let size = S::from_u32(2u32.pow(this_nodes_level)).unwrap();
    let epislon_v = Vector3::<S>::new(epislon, epislon, epislon);
    Aabb3::new(
        v_to_p(pos) - epislon_v,
        v_to_p(&(pos + Vector3::<S>::new(size, size, size))) + epislon_v,
    )
}

pub fn expand_bb_by<S: cgmath::BaseNum>(bb: &Aabb3<S>, by: S) -> Aabb3<S> {
    let by_v = Vector3::<S>::new(by, by, by);
    Aabb3::new(bb.min - by_v, bb.max + by_v)
}

pub fn get_bb_mid<S: cgmath::BaseNum + FromPrimitive>(bb: &Aabb3<S>) -> Point3<S> {
    (bb.min + p_to_v(&bb.max)) / S::from_u32(2).unwrap()
}

pub fn get_bb_top_mid<S: cgmath::BaseNum + FromPrimitive>(bb: &Aabb3<S>) -> Point3<S> {
    let mut mid = (bb.min + p_to_v(&bb.max)) / S::from_u32(2).unwrap();
    mid.y = bb.max.y; // set the middle point to the max height of the bb. aka on the top
    return mid;
}

pub fn scale_bb_by<S: cgmath::BaseNum + FromPrimitive>(bb: &Aabb3<S>, by: S) -> Aabb3<S> {
    let mid = get_bb_mid(bb);
    let mut radius = mid - bb.min;
    radius *= by;
    Aabb3::new(mid - radius, mid + radius)
}

pub fn exponentiate_bb_by(bb: &Aabb3<i64>, by: u32) -> Aabb3<i64> {
    let mid = get_bb_mid(bb);
    let mut radius_scalar = mid.x - bb.min.x;
    radius_scalar = radius_scalar.pow(by);

    let radius = Vector3::new(radius_scalar, radius_scalar, radius_scalar);
    Aabb3::new(mid - radius, mid + radius)
}

#[derive(Debug)]
pub struct RayIntersectResult<'a, L: Debug, B: Debug, U: Debug + Default> {
    pub pos: ICoord,
    pub level: u8,
    pub node: &'a OctNode<L, B, U>,
}

pub fn ray_node_intersection<'a, L: Debug + IsAir, B: Debug, U: Debug + Default>(
    origin: &Point3<f32>,
    dir: &F32Coord,
    node: &'a OctNode<L, B, U>,
    node_pos: &ICoord,
    node_level: u32,
    target_level: u32,
    max_dist: Option<f32>,
) -> Option<RayIntersectResult<'a, L, B, U>> {
    let ray = Ray::new(*origin, dir.normalize());
    let bb = get_bb(&node_pos.cast::<f32>().unwrap(), node_level);
    let inter = ray.intersection(&bb);
    let contains = bb.contains(origin);
    // check if we intersected with this node's bounding box or if the
    // bounding box contains the origin
    if inter.is_some() || contains {
        if !contains {
            if let Some(dist) = max_dist {
                match inter {
                    Some(point) => {
                        let p_to_o = (point - origin).magnitude();
                        if p_to_o > dist {
                            return None;
                        }
                    }
                    None => (),
                }
            }
        }

        match node {
            OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => {
                if l.is_air() {
                    None
                } else {
                    Some(RayIntersectResult {
                        pos: *node_pos,
                        level: node_level as u8,
                        node,
                    })
                }
            }
            OctNode::Branch(_, children) => {
                let hsize = 2_u32.pow(node_level - 1) as i64;
                let origin_to_node_center = (origin.cast::<i64>().unwrap() * -1
                    + node_pos
                    + ICoord {
                        x: hsize,
                        y: hsize,
                        z: hsize,
                    })
                .cast::<f32>()
                .unwrap();

                let sx = if origin_to_node_center.x > 0. { 1 } else { 0 };
                let sy = if origin_to_node_center.y > 0. { 1 } else { 0 };
                let sz = if origin_to_node_center.z > 0. { 1 } else { 0 };

                for flip_bits in (0..=7u8).rev() {
                    let index = ((sx | (sy << 1) | (sz << 2)) ^ flip_bits) as usize;
                    let child = &children[index];
                    let result = ray_node_intersection(
                        origin,
                        dir,
                        child,
                        &(node_pos + OCTAL_OFFSETS[index] * hsize),
                        node_level - 1,
                        target_level,
                        max_dist,
                    );
                    if result.is_some() {
                        if target_level == node_level {
                            // target_level refers to us, so return this branch instead
                            return Some(RayIntersectResult {
                                pos: *node_pos,
                                level: node_level as u8,
                                node,
                            });
                        } else {
                            // else, return the real result
                            return result;
                        }
                    }
                }
                None
            }
            OctNode::Unloaded(_) => Some(RayIntersectResult {
                pos: *node_pos,
                level: node_level as u8,
                node,
            }),
        }
    } else {
        None
    }
}

pub fn ray_intersection<'a, L: Debug + IsAir, B: Debug, U: Debug + Default>(
    eye: &Point3<f32>,
    dir: &F32Coord,
    tree: &'a Octree<L, B, U>,
    target_level: u32,
    max_dist: Option<f32>,
) -> Option<RayIntersectResult<'a, L, B, U>> {
    ray_node_intersection(
        eye,
        dir,
        &tree.root,
        &tree.root_coord,
        tree.root_level,
        target_level,
        max_dist,
    )
}

pub fn exact_ray_intersection<'a, L: Debug + IsAir, B: Debug, U: Debug + Default>(
    eye: &Point3<f32>,
    dir: &F32Coord,
    tree: &'a Octree<L, B, U>,
    target_level: u32,
    max_dist: Option<f32>,
) -> Option<Vector3<f32>> {
    // TODO, find the proper intersection, not just the block coordinate
    ray_intersection(eye, dir, tree, target_level, max_dist).map(|r| r.pos.cast::<f32>().unwrap())
}
