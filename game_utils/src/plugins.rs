//! Setup for code plugins that can be run after compilation
//!
//! These plugins have a requirement of being built on the same machine as amalgom, so this isn't useful for production, but can still offer good value for debugging

use crate::events::{client::ClientListenerManager, server::ServerListenerManager};

use libloading::{Library, Symbol};
use log::*;
use std::{any::Any, ffi::OsStr};

// TODO: Investigate `gaol` for plugin sandboxing.

/// A plugin which allows you to add extra functionality to the REST client.
pub trait Plugin: Any + Send + Sync {
    /// Get a name describing the `Plugin`.
    fn name(&self) -> &'static str;
    /// A callback fired immediately after the plugin is loaded. Usually used
    /// for initialization.
    fn on_plugin_load(&self) {}
    /// A callback fired immediately before the plugin is unloaded. Use this if
    /// you need to do any cleanup.
    fn on_plugin_unload(&self) {}
}

pub trait ClientPlugin: Plugin {
    fn register_listeners(&self, manager: &mut ClientListenerManager);
}

pub trait ServerPlugin: Plugin {
    fn register_listeners(&self, manager: &mut ServerListenerManager);
}

#[macro_export]
macro_rules! declare_plugin {
    ($plugin_type:ty) => {
        #[no_mangle]
        pub extern "C" fn _plugin_create() -> *mut $crate::plugins::Plugin {
            let plug: $plugin_type = Default::default();
            let boxed: Box<dyn $crate::plugins::Plugin> = Box::new(plug);
            Box::into_raw(boxed)
        }
    };
}

pub struct PluginManager {
    plugins: Vec<Box<dyn Plugin>>,
    loaded_libraries: Vec<Library>,
}

impl Default for PluginManager {
    fn default() -> PluginManager {
        PluginManager {
            plugins: Vec::new(),
            loaded_libraries: Vec::new(),
        }
    }
}

impl PluginManager {
    /// # Safety
    /// This is only safe for development purposes, not production. The same version of the compiler must be used.
    pub unsafe fn load_plugin<P: AsRef<OsStr>>(
        &mut self,
        filename: P,
    ) -> Result<(), Box<dyn std::error::Error>> {
        type PluginCreate = unsafe fn() -> *mut dyn Plugin;

        let lib = Library::new(filename.as_ref()).expect("Unable to load the plugin");

        // We need to keep the library around otherwise our plugin's vtable will
        // point to garbage. We do this little dance to make sure the library
        // doesn't end up getting moved.
        self.loaded_libraries.push(lib);

        let lib = self.loaded_libraries.last().unwrap();

        let constructor: Symbol<PluginCreate> = lib
            .get(b"_plugin_create")
            .expect("The `_plugin_create` symbol wasn't found.");
        let boxed_raw = constructor();

        let plugin = Box::from_raw(boxed_raw);
        debug!("Loaded plugin: {}", plugin.name());
        plugin.on_plugin_load();
        self.plugins.push(plugin);

        Ok(())
    }

    /// Unload all plugins and loaded plugin libraries, making sure to fire
    /// their `on_plugin_unload()` methods so they can do any necessary cleanup.
    pub fn unload(&mut self) {
        debug!("Unloading plugins");

        for plugin in self.plugins.drain(..) {
            trace!("Firing on_plugin_unload for {:?}", plugin.name());
            plugin.on_plugin_unload();
        }

        for lib in self.loaded_libraries.drain(..) {
            drop(lib);
        }
    }
}

impl Drop for PluginManager {
    fn drop(&mut self) {
        if !self.plugins.is_empty() || !self.loaded_libraries.is_empty() {
            self.unload();
        }
    }
}
