//! Special thread pool that handles priority queues
//!
//! This is currently used to handle terrain generation requests at differing priorities. For example, terrain that is farther away has less of a priority to be loaded in as terrain that is closer.

use crossbeam::{
    deque::{Injector, Steal},
    utils::Backoff,
};
use std::{sync::Arc, thread};

pub struct PriorityQueues<T: Send + 'static> {
    pub queues: Vec<Injector<T>>,
}

impl<T: Send + 'static> PriorityQueues<T> {
    pub fn with_size(size: usize) -> Arc<Self> {
        let mut queues = Vec::with_capacity(size);
        for _ in 0..size {
            queues.push(Injector::new())
        }
        Arc::new(PriorityQueues { queues })
    }

    /// Only call once
    /// Num threads defaults to num_cpus / 2
    pub fn spawn_workers<F>(self: &Arc<Self>, worker: F, num_threads: Option<usize>)
    where
        F: Fn(T) + Clone + Send + 'static,
    {
        let num_threads = num_threads.unwrap_or_else(|| (num_cpus::get() / 2).max(1));
        log::info!("PriorityQueues spawning {} workers", num_threads);

        for _ in 0..num_threads {
            let pq = self.clone();
            let wkr = worker.clone();
            thread::spawn(move || {
                let backoff = Backoff::new();
                'inf: loop {
                    'queues: for queue in &pq.queues {
                        'retry: loop {
                            match queue.steal() {
                                Steal::Empty => {
                                    continue 'queues; //try next queue
                                }
                                Steal::Success(task) => {
                                    wkr(task);
                                    continue 'inf;
                                }
                                Steal::Retry => {
                                    continue 'retry;
                                }
                            }
                        }
                    }
                    backoff.snooze();
                }
            });
        }
    }
}
