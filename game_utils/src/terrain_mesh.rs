//! Defines data structure for terrain information sent from logic thread to render thread

use arrayvec::ArrayVec;
use base_utils::{dimension::OcclusionPlane, Face, ICoord};
use zerocopy::{AsBytes, FromBytes};

pub struct TerrainMesh {
    pub vertices: [Vec<Vertex>; 6],
    pub indices: [Vec<u32>; 6],
    pub alloc_size: usize,
    pub occlusion_planes: ArrayVec<[OcclusionPlane; 6]>,
}

impl TerrainMesh {
    pub fn new(
        vertices: [Vec<Vertex>; 6],
        indices: [Vec<u32>; 6],
        occlusion_planes: ArrayVec<[OcclusionPlane; 6]>,
    ) -> Self {
        let alloc_size = vertices.iter().zip(indices.iter()).fold(0, |sum, (v, i)| {
            sum + v.as_bytes().len() + i.as_bytes().len()
        });
        TerrainMesh {
            vertices,
            indices,
            alloc_size,
            occlusion_planes,
        }
    }
}

impl std::fmt::Debug for TerrainMesh {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("").field(&self.alloc_size).finish()
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, AsBytes, FromBytes)]
pub struct Vertex {
    pos: [f32; 3],
    meta: u32, // Meta:
               // corner_and_level: u8, //corner: u2, level: u6
               // tex_palette: u8,      //tex_palette is currently unused
               // tex_id: u8,
               // face: u8,
}

impl Vertex {
    /// level must only use the least-significant 6 out of 8 bits.
    /// corner must only be two bits. 00 for top left, 01 for top right, 10 for bottom left, and 11 for bottom right.
    pub fn new(
        pos: ICoord,
        face: Face,
        tex_palette: u8,
        tex_id: u8,
        corner: u8,
        level: u8,
    ) -> Vertex {
        Vertex {
            pos: [pos.x as f32, pos.y as f32, pos.z as f32],
            meta: ((((((corner & 0b11) << 6) | (level & 0b00111111)) as u32) << 24)
                | ((tex_palette as u32) << 16)
                | ((tex_id as u32) << 8)
                | u32::from(face)),
        }
    }
}
