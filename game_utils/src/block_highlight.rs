//! Tiny struct for block highlight location

use base_utils::Loc;

#[derive(Default, Debug, PartialEq, Clone, Copy)]
pub struct BlockHighlight {
    pub loc: Option<Loc>, // is only Some when a box should currently be drawn
}

pub enum BlockHighlightMsg {
    Add(Loc),
    Remove(Loc),
}
