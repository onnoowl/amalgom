# Game_utils

Utils for common functionality between the render and logic crates. This crate takes advantage of the octree crate to make utilities that wouldn't be possible in base_utils.

## Modules

[camera](src/camera.rs) - Struct and helper functions for data pertaining to the player's perspective, or camera

[block_highlight](src/block_highlight.rs) - Tiny struct for block highlight location

[events](src/events.rs) - Legion wrappers for defining events and listeners

[input](src/input.rs) - Definitions for keyboard and mouse input variants

[intersection](src/intersection.rs) - Collision logic using bounding boxes and octrees

[plugins](src/plugins.rs) - Setup for code plugins that can be run after compilation

[terrain_mesh](src/terrain_mesh.rs) - Defines data structure for terrain information sent from logic thread to render thread

[thread_comm](src/thread_comm.rs) - Defines communication structure between render thread and logic thread

[thread_pool](src/thread_pool.rs) - Special thread pool that handles priority queues
