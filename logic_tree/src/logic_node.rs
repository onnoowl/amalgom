pub mod branch;
pub mod leaf;
pub mod unloaded;

pub use branch::LogicBranchData;
use culling::FaceData;
pub use leaf::{block::Block, LogicLeaf};
use neighb_lod_info::NeighbLODInfo;
pub use unloaded::UnloadedData;

use super::*;
use base_utils::{Dimension, Face, ICoord, OCTAL_OFFSETS};
use octree::{
    coord_to_address, culling,
    traits::HasUnderConstruction,
    traits::{BranchNew, HasBuffer},
    traits::{HasFaceData, HasID, HasMaxDataLOD, HasMaxDecLOD, HasMode, HasNoUnloaded, IsAir},
    Address, OctNode,
    OctNode::*,
};

use extend::ext;

pub type LogicNode = OctNode<LogicLeaf, LogicBranchData, UnloadedData>;

#[ext(pub, name = LogicNodeExt)]
impl LogicNode {
    /*
    This is only called when this node has all of its neighbors loaded in and it is safe to try to load structures.
    start_lod is exclusive and end_lod is inclusive
    */
    fn decorate(&mut self, coord: &ICoord, level: u8, start_lod: u8, end_lod: u8) {
        match self {
            Leaf(l) | LODLeaf(l, _) => {
                // Assuming that we only need to re-decorate big leafs when they have quadtrees as face data.
                // TODO, re-add this logic?
                // if let FaceData::QuadTree(_) = l.get_face_data() {
                //     grass_pass_bet_your_ass(l);
                // } else level < start_lod {
                //     grass_pass_bet_your_ass(l);
                // }

                grass_pass_bet_your_ass(l);
                l.set_max_dec_lod(end_lod);
            }
            Branch(b, children) => {
                b.set_max_dec_lod(b.get_max_dec_lod().min(end_lod));
                if level < start_lod {
                    /* For block, turn dirt into grass if it has a visible top */
                    if let Some(mode) = b.get_mode_mut() {
                        grass_pass_bet_your_ass(mode);
                    }
                }
                if level > end_lod {
                    let child_spacing = 2u32.pow(level as u32 - 1) as i64;
                    for (child, offset) in children.iter_mut().zip(OCTAL_OFFSETS.iter()) {
                        child.decorate(
                            &(coord + offset * child_spacing),
                            level - 1,
                            start_lod,
                            end_lod,
                        );
                    }
                }
            }
            Unloaded(_) => { /* Stop recursing */ }
        }
    }

    fn is_face_solid(&self, face: &Face, level: u32, lod: u32) -> bool {
        match self {
            OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => !l.is_air(),
            OctNode::Branch(b, children) => {
                if level == lod {
                    if let Some(block) = &b.block {
                        !block.is_air()
                    } else {
                        false
                    }
                } else {
                    face.get_zipped_octal_and_quad().all(|(child_index, _)| {
                        children[child_index].is_face_solid(face, level - 1, lod)
                    })
                }
            }
            OctNode::Unloaded(_) => {
                panic!("Unloaded in meshing range")
            }
        }
    }

    #[allow(clippy::too_many_arguments)]
    fn package_at_data_depth(
        &self,
        pos: ICoord,
        level: u32,
        target_data_depth: u32,
        packages: &mut Vec<(Address, ICoord, u8, Option<Face>)>,
        root_coord: &ICoord,
        root_level: u32,
        neighb_max_data_lods: [NeighbLODInfo; 6],
    ) {
        let max_data_lod = self.get_max_data_lod();
        match self {
            Branch(_, children) => {
                if level - max_data_lod as u32 > target_data_depth {
                    let child_separation = 2_u32.pow(level - 1) as i64;
                    for (child, o) in children.iter().zip(OCTAL_OFFSETS.iter()) {
                        let mut child_neighb_lods = neighb_max_data_lods;
                        for dim in Dimension::variants() {
                            let external_face = dim.to_face_with_offset(o[dim as usize] == 1);
                            let internal_face = external_face.flip();

                            child_neighb_lods[internal_face as usize] =
                                NeighbLODInfo::Single(max_data_lod);

                            child_neighb_lods[external_face as usize].make_specific(o, dim);
                        }
                        child.package_at_data_depth(
                            pos + o * child_separation,
                            level - 1,
                            target_data_depth,
                            packages,
                            root_coord,
                            root_level,
                            child_neighb_lods,
                        );
                    }
                    return;
                }
            }
            Unloaded(_) => panic!("Packaged unloaded after extension"),
            _ => {}
        };

        let mut addr = Address::new();
        coord_to_address(&pos, root_coord, &mut addr, root_level, level)
            .expect("Leaf location doesn't exist. Likely from extension");

        let mut max_data_lod_of_neighbors = max_data_lod;

        for n_mlod in neighb_max_data_lods.iter() {
            let n_mlod = n_mlod.get_max_data_lod();
            if n_mlod >= level as u8 {
                return;
            } else {
                max_data_lod_of_neighbors = max_data_lod_of_neighbors.max(n_mlod);
            }
        }

        // TODO: would be good to check buffer depth. Would need buffer depth to be moved to trait though.
        if self.get_max_dec_lod() >= max_data_lod_of_neighbors {
            packages.push((addr, pos, max_data_lod_of_neighbors, None));
        }
    }

    /// Continues to recurse using the provided indices as long as the data depth is > than the target depth
    /// or there are unloadeds present
    fn map_recurse_with_indices_to_depth<
        I: Iterator<Item = usize> + Clone,
        F: FnMut(&Self, ICoord, u32),
    >(
        &self,
        indices: &I,
        target_depth: u32,
        coord: ICoord,
        level: u32,
        f: &mut F,
    ) {
        if self.has_no_unloaded() && (level - self.get_max_data_lod() as u32) <= target_depth {
            f(self, coord, level)
        } else {
            match self {
                Self::Branch(_, children) => {
                    let child_separation = 2u32.pow(level - 1) as i64;
                    for i in indices.clone() {
                        children[i].map_recurse_with_indices_to_depth(
                            indices,
                            target_depth,
                            coord + child_separation * OCTAL_OFFSETS[i],
                            level - 1,
                            f,
                        );
                    }
                }
                _ => f(self, coord, level),
            }
        }
    }

    fn get_face_data(&self) -> Option<&FaceData> {
        match self {
            OctNode::Leaf(l) | LODLeaf(l, _) => Some(l.get_face_data()),
            OctNode::Branch(b, _) => match b.get_mode() {
                Some(m) => Some(m.get_face_data()),
                None => None,
            },
            OctNode::Unloaded(_) => None,
        }
    }

    fn get_face_data_mut(&mut self) -> Option<&mut FaceData> {
        match self {
            OctNode::Leaf(l) | LODLeaf(l, _) => Some(l.get_face_data_mut()),
            OctNode::Branch(b, _) => match b.get_mode_mut() {
                Some(m) => Some(m.get_face_data_mut()),
                None => None,
            },
            OctNode::Unloaded(_) => None,
        }
    }

    fn is_under_construction(&self) -> bool {
        match self {
            Leaf(l) | LODLeaf(l, _) => l.is_under_construction(),
            Branch(b, _) => b.is_under_construction(),
            Unloaded(u) => u.is_under_construction(),
        }
    }
    fn set_under_construction(&mut self, under_construction: bool) {
        match self {
            Leaf(l) | LODLeaf(l, _) => l.set_under_construction(under_construction),
            Branch(b, _) => b.set_under_construction(under_construction),
            Unloaded(u) => u.set_under_construction(under_construction),
        }
    }

    fn has_no_unloaded(&self) -> bool {
        match self {
            Leaf(_) | LODLeaf(_, _) => true,
            Branch(b, _) => b.has_no_unloaded(),
            Unloaded(_) => false,
        }
    }

    fn set_has_no_unloaded(&mut self, has_no_unloaded: bool) {
        match self {
            Leaf(_) | LODLeaf(_, _) => (),
            Branch(b, _) => b.set_has_no_unloaded(has_no_unloaded),
            Unloaded(_) => (),
        }
    }

    fn get_max_dec_lod(&self) -> u8 {
        match self {
            OctNode::Branch(b, _) => b.get_max_dec_lod(),
            OctNode::Leaf(l) | LODLeaf(l, _) => l.get_max_dec_lod(),
            OctNode::Unloaded(_) => u8::MAX,
        }
    }
    fn set_max_dec_lod(&mut self, max_dec_lod: u8) {
        match self {
            OctNode::Branch(b, _) => b.set_max_dec_lod(max_dec_lod),
            OctNode::Leaf(l) | OctNode::LODLeaf(l, _) => l.set_max_dec_lod(max_dec_lod),
            OctNode::Unloaded(_) => (),
        }
    }

    fn get_max_data_lod(&self) -> u8 {
        match self {
            Unloaded(_) => u8::MAX,
            Branch(b, _) => b.get_max_data_lod(),
            LODLeaf(_, lod) => *lod,
            Leaf(_) => 0,
        }
    }
    fn set_max_data_lod(&mut self, new_lod: u8) {
        debug_assert_ne!(new_lod, u8::MAX);
        match self {
            Unloaded(_) => log::error!("Tried to set_max_data_lod on unloaded."),
            Branch(b, _) => b.set_max_data_lod(new_lod),
            LODLeaf(_, lod) => *lod = new_lod,
            Leaf(_) => log::error!("Tried to set_max_data_lod on leaf."),
        }
    }

    fn get_id(&self) -> Option<u8> {
        match self {
            Leaf(l) => l.get_id(),
            LODLeaf(l, _) => l.get_id(),
            Branch(b, _) => b.get_id(),
            Unloaded(_) => None,
        }
    }
    fn set_id(&mut self, id: Option<u8>) {
        match self {
            Leaf(l) => l.set_id(id),
            LODLeaf(l, _) => l.set_id(id),
            Branch(b, _) => b.set_id(id),
            Unloaded(_) => (),
        }
    }

    fn get_buffer_lod(&self, level: u32, buffer_max: u32) -> u8 {
        match self {
            Leaf(l) => {
                if l.get_has_buffer() {
                    return 0;
                }
            }
            LODLeaf(l, lod) => {
                if l.get_has_buffer() {
                    return (*lod).max((level - buffer_max) as u8);
                }
            }
            Branch(b, _) => {
                if b.get_has_buffer() {
                    return b.get_max_dec_lod().max((level - buffer_max) as u8);
                }
            }
            Unloaded(_) => {}
        }
        u8::MAX
    }
}

fn grass_pass_bet_your_ass<T: HasID + HasFaceData>(block: &mut T) {
    if let Some(id) = block.get_id() {
        if id == 3 {
            if block
                .get_face_data()
                .get_side(&Face::Top)
                .as_ref()
                .is_ever_visible()
            {
                block.set_id(Some(2));
            }
        }
    }
}
