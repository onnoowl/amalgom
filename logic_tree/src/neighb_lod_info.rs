use base_utils::{Dimension, ICoord};

#[derive(Copy, Clone, Debug)]
pub enum NeighbLODInfo {
    Single(u8),
    Quad([u8; 4]),
}

impl NeighbLODInfo {
    pub fn get_max_data_lod(&self) -> u8 {
        match self {
            NeighbLODInfo::Single(m_lod) => *m_lod,
            NeighbLODInfo::Quad(m_lods) => *m_lods.iter().max().unwrap(),
        }
    }

    pub fn make_specific(&mut self, offset: &ICoord, dim: Dimension) {
        match self {
            NeighbLODInfo::Quad(q) => {
                let (quad_dim_b, quad_dim_a) = match dim {
                    Dimension::X => (offset.z as usize, offset.y as usize),
                    Dimension::Y => (offset.z as usize, offset.x as usize),
                    Dimension::Z => (offset.y as usize, offset.x as usize),
                };
                let quad_index = (quad_dim_a << 1) | quad_dim_b;
                *self = NeighbLODInfo::Single(q[quad_index])
            }
            NeighbLODInfo::Single(_) => {}
        }
    }
}
