use super::{logic_node::*, neighb_lod_info::NeighbLODInfo};
use base_utils::{face::FACES, Face, ICoord, OCTAL_OFFSETS};
use octree::{
    culling,
    error::*,
    traits::{HasFaceData, IsAir},
    Address,
    OctNode::*,
    Octree,
};
use reporting::{bail, debug_ensure, Context, Error};

use extend::ext;

pub type LogicTree = Octree<LogicLeaf, LogicBranchData, UnloadedData>;

#[ext(pub, name = LogicTreeExt)]
impl LogicTree {
    #[allow(clippy::too_many_arguments)]
    fn check_node_completion(
        &mut self,
        mut node: Option<&mut LogicNode>,
        caller: Option<(&Face, Option<NeighbLODInfo>)>,
        address: Address,
        coord: &ICoord,
        level: u32,
        max_data_lod: u8,
        buffer_lod: u8,
        should_package_self: bool,
        node_packages: &mut Vec<(Address, ICoord, u8, Option<Face>)>,
        buffer_depth_and_min_level: u32,
    ) -> Result<[NeighbLODInfo; 6], Error> {
        let result: Result<[NeighbLODInfo; 6], Error> = try {
            debug_ensure!(
                max_data_lod != u8::MAX,
                "check_node_completion called with max_data_lod of u8::MAX"
            );
            let size = 2i64.pow(level);
            let mut neighb_max_data_lods: [NeighbLODInfo; 6] = [NeighbLODInfo::Single(u8::MAX); 6];
            for (i, f) in FACES.iter().enumerate() {
                // No need to re-lookup the caller. The caller gave us its info so we don't have to
                if let Some((c_f, c_mlod)) = caller {
                    if c_f == f {
                        if let Some(mlod) = c_mlod {
                            neighb_max_data_lods[i] = mlod;
                            continue;
                        }
                    }
                }

                let mut neighb_addr = Address::new();
                let mut neighb_coord = coord + ICoord::from(*f) * size;
                if self
                    .coord_to_address(&neighb_coord, &mut neighb_addr, level)
                    .is_err()
                {
                    continue; // Neighbor is out of bounds. Default to max_lod of u8::MAX
                }

                // Get the info we need from the neighboring node
                let (neighb_max_data_lod, neighb_level, neighb_node) = match self
                    .root
                    .get_node_mut_with_address(&neighb_addr)
                {
                    Ok(neighb) => {
                        let max_data_lod = neighb.get_max_data_lod();
                        if let Some(n) = node.as_mut() {
                            if max_data_lod as u32 <= level {
                                culling::cull_pair(n, neighb, f);
                            }
                        }
                        (max_data_lod, level, neighb)
                    }
                    Err(np) => {
                        match np {
                            NotPresentMut::IsUnloaded {
                                depth: _,
                                unloaded_node: _,
                            } => continue, // Continue means we default to max_lod of u8::MAX
                            NotPresentMut::IsHighLevelLeaf {
                                depth: depth_from_root,
                                high_level_leaf_node: leaf_node,
                            } => {
                                // Special logic for culling with high level leaf. Could be moved to separate function
                                if node.is_some() {
                                    let leaf = leaf_node.get_leaf_mut().unwrap();
                                    if leaf.is_air() {
                                        let n = node.unwrap();
                                        match n {
                                            Leaf(l) | LODLeaf(l, _) => {
                                                l.get_face_data_mut().mark_side_visible(f)
                                            }
                                            Branch(b, children) => {
                                                culling::branch_mark_side_visible(
                                                    b,
                                                    children.as_mut(),
                                                    f,
                                                );
                                            }
                                            Unloaded(_) => {} //do nothing
                                        };
                                        node = Some(n);
                                    } else {
                                        log::warn!("Encountered a high level leaf bigger than the unit of generation when culling");
                                        leaf.get_face_data_mut().mark_side_visible(&f.flip());
                                        // TODO, temporary
                                    }
                                }

                                let lod = leaf_node.get_max_data_lod();
                                (lod, self.root_level - depth_from_root, leaf_node)
                            }
                        }
                    }
                };

                let has_no_unloadeds = neighb_node.has_no_unloaded();
                if has_no_unloadeds {
                    neighb_max_data_lods[i] = NeighbLODInfo::Single(neighb_max_data_lod);
                }

                // Nothing to do with a single unloaded
                // max_data_lod of u8::MAX means that node is one big unloaded
                // TODO: would be cleaner if this if just checked to see if node was unloaded
                if neighb_max_data_lod != u8::MAX {
                    debug_ensure!(
                        neighb_max_data_lod as u32 <= neighb_level,
                        "Expected neighb_max_data_lod({}) <= neighb_level({})",
                        neighb_max_data_lod,
                        neighb_level
                    );
                    let dif_depth = buffer_depth_and_min_level as i8
                        - (neighb_level - neighb_max_data_lod as u32) as i8;

                    // We should recurse if there was no caller that sent us here or if the neighboring node in question is bigger than us
                    let should_recurse = if let Some((c_f, _)) = caller {
                        c_f != f && dif_depth > 0 && has_no_unloadeds
                    } else {
                        true
                    };

                    if should_recurse {
                        // Check to see if the neighbor is ready for packaging now
                        if dif_depth > 0 && has_no_unloadeds {
                            let parent_level = neighb_level + dif_depth as u32;

                            neighb_coord = self
                                .truncate_coord_at_level(&neighb_coord, parent_level as u8)
                                .unwrap();
                            self.coord_to_address(
                                &neighb_coord,
                                &mut neighb_addr,
                                parent_level,
                            )
                            .expect(
                                "This should always pass because we've already done the check previously",
                            );
                            if let Ok(n) = self.root.get_node_mut_with_address(&neighb_addr) {
                                debug_ensure!(
                                    n.has_no_unloaded(),
                                    "Unloadeds found within bottom range of buffer_depth_min_level. This means chunks are being created with a depth less than buffer_depth_and_min_level. Parent of node in question: {:?}\n\tdif_depth: {:?}, neighb_level: {:?}, neighb_max_data_lod: {:?}",
                                    n,
                                    dif_depth,
                                    neighb_level,
                                    neighb_max_data_lod
                                );

                                let parent_max_data_lod = n.get_max_data_lod();
                                let parent_buffer_lod = n.get_buffer_lod(
                                    neighb_level + dif_depth as u32,
                                    buffer_depth_and_min_level,
                                );
                                debug_ensure!(
                                    neighb_level as i32 + dif_depth as i32 - parent_max_data_lod as i32 ==
                                    buffer_depth_and_min_level as i32,
                                    "Expected neighb_level({}) + dif_depth({}) - parent_max_data_lod({}) == buffer_depth_and_min_level({}). Neighb_max_data_lod was ({})",
                                    neighb_level, dif_depth, parent_max_data_lod, buffer_depth_and_min_level, neighb_max_data_lod
                                );
                                self.check_node_completion(
                                    None,
                                    Some((&f.flip(), None)),
                                    neighb_addr,
                                    &neighb_coord,
                                    neighb_level + dif_depth as u32,
                                    parent_max_data_lod,
                                    parent_buffer_lod,
                                    true,
                                    node_packages,
                                    buffer_depth_and_min_level,
                                )?;
                            }
                        } else if dif_depth == 0
                            || neighb_node.get_branch().is_none() && has_no_unloadeds
                        {
                            // Same size as us scenario (Or high level leaf sometimes)

                            let buffer_lod = neighb_node
                                .get_buffer_lod(neighb_level, buffer_depth_and_min_level);

                            self.check_node_completion(
                                None,
                                Some((&f.flip(), Some(NeighbLODInfo::Single(max_data_lod)))),
                                neighb_addr,
                                &neighb_coord,
                                neighb_level,
                                neighb_max_data_lod,
                                buffer_lod,
                                true,
                                node_packages,
                                buffer_depth_and_min_level,
                            )?;
                        } else if dif_depth < 0 || !has_no_unloadeds {
                            // Smaller than us + Higher resolution
                            // let (_, neighb_children) = neighb_node.get_branch_mut().expect(
                            //     "Earlier branch should always be called if this is not a branch type",
                            // ); // TODO pass this in instead of second lookup
                            debug_ensure!(
                                level == neighb_level,
                                "Expected level({}) == neighb_level({})",
                                level,
                                neighb_level
                            );

                            let mut quad_mlods = [u8::MAX; 4];
                            f.flip()
                                .get_zipped_octal_and_quad()
                                .map(|(closer_child, quad_index)| -> Result<(), Error> {
                                    quad_mlods[quad_index] = self
                                        .check_node_completion_of_closer_children(
                                            // &mut neighb_children[closer_child],
                                            neighb_coord + OCTAL_OFFSETS[closer_child] * size / 2,
                                            neighb_level - 1,
                                            &f.flip(),
                                            max_data_lod,
                                            node_packages,
                                            buffer_depth_and_min_level,
                                        )?;
                                    Ok(())
                                })
                                .collect::<Result<(), Error>>()?;
                            neighb_max_data_lods[i] = NeighbLODInfo::Quad(quad_mlods);
                        }
                    }
                }
            }

            if should_package_self {
                let mut max_data_lod_of_neighbors = max_data_lod;
                for n_mlod in neighb_max_data_lods.iter() {
                    let n_mlod = n_mlod.get_max_data_lod();
                    if n_mlod >= level as u8 {
                        return Ok(neighb_max_data_lods);
                    } else {
                        max_data_lod_of_neighbors = max_data_lod_of_neighbors.max(n_mlod);
                    }
                }
                // All neighbors are at the same level so we just need to say that this node is ready for packaging
                if buffer_lod >= max_data_lod_of_neighbors {
                    let calling_face = match caller {
                        Some((c_f, _)) => Some(*c_f),
                        None => None,
                    };
                    debug_ensure!(
                        (level as u8) >= max_data_lod_of_neighbors,
                        "Attempt to package node to be rendered at higher ({:?}) level than the node itself({:?})",
                        max_data_lod_of_neighbors,
                        level
                    );
                    node_packages.push((address, *coord, max_data_lod_of_neighbors, calling_face));
                }
            }
            neighb_max_data_lods
        };
        result.with_context(|| {
            format!(
                "check_node_completion failed with arguments - node: {:#?}, caller: {:?}, coord: {:?}, level: {:?}, max_data_lod: {}, buffer_lod: {}, should_package_self: {}",
                node, caller, coord, level, max_data_lod, buffer_lod, should_package_self
            )
        })
    }

    fn check_node_completion_of_closer_children(
        &mut self,
        // node: &mut SparseOctalNode<L, B, U>,
        coord: ICoord,
        level: u32,
        side_to_recurse: &Face,
        caller_max_data_lod: u8,
        node_packages: &mut Vec<(Address, ICoord, u8, Option<Face>)>,
        buffer_depth_and_min_level: u32,
    ) -> Result<u8, Error> {
        let node = match self.get_node_mut(&coord, level) {
            Ok(n) => n,
            Err(e) => match e {
                GetMutError::OutOfBounds(_) => panic!("Was out of bounds"),
                GetMutError::NotPresentMut(np) => match np {
                    NotPresentMut::IsUnloaded {
                        depth: _,
                        unloaded_node: _,
                    } => return Ok(u8::MAX),
                    NotPresentMut::IsHighLevelLeaf {
                        depth: _,
                        high_level_leaf_node: _,
                    } => panic!("Found high level leaf"),
                },
            },
        }; // TODO get rid of second lookup

        if node.is_unloaded() {
            return Ok(u8::MAX);
        }

        let mut children = Vec::new();
        node.map_recurse_with_indices_to_depth(
            &side_to_recurse
                .get_zipped_octal_and_quad()
                .map(|(closer_child, _)| closer_child),
            buffer_depth_and_min_level,
            coord,
            level,
            &mut |child_node: &LogicNode, c: ICoord, l: u32| {
                children.push((
                    c,
                    l,
                    child_node.get_max_data_lod(),
                    child_node.get_buffer_lod(l, buffer_depth_and_min_level),
                ))
            },
        );
        if !children.is_empty() {
            let mut max_data_lod = 0;
            for (c_c, c_l, c_mlod, c_blod) in children {
                max_data_lod = max_data_lod.max(c_mlod);

                let mut child_addr = Address::new();
                self.coord_to_address(&c_c, &mut child_addr, c_l)
                    .expect("Address that should be valid is actually out of bounds");
                if c_mlod != u8::MAX {
                    self.check_node_completion(
                        None,
                        Some((
                            side_to_recurse,
                            Some(NeighbLODInfo::Single(caller_max_data_lod)),
                        )),
                        child_addr,
                        &c_c,
                        c_l,
                        c_mlod,
                        c_blod,
                        true,
                        node_packages,
                        buffer_depth_and_min_level,
                    )?;
                }
            }
            Ok(max_data_lod)
        } else {
            bail!("No children to recurse into")
        }
    }
}
