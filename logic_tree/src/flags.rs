//! Bit flag definitions for descriptors for a LogicNode
//!
//! * UNDER_CONSTRUCTION - if true, this node is currently being modified by another thread and shouldn't be touched
//! * HAS_NO_UNLOADED - if true, this node has no children nodes that are listed as unloaded
//! * HAS_BUFFER - if true, rendering information for this LogicNode have been sent to the render thread

use bitflags::bitflags;

bitflags! {
    pub struct Flags: u8 {
        const DEFAULT = 0;
        const UNDER_CONSTRUCTION = 0b00000001;
        const HAS_NO_UNLOADED = 0b00000010;
        const HAS_BUFFER = 0b00000100;
    }
}

impl Flags {
    pub fn under_construction(&self) -> bool {
        *self & Self::UNDER_CONSTRUCTION != Self::DEFAULT
    }

    pub fn has_no_unloaded(&self) -> bool {
        *self & Self::HAS_NO_UNLOADED != Self::DEFAULT
    }

    pub fn has_buffer(&self) -> bool {
        *self & Self::HAS_BUFFER != Self::DEFAULT
    }

    pub fn set_under_construction(&mut self, val: bool) {
        if val {
            *self |= Self::UNDER_CONSTRUCTION;
        } else {
            *self &= !Self::UNDER_CONSTRUCTION;
        }
    }

    pub fn set_has_no_unloaded(&mut self, val: bool) {
        if val {
            *self |= Self::HAS_NO_UNLOADED;
        } else {
            *self &= !Self::HAS_NO_UNLOADED;
        }
    }

    pub fn set_has_buffer(&mut self, val: bool) {
        if val {
            *self |= Self::HAS_BUFFER;
        } else {
            *self &= !Self::HAS_BUFFER;
        }
    }
}

impl Default for Flags {
    fn default() -> Self {
        Flags::DEFAULT
    }
}
