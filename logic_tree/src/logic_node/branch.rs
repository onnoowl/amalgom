use super::{leaf::block::Block, LogicNode};
use crate::{flags::Flags, logic_node::*};

use octree::traits::{
    HasBuffer, HasID, HasMaxDataLOD, HasMaxDecLOD, HasMode, HasNoUnloaded, HasUnderConstruction,
    ModeFromNode,
};

#[derive(Debug, Clone)]
pub struct LogicBranchData {
    pub block: Option<Block>,
    pub max_dec_lod: u8,
    pub max_data_lod: u8,
    pub flags: Flags,
}

impl LogicBranchData {
    pub fn new(block: Option<Block>, max_data_lod: u8) -> Self {
        Self {
            block,
            max_dec_lod: u8::MAX,
            max_data_lod,
            flags: Flags::DEFAULT,
        }
    }
}

impl ModeFromNode for LogicBranchData {
    type Node = LogicNode;

    fn mode_from_node(node: &LogicNode) -> Self {
        Self::new(node.get_id().map(|id| id.into()), node.get_max_data_lod())
    }
}

impl HasBuffer for LogicBranchData {
    fn get_has_buffer(&self) -> bool {
        self.flags.has_buffer()
    }
    fn set_has_buffer(&mut self, val: bool) {
        self.flags.set_has_buffer(val)
    }
}

impl HasUnderConstruction for LogicBranchData {
    fn is_under_construction(&self) -> bool {
        self.flags.under_construction()
    }
    fn set_under_construction(&mut self, under_construction: bool) {
        self.flags.set_under_construction(under_construction)
    }
}

impl HasNoUnloaded for LogicBranchData {
    fn has_no_unloaded(&self) -> bool {
        self.flags.has_no_unloaded()
    }
    fn set_has_no_unloaded(&mut self, has_no_unloaded: bool) {
        self.flags.set_has_no_unloaded(has_no_unloaded)
    }
}

impl HasID for LogicBranchData {
    fn get_id(&self) -> Option<u8> {
        match &self.block {
            Some(b) => b.get_id(),
            None => None,
        }
    }
    fn set_id(&mut self, id: Option<u8>) {
        match &mut self.block {
            Some(b) => b.set_id(id),
            None => {
                if let Some(id) = id {
                    self.block = Some(Block {
                        id,
                        face_data: Default::default(),
                    });
                }
            }
        }
    }
}

impl HasMaxDecLOD for LogicBranchData {
    fn get_max_dec_lod(&self) -> u8 {
        self.max_dec_lod
    }
    fn set_max_dec_lod(&mut self, max_dec_lod: u8) {
        self.max_dec_lod = max_dec_lod
    }
}

impl HasMaxDataLOD for LogicBranchData {
    fn get_max_data_lod(&self) -> u8 {
        self.max_data_lod
    }
    fn set_max_data_lod(&mut self, max_data_lod: u8) {
        self.max_data_lod = max_data_lod
    }
}

impl HasMode for LogicBranchData {
    type ModeType = Block;
    fn get_mode(&self) -> Option<&Self::ModeType> {
        self.block.as_ref()
    }
    fn get_mode_mut(&mut self) -> Option<&mut Self::ModeType> {
        self.block.as_mut()
    }
}

impl BranchNew for LogicBranchData {
    type Node = OctNode<LogicLeaf, LogicBranchData, UnloadedData>;
    fn new_branch(level: u8, children: &[Self::Node]) -> LogicBranchData {
        let branch_max_data_lod = children
            .iter()
            .map(|child| child.get_max_data_lod())
            .max()
            .unwrap();
        LogicBranchData::new(None, branch_max_data_lod.min(level))
        // TODO: Calculate mode
    }
}
