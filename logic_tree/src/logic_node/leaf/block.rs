use octree::{
    culling::FaceData,
    traits::{HasFaceData, HasID, IsAir},
};

#[derive(Debug, Default, Clone, PartialEq)]
pub struct Block {
    pub id: u8,
    pub face_data: FaceData,
}

impl HasFaceData for Block {
    fn get_face_data(&self) -> &FaceData {
        &self.face_data
    }
    fn get_face_data_mut(&mut self) -> &mut FaceData {
        &mut self.face_data
    }
}

impl IsAir for Block {
    fn is_air(&self) -> bool {
        self.id == 0
    }
}

impl HasID for Block {
    fn get_id(&self) -> Option<u8> {
        Some(self.id)
    }
    fn set_id(&mut self, id: Option<u8>) {
        if let Some(id) = id {
            self.id = id;
        }
    }
}

impl From<u8> for Block {
    fn from(id: u8) -> Self {
        Self {
            id,
            face_data: Default::default(),
        }
    }
}

impl From<Block> for u8 {
    fn from(b: Block) -> Self {
        b.id
    }
}
