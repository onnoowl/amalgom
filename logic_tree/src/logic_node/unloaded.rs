use octree::traits::HasUnderConstruction;

#[derive(Debug, Default, Clone, PartialEq)]
pub struct UnloadedData {
    under_construction: bool,
}
impl HasUnderConstruction for UnloadedData {
    fn is_under_construction(&self) -> bool {
        self.under_construction
    }
    fn set_under_construction(&mut self, under_construction: bool) {
        self.under_construction = under_construction
    }
}
