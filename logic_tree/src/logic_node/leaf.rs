pub mod block;

use block::Block;

use octree::{
    culling::FaceData,
    traits::{HasBuffer, HasFaceData, HasID, HasMaxDecLOD, HasUnderConstruction, IsAir},
};

#[derive(Debug, Default, Clone, PartialEq)]
pub struct LogicLeaf {
    pub block: Block,
    pub has_buffer: bool,
    under_construction: bool, // TODO: leafs (not LODLeaf) will never need this flag
    pub dec_lod: u8,
}

impl LogicLeaf {
    pub fn new(block_id: u8) -> Self {
        Self::new_with_dec_lod(block_id, u8::MAX)
    }

    pub fn new_with_dec_lod(block_id: u8, max_dec_lod: u8) -> Self {
        Self {
            block: block_id.into(),
            has_buffer: false,
            under_construction: false,
            dec_lod: max_dec_lod,
        }
    }
}

impl HasBuffer for LogicLeaf {
    fn get_has_buffer(&self) -> bool {
        self.has_buffer
    }
    fn set_has_buffer(&mut self, val: bool) {
        self.has_buffer = val
    }
}

impl HasMaxDecLOD for LogicLeaf {
    fn get_max_dec_lod(&self) -> u8 {
        self.dec_lod
    }
    fn set_max_dec_lod(&mut self, max_dec_lod: u8) {
        self.dec_lod = max_dec_lod
    }
}

impl HasUnderConstruction for LogicLeaf {
    fn is_under_construction(&self) -> bool {
        self.under_construction
    }
    fn set_under_construction(&mut self, under_construction: bool) {
        self.under_construction = under_construction
    }
}

impl HasFaceData for LogicLeaf {
    fn get_face_data(&self) -> &FaceData {
        self.block.get_face_data()
    }
    fn get_face_data_mut(&mut self) -> &mut FaceData {
        self.block.get_face_data_mut()
    }
}

impl IsAir for LogicLeaf {
    fn is_air(&self) -> bool {
        self.block.is_air()
    }
}

impl HasID for LogicLeaf {
    fn get_id(&self) -> Option<u8> {
        self.block.get_id()
    }
    fn set_id(&mut self, id: Option<u8>) {
        self.block.set_id(id)
    }
}

impl From<LogicLeaf> for u8 {
    fn from(b: LogicLeaf) -> Self {
        b.block.id
    }
}
